// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "ReserveByException.h"
#include "SpinLock.h"
#include <math.h>
#include <stdio.h>
#include "iolock.h"

#define NOTREACHED  0

int CpuMux::Muxs;
SpinLock CpuMux::MuxsLock;

CpuMux * CpuMux::ThisCpu;

int CpuMux::Debug;

//
//	This can not be private, or we wont see all the action
//

static int StopCpuMux;

int CpuMuxDebugFlag = 0;

//
// Used by some external C programs (like malloc) for consistency checks
//
static int DefaultIdPtr;
int *AwesimeCpuId = &DefaultIdPtr;

CpuMux::CpuMux(int debug) : systemContext(0, 0)
{
    pNameTemplate = "CpuMux";
    currentThread = 0;
    enabled = 0;
    iYam = 0;
    AwesimeCpuId = &iYam;
    pid = 0;
    CpuMuxDebugFlag = debug;
    ThisCpu = this;
    terminated = &StopCpuMux;
    sprintf(nameSpace, "[%s-%d] ", pNameTemplate, iYam);
    pName = nameSpace;
}

CpuMux::~CpuMux()
{
    // empty
}

void
CpuMux::debug(int newdebug)
{
    CpuMuxDebugFlag = newdebug;
}

int
CpuMux::debug()
{
    return(CpuMuxDebugFlag);
}

void
CpuMux::terminateAll_()
{
    *terminated = 1;
}


void
CpuMux::addReserve_()
{
    // empty
}

void
CpuMux::addUnlocked_(Thread *thread)
{
    add(thread);
}

void
CpuMux::addRelease_()
{
    // empty
}

void
CpuMux::reschedule_(Thread* to, CpuMux *ThisCpu)
{
    ThisCpu->currentThread -> cpuAffinity = -1;
    //
    // prefer no one
    //
    Thread* newThread = (to==0) ? ThisCpu->remove() : to;

    rescheduleException.switchTo( ThisCpu->currentThread,newThread );

    if (newThread == NULL)
        ThisCpu -> raise (&(ThisCpu->rescheduleException));
    else {
        Thread *old = ThisCpu -> currentThread;
	ThisCpu -> currentThread = newThread;
	ThisCpu -> raisedBy = &(ThisCpu -> rescheduleException);
	CpuMux::threadTransfer(old,newThread);
    }
    
}



