/****************************************************************************/
	.text	
	.align 3
	.globl getSp__15HardwareContextXv
	.ent getSp__15HardwareContextXv 2
getSp__15HardwareContextXv:
getSp__15HardwareContextXv..ng:
	.frame $30,0,$26,0
	.prologue 0
	bis $30,$30,$0
	ret $31,($26),1
	.end getSp__15HardwareContextXv
/****************************************************************************/
	.text	
	.align 3
	.globl magicSwitchTo__15HardwareContextXP15HardwareContext
	.ent magicSwitchTo__15HardwareContextXP15HardwareContext 2
magicSwitchTo__15HardwareContextXP15HardwareContext:
magicSwitchTo__15HardwareContextXP15HardwareContext..ng:
	.frame $30,0,$26,0
	.prologue 0
		stq	$9,32($16)
		ldq	$9,32($17)
		stq	$10,40($16)
		ldq	$10,40($17)
		stq	$11,48($16)
		ldq	$11,48($17)
		stq	$12,56($16)
		ldq	$12,56($17)
		stq	$13,64($16)
		ldq	$13,64($17)
		stq	$14,72($16)
		ldq	$14,72($17)
		stq	$22,80($16)
		ldq	$22,80($17)
		stq	$23,88($16)
		ldq	$23,88($17)
		stt	$f2,96($16)
		ldt	$f2,96($17)
		stt	$f3,104($16)
		ldt	$f3,104($17)
		stt	$f4,112($16)
		ldt	$f4,112($17)
		stt	$f5,120($16)
		ldt	$f5,120($17)
		stt	$f6,128($16)
		ldt	$f6,128($17)
		stt	$f7,136($16)
		ldt	$f7,136($17)
		stt	$f8,144($16)
		ldt	$f8,144($17)
		stt	$f9,152($16)
		ldt	$f9,152($17)
		stq	$26,24($16)
		ldq	$26,24($17)
		stq	$30,0($16)
		stq	$15,8($16)
		ldq	$15,8($17)
		ldq	$30,0($17)
	ret $31,($26),1
	.end magicSwitchTo__15HardwareContextXP15HardwareContext
/***********************************************************************/
		.text
		.align 3
		.globl	__magicReturnFunction
		.ent	__magicReturnFunction
	__magicReturnFunction:
		mov $9,$16
		mov $10,$27
		jmp	$31,($27),0
		.end	__magicReturnFunction

/***********************************************************************/
	.text
	.align 3
	.globl SpinLockReserve__XPVi
	.ent SpinLockReserve__XPVi 0
SpinLockReserve__XPVi:
SpinLockReserve__XPVi..ng:
	.frame $30,0,$26,0
	.prologue 0
	.align 5
$448:
	ldl_l	$2,0($16)
	bne $2,$451
	bis $31,1,$2
	stl_c	$2,0($16)
	ret $31,($26),1
	.align 4
$451:
	ldl $1,0($16)
	beq $1,$448
	.align 5
$455:
	ldl $1,0($16)
	bne $1,$455
	br $31,$448
	.align 4
	.end SpinLockReserve__XPVi
/***********************************************************************/
	.text
	.align 3
	.globl SpinLockReserveNoBlock__XPVi
	.ent SpinLockReserveNoBlock__XPVi 0
SpinLockReserveNoBlock__XPVi:
SpinLockReserveNoBlock__XPVi..ng:
	.frame $30,0,$26,0
	.prologue 0
	ldl_l	$0,0($16)
	bne $0,$459
	bis $31,1,$0
	stl_c	$0,0($16)
	cmpult $31,$0,$0
	ret $31,($26),1
	.align 4
$459:
	bis $31,$31,$0
	ret $31,($26),1
	.end SpinLockReserveNoBlock__XPVi
