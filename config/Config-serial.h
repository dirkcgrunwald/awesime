/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
/*
 * This may look like C code, but it is really -*- C++ -*-
 * 
 * Copyright (C) 1988 University of Illinois, Urbana, Illinois
 * Copyright (C) 1989 University of Colorado, Boulder, Colorado
 * Copyright (C) 1990 University of Colorado, Boulder, Colorado
 *
 * written by Dirk Grunwald (grunwald@foobar.colorado.edu)
 *
 */
#ifndef AwesimeConfig_h
#define AwesimeConfig_h

#ifdef __GNUG__
#  pragma interface
#endif

/*
 *
 *	Generic single processor. Spinlocks are eliminated
 *	and no shared memory is used.
 *
 */

#ifndef _MAXIMUM_CPU_MUXS_
#   define _MAXIMUM_CPU_MUXS_ 1
#endif

#define DefaultSpinBarrierLoops 0
#define DefaultBarrierLoops 0

#ifdef __GNUC__
  typedef volatile int VolatileInt;
  typedef volatile unsigned char  VolatileUsignedChar;
#else
#define volatile
  typedef int VolatileInt;
  typedef unsigned char  VolatileUsignedChar;
#endif
  
#ifndef VAX
# ifndef _IEEE
#  define _IEEE 1
# endif
#endif

#if defined(m68k) || defined(sun) || defined(MIPSEB) || defined(__ksr__)
#  define _BIG_ENDIAN_
#elif defined(MIPSEL) || defined(ns32k) || defined(vax) || defined(i386)
#  define _LITTLE_ENDIAN_
#endif

#if defined(sunos) || defined(sun) || defined(ultrix) || defined(__alpha)
# define MPROTECT
#endif

#if defined(__ksr__) || defined(__alpha)
   typedef unsigned int u32bit;
#else
   typedef unsigned long u32bit;
#endif

#include "aweconfig.h"

#ifdef USE_STD_CXX_HEADERS
#  include <iostream>
#  include <ostream>
#  include <istream>
#  include <map>
#  include <iomanip>
#else
#  include <iostream.h>
#  include <ostream.h>
#  include <istream.h>
#  include <map.h>
#  include <iomanip.h>
#endif


using namespace std;

#endif /* AwesimeConfig_h */
