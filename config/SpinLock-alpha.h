/* This may look like C code, but it is really -*- C -*- */
/*	(well, it's a mix of C and C++) */

/* 
 *  Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
 *  
 *  This file is part of Awesime.
 *  
 *  Awesime is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY.  No author or distributor accepts responsibility to
 *  anyone for the consequences of using it or for whether it serves any
 *  particular purpose or works at all, unless he says so in writing.
 *  Refer to the GNU General Public License for full details.
 *  
 *  Everyone is granted permission to copy, modify and redistribute
 *  Awesime, but only under the conditions described in the Gnu General
 *  Public License.  A copy of this license is supposed to have been given
 *  to you along with Awesime so you can know your rights and
 *  responsibilities.  It should be in a file named COPYING.  Among other
 *  things, the copyright notice and this notice must be preserved on all
 *  copies.
 *  
 */

#define SpinLockFree 0
typedef int SpinLockType;
typedef volatile int VolatileSpinLockType;

/*
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 */

static inline void SpinLockRelease(VolatileSpinLockType *lock)
{
  *lock = SpinLockFree;
}

static _MALLOC_INLINE_ void SpinLockReserve(VolatileSpinLockType *lock)
{
  for( ;; ) {
    /* Do not need to initialize oldState -- it is not a swap insn */
    SpinLockType oldState;
    asm volatile( "ldl_l	%0,%2"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
    if( oldState == SpinLockFree ) {
	oldState = 1;
        asm volatile( "stl_c	%0,%2"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
	if ( oldState == 0 ) {
		/* Failure .. */
		break;
	} else {
		return;
	}
    }
    else {
      while( *lock != SpinLockFree );
    }
  }
}

/*
 * This is a test-and-test-and-set
 */
static _MALLOC_INLINE_ int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{

    SpinLockType oldState;
    asm volatile( "ldl_l	%0,%2"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
    if( oldState == SpinLockFree ) {
	oldState = 1;
        asm volatile( "stl_c	%0,%2"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
	if ( oldState == 0 ) {
		/* Failure .. */
		return(0);
	} else {
		return (1);
	}
    } else {
	return(0);
    }
}
