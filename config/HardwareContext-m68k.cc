/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>

//
//	Many machines have the same (or very similar) stack format.
//	The 68K & 32K are examples of such machines.
//
//	The const *registers* defines the number of additional longwords
//	needed on the stack past the last frame pointer. This value needs
//	to jibe with the one in HardwareContext-<arch>.s.
//


const long MagicStackMarker = 0x464f4f20;	// this says 'FOO '


//
//	checkBytesSaved won't include a6 and a7, so it thinks
//	only 10 registers (6 data, 4 address) are saved.
//
//
//	Although the 68881 only has 8 registers, they store 12 bytes each.
//
static const int HardwareContextFloatQuads
	= (6 * 12) / sizeof(HardwareContextQuad);

static const int expectedBytesSavedInContext
	= 4 * 10
	+ 4 * HardwareContextFloatQuads;

static int numberOfBytesSavedInContext = 0;
static void computeBytesSaved();

extern "C" {
#include <sys/vadvise.h>
#include <sys/mman.h>
extern void *mprotect(void *, int, int);
int malloc_verify();
}
static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;



HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackBase = 0;
    stackEnd = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackBase should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_NONE:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = stackMallocAt;
	    stackBase = &stackMallocAt[stackSize-1];
	    break;

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (void *) valloc( (stackSize<<2) + pageSizeInBytes);

	    stackEnd = &stackMallocAt[pageSizeInQuads];
	    stackBase = &stackMallocAt[pageSizeInQuads + stackSize-1];

	    //
	    // now disable access to lower page
	    //

	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ);
	    break;

	default:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = &stackMallocAt[0];
	    stackBase = &stackMallocAt[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;

	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;

	//
	// tell the O/S that we're weird.
	//
	vadvise(VA_ANOM);
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {
	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ | PROT_WRITE);
	}
	free(stackMallocAt);
	stackMallocAt = 0;
    }
}

void
HardwareContext::magicSwitchTo(HardwareContext *to)
{
    //
    //	UNTESTED
    //
    asm( " " : : :
	"d1", "d2", "d3", "d4", "d5", "d6", "d7",
	"a0", "a1", "a2", "a3", "a4", "a5", "a6", "sp",
	"fp0", "fp1", "fp2", "fp3", "fp4", "fp5", "fp6", "fp7");

    asm("movl	a7,%0" : "=g" (sp));
    asm("movl	a6,%0" : "=g" (fp));
    asm("movl	%0,a7" : : "g" (to -> sp));
    asm("movl	%0,a6" : : "g" (to -> fp));
}

void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackBase - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << hex(long(getSp()));
	cerr << " and stackBase = " << hex(long(stackBase)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}


void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //
    
    stackCheck = (long *) stackBase;
    HardwareContextQuad **stack = (HardwareContextQuad **) stackBase;
    *(stack--) = (HardwareContextQuad *) MagicStackMarker;
    *(stack--) = 0;			// return address
    HardwareContextQuad **startOffFp = stack;
    *(stack--) = 0;			// frame pointer
    
    //
    //	Construct the stack frame that will be used in procedure startOff.
    //	startOff needs to know the value for *this*, which is passed in
    //	as the first parameter. It also gets the back-link FP for the
    //	last frame (build above).
    //
    
    *(stack--) = (HardwareContextQuad *) returnThis;
    HardwareContextQuad **nullFp = stack;
    
    // FP for previous frame
    
    *(stack--) = (HardwareContextQuad *) startOffFp;
    //
    //	Now build the stack frame that is used to return to startOff
    //
    
    *(stack--) = (HardwareContextQuad *) returnAddress;
    
    fp = (HardwareContextQuad) stack;
    
    *(stack) = (HardwareContextQuad *) nullFp;
    sp = (HardwareContextQuad) stack;
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex(long(stackEnd));
    s << " to " << hex(long(stackBase));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
    s << "[HardwareContext] fp = " << hex(long(fp));
    s << " sp = " << hex(long(sp));
    long p = *( (long *) fp );
    s << " @fp = " << hex(p) << "\n";
}
