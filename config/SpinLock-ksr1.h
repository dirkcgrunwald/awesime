/* This may look like C code, but it is really -*- C -*- */
/*	(well, it's a mix of C and C++) */

/* 
 *  Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
 *  
 *  This file is part of Awesime.
 *  
 *  Awesime is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY.  No author or distributor accepts responsibility to
 *  anyone for the consequences of using it or for whether it serves any
 *  particular purpose or works at all, unless he says so in writing.
 *  Refer to the GNU General Public License for full details.
 *  
 *  Everyone is granted permission to copy, modify and redistribute
 *  Awesime, but only under the conditions described in the Gnu General
 *  Public License.  A copy of this license is supposed to have been given
 *  to you along with Awesime so you can know your rights and
 *  responsibilities.  It should be in a file named COPYING.  Among other
 *  things, the copyright notice and this notice must be preserved on all
 *  copies.
 *  
 */
typedef volatile unsigned char VolatileSpinLockType;
#define SpinLockFree '\000'

extern "C" {
    void pstsp(VolatileSpinLockType *x);
    void rsp(VolatileSpinLockType *x);
    int gspnwt(VolatileSpinLockType *x);
}

static inline void SpinLockRelease(VolatileSpinLockType *lock)
{
  *lock = SpinLockFree;
}


/*
 * KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR 
 * KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR 
 * KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR 
 * KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR KSR 
*/

static void SpinLockReserve(VolatileSpinLockType *lock)
{
    long i=0;
    for (;;)  {
       if (*lock == SpinLockFree) {
         while (!gspnwt(lock)) {   /* retry once every 30us */
             while (++i & 0x7f)    /* about 4 cycles per iteraction */
                continue;
             if (i > 60*1000000*20/4)  {     /* about 60 seconds */
                assert2(0,"Failure to get sub_page\n");
	     }
         }
         if (*lock == SpinLockFree) {
             *lock = ~SpinLockFree;
             pstsp(lock); 
             rsp(lock);
	     return;
         }
         rsp(lock);
       }
     }
}

/*
 * This is a test-and-test-and-set
 */
static  int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{

  if ( *lock == SpinLockFree ) {
     long i=0;
     while (!gspnwt(lock)) {           /* retry once every 30us */
        while (++i & 0x7f)              /* about 4 cycles per iteraction */
            continue;
        if (i > 60*1000000*20/4) {       /* about 60 seconds */
           assert2(0,"Failure to get sub_page\n");
	}
     }
     if (*lock == SpinLockFree) {
        *lock = ~SpinLockFree;
        pstsp(lock); 
        rsp(lock);
        return(1);
     }
     else {
        rsp(lock);
 	return(0);
     }
  }
  else {
     return(0);
  }
}



