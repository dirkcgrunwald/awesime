/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
// AWEsim Context switching code for SPARC. This version is more optimizied
// than a straight-forward approach, but it's a little
//
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext-BSD.h"
#include "HardwareContext-BSDP.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>

extern "C" {
#include <sys/vadvise.h>
#include <sys/mman.h>
extern void *mprotect(void *, int, int);
int malloc_verify();
}

long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;

HardwareContextBSD::HardwareContext-BSD (int check, unsigned size)
{
    checkStackLimits = check;

    stackBase = 0;
    stackEnd = 0;
    stackMax = 0;
    stackSize = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackBase should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_NONE:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = stackMallocAt;
	    stackBase = &stackMallocAt[stackSize-1];
	    break;

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (void *) valloc( (stackSize<<2) + pageSizeInBytes);

	    stackEnd = &stackMallocAt[pageSizeInQuads];
	    stackBase = &stackMallocAt[pageSizeInQuads + stackSize-1];

	    //
	    // now disable access to lower page
	    //

	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ);
	    break;

	default:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = &stackMallocAt[0];
	    stackBase = &stackMallocAt[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;

	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;

	//
	// tell the O/S that we're weird.
	//
	vadvise(VA_ANOM);
    }
}

void
HardwareContextBSD::reclaimStack()
{
    if ( stackMallocAt ) {
	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ | PROT_WRITE);
	}
	free(stackMallocAt);
	stackMallocAt = 0;
    }
}


void
HardwareContextBSD::classPrintOn(ostream& s)
{
    s << "[HardwareContext-BSD] Stack spans " << hex(long(stackEnd));
    s << " to " << hex(long(stackBase));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
}

static
void PrintSingleStackFrame()
{
}
