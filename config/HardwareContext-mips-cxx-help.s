	.verstamp	2 10
	.text	
	.align	2
	.globl	getSp__15HardwareContextXv
	.loc	2 65
	.ent	getSp__15HardwareContextXv
getSp__15HardwareContextXv:
	.option	O1
#if 0
;;
;; $2 is the return value, $29 is SP
;;
#endif
	move	$2,$29
	j	$31
	.end	getSp__15HardwareContextXv

	.text	
	.align	2
	.file	2 "HardwareContext.cc"
	.globl	magicSwitchTo__15HardwareContextXP15HardwareContext
	.loc	2 155
	.ent	magicSwitchTo__15HardwareContextXP15HardwareContext 2
magicSwitchTo__15HardwareContextXP15HardwareContext:
	.option	O1
	.frame	$sp, 0, $31
	.loc	2 157
#if 0
;;
;; Notice that this implementation assumes there's only *one* possibler
;; caller of the magicSwitchTo routine -- we don't save the PC, and always
;; return to the PC of the caller, albeit with a different
;; SP & FP.
#endif

#define REG_SP	0
#define REG_FP	4
#define REG_S_0 8
#define REG_S_1 (REG_S_0 + 4)
#define REG_S_2 (REG_S_1 + 4)
#define REG_S_3 (REG_S_2 + 4)
#define REG_S_4 (REG_S_3 + 4)
#define REG_S_5 (REG_S_4 + 4)
#define REG_S_6 (REG_S_5 + 4)
#define REG_S_7 (REG_S_6 + 4)
#define REG_RA  (REG_S_7 + 4)
#define REG_F_0 (REG_RA + 4)
#define REG_F_1 (REG_F_0 + 8)
#define REG_F_2 (REG_F_1 + 8)
#define REG_F_3 (REG_F_2 + 8)
#define REG_F_4 (REG_F_3 + 8)
#define REG_F_5 (REG_F_4 + 8)

#define THIS $4
#define TO   $5
	sw	$16, REG_S_0( THIS )
	lw	$16, REG_S_0( TO )

	sw	$17, REG_S_1( THIS )
	lw	$17, REG_S_1( TO )

	sw	$18, REG_S_2( THIS )
	lw	$18, REG_S_2( TO )

	sw	$19, REG_S_3( THIS )
	lw	$19, REG_S_3( TO )

	sw	$20, REG_S_4( THIS )
	lw	$20, REG_S_4( TO )

	sw	$21, REG_S_5( THIS )
	lw	$21, REG_S_5( TO )

	sw	$22, REG_S_6( THIS )
	lw	$22, REG_S_6( TO )

	sw	$22, REG_S_6( THIS )
	lw	$22, REG_S_6( TO )

	sw	$31, REG_RA( THIS )
	lw	$31, REG_RA( TO )

	s.d	$f20, REG_F_0( THIS )
	l.d	$f20, REG_F_0( TO )

	s.d	$f22, REG_F_1( THIS )
	l.d	$f22, REG_F_1( TO )

	s.d	$f24, REG_F_2( THIS )
	l.d	$f24, REG_F_2( TO )

	s.d	$f26, REG_F_3( THIS )
	l.d	$f26, REG_F_3( TO )

	s.d	$f28, REG_F_4( THIS )
	l.d	$f28, REG_F_4( TO )

	s.d	$f30, REG_F_5( THIS )
	l.d	$f30, REG_F_5( TO )

	sw	$29, REG_SP(THIS)
	sw	$30, REG_FP(THIS)

	lw	$29, REG_SP(TO)
	lw	$30, REG_FP(TO)

	j	$31
	.end	magicSwitchTo__15HardwareContextXP15HardwareContext

	.text
	.ent	__magicReturnFunction
	.globl	__magicReturnFunction
__magicReturnFunction:
	move 	$4,$17
	j	$16
	.end	__magicReturnFunction
