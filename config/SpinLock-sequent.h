/* This may look like C code, but it is really -*- C -*- */
/*	(well, it's a mix of C and C++) */

/* 
 *  Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
 *  
 *  This file is part of Awesime.
 *  
 *  Awesime is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY.  No author or distributor accepts responsibility to
 *  anyone for the consequences of using it or for whether it serves any
 *  particular purpose or works at all, unless he says so in writing.
 *  Refer to the GNU General Public License for full details.
 *  
 *  Everyone is granted permission to copy, modify and redistribute
 *  Awesime, but only under the conditions described in the Gnu General
 *  Public License.  A copy of this license is supposed to have been given
 *  to you along with Awesime so you can know your rights and
 *  responsibilities.  It should be in a file named COPYING.  Among other
 *  things, the copyright notice and this notice must be preserved on all
 *  copies.
 *  
 */

#ifndef SpinLock_h
#define SpinLock_h

#ifdef __GNUG__
#  pragma interface
#endif

/*
 *
 *	Implement a UNIX process spin-lock. Suitable for Threads,
 *	(if you know blocking thread isn't switched) and for Unix
 *	processes.
 */

#if defined(__GNUG__)
#  include <assert.h>
#endif

#include <AwesimeConfig.h>

/*
 *	C Version (always included)
 */

#ifdef _SPIN_NO_INLINE_
#  define _MALLOC_INLINE_ 
#else
#  define _MALLOC_INLINE_ inline
#endif
/*
 *	Specialize per architecture
 */

#if	defined(encore) || defined(sequent)
#  define SpinLockFree '\000'
typedef unsigned char SpinLockType;
typedef volatile unsigned char VolatileSpinLockType;
#elif defined(sparc)
#  define SpinLockFree '\000'
typedef int SpinLockType;
typedef volatile int VolatileSpinLockType;
#else
#  define SpinLockFree '\000'
typedef unsigned char SpinLockType;
typedef volatile unsigned char VolatileSpinLockType;
#endif


/*
 *	Machines that don't actually use shared memory and spinlocks
 *	just check the current state to make certain things wouldn't
 *	deadlock on a true shared memory machine.
 */

static inline void SpinLockRelease(VolatileSpinLockType *lock)
{
  *lock = SpinLockFree;
}

#if !defined(_USE_SPIN_LOCKS_)

static inline void SpinLockReserve(VolatileSpinLockType *lock)
{
#if defined(__GNUG__)
  assert(*lock == SpinLockFree);
#endif
  *lock = ~SpinLockFree;
}

static inline int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{
#if defined(__GNUG__)
  assert(*lock == SpinLockFree);
#endif
  *lock = ~SpinLockFree;
  return(1);
}


/*
 *	The following are all SHARED MEMORY spinlocks.
 */

/*
 * ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE
 * ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE
 * ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE
 * ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE ENCORE
 */
#elif	defined(encore)

static _MALLOC_INLINE_ void SpinLockReserve(VolatileSpinLockType *lock)
{
  for( ;; ) {
    volatile SpinLockType * x = lock;
    SpinLockType oldState = 1;
    
    /* coded this way due to error in G++/Gcc 1.35 */
    
    asm volatile( "sbitib $0,%0" : "=g" (*x) );
    asm volatile( "sfsb %0" : "=g" (oldState) );
    if( oldState ) {
      while( *x != SpinLockFree );
    }
    else {
      return;
    }
  }
}

static _MALLOC_INLINE_ int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{
  volatile SpinLockType *x = lock;
  SpinLockType oldState = 1;
  asm volatile( "sbitib $0,%0" : "=g" (*x) );
  asm volatile( "sfsb %0" : "=g" (oldState) );
  /* 
   *	oldState == 0 -> we now own the lock
   *  oldState == 1 -> lock was owned, we don't own it.
   */
  return( ~ oldState );
}

/*
 * SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT 
 * SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT 
 * SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT 
 * SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT SEQUENT 
 */

#elif	defined(sequent)

static _MALLOC_INLINE_ void SpinLockReserve(VolatileSpinLockType *lock)
{
  for( ;; ) {
    SpinLockType oldState = ~SpinLockFree;
    asm volatile( "xchgb	%2,%0"
		 : "=a" (oldState)
		 : "0" (oldState), "m" (*lock) );
    if( oldState == SpinLockFree ) {
      return;
    }
    else {
      while( *lock != SpinLockFree );
    }
  }
}

static _MALLOC_INLINE_ int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{
  if ( *lock == SpinLockFree ) {
    SpinLockType oldState = ~SpinLockFree;
    asm volatile( "xchgb	%2,%0"
		 : "=a" (oldState)
		 : "0" (oldState), "m" (*lock) );
    return( oldState == SpinLockFree );
  } else {
    return (0);
  }
}

/*
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 * SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC SPARC 
 */

#elif	defined(sparc)

static _MALLOC_INLINE_ void SpinLockReserve(VolatileSpinLockType *lock)
{
  for( ;; ) {
    /* Do not need to initialize oldState -- it is not a swap insn */
    SpinLockType oldState;
    asm volatile( "ldstub	%2,%0"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
    if( oldState == SpinLockFree ) {
      return;
    }
    else {
      while( *lock != SpinLockFree );
    }
  }
}

/*
 * This is a test-and-test-and-set
 */
static _MALLOC_INLINE_ int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{
  if ( *lock == SpinLockFree ) {
    SpinLockType oldState;
    asm volatile( "ldstub	%2,%0"
		 : "=r" (oldState)
		 : "0" (oldState), "m" (*lock) );
    return( oldState == SpinLockFree );
  } else {
    /*
     * If the lock was not free, then we didn't get it.
     */
    return( 0 );
  }
}
#endif

/*
 *	G++ INTERFACE
 */

#if defined(__GNUG__)

class SpinLock {
  VolatileSpinLockType state;
 public:
  SpinLock();
  void reserve();
  int reserveNoBlock();
  void release();
};

inline SpinLock::SpinLock()
{
  state = SpinLockFree;
}

#ifndef _SPIN_NO_INLINE_
inline void SpinLock::reserve()
{
  SpinLockReserve(&state);
}

inline int SpinLock::reserveNoBlock()
{
  return( SpinLockReserveNoBlock(&state) );
}
#endif

inline void SpinLock::release()
{
  SpinLockRelease(&state);
}

#endif

#endif
