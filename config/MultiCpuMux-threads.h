// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//
#ifndef MultiCpuMux_h
#define MultiCpuMux_h
#include <CpuMux.h>
#include <MultiCpuMuxExceptions.h>

//
//	Filled in by the single original UNIX process. This value may
//	change during execution
//

class ThreadContainer;
class Thread;
class MultiCpuMux : public CpuMux {

protected:
    int eventsAddedThisBatch;

    Thread *RemoveFromCpu(int cpu);
    Thread *RemoveAffinity(int cpu);


protected:

    int oldGeneration; // placed here to avoid optimizer screwups

    void allocateLocalEventStructures();
    void deallocateEventStructures();

    virtual ThreadContainer* allocateThreadContainer();

    //
    //	like add, but adds to another CPU
    //
    virtual void addToCpu_(int, Thread *);
    virtual Thread *remove();

    //
    //	Exception handlers.
    //

    friend class ExceptionRelocate;
    ExceptionRelocate relocateException;

    friend class ExceptionEnrollDismissCpu;
    ExceptionEnrollDismissCpu enrollDismissCpuException;

    virtual void warmThePot(int);
    virtual void stirItAround();
    virtual void coolItDown();

    virtual void enrollCpu();
    virtual void dismissCpu();

    virtual void add_(Thread *);

    virtual void addReserve_();
    virtual void addUnlocked_(Thread*);
    virtual void addRelease_();
    MultiCpuMux(int debug = 0);
    virtual ~MultiCpuMux();

  void ParaDo(aThread_t);
  virtual MultiCpuMux* clone() = 0;

public:
    void EnrollDismissCpu(int enrollOrDismiss);
    virtual void fireItUp(int Cpus = 1,
			  unsigned long shared = long(4196 * 500));
    static void AddToAnother(int, Thread*);
    static MultiCpuMux *Cpu();
};

inline MultiCpuMux *MultiCpuMux::Cpu()
{
    return( (MultiCpuMux *) CpuMux::Cpu() ) ;
}

inline void MultiCpuMux::AddToAnother(int i, Thread *t)
{
    MultiCpuMux::Cpu() -> addToCpu_(i, t);
}

#endif /* MultiCpuMux_h */



