/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "HardwareContextP.h"
#include "ReserveByException.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>
#include <unistd.h>
#include "iolock.h"

extern "C" {
#include <sys/mman.h>
// #include <sys/vadvise.h>
// extern int vadvise(int);
// extern void *mprotect(void *, int, int);
extern int getpagesize();
}
extern "C" {
 void __magicReturnFunction();
 void SwitchTo(HardwareContext *, HardwareContext *);

}
//
//	Many machines have the same (or very similar) stack format.
//	The 68K & 32K are examples of such machines.
//
//	The const *registers* defines the number of additional longwords
//	needed on the stack past the last frame pointer. This value needs
//	to jibe with the one in HardwareContext-<arch>.s.
//

static long pageSizeInBytes = 0l;
static long pageSizeInQuads = 0l;
static long pageSizeMask = 0l;


HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackTop = 0;
    stackBottom = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0L;

    if (size > 0) {
	stackSize = size;
	//
	// stackTop should point to the first writeable cell of the
	// new stack.
	//
	unsigned long baseAddr;
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
/*
            This code causes KSR to BOMB

	    stackMallocAt = (unsigned long)
		malloc( stackSize * sizeof(HardwareContextQuad)
		       + pageSizeInBytes
		       + pageSizeInBytes );

	    //
	    // following assumes pages are power-of-two
	    //
	    baseAddr = stackMallocAt + pageSizeInBytes + 1;
	    pageAlignedStart = baseAddr & (~pageSizeMask);

	    mprotect((caddr_t ) pageAlignedStart, pageSizeInBytes, PROT_READ);

	    stackBottom = (void **) (pageAlignedStart + pageSizeInBytes);
	    stackTop = &stackBottom[stackSize-1];
	    break;
*/

	default:
	    stackMallocAt = (unsigned int) new void *[ stackSize ];
	    stackBottom = (void **) stackMallocAt;
	    stackTop = &stackBottom[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;
	pageSizeMask = pageSizeInBytes - 1;


	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;

	//
	// tell the O/S that we're weird.
	//
    }
}

void HardwareContext::magicSwitchTo(HardwareContext *to)
{
  SwitchTo(this,to);
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {

	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
#if 0
	    mprotect((caddr_t) pageAlignedStart, pageSizeInBytes,
		     PROT_READ | PROT_WRITE);
#endif
	}

	free((void *) stackMallocAt);
	stackMallocAt = 0;
    }
}

void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackTop - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << lock << "\nStack overflow\n";
	cerr << " getSp() = " << hex << (long(getSp()));
	cerr << " and stackTop = " << hex << (long(stackTop)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n" <<unlock;
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}

//
//	Unlike other architectures, the MIPS processor passes arguments
//	in registers. Thus, startOff will assume that *this* is stored
//	in $a0 -- this may not be true, because we're not causing a0,etc
//	to be saved & restored. In general, compilers may save/restore
//	the argument registers in other registers. Hence, this hack.
//
//	We have the new thread return to __magicReturnFunction with four
//	arguments explicitly stored in the stack. We load the first of these
//	two arguments, which is the value of *this* to be passed to the
//	function addressed in the second argument. Clear?
//

#define STACK_FRAME_SIZE 768
void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //
    
    stackCheck = (long *) stackTop;
    register HardwareContextQuad **stack;
    stack = (HardwareContextQuad **) stackTop;

    /////////////// FIRST FRAME
    // space for four args
    stack -= 2;
    stackCheck = (long *) stack;
    *(stack--) = (HardwareContextQuad *) MagicStackMarker;

    fp = (HardwareContextQuad) stack;

    *(stack--) = (HardwareContextQuad *) returnThis; 
    *(stack--) =  (HardwareContextQuad *) returnAddress;
    
    for (int i = 0; i < STACK_FRAME_SIZE/(sizeof(HardwareContextQuad))-2; i++) {
	*(stack--) = 0;
    }

    sp = (HardwareContextQuad) stack;
    ra = (HardwareContextQuad) __magicReturnFunction;

    cp = (HardwareContextQuad ) returnAddress;

#if 0
    cerr lock << "[HardwareContext-ksr1] Build stack for "
	<< (long(this)) << "\n";
    cerr << "[HardwareContext-ksr1] sp    = " << (long(sp)) << "\n";
    cerr << "[HardwareContext-ksr1] returnThis    = " << (long(returnThis)) << "\n";
    cerr << "[HardwareContext-ksr1] ra    = " << (long(ra)) << "\n";
    cerr << "[HardwareContext-ksr1] startOff    = " << (long(returnAddress)) << "\n";
    cerr << "[HardwareContext-ksr1] *startOff    = " << (long(*returnAddress)) << "\n" << unlock;
#endif
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex(long(stackBottom));
    s << " to " << hex(long(stackTop));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
    s << "[HardwareContext] fp = " << hex(long(fp));
    s << " sp = " << hex(long(sp));
    long p = *( (long *) fp );
    s << " @fp = " << hex(p) << "\n";
}

static
void PrintSingleStackFrame()
{
}
