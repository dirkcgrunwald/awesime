// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#include "MultiCpuMux.h"
#include "CpuMuxP.h"
#include "HardwareContextP.h"
#include "SpinLock.h"
#include "SpinBarrier.h"
#include "SpinFetchAndOp.h"
#include "Thread.h"
#include "ThreadContainer.h"
#include "ReserveByException.h"
//#include "Pragma.h"
#include <math.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "iolock.h"
#include "Posix.h"

#include <sys/time.h>
#include <sys/times.h>

typedef MultiCpuMux* (MultiCpuMux::*CloneFuncPtr )();

extern "C" {
    extern void mkdir(char*, int);
    extern int aInit(int);
    extern int aCreate(aThread_t*,voidPtrFuncP,void*);
    extern int aJoin(aThread_t);
    extern int aBind(aThread_t, aProcessor_name_t);
    extern aThread_t aSelf(void);
    extern aProcessor_name_t aGetproc(aThread_t);
}
//
//	Things left to do:
//
//	+ Capture signals, transfer them to an Exception class. Can
//	  use this to implement time-slices & the like, as well as....
//
//	+ Put in *addCpu* and *removeCpu* calls to CpuMux.
//	  This would allow run-time addition/removal of CPUS, so
//	  you can tailor your program to system 
//		This is tricky. Should probably do it when you
//		advance the clock, but it will be tricky to get all
//		the CPUs to agree on the barrier height for the
//		rendezvous. Also might complicate the *distinct
//		pools of threads per cpu*.
//

static SpinFetchAndOp GlobalCurrentEventsCounter(0);
static SpinLock GivingUpLock;
static VolatileInt GivingUpCounter = 0;
static VolatileInt GivingUpGeneration = 0;

static SpinBarrier CpuBarrier(1);

//
//	A currentEvents pile for each processor. The count is only correct
//	if you have reserved the spin lock -- its used as a guess.
//
//	This should be made a private data type to encapsulate the
//	manipulations of the data structures
//
static SpinLock CurrentEventsLock[_MAXIMUM_CPU_MUXS_];
static VolatileInt CurrentEventsCounter[_MAXIMUM_CPU_MUXS_];
static ThreadContainer *CurrentEvents[_MAXIMUM_CPU_MUXS_];
static ThreadContainer *affinityEvents[_MAXIMUM_CPU_MUXS_];
static VolatileInt affinityEventsCounter[_MAXIMUM_CPU_MUXS_];
static SpinLock affinityEventsLock[_MAXIMUM_CPU_MUXS_];
static aThread_t aThread_ids[_MAXIMUM_CPU_MUXS_];
 
void
MultiCpuMux::addToCpu_(int cpu, Thread *who)
{
    int affin = who->affinity();
    if (affin >= 0) {
        affinityEventsLock[affin].reserve();
	affinityEvents[affin]-> add(who);
	affinityEventsCounter[affin]++;
	affinityEventsLock[affin].release();
    }
    else {
        CurrentEventsLock[cpu].reserve();
        CurrentEvents[cpu] -> add( who );
        CurrentEventsCounter[cpu]++;
        CurrentEventsLock[cpu].release();
    }
    GlobalCurrentEventsCounter += 1;
}

//
// Possibly remove something from a given CPU queue; we err on the 
// side of optimization
//


Thread *
MultiCpuMux::RemoveAffinity(int cpu)
{
    Thread *x = 0;
    
    if ( affinityEventsCounter[cpu] != 0 ) {
	affinityEventsLock[cpu].reserve();
	if ( affinityEventsCounter[cpu] > 0 ) {
       	    x = affinityEvents[cpu] -> remove();
            affinityEventsCounter[cpu]--;
            GlobalCurrentEventsCounter -= 1;
	} 
	affinityEventsLock[cpu].release();
    }
    return( x );
}

Thread *
MultiCpuMux::RemoveFromCpu(int cpu)
{
    Thread *x = 0;
    
    if ( CurrentEventsCounter[cpu] != 0 
	 && CurrentEventsLock[cpu].reserveNoBlock() ) {
	if ( CurrentEventsCounter[cpu] > 0 ) {
   	    x = CurrentEvents[cpu] -> remove();
            CurrentEventsCounter[cpu]--;
            GlobalCurrentEventsCounter -= 1;
	} 
	CurrentEventsLock[cpu].release();
    }
    return( x );
}


#ifdef _PARANOID_
//
// RealityCheck counts the number of tasks in each queue
// and compares that to the 
void
CpuMuxRealityCheck(char *name, int line)
{
    
    int cpu;
    for ( cpu = 0; cpu < CpuMux::Muxs; cpu++ ) {
	CurrentEventsLock[cpu].reserve();
    }
    
    int inQueues = 0;
    int inQueue[CpuMux::Muxs];
    int inGlobal = GlobalCurrentEventsCounter.value();
    
    for ( cpu = 0; cpu < CpuMux::Muxs; cpu++ ) {
	
	if ( CurrentEventsCounter[cpu] < 0 ) {
	    cerr << "BAD NEWS: CurrentEventsCounter[" << cpu << "] = ";
	    cerr << CurrentEventsCounter[cpu] << "\n";
	}
	inQueue[ cpu ] = CurrentEventsCounter[cpu];
	inQueues += inQueue[ cpu ];
    }
    
    for ( cpu = 0; cpu < CpuMux::Muxs; cpu++ ) {
	CurrentEventsLock[cpu].release();
    }
    
    if ( inQueues != inGlobal ) {
	cerr << lock;
	cerr << name << "Found " << inQueues << " in queues ";
	cerr << "and " << inGlobal << " in global counter\n";
	cerr << name << " at line " << line << "\n";
	
	for (cpu = 0; cpu < CpuMux::Muxs; cpu++ ) {
	    cerr << "Cpu " << cpu << " has " << inQueue[cpu] << "\n";
	}
	cerr << unlock;
    }
}
#endif


MultiCpuMux::MultiCpuMux(int debug) : CpuMux(debug)
{
    pNameTemplate = "MultiCpuMux";
    CpuMux::Debug = debug;
}


MultiCpuMux::~MultiCpuMux()
{
}
void MultiCpuMux::EnrollDismissCpu(int enrollOrDismiss)
{
    // yet to be implemented
}
//
// Add a single CPU to a set of current CPUs. There is an advantage of
// having all child processes be spawned by CPU #0; all child signals
// will be caught by the single parent.
//
// This entry is called by a Thread.
//
void MultiCpuMux::enrollCpu()
{
    //
    // move thread to master process. There is a distinct possibility
    // that this guy will get stolen from Cpu #0 if everyone else is
    // looking for work.
    //
    while (iYam != 0) {
	currentThread -> affinity(0);
	relocateException.cpu(0);
	raise( &relocateException );
    }
    //
    // raise an exception to do the actual fork. This means that
    // control flow for the new child process will be in the
    // stirItAround loop, as opposed to here.
    //
    enrollDismissCpuException.enroll();
    raise( &enrollDismissCpuException );
    currentThread -> affinity(-1);
}

void
MultiCpuMux::dismissCpu()
{
    assert(0);
}

void
MultiCpuMux::allocateLocalEventStructures()
{
    pName = nameSpace;

    if ( CpuMux::Debug ) {
	cerr << lock;
	cerr << name() << "Allocate CpuMux structures for new CPU (";
	cerr << unlock;
    }
    CurrentEventsCounter[iYam] = 0;
    CurrentEvents[iYam] = allocateThreadContainer();
    affinityEventsCounter[iYam] = 0;
    affinityEvents[iYam] = allocateThreadContainer();
    GivingUpLock.reserve();
    if (GivingUpCounter >= CpuMux::Muxs) {
	GivingUpGeneration++;
	GivingUpCounter = 0;
    }
    GivingUpLock.release();
}

//
// Assumes iYam != 0
//
void
MultiCpuMux::deallocateEventStructures()
{
    assert( iYam != 0 );
    
    if ( CpuMux::Debug ) {
	cerr << lock << name() <<" Deallocate CpuMux structures for " << iYam << "\n" << unlock;
    }
    
    
    CurrentEventsLock[ iYam ].reserve();
//    cerr << lock << name() <<" Deallocate 2 for " <<iYam <<"\n"<< unlock;
    ThreadContainer *myEvents = CurrentEvents[ iYam ];
    //
    // Move remaining events to another queue. We are not adding new
    // events, just moving them around, so we do not increase
    // GlobalCurrentEventsCounter
    //

    while ( CurrentEventsCounter[iYam] > 0 ) {
	CurrentEventsLock[0].reserve();
	assert(CurrentEvents[0] != 0);
	while( ! myEvents -> isEmpty() )  {
	    Thread *t = myEvents -> remove();
	    CurrentEvents[0] -> add( t );
	    CurrentEventsCounter[0]++;
	    CurrentEventsCounter[iYam]--;
	}
	CurrentEventsLock[0].release();
    }
    CpuMux::MuxsLock.reserve();
    CpuMux::Muxs--;
    
    GivingUpLock.reserve();
    if (GivingUpCounter >= CpuMux::Muxs) {
	GivingUpGeneration++;
	GivingUpCounter = 0;
    }
    GivingUpLock.release();
    
    CpuMux::MuxsLock.release();
    
    if ( CpuMux::Debug ) {
	cerr << lock;
	cerr << name() << "set CpuMux::Muxs to " << CpuMux::Muxs;
	cerr << " and trigger GivingUp\n";
	cerr << unlock;
    }
    
    delete CurrentEvents[iYam];
    CurrentEvents[iYam] = 0;
    CurrentEventsCounter[iYam] = 0;
    
    CurrentEventsLock[iYam].release();
}

static void YouKillMe(int)
{
    cerr << "Change in child state - has child has died?\n";
}

void
MultiCpuMux::fireItUp(int cpus, unsigned long)
{
    assert(cpus > 0);
    
    if ( cpus > _MAXIMUM_CPU_MUXS_ ) {
	cpus = _MAXIMUM_CPU_MUXS_;
    }
    
    //
    // Set the barrier height so everyone can rendezvous..
    //
    CpuBarrier.height(cpus);
    if ( iYam == 0 ) {
	signal(SIGCHLD, YouKillMe);
    }  

    warmThePot(cpus);

    if ( CpuMux::Debug ) {
	cerr << lock;
	cerr << name() << " returns from warmThePot, join barrier\n";
	cerr << unlock;
    
    }
    if ((pid = aSelf()) == -1) {
       perror("Error in calling POSIX aThread_self\n");
       exit(-1);
    }
    aThread_ids[iYam] = pid;

    if (CpuMux::Debug)
       cerr << lock << "pid, proc " << pid <<"," <<aGetproc(pid)<< "\n" << unlock;
    if (aBind(pid,iYam)== -1) {
       perror("MultiCpuMux.C: Error in binding POSIX thread");
       exit(-1);
    }
    CpuBarrier.rendezvous();
    stirItAround();
    CpuBarrier.rendezvous();
    coolItDown();

  
}


void ParaDoWork(void* arg)
{

    ATHREAD_ARG* argPtr = (ATHREAD_ARG *) arg;

    MultiCpuMux* oldMux = (MultiCpuMux *)(argPtr->originalToClone);

    CloneFuncPtr Cfp = &MultiCpuMux::clone;

    MultiCpuMux* newMux = (oldMux->*Cfp)();
 
    newMux->ParaDo(argPtr->pid);
}


void MultiCpuMux::ParaDo(aThread_t PID) {

    pid = PID;
    aThread_ids[iYam] = PID;
    enabled = 1;
    if ( CpuMux::Debug ) {
        cerr << lock << name();
        cerr << "Created POSIX thread: iYam, thread id, Proc id, ThisCpu" << iYam << "," << pid << "," << aGetproc(pid) << "," <<  CpuMux::Cpu()<< "\n";
        cerr << unlock;
    }

    if ( CpuMux::Debug ) {
        cerr << lock;
        cerr << name() << "I am now id " << iYam << " and pid " << pid <<" \n";

        //
        // give each child a distinct temporary directory for core dumps
        // when debugging.
        //

        char tempName[L_tmpnam];
        tmpnam(tempName);
        mkdir(tempName,0777);
        int xx = chdir(tempName);

        cerr << name() << "change dir to " << tempName;
        if ( xx >= 0 ) {
            cerr << " worked\n";
        } else {
            cerr << " did not work\n";
        }

        cerr << unlock;
    }
    CpuBarrier.rendezvous();
    stirItAround();
    deallocateEventStructures();
    CpuBarrier.rendezvous();
    if (CpuMux::Debug) {
 	cerr << lock << name() << "exit\n" << unlock ;
    }
    return;
}


void
MultiCpuMux::warmThePot(int cpus)
{
    ATHREAD_ARG *argPtr;
    assert(cpus > 0);
    CpuMux::Muxs = cpus;
    enabled = 1;
    struct tms tim;
    double s_time;
    //
    //	Spawn the children, giving each a unique number from 0..(cpus-1).
    //  The first child gets id (cpus-1), and the original process gets 0.
    //
    
    
    if ( CpuMux::Debug ) {
	cerr << lock;
	cerr << name() << "Allocate " << CpuMux::Muxs << " cpus\n";
	cerr << unlock;
    }
    if (CpuMux::Debug) {
       s_time = times(&tim);
    }
    
    aInit(CpuMux::Muxs);

    for (int whoAmI = 1; whoAmI < CpuMux::Muxs; whoAmI++) {
        argPtr = new ATHREAD_ARG;
        argPtr->originalToClone = (void*)this;
        if (aCreate(&(argPtr->pid),(voidPtrFuncP) ParaDoWork, (void*)argPtr) == -1) {
           perror("MultiCpuMux.C");
           cerr << lock;
	   cerr << "Error in creating a POSIX thread \n";
	   cerr << unlock;
	   exit(-1);
	}
        if (CpuMux::Debug) {
           cerr << lock << "argPtr->pid, proc " << argPtr->pid <<"," <<aGetproc(argPtr->pid)<< "\n" << unlock;
	    }

	if ( CpuMux::Debug ) {
	    cerr << lock;
	    cerr << pid << " is born,";
	    cerr << unlock;
	}
    }
    if (CpuMux::Debug) {
       double eltime = times(&tim) - s_time;
       cerr << lock << "Time to create Posix threads: " << (eltime/sysconf(_SC_CLK_TCK)) << "\n"<< unlock; 
    }
}

void
MultiCpuMux::coolItDown()
{
  for (int id = 1; id < CpuMux::Muxs; id++) {
     if (aJoin(aThread_ids[id]) == -1) {
         cerr << lock << id << " POSIX thread join failure \n" << unlock; 
     }	
 }
}

void
MultiCpuMux::add_(Thread *who)
{
    if ( CpuMux::Debug ) {
	cerr << lock;
	if (who != 0 && who -> name() != 0) {
	    cerr << name() << " add " << who -> name() << "\n";
	} else {
	    cerr << name() << " add " << hex(long(who)) << "\n";
	}
	cerr << unlock;
    }
    
    addToCpu_(iYam,who);
}

void
MultiCpuMux::addReserve_()
{

    CurrentEventsLock[iYam].reserve();
    eventsAddedThisBatch = 0;
}

void
MultiCpuMux::addUnlocked_(Thread *thread)
{
    CurrentEvents[iYam] -> add(thread);
    CurrentEventsCounter[iYam]++;
    eventsAddedThisBatch++;
}

void
MultiCpuMux::addRelease_()
{
    GlobalCurrentEventsCounter += eventsAddedThisBatch;
    CurrentEventsLock[iYam].release();
}

Thread *
MultiCpuMux::remove()
{
    
    //
    // remov()::
    //
    // Check to see if there is a current event, either in our current
    // events queue or someone elses current events queue. If there is
    // nothing, return 0.
    //
    
    //
    // Optimizations in getting a thread:
    //
    
    // We are *not* locking before looking at CurrentEventsCount -- we
    // treat this as a *guess* before bothering to lock on it. Admittedly,
    // this can cause problems, but the global count of tasks is always
    // locked correctly, so we will never make a mistake -- it is just that
    // we might be inefficient.
    //
    // Also, we use reserveNoBlock to avoid busy task queues -- this is
    // again an optimization that might bite us.
    
    Thread *threadToExecute = 0;
    
    if (*terminated) return(0);
    
    //
    //	Maybe someone else has something to do?
    //
    if ( GlobalCurrentEventsCounter.value() > 0 )  {
	if ((threadToExecute = RemoveAffinity(iYam)) == 0) {	    
	   int ask = iYam;
	    do {
	       if ( CpuMux::Debug ) {
		  cerr << lock;
		  cerr << name() << "Ask " << ask << " about events \n";
		  cerr << unlock;
	       }
	       threadToExecute = RemoveFromCpu(ask);
	       ask++;
	       if ( ask >= CpuMux::Muxs ) ask = 0;
	    } while (ask != iYam && threadToExecute == 0);
        }
	
    }
    else {
	if ( CpuMux::Debug ) {
	    cerr << lock;
	    cerr << name() << " Found no global events";
	    cerr << " while looking for thread\n";
	    cerr << unlock;
	}
	
    }
    
    
    if ( CpuMux::Debug ) {
	cerr << lock;
	cerr << name() << "find ";
	if (threadToExecute == 0) {
	    cerr << "nothing\n";
	} else {
	    cerr << threadToExecute -> name() << "\n";
	}
	cerr << unlock;
    }
    
    return( threadToExecute );
}

void
MultiCpuMux::stirItAround()
{
    currentThread = 0;
    
    assert( CurrentEvents[ iYam ] != 0);
    
    if (!enabled) {
	cerr << "Need to initialize CpuMux before using it\n";
    }
    
    while( ! *terminated ) {

	while ( currentThread == 0 ) {
	    
	    currentThread = remove();
	    
	    //
	    // run if we got one
	    //
	    if (currentThread != 0) break;
	    
	    //
	    // reloop if some should exist
	    //
	    if ( GlobalCurrentEventsCounter.value() > 0 ) continue;
	    
	    if ( CpuMux::Debug ) {
		cerr << lock;
		cerr << name() << "check if I should quit\n";
		cerr << unlock;
	    }
	    
	    GivingUpLock.reserve();
	    
	    GivingUpCounter++;
	    
	    if ( CpuMux::Debug ) {
		cerr << lock;
		cerr << name() << GivingUpCounter;
		cerr << " CPUs attempting to give up\n";
		cerr << unlock;
	    }
	    
	    assert( GivingUpCounter > 0);
	    assert( GivingUpCounter <= CpuMux::Muxs);
	    
	    if ( GivingUpCounter == CpuMux::Muxs 
		 && GlobalCurrentEventsCounter.value() == 0)
	    {
		
		GivingUpGeneration ++;
		GivingUpCounter = 0;
		GivingUpLock.release();
		
		if ( CpuMux::Debug ) {
		    cerr << lock;
		    cerr << name() << "give up\n";
		    cerr << name() << " GCEC = ";
		    cerr << GlobalCurrentEventsCounter.value() << "\n";
		    cerr << name() << "my CEC = ";
		    cerr << CurrentEventsCounter[ iYam ] << "\n";
		    cerr << unlock;
		}
		
		return;
	    }
	    else {
		
		//
		// Record the current generation of the the giving up
		// barrier -- we are going to give up only if every
		// one else agrees to give up and nothing new to do
		// comes along in the meantime.
		//
		
		int generation = GivingUpGeneration;
		VolatileInt *genp = &GivingUpGeneration;
		
		GivingUpLock.release();
		
		if ( CpuMux::Debug ) {
		    cerr << lock;
		    cerr << name() << " *genp = " << *genp << "\n";
		    cerr << name() << " generation = " << generation << "\n";
		    cerr << unlock;
		}
		
		while( generation == *genp
		       && GlobalCurrentEventsCounter.value() == 0
		       && !*terminated );
		
		
		GivingUpLock.reserve();
		if ( *genp != generation || *terminated ) {
		    
		    if ( CpuMux::Debug ) {
			cerr << lock;
			cerr << name() << " *genp = " << *genp << "\n";
			cerr << name() << " generation = ";
			cerr << generation << "\n";
			cerr << name() << " GCEC = ";
			cerr << GlobalCurrentEventsCounter.value() << "\n";
			cerr << name() << " *terminated = ";
			cerr << *terminated << "\n";
			cerr << name() << " giving up\n";
			cerr << name() << "my CEC = ";
			cerr << CurrentEventsCounter[iYam] << "\n";
			cerr << unlock;
		    }
		    
		    GivingUpLock.release();
		    return;
		}
		else {
		    
		    if ( CpuMux::Debug ) {
			cerr << lock << name() << " bail out\n" << unlock;
		    }
		    
		    GivingUpCounter--;
		    assert(GivingUpCounter >= 0);
		    GivingUpLock.release();
		    
		    if ( CpuMux::Debug ) {
			cerr << lock;
			cerr << name() << " check for something\n";
			cerr << name() << " i have ";
			cerr << CurrentEventsCounter[iYam];
			cerr << " events\n" ;
			
			cerr << name() << " out of " ;
			cerr << GlobalCurrentEventsCounter.value() << "\n";
			cerr << unlock;
		    }
		}
	    }
	}
	
	
        if ( CpuMux::Debug ) {
	    cerr << lock;
	    cerr << name() << " switch to ";
	    cerr << currentThread->name() << "\n";
	    cerr << unlock;
	}
    
        raisedBy = 0;
	systemTransfer( currentThread );
	//
	// Come back via a raise, which may already be handled
	//
	currentThread = 0;
	CpuMux::handoff();
    }
}

ThreadContainer*
MultiCpuMux::allocateThreadContainer()
{
    assert(0);
    abort();
    return ((ThreadContainer *) NULL);          // to make KSR compiler happy
}
