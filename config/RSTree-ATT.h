// -*-c++-*-
//
// Adapted from the LEDA-3.0 class 'rs_tree.h'.
//
//  Copyright (c) 1992  by  Max-Planck-Institut fuer Informatik
//  Im Stadtwald, 6600 Saarbruecken, FRG     
//  All rights reserved.
// 
//
// (Generalized) Randomized Search Trees
// -------------------------------------
// by C.Aragon and R.Seidel
//
//
// variable probability distribution
// doubly-linked with pred-succ-pointer
//
// 6/92, M.Paul
//


#ifndef RSTREE_H
#define RSTREE_H

#undef _DEBUG_RSTREE_
#define _DEBUG_RSTREE_

#include <math.h>
#include <stdlib.h>
//
// Ansi equiv of values
//
#include <limits.h>
#include <Pix.h>
#include <RNG.h>
#include <MLCG.h>

/* still to implement: conc, split */

//
// Note - this is shared between all threads -- you should
// really specify your only RNG for the RSTree classes.
//
class MLCG;
extern MLCG RSTreeDefaultRng;

template <class NodeKey, class NodeData>
struct RstNode {
  public:
    NodeKey key ;
    NodeData inf ;
    int prio ;    // priority between 0 and INT_MAX-1
    RstNode<NodeKey,NodeData> *parent;
    RstNode<NodeKey,NodeData> *preditem;
    RstNode<NodeKey,NodeData> *succitem;
    RstNode<NodeKey,NodeData> *l_child;
    RstNode<NodeKey,NodeData> *r_child;

    RstNode(NodeKey& key_, NodeData& inf_, int prio_) {
	key = key_;
	inf = inf_;
	prio = prio_;
	parent = l_child = r_child = preditem = succitem = 0;
    }

    RstNode() {}
};

template <class MapKey, class MapData>
class RSTree
{

  enum rst_dir { rst_left=1, rst_right=2 } ;
  
  RstNode<MapKey,MapData>* root ;			// pointer to root AND leaf
  int count ;				// number of entries
  RNG *rng;
  
public:
  
  RstNode<MapKey,MapData>* search( MapKey searchkey,
				   RstNode<MapKey,MapData>* parent ) const  {
      MapKey key = searchkey ;
      root->key = key ;				// ensure stop at root
      
      RstNode<MapKey,MapData>* pp = root;                       // parent of p 
      RstNode<MapKey,MapData>* p = pp -> r_child ;		// actual node
      
      while( key != p->key  ) {
	  pp = p ;
	  if( key < p -> key ) 
	      p = p -> l_child ; 
	  else 
	      p = p -> r_child ;
      }
      
      parent = pp ;
      return (p==root) ? 0 : p ;
  }
  
  RstNode<MapKey,MapData>* insert_at_item( RstNode<MapKey,MapData>* pn, MapKey searchkey, MapData inf ) {
      root->prio = INT_MAX ; 			// ensure stop at root

      int prio = rng -> asInt();
      if (prio < 0) prio = -prio;

      MapKey key = searchkey ;
      RstNode<MapKey,MapData>* p;
      RstNode<MapKey,MapData>* pp = pn ;

      count++ ;					// create new node

      p = new RstNode<MapKey,MapData>(key,inf,prio);

      RstNode<MapKey,MapData>* pl;
      RstNode<MapKey,MapData>* pr;

      if( key < pp->key ) { 			// update pred-succ-ptr
	  pl = pp -> preditem ;
	  pr = pp ;
      }
      else {
	  pr = pp -> succitem ;
	  pl = pp ;
      }

      pl -> succitem = p ;
      p -> preditem = pl ;
      p -> succitem = pr ;
      pr -> preditem = p ;
      
      pl = pr = root ;				// children of p

      while( prio > pp->prio ) {			// rotate p up

	  if( key < pp->key) {
	      pp -> l_child = pr ;  pr -> parent = pp ;
	      pr = pp ;  pp = pp -> parent ;
	  }
	  else {
	      pp -> r_child = pl ;  pl -> parent = pp ;
	      pl = pp ;  pp = pp -> parent ;
	  }
      } ;

      if( prio==pp->prio && key < pp->key ) {	// one more rot 
	  pp -> l_child = pr ;  pr -> parent = pp ;	// not needed for corr.
	  pr = pp ;  pp = pp -> parent ;
      }

      p -> l_child = pl ;  pl -> parent = p ;		// insert rs node
      p -> r_child = pr ;  pr -> parent = p ;

      if( key < pp->key )
	  pp -> l_child = p ;
      else
	  pp -> r_child = p ;
      p -> parent = pp ;

      return p ;
  }

  Pix pred( Pix pin ) const {
      RstNode<MapKey,MapData>* p = (RstNode<MapKey,MapData>*) pin;
      RstNode<MapKey,MapData>* q;
      q = p->preditem ;
      return Pix((q==root) ? 0 : q) ;
  }
  
  Pix succ( Pix pin ) const {
      RstNode<MapKey,MapData>* p = (RstNode<MapKey,MapData>*) pin;
      RstNode<MapKey,MapData>* q = p->succitem ;
      return Pix((q==root) ? 0 : q) ;
  }

  
  MapKey key( Pix p_ ) const {
      RstNode<MapKey,MapData>* p = (RstNode<MapKey,MapData >*) p_;
      return p -> key;
  }

  MapData& data( Pix p_ ) const {
      RstNode<MapKey,MapData>* p = (RstNode<MapKey,MapData >*) p_;
      return p->inf ;
  };

  MapData& operator()(Pix p_) const {
      RstNode<MapKey,MapData>* p = (RstNode<MapKey,MapData >*) p_;
      return p->inf ;
  };


  Pix first() const  {
      return(succ(Pix(root)));
  };

  void next( Pix& p_ ) const  {
      p_ = succ(p_);
  }
  
  Pix min() const {return succ(Pix(root)) ; }
  Pix max() const { return pred(Pix(root)) ; }
  int size() const { return count;}
  int empty() const {return (count==0);};
  
  //
  // Constructor
  //
  RSTree(RNG& rng_ = RSTreeDefaultRng) {
      rng = &rng_;
      root = new RstNode<MapKey,MapData>;
      root -> r_child = root -> preditem = root -> succitem = root ;
      root -> l_child = root -> parent = 0 ;
      //
      // value of root -> l_child and root -> parent are always undefined
      //
      count = 0 ;  
  }
  
  RSTree( const RSTree<MapKey,MapData>& rst, RNG& rng_ = RSTreeDefaultRng) {
      rng = &rng_;
      root = new RstNode<MapKey,MapData>;
      root -> r_child = root -> preditem = root -> succitem = root ;
      root -> l_child = root -> parent = 0 ;
      count = 0 ;
      
      Pix p = rst.first() ;
      
      while( p ) {					// insert all elements
	  insert( rst.key(p), rst.data(p) ) ;
	  rst.next(p) ;
      }
      
  }
  
  RSTree<MapKey,MapData>& operator=( const RSTree<MapKey,MapData>& rst ) {
      clear() ;  
      
      for (Pix p = rst.first(); p; rst.next(p) ) {
	  insert( rst.key(p), rst.data(p) ) ;
      }
      
      return *this ;
      
  }

  ~RSTree()  {
      clear() ;
      delete root ;
  }
  

  //
  // Lookup the key and return a pointer to the RstItem
  //
  Pix lookup( MapKey key ) const {
      RstNode<MapKey,MapData>* pp = 0;
      RstNode<MapKey,MapData>* p = search( key, pp ); 
      return Pix(p); 
  }

  //
  // locate a key in a possibly empty map? I have no idea
  // what this does..
  //
  Pix locate( MapKey searchkey ) const {
      Pix result = 0;
      if ( empty() ) {
	  RstNode<MapKey,MapData>* pp = 0 ;
	  RstNode<MapKey,MapData>* p = search( searchkey, pp ) ; 
	  
	  if( !p ) {
	      if( searchkey < pp->key )
		  p = pp ;
	      else
		  p = (RstNode<MapKey,MapData >*) (succ(Pix(pp))) ;
	  }
	  result = Pix(p);
      }
      
      return result;
      
  }

  Pix locate_pred( MapKey searchkey ) const {
      Pix result = 0;
      if( empty() ) {
	  
	  RstNode<MapKey,MapData>* pp = 0;
	  RstNode<MapKey,MapData>* p = search( searchkey, pp ) ; 
	  
	  if( !p ) {
	      if( searchkey < pp->key)
		  p = (RstNode<MapKey,MapData >*) pred((Pix(pp))) ;
	      else
		  p = pp ;
	  }
	  
	  result =  Pix(p) ;
      }
      return result;
      
  }

  Pix insert( MapKey key, MapData inf ) {
      RstNode<MapKey,MapData>* pp = 0;
      RstNode<MapKey,MapData>* p = search( key, pp ) ;
      Pix result;
      if( p ) {
	  p->inf = inf ;
	  result = Pix(p) ;
      }
      else  {
	  result = Pix(insert_at_item( pp, key, inf )) ;
      }
      return result;
  }


  void del( MapKey key ) {
      RstNode<MapKey,MapData>* pp = 0;
      RstNode<MapKey,MapData>* p = search( key, pp ) ;
      if( p ) {
	  del_item( p ) ;
      }
  }

  void clear()  {
      Pix p = first() ;
      while( p ) {
	  Pix q = p;
	  next(p) ;
	  delete q ;
      }
      root -> r_child = root -> preditem = root -> succitem = root ;
      root -> l_child = root -> parent = 0 ;
      count = 0 ;
  }

  //
  // additional priority queue operations
  //
  
  Pix find_min() const { return(first()); }

  MapData& front() const  {
      return(data(first()));
  };

  MapData& deq() {
      //
      // You need to check for empty calling this.
      //
      RstNode<MapKey,MapData>* q = root -> succitem;
      MapData& foo = q -> inf;
      if ( q != root ) {
	  del_item( q );
      }
      return( foo );
  };

  void decrease_key( Pix pix, MapKey key ) {
      insert( key, data(pix) ) ;
      del_item((RstNode<MapKey,MapData >*) pix) ;
  }

  void del_item( RstNode<MapKey,MapData>* p ) {

      root->prio = -1 ;			// ensure stop at root
      
      RstNode<MapKey,MapData>* pp = p -> parent;
      RstNode<MapKey,MapData>* pl = p -> preditem;
      RstNode<MapKey,MapData>* pr = p -> succitem;  

      pl -> succitem = pr;
      pr -> preditem = pl;			// update pred-succ-ptr
      
      rst_dir dir = ( p==pp -> r_child ? rst_right : rst_left ) ;
      
      pl = p -> l_child ;
      pr = p -> r_child ;

      delete p ;
      count--;
      
      //
      // Rebalance tree
      //
      while( pl != pr ) {
	  if( pr->prio > pl->prio ) {
	      if ( dir == rst_left ) {
		  pp -> l_child = pr ;  pr -> parent = pp ;
		  pp = pr ;  dir = rst_left ;  pr = pp -> l_child;
	      } else {
		  pp -> r_child = pr ;  pr -> parent = pp;
		  pp = pr ;  dir = rst_left ;  pr = pp -> r_child;
	      }
	  }
	  else {
	      if ( dir == rst_left ) {
		  pp -> l_child = pl ;  pl -> parent = pp ;
		  pp = pl ;  dir = rst_right ;  pl = pp -> l_child;
	      } else {
		  pp -> r_child = pl ;  pl -> parent = pp ;
		  pp = pl ;  dir = rst_right ;  pl = pp -> r_child;
	      }
	  }
      }

      if ( dir == rst_left ) {
	  pp -> l_child = root;
      } else {
	  pp -> r_child = root;
      }
  }

};

#endif

 

