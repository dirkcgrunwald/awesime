/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
/*
 * This may look like C code, but it is really -*- C++ -*-
 * 
 * Copyright (C) 1988 University of Illinois, Urbana, Illinois
 * Copyright (C) 1989 University of Colorado, Boulder, Colorado
 * Copyright (C) 1990 University of Colorado, Boulder, Colorado
 *
 * written by Dirk Grunwald (grunwald@foobar.colorado.edu)
 */

#ifndef AwesimeConfig_h
#define AwesimeConfig_h

#ifdef __GNUG__
#  pragma interface
#endif

/*
 *
 *	Generic parallel processor.
 *
 */

#ifndef _MAXIMUM_CPU_MUXS_
#   define _MAXIMUM_CPU_MUXS_ 50
#endif

static const int DefaultSpinBarrierLoops = 1000;
static const int DefaultBarrierLoops = 100;

typedef volatile int VolatileInt;
typedef volatile short VolatileShort;
typedef volatile unsigned char  VolatileUsignedChar;

#ifndef VAX
# ifndef _IEEE
#  define _IEEE 1
# endif
#endif

#if defined(m68k) || defined(sparc) || defined(MIPSEB) || defined(__ksr__)
#  define _BIG_ENDIAN_
#elif defined(MIPSEL) || defined(ns32k) || defined(vax) || defined(i386) || defined(alpha)
#  define _LITTLE_ENDIAN_
#endif


#if defined(sunos) || defined(sun) || defined(ultrix)
# define MPROTECT
#endif

#if defined(sparc) || defined(sequent)
#  define _USE_SHARED_MEMORY_MMAP_
#elif defined(encore)
#  define _USE_SHARED_MEMORY_SHARE_
#endif

#if defined(_USE_SHARED_MEMORY_MMAP_) || defined(_USE_SHARED_MEMORY_SHARE_)
#  define _USE_SHARED_MEMORY_	1
#endif

#if defined(encore) || defined(sequent) || defined(sparc) || defined(__ksr__) || defined(__alpha)
#  define _USE_SPIN_LOCKS_
#endif

#if defined(__ksr__) || defined(__alpha)
   typedef unsigned int u32bit;
#else
   typedef unsigned long u32bit;
#endif

#endif /* AwesimeConfig_h */
