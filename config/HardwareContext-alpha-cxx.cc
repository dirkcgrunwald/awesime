/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "HardwareContextP.h"
#include "assert.h"
#include <iostream>
#include <unistd.h>
#include <memory.h>
#include <stdlib.h>

extern "C" {
#include <sys/mman.h>
extern int getpagesize();
}

long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;
static long pageSizeMask = 0;

HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackTop = 0;
    stackBottom = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackTop should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (unsigned long)
		malloc( stackSize * sizeof(HardwareContextQuad)
		       + pageSizeInBytes
		       + pageSizeInBytes );

	    //
	    // following assumes pages are power-of-two
	    //
	    unsigned long baseAddr = long(stackMallocAt) + pageSizeInBytes + 1;
	    pageAlignedStart = (baseAddr & (~pageSizeMask));

	    mprotect( (char *) (pageAlignedStart), pageSizeInBytes, PROT_READ);

	    stackBottom = (void **) (pageAlignedStart + pageSizeInBytes);
	    stackTop = &stackBottom[stackSize-1];
	    break;

	default:
	    stackMallocAt = (unsigned long) new void *[ stackSize ];
	    stackBottom = (void **) stackMallocAt;
	    stackTop = &stackBottom[stackSize-1];
	    break;
	}
    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;
	pageSizeMask = pageSizeInBytes - 1;


	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {
	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect((char *) pageAlignedStart, pageSizeInBytes,
		     PROT_READ | PROT_WRITE);
	}

	free((void *) stackMallocAt);
	stackMallocAt = 0;
    }
}

void
HardwareContext::stackOverflow()
{
    unsigned long depth = stackTop - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << hex(long(getSp()));
	cerr << " and stackTop = " << hex(long(stackTop)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}

//
//	Unlike other architectures, the MIPS processor passes arguments
//	in registers. Thus, startOff will assume that *this* is stored
//	in $a0 -- this may not be true, because we're not causing a0,etc
//	to be saved & restored. In general, compilers may save/restore
//	the argument registers in other registers. Hence, this hack.
//
//	We have the new thread return to __magicReturnFunction with four
//	arguments explicitly stored in the stack. We load the first of these
//	two arguments, which is the value of *this* to be passed to the
//	function addressed in the second argument. Clear?
//
extern "C" {
 void __magicReturnFunction();
}

void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //

	unsigned long rt = (unsigned long) returnThis;
	unsigned long ra = (unsigned long) returnAddress;
    
    stackCheck = (long *) stackTop;
    register HardwareContextQuad **stack;
    stack = (HardwareContextQuad **) stackTop;

    stackCheck = (long *) stack;

    stack[0] = (HardwareContextQuad *) MagicStackMarker;
    stack[-1] = 0;
    stack[-2] = (HardwareContextQuad *) &stack[-1];
    stack[-3] = (HardwareContextQuad *) &stack[-2];
    ret_pc = (HardwareContextQuad) __magicReturnFunction;
    reg_s[0] = (HardwareContextQuad) returnThis;
    reg_s[1] = (HardwareContextQuad) returnAddress;
    sp = (HardwareContextQuad) &stack[-3];
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex  << stackBottom;
    s << " to " << hex << stackTop << "\n";
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
    s << "[Hdw] fp = " << hex << fp << " sp = " << hex << sp << "\n";
}

static
void PrintSingleStackFrame()
{
}
