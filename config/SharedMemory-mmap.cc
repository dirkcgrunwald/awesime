/* -*- C -*-
 *
 * Edit this in C-mode, and keep it as C compatible as possible,
 * but in Awesime, it's compiled by C++.
 *
 * Define this if you need to debug the shared memory code
 */

#include <assert.h>
#include <stdio.h>
#include "libc.h"
#include "unistd.h"

extern "C" {
void *sbrk(int);
void AwesimeMarkBreak();
void exit(int x);
void free(void *);
void perror(const char*);
extern int getpagesize();
extern int mkstemp();
#  include <sys/file.h>
#  include <sys/types.h>
#  include </usr/include/sys/mman.h>
};

typedef unsigned long SMT;

#if defined(sequent)
extern SMT mmap (
		 caddr_t addr,   /* starting virt-addr */
		 int     	len,    /* length (bytes) to map */
		 int     prot,   /* RO, RW encoding */
		 int     share,  /* private/shared modifications */
		 int     fd,     /* open file to be mapped */
		 off_t   pos    /* where in file to begin map */
		 );
#endif

int __SharedMemoryEnabled__ = 0;

static int AmountOfSharedMemoryExtra = 0;

#ifndef L_tmpnam
#  define L_tmpnam 128
#endif

#undef assert
#undef assert2
#define assert(ex) { if ((ex)) ; else _assert( #ex, __FILE__,__LINE__); }
#define assert2(ex,s) { if ((ex)) ; else _assert2( #ex, __FILE__,__LINE__, s);}
static void _assert(const char * str, const char *file, const int line)
{
  fprintf(stderr,"Shared memory assertion failed: %s, line %d of file %s\n",
	  str, line, file);
  fprintf(stderr,"You requested %d extra bytes of shared memory\n",
	  AmountOfSharedMemoryExtra );
}

/*
 *	Not really a part of CpuMux, but used by it & hidden here.
 */
void SharedMemoryInit( unsigned extra )
{
  /*
   * Bump the break if necessary.
   */ 
  
  SMT oldBreak;
  SMT newBreak;
  
  SMT pagesize;
  /*
   * Share .data, .bss, and the extra.
   */ 
  extern char**   environ;
  extern int etext;
  
  SMT shareStart;
  SMT shareSize;
  SMT oldData;
  static char templateName[L_tmpnam];
  SMT file;
  SMT bytesWritten;
#ifdef SEEK_SET
  int seek_set = SEEK_SET;
#else
  int seek_set = 0;
#endif
  SMT nowAt;
  int theData = 0;
  int returnStatus;
  SMT realshmem;
  
  AmountOfSharedMemoryExtra = extra;

  assert(extra >= 0);
  oldBreak = (SMT) sbrk(0);
  
  /*
   * Tell the sbrk handler to record the current mark and then ...
   */
  AwesimeMarkBreak();
  
  /*
   * allocate some more space...
   */
  
  newBreak = (SMT) sbrk( extra );
  
  if ( newBreak == -1 ) {
    perror( "Share: sbrk" );
    fprintf( stderr, "Old break value was            = 0x%x\n", oldBreak);
    fprintf( stderr, "You requested this much memory = %d\n", extra);
    exit( 1 );
  }
  newBreak = (SMT) sbrk( 0 );
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"newBreak = 0x%x before rounding\n", newBreak);
#endif
  
  /* truncate to pagesize number of pages, rounding up */
  /* Assumed pagesize is power-of-two */
  
  pagesize = getpagesize();
  
  newBreak = (newBreak + pagesize) & (~(pagesize-1));
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"newBreak = 0x%x\n", newBreak);
#endif
  
#if 0
  shareStart = (SMT) &environ;
#else
  shareStart = (((SMT) &etext) + pagesize-1) / pagesize;
  shareStart *= pagesize;
#endif
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"shareStart = 0x%x\n", shareStart);
#endif
  
  shareSize = (SMT) newBreak - shareStart;
  oldData   = (SMT) oldBreak - shareStart + 4;
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"shareSize = 0x%x\n", shareSize);
#endif
  
  
#if defined(sequent)
  sprintf(templateName, "/tmp/malloc.$$", getpid());
#else
  tmpnam(templateName);
#endif
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"file templateName is %s\n", templateName);
#endif
  
#if defined(sequent)
  file = open(templateName, O_TRUNC | O_CREAT | O_RDWR, 0777);
  if ( file ==  -1 ) {
    perror("open ");
  }
#else

  file = mkstemp(templateName);
#endif
  
  /*
   * delete the file now to avoid problems with it remaining after
   * the application is done.
   */
  
  unlink(templateName);
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"file = 0x%x\n", file);
#endif
  
  bytesWritten = write(file, (char *) shareStart, (int) oldData);
  if (bytesWritten != oldData ) {
    fprintf(stderr,"bytesWritten = 0x%x\n", bytesWritten);
    fprintf(stderr,"oldData      = 0x%x\n", oldData);
    assert( bytesWritten == oldData);
  }
  
  /*
   * lseek to the end of the file and write that..
   */
  
  nowAt = lseek(file, newBreak, seek_set);
  if ( nowAt != newBreak ) {
    fprintf(stderr, "nowAt = 0x%x\n", nowAt);
    fprintf(stderr, "newBreak = 0x%x\n", newBreak);
    assert(nowAt == newBreak);
  }
  
  bytesWritten = write(file, &theData, sizeof(theData));
  if ( bytesWritten != sizeof(theData)) {
    perror("write");
    fprintf(stderr,"bytesWritten    = 0x%x\n", bytesWritten);
    fprintf(stderr,"Wanted to write = 0x%x\n", sizeof(theData));
  }

  assert( bytesWritten == sizeof(theData) );
  
#if defined(sequent)
  
  returnStatus =  mmap((caddr_t) shareStart, shareSize,
		       PROT_READ | PROT_WRITE,
		       MAP_SHARED,
		       file, 0);
  assert( returnStatus == 0 );
  
#else
  realshmem = (SMT) mmap(shareStart, shareSize,
			 PROT_READ | PROT_WRITE,
			 MAP_FIXED | MAP_SHARED,
			 file, 0);
  
#ifdef SHARED_MEMORY_OUTPUT
  fprintf(stderr,"realshmem = 0x%x\n", realshmem);
#endif
  
  if ( realshmem != shareStart ) {
    perror("mmap");
    fprintf(stderr, "realshmem = 0x%x\n", realshmem);
    fprintf(stderr, "shareStart = 0x%x\n", shareStart);
    fprintf(stderr, "shareSize = 0x%x\n", shareSize);
    assert( realshmem == shareStart);
  }
  
#endif
  
  __SharedMemoryEnabled__ = 1;
}
