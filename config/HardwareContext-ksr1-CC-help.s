	.text
        .file "HardwareContext.C"
	.globl  	getSp__15HardwareContextFv
	.globl  	getSp__15HardwareContextFv$TXT
	.vstamp 3

        .data
getSp__15HardwareContextFv:	.word	  getSp__15HardwareContextFv$TXT
	.half	  0x0, 0x0, 0x7ffff000, 0x7ffff000

	.text
getSp__15HardwareContextFv$TXT:
   finop				;  cxnop
   finop				;  cxnop
   add8.ntr	4, %i31, %i31		;  movb8_8	%sp, %i0
   finop				;  jmp		32(%c14)
   finop				;  cxnop
   mov8_8	%i0, %i0		;  cxnop



	.file "HardwareContext.C"
	.def 	.debug;	.endef
	.globl	SwitchTo
	.globl	SwitchTo$TXT
	.data
	.half  0x0, 0x0, 0x7ffff000, 0x7fff800

SwitchTo:
        .word	SwitchTo$TXT
	.text	
SwitchTo$TXT:
#if 0
#
# Notice that this implementation assumes there's only *one* possibler
#  caller of the magicSwitchTo routine -- we don't save the PC, and always
#  return to the PC of the caller, albeit with a different
#  SP & FP.
#endif

# parameters: this - %i2,  to -- %i3

        finop		; cxnop; 			# entry prefix
	finop		; cxnop; 			# entry prefix
# save special registers on 'this'
	finop		; movb8_8 %i2,%c6		# 'this'
	finop		; movi8 768,%c5			# stack adjust
        finop		; ssub8.ntr 0, %sp, %c5, %sp	# stack frame multiple of 128
	finop		; movb8_8 %i3,%c7		# 'to' 

        finop 		; st8 %cp, 0(%c6)		# constant pointer
	finop		; st8 %fp, 8(%c6)    		# frame pointer
	finop		; st8 %sp, 16(%c6)		# stack pointer
	finop		; st8 %c14,24(%c6)		# return address


	finop		; mov8_8 %c10, %cp		# our constanst pntr
	finop		; sadd8.ntr 0,%sp,%c5,%fp	# our frame pointer

# save other registers on stack f16-f63 (384 bytes), c15-30 (128 bytes), i12-i30 (152 bytes)
     
        finop		; st8 %i12, 768(%sp)
        finop		; st8 %i13, 760(%sp)
        finop		; st8 %i14, 752(%sp)
        finop		; st8 %i15, 744(%sp)
        finop		; st8 %i16, 736(%sp)
        finop		; st8 %i17, 728(%sp)
        finop		; st8 %i18, 720(%sp)
        finop		; st8 %i19, 712(%sp)
        finop		; st8 %i20, 704(%sp)
        finop		; st8 %i21, 696(%sp)
        finop		; st8 %i22, 688(%sp)
        finop		; st8 %i23, 680(%sp)
        finop		; st8 %i24, 672(%sp)
        finop		; st8 %i25, 664(%sp)
        finop		; st8 %i26, 656(%sp)
        finop		; st8 %i27, 648(%sp)
        finop		; st8 %i28, 640(%sp)
        finop		; st8 %i29, 632(%sp)
        finop		; st8 %i30, 624(%sp)

        finop		; st8 %f16, 616(%sp)
        finop		; st8 %f17, 608(%sp)
        finop		; st8 %f18, 600(%sp)
        finop		; st8 %f19, 592(%sp)
        finop		; st8 %f20, 584(%sp)
        finop		; st8 %f21, 576(%sp)
        finop		; st8 %f22, 568(%sp)
        finop		; st8 %f23, 560(%sp)
        finop		; st8 %f24, 552(%sp)
        finop		; st8 %f25, 544(%sp)
        finop		; st8 %f26, 536(%sp)
        finop		; st8 %f27, 528(%sp)
        finop		; st8 %f28, 520(%sp)
        finop		; st8 %f29, 512(%sp)
        finop		; st8 %f30, 504(%sp)
        finop		; st8 %f31, 496(%sp)
        finop		; st8 %f32, 488(%sp)
        finop		; st8 %f33, 480(%sp)
        finop		; st8 %f34, 472(%sp)
        finop		; st8 %f35, 464(%sp)
        finop		; st8 %f36, 456(%sp)
        finop		; st8 %f37, 448(%sp)
        finop		; st8 %f38, 440(%sp)
        finop		; st8 %f39, 432(%sp)
        finop		; st8 %f40, 424(%sp)
        finop		; st8 %f41, 416(%sp)
        finop		; st8 %f42, 408(%sp)
        finop		; st8 %f43, 400(%sp)
        finop		; st8 %f44, 392(%sp)
        finop		; st8 %f45, 384(%sp)
        finop		; st8 %f46, 376(%sp)
        finop		; st8 %f47, 368(%sp)
        finop		; st8 %f48, 360(%sp)
        finop		; st8 %f49, 352(%sp)
        finop		; st8 %f50, 344(%sp)
        finop		; st8 %f51, 336(%sp)
        finop		; st8 %f52, 328(%sp)
        finop		; st8 %f53, 320(%sp)
        finop		; st8 %f54, 312(%sp)
        finop		; st8 %f55, 304(%sp)
        finop		; st8 %f56, 296(%sp)
        finop		; st8 %f57, 288(%sp)
        finop		; st8 %f58, 280(%sp)
        finop		; st8 %f59, 272(%sp)
        finop		; st8 %f60, 264(%sp)
        finop		; st8 %f61, 256(%sp)
        finop		; st8 %f62, 248(%sp)
        finop		; st8 %f63, 240(%sp)

#       finop		; st8 %c14, 232(%sp)
        finop		; st8 %c15, 224(%sp)
        finop		; st8 %c16, 216(%sp)
        finop		; st8 %c17, 208(%sp)
        finop		; st8 %c18, 200(%sp)
        finop		; st8 %c19, 192(%sp)
        finop		; st8 %c20, 184(%sp)
        finop		; st8 %c21, 176(%sp)
        finop		; st8 %c22, 168(%sp)
        finop		; st8 %c23, 160(%sp)
        finop		; st8 %c24, 152(%sp)
        finop		; st8 %c25, 144(%sp)
        finop		; st8 %c26, 136(%sp)
        finop		; st8 %c27, 128(%sp)
        finop		; st8 %c28, 120(%sp)
        finop		; st8 %c29, 112(%sp)
        finop		; st8 %c30, 104(%sp)


# point of restore

# restore special registers
        finop		; ld8 16(%c7),%sp		# stack pointer
	finop		; ld8 0(%c7), %cp       	# constant pointer
        finop		; ld8 8(%c7), %fp		# frame pointer
  	finop 		; ld8 24(%c7), %c14		# return address

# restore other registers 

        finop		; ld8 768(%sp), %i12
        finop		; ld8 760(%sp), %i13
        finop		; ld8 752(%sp), %i14
        finop		; ld8 744(%sp), %i15
        finop		; ld8 736(%sp), %i16
        finop		; ld8 728(%sp), %i17
        finop		; ld8 720(%sp), %i18
        finop		; ld8 712(%sp), %i19
        finop		; ld8 704(%sp), %i20
        finop		; ld8 696(%sp), %i21
        finop		; ld8 688(%sp), %i22
        finop		; ld8 680(%sp), %i23
        finop		; ld8 672(%sp), %i24
        finop		; ld8 664(%sp), %i23
        finop		; ld8 656(%sp), %i26
        finop		; ld8 648(%sp), %i27
        finop		; ld8 640(%sp), %i28
        finop		; ld8 632(%sp), %i29
        finop		; ld8 624(%sp), %i30

        finop		; ld8 616(%sp), %f16
        finop		; ld8 608(%sp), %f17
        finop		; ld8 600(%sp), %f18
        finop		; ld8 592(%sp), %f19
        finop		; ld8 584(%sp), %f20
        finop		; ld8 576(%sp), %f21
        finop		; ld8 568(%sp), %f22
        finop		; ld8 560(%sp), %f23
        finop		; ld8 552(%sp), %f24
        finop		; ld8 544(%sp), %f25
        finop		; ld8 536(%sp), %f26
        finop		; ld8 528(%sp), %f27
        finop		; ld8 520(%sp), %f28
        finop		; ld8 512(%sp), %f29
        finop		; ld8 504(%sp), %f30
        finop		; ld8 496(%sp), %f31
        finop		; ld8 488(%sp), %f32
        finop		; ld8 480(%sp), %f33
        finop		; ld8 472(%sp), %f34
        finop		; ld8 464(%sp), %f35
        finop		; ld8 456(%sp), %f36
        finop		; ld8 448(%sp), %f37
        finop		; ld8 440(%sp), %f38
        finop		; ld8 432(%sp), %f39
        finop		; ld8 424(%sp), %f40
        finop		; ld8 416(%sp), %f41
        finop		; ld8 408(%sp), %f42
        finop		; ld8 400(%sp), %f43
        finop		; ld8 392(%sp), %f44
        finop		; ld8 384(%sp), %f45
        finop		; ld8 376(%sp), %f46
        finop		; ld8 368(%sp), %f47
        finop		; ld8 360(%sp), %f48
        finop		; ld8 352(%sp), %f49
        finop		; ld8 344(%sp), %f50
        finop		; ld8 336(%sp), %f51
        finop		; ld8 328(%sp), %f52
        finop		; ld8 320(%sp), %f53
        finop		; ld8 312(%sp), %f54
        finop		; ld8 304(%sp), %f55
        finop		; ld8 296(%sp), %f56
        finop		; ld8 288(%sp), %f57
        finop		; ld8 280(%sp), %f58
        finop		; ld8 272(%sp), %f59
        finop		; ld8 264(%sp), %f60
        finop		; ld8 256(%sp), %f61
        finop		; ld8 248(%sp), %f62
        finop		; ld8 240(%sp), %f63

#       finop		; ld8 232(%sp), %c14
        finop		; ld8 224(%sp), %c15
        finop		; ld8 216(%sp), %c16
        finop		; ld8 208(%sp), %c17
        finop		; ld8 200(%sp), %c18
        finop		; ld8 192(%sp), %c19
        finop		; ld8 184(%sp), %c20
        finop		; ld8 176(%sp), %c21
        finop		; ld8 168(%sp), %c22
        finop		; ld8 160(%sp), %c23
        finop		; ld8 152(%sp), %c24
        finop		; ld8 144(%sp), %c25
        finop		; ld8 136(%sp), %c26
        finop		; ld8 128(%sp), %c27
        finop		; ld8 120(%sp), %c28
        finop		; ld8 112(%sp), %c29
        finop		; ld8 104(%sp), %c30

        finop		; mov8_8 %cp, %c10
	finop		; jmp 32(%c14)			# jump back to thread
        finop		; movi8 768, %c5
	finop		; sadd8.ntr 0, %sp, %c5, %sp	# stack adjust

	.vstamp 3
	.file  "HardwareContext.C"
	.globl	__magicReturnFunction
	.globl	__magicReturnFunction$TXT
__magicReturnFunction:
        .data	
	.word __magicReturnFunction$TXT
        .half     0x0, 0x0, 0x7ffff000, 0x7ffff000

	.text
__magicReturnFunction$TXT:
        finop			;  cxnop
        finop			;  cxnop

        finop			;  cxnop
        finop			;  cxnop

	mov8_8 %i12,%i2         ;  movb8_8 %i13, %c6
	finop			;  cxnop
	finop			;  cxnop
	finop			;  ld8 0(%c6),%c7
        finop			;  cxnop
        finop			;  cxnop
	finop	                ;  jsr %c14,16(%c7) # call startOff, never returns
	finop			;  cxnop
	finop			;  cxnop
	movi8 2, %i0		;  movi8 0,%c8	     # nargs brain dmg






















