/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
// AWEsim Context switching code for SPARC. This version is more optimizied
// than a straight-forward approach, but it's a little
//
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "HardwareContextP.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>
#include <unistd.h>
#include <memory.h>

extern "C" {
#include <sun4/asm_linkage.h>
#include <sun4/trap.h>
#include <sys/vadvise.h>
#include </usr/include/sys/mman.h>
extern void *mprotect(void *, int, int);
int malloc_verify();
};

long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;
static long pageSizeMask = 0;

HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackTop = 0;
    stackBottom = 0;
    stackMax = 0;
    stackSize = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackTop should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (unsigned int)
		malloc( stackSize * sizeof(HardwareContextQuad)
		       + pageSizeInBytes
		       + pageSizeInBytes );

	    //
	    // following assumes pages are power-of-two
	    //
	    unsigned int baseAddr = stackMallocAt + pageSizeInBytes + 1;
	    pageAlignedStart = baseAddr & (~pageSizeMask);

	    mprotect((void *) pageAlignedStart, pageSizeInBytes, PROT_READ);

	    stackBottom = (void **) (pageAlignedStart + pageSizeInBytes);
	    stackTop = &stackBottom[stackSize-1];
	    break;

	default:
	    stackMallocAt = (unsigned int) new void *[ stackSize ];
	    stackBottom = (void **) stackMallocAt;
	    stackTop = &stackBottom[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;
	pageSizeMask = pageSizeInBytes - 1;

	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;

	//
	// tell the O/S that we're weird.
	//
	vadvise(VA_ANOM);
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {
	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect((void *) pageAlignedStart, pageSizeInBytes,
		     PROT_READ | PROT_WRITE);
	}
	free((void *) stackMallocAt);
	stackMallocAt = 0;
    }
}


//
//	This is very messy, but I've found that it gives about a 11% or
//	12% speedup over a simpler implementation that uses a non-leaf
//	procedure, for long running programs (where it's important).
//
//	The ta3 does a ST_FLUSH_WINDOWS, saving IN/LOCALS -- we don't
//	need to save/restore them again, but we need to load the new
//	contexts registers
//

asm("	.text");
asm("	.globl _magicSwitchTo__15HardwareContextPT0");
asm("	.proc 1");
asm("_magicSwitchTo__15HardwareContextPT0:");

asm("ta	3");

asm("st	%sp,[%o0+ 0]" );
asm("st	%o7,[%o0+ 4]" );

//
//	load destn %sp to o2
//
asm("ld	[%o1+ 0],%o2" );
asm("ld	[%o1+ 4],%o7" );
asm("ld	[%o1+ 8],%o0" );

//
//	create temp local context for local save area (to avoid smashing
//	save space with active registers)
//
asm("add %o2,-64,%sp" );

asm("ld	[%o2+ 0],%l0" );
asm("ld	[%o2+ 4],%l1" );
asm("ld	[%o2+ 8],%l2" );
asm("ld	[%o2+12],%l3" );
asm("ld	[%o2+16],%l4" );
asm("ld	[%o2+20],%l5" );
asm("ld	[%o2+24],%l6" );
asm("ld	[%o2+28],%l7" );

asm("ld	[%o2+32],%i0" );
asm("ld	[%o2+36],%i1" );
asm("ld	[%o2+40],%i2" );
asm("ld	[%o2+44],%i3" );
asm("ld	[%o2+48],%i4" );
asm("ld	[%o2+52],%i5" );
asm("ld	[%o2+56],%i6" );
asm("ld	[%o2+60],%i7" );
asm("retl");
asm("mov %o2,%sp");

void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //
  
    HardwareContextQuad **stack= (HardwareContextQuad **) stackTop ;
    HardwareContextQuad *In;
    HardwareContextQuad **FP[2];
    HardwareContextQuad **SP[2];

    /////////////// FIRST FRAME
    stackCheck = (HardwareContextQuad *) stack;
    *stackCheck = MagicStackMarker;
    //
    //	save space for top level fcn
    //
    stack  -= 1;
    FP[0] = stack;
    stack -= (MINFRAME/4);
    SP[0] = stack;
    In    = ((HardwareContextQuad *) &SP[0][8]);
    In[7] = 0;
    In[6] = 0;

    //
    //	second frame
    //
    FP[1] = stack;
    stack -= (MINFRAME/4);
    SP[1] = stack;

    In    = ((HardwareContextQuad *) &SP[0][8]);
    In[6] = (HardwareContextQuad) FP[1];	// FP for new task
    sp = (HardwareContextQuad) SP[1];
    pc = ((HardwareContextQuad) returnAddress) - 8;
    out0 = (HardwareContextQuad) returnThis;

#ifdef DEBUG
    cerr << "[HardwareContext] Build stack for " << hex(long(this)) << "\n";
    cerr << "[HardwareContext] FP[0] = " << hex(long(FP[0])) << "\n";
    cerr << "[HardwareContext] SP[0] = " << hex(long(SP[0])) << "\n";
    cerr << "[HardwareContext] FP[1] = " << hex(long(FP[1])) << "\n";
    cerr << "[HardwareContext] SP[1] = " << hex(long(SP[1])) << "\n";
#endif

}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex(long(stackBottom));
    s << " to " << hex(long(stackTop));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
//    s << "[HardwareContext] fp = " << hex(long(INs[6]));
//    s << " sp = " << hex(long(sp));
//    long p = *( (long *) INs[6] );
//    s << " @fp = " << hex(p) << "\n";
}

static
void PrintSingleStackFrame()
{
}

void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackTop - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( depth < 0 || stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << hex(long(getSp()));
	cerr << " and stackTop = " << hex(long(stackTop)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}
