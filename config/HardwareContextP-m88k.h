// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef	HardwareContextP_h
#define	HardwareContextP_h

#ifdef __GNUG__
#  pragma interface
#endif

#include "HardwareContext.h"
#include "CpuMux.h"

inline void **
HardwareContext::mallocAt()
{
    return( stackMallocAt );
}

inline long
HardwareContext::maxStackDepth()
{
    return( long(stackMax) );
}

inline void
HardwareContext::switchContext(HardwareContext *to)
{
    if ( checkStackLimits ) {	// check old context
	checkStack();
    }

    magicSwitchTo(to);
    CpuMux::handoff();

    if ( checkStackLimits ) {	// check new context
	checkStack();
    }
}

#endif	HardwareContextP_h
