/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "HardwareContextP.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>
#include <unistd.h>
#include <memory.h>

extern "C" {
#include <sys/mman.h>
#include <sys/vadvise.h>
extern void *mprotect(void *, int, int);
}

//
//	Many machines have the same (or very similar) stack format.
//	The 68K & 32K are examples of such machines.
//
//	The const *registers* defines the number of additional longwords
//	needed on the stack past the last frame pointer. This value needs
//	to jibe with the one in HardwareContext-<arch>.s.
//

long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;
static long pageSizeMask = 0;

HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackTop = 0;
    stackBottom = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackTop should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (unsigned int)
		malloc( stackSize * sizeof(HardwareContextQuad)
		       + pageSizeInBytes
		       + pageSizeInBytes );

	    //
	    // following assumes pages are power-of-two
	    //
	    unsigned int baseAddr = stackMallocAt + pageSizeInBytes + 1;
	    pageAlignedStart = baseAddr & (~pageSizeMask);

	    mprotect((void *) pageAlignedStart, pageSizeInBytes, PROT_READ);

	    stackBottom = (void **) (pageAlignedStart + pageSizeInBytes);
	    stackTop = &stackBottom[stackSize-1];
	    break;

	default:
	    stackMallocAt = (unsigned int) new void *[ stackSize ];
	    stackBottom = (void **) stackMallocAt;
	    stackTop = &stackBottom[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;
	pageSizeMask = pageSizeInBytes - 1;


	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;

	//
	// tell the O/S that we're weird.
	//
	vadvise(VA_ANOM);
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {

	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect((void *) pageAlignedStart, pageSizeInBytes,
		     PROT_READ | PROT_WRITE);
	}

	free((void *) stackMallocAt);
	stackMallocAt = 0;
    }
}

void
HardwareContext::magicSwitchTo(HardwareContext *to)
{
    asm volatile ("	sw	$16,%0" : : "m" (reg_s[0]));
    asm volatile ("	lw	$16,%0" : "=m" ( to -> reg_s[0]));

    asm volatile ("	sw	$17,%0" : : "m" (reg_s[1]));
    asm volatile ("	lw	$17,%0" : "=m" ( to -> reg_s[1]));

    asm volatile ("	sw	$18,%0" : : "m" (reg_s[2]));
    asm volatile ("	lw	$18,%0" : "=m" ( to -> reg_s[2]));

    asm volatile ("	sw	$19,%0" : : "m" (reg_s[3]));
    asm volatile ("	lw	$19,%0" : "=m" ( to -> reg_s[3]));

    asm volatile ("	sw	$20,%0" : : "m" (reg_s[4]));
    asm volatile ("	lw	$20,%0" : "=m" ( to -> reg_s[4]));

    asm volatile ("	sw	$21,%0" : : "m" (reg_s[5]));
    asm volatile ("	lw	$21,%0" : "=m" ( to -> reg_s[5]));

    asm volatile ("	sw	$22,%0" : : "m" (reg_s[6]));
    asm volatile ("	lw	$22,%0" : "=m" ( to -> reg_s[6]));

    asm volatile ("	sw	$23,%0" : : "m" (reg_s[7]));
    asm volatile ("	lw	$23,%0" : "=m" ( to -> reg_s[7]));
    
    asm volatile ("	sw	$31,%0" : : "m" (reg_ra));
    asm volatile ("	lw	$31,%0" : "=m" ( to -> reg_ra));
    
    asm volatile ("	s.d	$f20,%0" : : "m" (reg_fp[0]));
    asm volatile ("	l.d	$f20,%0" : "=m" ( to -> reg_fp[0]));

    asm volatile ("	s.d	$f22,%0" : : "m" (reg_fp[1]));
    asm volatile ("	l.d	$f22,%0" : "=m" ( to -> reg_fp[1]));

    asm volatile ("	s.d	$f24,%0" : : "m" (reg_fp[2]));
    asm volatile ("	l.d	$f24,%0" : "=m" ( to -> reg_fp[2]));

    asm volatile ("	s.d	$f26,%0" : : "m" (reg_fp[3]));
    asm volatile ("	l.d	$f26,%0" : "=m" ( to -> reg_fp[3]));

    asm volatile ("	s.d	$f28,%0" : : "m" (reg_fp[4]));
    asm volatile ("	l.d	$f28,%0" : "=m" ( to -> reg_fp[4]));

    asm volatile ("	s.d	$f30,%0" : : "m" (reg_fp[5]));
    asm volatile ("	l.d	$f30,%0" : "=m" ( to -> reg_fp[5]));
    
    asm volatile ("	sw	$29,%0" : : "m" (sp));
    asm volatile ("	sw	$30,%0" : : "m" (fp));

    asm volatile ("	lw	$30,%0" : "=m" (to -> fp));
    asm volatile ("	lw	$29,%0" : "=m" (to -> sp));
}

void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackTop - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << hex(long(getSp()));
	cerr << " and stackTop = " << hex(long(stackTop)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}

//
//	Unlike other architectures, the MIPS processor passes arguments
//	in registers. Thus, startOff will assume that *this* is stored
//	in $a0 -- this may not be true, because we're not causing a0,etc
//	to be saved & restored. In general, compilers may save/restore
//	the argument registers in other registers. Hence, this hack.
//
//	We have the new thread return to __magicReturnFunction with four
//	arguments explicitly stored in the stack. We load the first of these
//	two arguments, which is the value of *this* to be passed to the
//	function addressed in the second argument. Clear?
//
extern "C" {
 void __magicReturnFunction();
};

asm("	.text");
asm("	.ent	__magicReturnFunction");
asm("	.globl	__magicReturnFunction");
asm("__magicReturnFunction:");
//asm("	lw	$4,0($30)");
//asm("	lw	$31,4($30)");
asm("move $4,$17"); // put this into $a0
asm("	j	$16");
asm("	.end	__magicReturnFunction");

void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //
    
    stackCheck = (long *) stackTop;
    register HardwareContextQuad **stack;
    stack = (HardwareContextQuad **) stackTop;
    HardwareContextQuad **FP[4];
    HardwareContextQuad **SP[4];

    /////////////// FIRST FRAME
    // space for four args
    FP[0] = stack;
    stack -= 2;
    stackCheck = (long *) stack;
    *(stack--) = (HardwareContextQuad *) MagicStackMarker;
    SP[0] = stack;

    for (int i = 0; i < 10; i++) {
	*(stack--) = 0;
    }

    reg_ra = (HardwareContextQuad) __magicReturnFunction;
    reg_s[1] = (HardwareContextQuad) returnThis;
    reg_s[0] = (HardwareContextQuad) returnAddress;

    sp = (HardwareContextQuad) stack;

#if 0
    cerr << "[HardwareContext-mips] Build stack for "
	<< hex(long(this)) << "\n";
    cerr << "[HardwareContext-mips] sp    = " << hex(long(sp)) << "\n";
    cerr << "[HardwareContext-mips] arg    = " << hex(long(returnThis)) << "\n";
//    cerr << "[HardwareContext-mips] sp[8]   = " << hex(long(stack[8])) << "\n";
#endif
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex(long(stackBottom));
    s << " to " << hex(long(stackTop));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
    s << "[HardwareContext] fp = " << hex(long(fp));
    s << " sp = " << hex(long(sp));
    long p = *( (long *) fp );
    s << " @fp = " << hex(p) << "\n";
}

static
void PrintSingleStackFrame()
{
}
