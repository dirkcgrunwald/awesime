/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef	HardwareContext_h
#define	HardwareContext_h

#ifdef __GNUG__
#  pragma interface
#endif

#include <iostream>
#include <assert.h>
#include <AwesimeConfig.h>

typedef void (*voidFuncP)();

class CpuMux;
class SingleCpuMux;
class MultiCpuMux;
class SimMux;
class SingleSimMux;

typedef unsigned long HardwareContextQuad;

#define HARDWARE_CONTEXT_CHECK_NONE 0
#define HARDWARE_CONTEXT_CHECK_SWITCH 1
#define HARDWARE_CONTEXT_CHECK_MPROTECT 2

class HardwareContext {
    //
    //	The following four fields are machine dependent & their order
    //	should not be changed.
    //
    HardwareContextQuad sp;
    HardwareContextQuad fp;
    HardwareContextQuad gp;
    HardwareContextQuad ret_pc;

    HardwareContextQuad reg_s[8];
    double reg_fp[8];

    void** stackTop;	// bottom of stack
    void** stackBottom;	// maximum depth of stack? (actually, a MIN value)
    unsigned long stackMax;	// maximum depth of stack? (actually, a MIN value)
    unsigned long stackSize;	// stack size in units of void*

    unsigned long stackMallocAt;
    unsigned long pageAlignedStart;

    long *stackCheck;	// point to MagicStackMarker to check for corruption
    unsigned checkStackLimits;
    
    friend class Thread;
    friend class CpuMux;
    friend class SingleCpuMux;
    friend class MultiCpuMux;
    friend class SimMux;
    friend class SingleSimMux;
    
    //
    //	Accessed by friend classes only
    //
    void switchContext(HardwareContext *to);
    void magicSwitchTo(HardwareContext *to);
    void **getSp();
    
    void **mallocAt();
    void buildReturnFrame(void * returnThis, voidFuncP returnAddress);
    void stackOverflow();
    
    //
    // never allocated by anything other than friend classes
    //
    HardwareContext(int checked, unsigned stackSize);
    void reclaimStack();
    
public:
    HardwareContext();
    
    long maxStackDepth();
    void checkStack(int overage = 0);
    void classPrintOn(ostream& strm);
};

inline
HardwareContext::HardwareContext()
{
    int NotReached = 0;
    assert( NotReached );
}

inline ostream&
operator<<(ostream& strm, HardwareContext& ob)
{
    ob.classPrintOn(strm);
    return strm;
}

inline void
HardwareContext::checkStack(int overage)
{
    extern long MagicStackMarker;
    unsigned depth = stackTop - getSp() + overage;
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize || *stackCheck != MagicStackMarker ) {
	stackOverflow();
    }
}

#endif
