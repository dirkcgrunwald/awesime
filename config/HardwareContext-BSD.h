// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef	HardwareContextBSD_h
#define	HardwareContextBSD_h

#ifdef __GNUG__
#  pragma interface
#endif

#include <iostream>
#include <assert.h>
#include <AwesimeConfig.h>

typedef void (*voidFuncP)();

class CpuMux;
class SingleCpuMux;
class MultiCpuMux;
class SimMux;
class SingleSimMux;

typedef long HardwareContextQuad;

#define HARDWARE_CONTEXT_CHECK_NONE 0
#define HARDWARE_CONTEXT_CHECK_SWITCH 1
#define HARDWARE_CONTEXT_CHECK_MPROTECT 2

class HardwareContextBSD {
protected:

    void **mallocAt();
    
    //
    // never allocated by anything other than friend classes
    //
    HardwareContextBSD(int checked, unsigned stackSize);
    void reclaimStack();
    
protected:
    HardwareContextBSD();
    
public:
    long maxStackDepth();
    void checkStack(int overage = 0);
    void classPrintOn(ostream& strm);
};

inline
HardwareContextBSD::HardwareContextBSD()
{
    int NotReached = 0;
    assert( NotReached );
}

inline ostream&
operator<<(ostream& strm, HardwareContextBSD& ob)
{
    ob.classPrintOn(strm);
    return strm;
}

inline long
HardwareContextBSD::maxStackDepth()
{
    return( long(stackMax) );
}

#endif	HardwareContextBSD_h
