/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
// modified by steven@dmav05.webo.dg.com from HardwareContext-generic.h
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "HardwareContext.h"
#include "HardwareContextP.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include <iostream>

//
//	Many machines have the same (or very similar) stack format.
//	The 68K & 32K are examples of such machines.
//
//	The const *registers* defines the number of additional longwords
//	needed on the stack past the last frame pointer. This value needs
//	to jibe with the one in HardwareContext-<arch>.s.
//

const long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

static int expectedBytesSavedInContext = 112;
static int numberOfBytesSavedInContext = 0;
static int numberOfQuadsSavedInContext = 0;

HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackBase = 0;
    stackEnd = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	stackMallocAt = new void *[ stackSize ];
	//
	// stackBase should point to the first writeable cell of the
	// new stack.
	//
	stackEnd = stackMallocAt;
	stackBase = &stackMallocAt[stackSize-1];
	// stackBase must be double-word aligned for 88K
	stackBase = (void**) ((int)stackBase & 0xfffffff8);
    }
    else {
	//
	// Have the main process figure out how many registers
	// (actually, Quads) are pushed when we save an entire context.
	// Do this by calling magicSwitchTo with itself. This has
	// the side-effect of storing fp & sp in the context.
	//
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {
	free(stackMallocAt);
	stackMallocAt = 0;
    }
}

void
HardwareContext::magicSwitchTo(HardwareContext *to)
{
    cerr << "switch from " << hex(long(this));
    cerr << " to " << hex(long(to)) << "\n";
    if ( setjmp(context) == 0 ) {
	longjmp(to -> context,1);
    }
}

void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackBase - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << hex(long(getSp()));
	cerr << " and stackBase = " << hex(long(stackBase)) << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}

void
HardwareContext::buildHiddenReturnFrame()
{
    if (setjmp(context) == 0) {
	cerr << "buildHiddenReturnFrame returns to temp for HC ";
	cerr << hex(long(this)) << "\n";
	cerr << " ..and should eventually jump to thread ...";
	cerr << hex(long(contextOf)) << "\n";
	Thread *who = (Thread *) contextOf;
	cerr << "Thread name is " << who -> name() << "\n";

	longjmp(temporary, 1);
    }
    else {
	cerr << "buildHiddenReturnFrame calls startoff HC ";
	cerr << hex(long(this)) << "\n";
	cerr << " ..and jumps to thread ...";
	cerr << hex(long(contextOf)) << "\n";

	Thread *who = (Thread *) contextOf;
	cerr << "Thread name is " << who -> name() << "\n";

	contextOf -> startOff();
	exit(1);
    }
}

void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    stackCheck = (long *) stackBase;
    *stackCheck = MagicStackMarker;
    contextOf = (Thread *) returnThis;

    cerr << "buildReturnFrame calls setjmp for HC " << hex(long(this));
    cerr << " w/thread " << hex(long(returnThis)) << "\n";

    Thread *who = (Thread *) returnThis;
    cerr << "Thread name is " << who -> name() << "\n";

    if (setjmp(temporary) == 0) {
	asm("or r31,%0,0" : : "r" (stackBase) );
	buildHiddenReturnFrame();
    }
    cerr << "buildReturnFrame exits\n";
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << hex(long(stackEnd));
    s << " to " << hex(long(stackBase));
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
}

static
void PrintSingleStackFrame()
{
}
