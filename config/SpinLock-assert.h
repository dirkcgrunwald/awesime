
#define SpinLockFree '\000'
typedef unsigned char SpinLockType;
typedef volatile unsigned char VolatileSpinLockType;


/*
 *	Machines that don't actually use shared memory and spinlocks
 *	just check the current state to make certain things wouldn't
 *	deadlock on a true shared memory machine.
 */

static inline void SpinLockRelease(VolatileSpinLockType *lock)
{
  *lock = SpinLockFree;
}

static inline void SpinLockReserve(VolatileSpinLockType *lock)
{
  assert(*lock == SpinLockFree);
  *lock = ~SpinLockFree;
}

static inline int SpinLockReserveNoBlock(VolatileSpinLockType *lock)
{
  assert(*lock == SpinLockFree);
  *lock = ~SpinLockFree;
  return(1);
}
