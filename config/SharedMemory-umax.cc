/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "AwesimeConfig.h"

#ifdef _USE_SHARED_MEMORY_SHARE_

#ifdef __GNUG__
#  pragma implementation
#endif

#include "SharedMemory.h"
#include "assert.h"


#ifdef __GNUG__
extern "C" {
#endif

void *sbrk(int);
void exit(int x = 0);
void *malloc( unsigned );
void free(void *);
void perror(const char*);

char *share(unsigned, unsigned);

#ifdef __GNUG__
};
#endif

//
//	Not really a part of CpuMux, but used by it & hidden here.
//
void
SharedMemoryInit( unsigned extra )
{
    //
    // Bump the break if necessary.
    // 
    assert(extra >= 0);
    void * newBreak = sbrk( extra );
    if ( newBreak == (char *) -1 ) {
	perror( "Share: sbrk" );
	exit( 1 );
    }
    newBreak = sbrk( 0 );
    
    //
    // Share .data, .bss, and the extra.
    // 
    extern int data_start;
    unsigned shareStart = (unsigned) &data_start;
    unsigned shareSize = (unsigned) newBreak - shareStart;
    char * sharePointer = share( shareStart, shareSize );
    if ( sharePointer == 0 ) {
	perror( "Share: share" );
	exit( 1 );
    }

    extern void disableFurtherBreaks();
    disableFurtherBreaks();
}

#endif
