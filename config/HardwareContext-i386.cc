// Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
// 
// This file is part of Awesime.
// 
// Awesime is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY.  No author or distributor accepts responsibility to
// anyone for the consequences of using it or for whether it serves any
// particular purpose or works at all, unless he says so in writing.
// Refer to the GNU General Public License for full details.
// 
// Everyone is granted permission to copy, modify and redistribute
// Awesime, but only under the conditions described in the Gnu General
// Public License.  A copy of this license is supposed to have been given
// to you along with Awesime so you can know your rights and
// responsibilities.  It should be in a file named COPYING.  Among other
// things, the copyright notice and this notice must be preserved on all
// copies.
// 
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//


#include "HardwareContext.h"
#include "CpuMux.h"
#include "Thread.h"
#include "assert.h"
#include "AwesimeConfig.h"
#include <unistd.h>

#if defined(sun) || defined(sunos)
# define MPROTECT
#endif

const long MagicStackMarker = 0x464f4f20;	// this says 'FOO '

const bool DebugHardware = false;


extern "C" {
#ifndef __linux
#  include <sys/vadvise.h>
#endif
#include <sys/mman.h>
}

static long pageSizeInBytes = 0;
static long pageSizeInQuads = 0;

HardwareContext::HardwareContext (int check, unsigned size)
{
    checkStackLimits = check;

    stackBase = 0;
    stackEnd = 0;
    stackMax = 0;
    stackSize = 0;
    stackCheck = 0;
    stackMallocAt = 0;

    if (size > 0) {
	stackSize = size;
	//
	// stackBase should point to the first writeable cell of the
	// new stack.
	//
	switch ( check ) { 

	case HARDWARE_CONTEXT_CHECK_NONE:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = stackMallocAt;
	    stackBase = &stackMallocAt[stackSize-1];
	    break;

#ifdef MPROTECT

	case HARDWARE_CONTEXT_CHECK_MPROTECT:
	    stackMallocAt = (void *) valloc( (stackSize<<2) + pageSizeInBytes);

	    stackEnd = &stackMallocAt[pageSizeInQuads];
	    stackBase = &stackMallocAt[pageSizeInQuads + stackSize-1];

	    //
	    // now disable access to lower page
	    //

	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ);
	    break;
#endif

	default:
	    stackMallocAt = new void *[ stackSize ];
	    stackEnd = &stackMallocAt[0];
	    stackBase = &stackMallocAt[stackSize-1];
	    break;
	}

    }
    else {
	//
	//	executed by toplevel UNIX processes
	//

	pageSizeInBytes = getpagesize();
	pageSizeInQuads = pageSizeInBytes >> 2;

	//
	// to make stack checks work
	//

	stackMallocAt = 0;
	stackSize = 0xffffffff;
    }
}

void
HardwareContext::reclaimStack()
{
    if ( stackMallocAt ) {

#ifdef MPROTECT
	//
	//	restore normal page protection so that free() doesn't choke
	//
	if ( checkStackLimits == HARDWARE_CONTEXT_CHECK_MPROTECT ) {
	    mprotect(stackMallocAt, pageSizeInBytes, PROT_READ | PROT_WRITE);
	}
#endif
	free(stackMallocAt);
	stackMallocAt = 0;
    }
}


extern "C" void __morePortableSwitch(HardwareContext *, HardwareContext *);

void
HardwareContext::magicSwitchTo(HardwareContext *to)
{
  __morePortableSwitch(this, to);
}

///
// Do this to avoid having the frame pointer placed in SOMETIMES (-O -g?)
//

    asm 
       ("\n\
	.text\n\
	.globl __morePortableSwitch\n\
__morePortableSwitch:\n\
	 push   %ebx\n\
	 push   %esi\n\
	 push   %edi\n\
	 movl	16(%esp),%eax\n\
	 movl	20(%esp),%edx\n\
	 movl	%esp,0(%eax)\n\
	 movl	%ebp,4(%eax)\n\
	 movl	0(%edx),%esp\n\
	 movl	4(%edx),%ebp\n\
	 pop   %edi\n\
	 pop   %esi\n\
	 pop   %ebx\n\
	ret\n\
");


void
HardwareContext::stackOverflow()
{
    register unsigned depth = stackBase - getSp();
    if (stackMax < depth) {
	stackMax = depth;
    }
    if ( stackMax >= stackSize ) {
	cerr << "\nStack overflow\n";
	cerr << " getSp() = " << getSp() ;
	cerr << " and stackBase = " << stackBase << "\n";
	cerr << *this << "\n";
	cerr << "Current task is \n";
	cerr << *(CpuMux::CurrentThread());
	cerr << "\n";
	cerr.flush();
    }
    assert( stackMax < stackSize );
    assert( *stackCheck == MagicStackMarker );
}


void
HardwareContext::buildReturnFrame(void *returnThis, voidFuncP returnAddress)
{
    //
    //	To build a thread, we return to the first address of startOff,
    //	which will use the current FP & SP to build a local context,
    //	and then call the user main.
    //
    //	startOff needs to have a valid frame. The return address for this
    //	frame is NULL, since we never exit procedure startOff.
    //

  if ( DebugHardware ) {
    cout << "build frame for retaddr " << hex << returnAddress;
    cout << " in object " << returnThis;
    cout << " at " << stackBase << endl;
  }

    stackCheck = (long *) stackBase;
    HardwareContextQuad **stack = (HardwareContextQuad **) stackBase;
    *(stack--) = (HardwareContextQuad *) MagicStackMarker;
    *(stack--) = (HardwareContextQuad*) returnThis;
    *(stack--) = (HardwareContextQuad*) 0; // bogus return address for startOff
    *(stack--) = (HardwareContextQuad*) returnAddress;
    //    *(stack--) = (HardwareContextQuad*) 'ebi ';
    *(stack--) = (HardwareContextQuad*) 'edi ';
    *(stack--) = (HardwareContextQuad*) 'esi ';
    *(stack--) = (HardwareContextQuad*) 'ebx '; 
    stack++; // leave SP pointing at ebx
    sp = (HardwareContextQuad) stack;

  if ( DebugHardware ) {
      cout << "Stack frame is " << endl;
      for(int i = 0; i < 10; i++) {
	cout << "[ sp + " << i << "] = " << hex << (unsigned long) (stack+i) << " ";
	cout << hex << stack[i] << endl;
      }
    }
}

void
HardwareContext::classPrintOn(ostream& s)
{
    s << "[HardwareContext] Stack spans " << stackEnd;
    s << " to " << stackBase;
    s << " used is  " << (stackMax) << " of " << stackSize << "\n";
    s << "[HardwareContext] fp = " << fp;
    s << " sp = " << sp;
    long p = *( (long *) fp );
    s << " @fp = " << p << "\n";
}

void
HardwareContext::switchContext(HardwareContext *to)
{
    if ( checkStackLimits ) {	// check old context
	checkStack();
    }

    magicSwitchTo(to);
    CpuMux::handoff();

    if ( checkStackLimits ) {	// check new context
	checkStack();
    }
}
