// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//
#ifndef CpuMux_h
#define CpuMux_h
#include <HardwareContext.h>
#include <CpuMuxExceptions.h>
#include <SpinLock.h>
#include "iolock.h"
#include "ThreadPackage.h"
//
//	Define the abstractions for per POSIX thread resources.
//


extern "C" {
    extern int read_iYam();
}

class Thread;
class CpuMux;


class CpuMux {
public:
  static CpuMux* ThisCpus[_MAXIMUM_CPU_MUXS_];
  static int  Muxs;
  static int Debug;
  static SpinLock MuxsLock;
  
protected:
friend class ReserveByException;
  
  Thread *currentThread;
  HardwareContext systemContext;
  
  void systemTransfer(Thread *);
  static void threadTransfer(Thread *, Thread *);
  
  int iYam;	// I yam what I yam
  long pid;	// ...except to POSIX
  
  char nameSpace[128];
  char *pNameTemplate;
  char *pName;
  
  int *terminated;
  int enabled;
  
  //
  // Remove doesn't always return a thread in general,
  // but this remove will always return either NULL or a thread.
  //
  virtual Thread *remove() = 0;
  
  //
  //	Exception handlers.
  //
  
friend class ExceptionClass;
  
  ExceptionClass *raisedBy;
  ExceptionReserve reserveException;
  ExceptionTerminate terminateException;
  ExceptionReschedule rescheduleException;
  ExceptionIveSuspended iveSuspendedException;
  
  
  //
  //	Public interfaces for exceptions -- this are non virtual and
  //  are inline defined in CpuMuxP.h
  //
protected:
  
  virtual void reschedule_(Thread* to, CpuMux* ThisCpu);
  
  virtual void add_(Thread *) = 0;
  
  virtual void addToCpu_(int , Thread *) = 0;
  
  virtual void terminateAll_();
  
  //
  // The following are instaniated by subclasses for classes that
  // add many threads to the CPU, and would prefer to do so without
  // additional locking overhead. They default to acting like ``add''
  //
  virtual void addReserve_() = 0;
  virtual void addUnlocked_(Thread*) = 0;
  virtual void addRelease_() = 0;
  
  CpuMux(int debug = 0);
  virtual ~CpuMux();
  
public:
  virtual void fireItUp(int Cpus = 1, unsigned long shared = (4196 * 500)) = 0;
  static void AddToCpu(int , Thread *);
  static CpuMux *Cpu();
  static Thread *CurrentThread();
  static void add(Thread *);
  static void addReserve();
  static void addUnlocked(Thread*);
  static void addRelease();
  static int cpuId();
  static char *name();
  static void debug(int newdebug);
  static int debug();
  static void raise(ExceptionClass *);
  static void reserveByException( ReserveByException * );
  static void threadTerminateException( Thread* );
  static void reschedule(Thread *to = 0);
  static void terminateAll();
  static void handoff();
};

inline CpuMux *
CpuMux::Cpu()
{
    return(ThisCpus[read_iYam()]);
}
inline int
CpuMux::cpuId()
{
    return(read_iYam());
}

inline char *
CpuMux::name()
{
    return(ThisCpus[read_iYam()] -> pName);
}

inline Thread *
CpuMux::CurrentThread()
{
    return( ThisCpus[read_iYam()] -> currentThread );
}

inline void
CpuMux::AddToCpu(int cpu, Thread *t)
{
    ThisCpus[read_iYam()]->addToCpu_(cpu,t);
}

inline void
CpuMux::add(Thread *t)
{
    ThisCpus[read_iYam()] -> add_(t);
}

inline void
CpuMux::addReserve()
{
    ThisCpus[read_iYam()] -> addReserve_();
}

inline void
CpuMux::addUnlocked(Thread *t)
{
    ThisCpus[read_iYam()] -> addUnlocked_(t);
}

inline void
CpuMux::addRelease()
{
    ThisCpus[read_iYam()] -> addRelease_();
}

inline void
CpuMux::reschedule(Thread *to)
{
    CpuMux* ThisCpu = ThisCpus[read_iYam()];
    ThisCpu -> reschedule_(to,ThisCpu);
}

inline void
CpuMux::terminateAll()
{
    ThisCpus[read_iYam()] -> terminateAll_();
}

inline void
CpuMux::handoff()
{
    if ( ThisCpus[read_iYam()] -> raisedBy != NULL ) {
	ThisCpus[read_iYam()] -> raisedBy -> handleException();
	ThisCpus[read_iYam()] -> raisedBy = NULL;
    }
}
#endif /* CpuMux_h */
