/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef MultiCpuMuxExceptions_h
#define MultiCpuMuxExceptions_h

#include <ExceptionClass.h>

//
//	ExceptionRelocate -- Can also be
//	used to move to another CPU (i.e. previous to killing a cpu or
//	spawning a CPU)
//

class ExceptionRelocate : public ExceptionClass {
    int newCpu;
public:
    ExceptionRelocate();
    void cpu(int = -1);
    virtual void handleException();
};

inline
ExceptionRelocate::ExceptionRelocate()
{
}

inline void
ExceptionRelocate::cpu(int c)
{
    newCpu = c;
}

//
// ExceptionEnrollDismissCpu -- enroll or dismiss a new cpu.
//
class ExceptionEnrollDismissCpu : public ExceptionClass {
    int enrollOrDismiss;
public:
    void enroll();
    void dismiss();
    virtual void handleException();
};

inline void
ExceptionEnrollDismissCpu::enroll()
{
    enrollOrDismiss = 1;
}

inline void
ExceptionEnrollDismissCpu::dismiss()
{
    enrollOrDismiss = 0;
}

#endif /* MultiCpuMuxExceptions_h */
