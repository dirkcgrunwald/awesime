/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifndef TournamentBarrier_h
#define TournamentBarrier_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	TournamentBarrier.h
//	The barrier is initially closed.
// 
#include <CpuMuxP.h>
#include <ReserveByException.h>
#include <SpinLock.h>
#include <FifoScheduler.h>
#include "Barrier.h"


typedef unsigned char role_enum;
#define LOSER ((role_enum) 1)
#define WINNER ((role_enum) 2)
#define CHAMPION ((role_enum) 4)
#define NOOP ((role_enum) 8)
#define DROPOUT ((role_enum) 16)


typedef struct {
	volatile int *opponent;
	role_enum role;
	volatile int flag;
} round_t;

typedef struct THREAD_PILE {
    FifoScheduler *pile;
    SpinLock      lck;
    int           expected_count;
    int           count;
    int           initCount;
} PILE_REC;


class TournamentBarrier : public ReserveByException {

    int pHeight;
    int numThreads;
    int initFlag;
    int championSense;

 protected:
    round_t     *rounds[_MAXIMUM_CPU_MUXS_];
    PILE_REC    ThreadPiles[_MAXIMUM_CPU_MUXS_];
    int        mySense[_MAXIMUM_CPU_MUXS_];
    
    int numCpus;
    int log_numCpus;
    int log_maximumCpus;
    SpinLock lck;
    void releaseAllOfMine(int iYam);
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);

  public: 
    virtual ~TournamentBarrier();
    TournamentBarrier(); 
    void rendezvous();
    void initBarrier(int height);
    void checkIn();
    void checkOut();

};
#endif /* TournamentBarrier_h */

















