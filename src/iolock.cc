#include "iolock.h"
#include "SpinLock.h"
#include "assert.h"

//
// Crude I/O locking. We keep a list of ios pointers, associate a
// semphore with each one and do the appropriate things.
//

class IosLock;

class IosLock {
public:
  SpinLock lock;
  ios *ptr;
  IosLock *next;

  IosLock(ios& p);
};

inline IosLock::IosLock(ios& p)
{
  ptr = &p;
  next = 0;
}

static IosLock *root = 0;

static IosLock *
lookup(ios& s) 
{
  ios *p = &s;
  IosLock* here = root;
  IosLock* follow = 0;

  while ( here ) {
    if ( here -> ptr == p ) {
      //
      // move to front, return pointer
      //
      if ( follow != 0 ) {
	//
	// unlink here
	//
	IosLock *tmp = here -> next;
	follow -> next = tmp;
	//
	// link here
	//
	here -> next = root;
	root = here;
      }
      return( here );
    }
    follow = here;
    here = here -> next;
  }
  IosLock* n = new IosLock(s);
  n -> next = root;
  root = n;
  return( n );
}

#if GNU_C_BROKEN
ios& lock(ios& os) 
{
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.reserve();
  return( os );
}


ios& unlock(ios& os)
{
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.release();
  return( os );
}
#endif


ostream& lock(ostream& s) 
{
  ios& os = s;
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.reserve();
  return( s );
}


ostream& unlock(ostream& s)
{
  ios& os = s;
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.release();
  return( s );
}


istream& lock(istream& s) 
{
  ios& os = s;
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.reserve();
  return( s );
}


istream& unlock(istream& s)
{
  ios& os = s;
  IosLock* p = lookup(os);
  assert(p != 0);
  p -> lock.release();
  return( s );
}

