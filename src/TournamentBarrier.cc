/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "AwesimeConfig.h"
#include "TournamentBarrier.h"
#include "Thread.h"
#include "FifoScheduler.h"
#include "AwesimeConfig.h"
#include <math.h>
#include <iolock.h>
#include <assert.h>

TournamentBarrier::TournamentBarrier()
{
    pHeight = 0;
    initFlag = 0;
    championSense = 0;
    for (int k = 0; k < _MAXIMUM_CPU_MUXS_; k++) {
          rounds[k] = (round_t *)malloc(sizeof(round_t)*log_maximumCpus);
	  ThreadPiles[k].pile = NULL;
    }


}

TournamentBarrier::~TournamentBarrier()
{
  for (int i=0; i < _MAXIMUM_CPU_MUXS_; i++) {
       delete ThreadPiles[i].pile;
  }
}

void TournamentBarrier::initBarrier(int h ) {    
    int k;

    lck.reserve();
    pHeight = h;
    numCpus = CpuMux::Muxs;

    for (k = 1,log_maximumCpus = 0; k < _MAXIMUM_CPU_MUXS_; log_maximumCpus++,k <<= 1);

    for (k = 1, log_numCpus= 0; k < numCpus; log_numCpus++, k <<=1);
    
    numThreads = 0;

    for (k=0; k < numCpus; k++) {
       if (ThreadPiles[k].pile == NULL)
          ThreadPiles[k].pile = new FifoScheduler;
       ThreadPiles[k].expected_count = 0;
       ThreadPiles[k].count = 0;
       ThreadPiles[k].initCount = 0;
    }

    for (k=0; k < pHeight; k++) 
        ThreadPiles[k%numCpus].expected_count++;

    initFlag = 1;

#if defined(__ksr__)
    pstsp((VolatileSpinLockType *)&initFlag);
#endif

    lck.release();
}

void TournamentBarrier::checkOut() {
    Thread *thread = CpuMux::CurrentThread();
    thread->affinity(-1);
}

void TournamentBarrier::checkIn() {

    while (initFlag == 0);

    int myHeightReached = 0;

    int iYam = CpuMux::cpuId();
    Thread *thread = CpuMux::CurrentThread();

    int oldiYam = iYam;
    int found = 0;

    while (!found) {
        if (ThreadPiles[iYam].initCount  < ThreadPiles[iYam].expected_count ) {
	    ThreadPiles[iYam].lck.reserve();
	    if (ThreadPiles[iYam].initCount < ThreadPiles[iYam].expected_count) 
	       if (++ThreadPiles[iYam].initCount == ThreadPiles[iYam].expected_count)
  	           myHeightReached = 1;
	    ThreadPiles[iYam].lck.release();
	    found = 1;
	}
	else 
  	   iYam = ++iYam%numCpus;
    }

    thread->affinity(iYam);

    if (!myHeightReached) {
	CpuMux::reschedule();
	return;
    }

// PER PROCESS WORK

    if (oldiYam !=iYam) 
	CpuMux::reschedule();

    int opponent;

    if (numCpus > 1 ) {
       register round_t *myRounds = &rounds[iYam][0];

       for (int k = 0; k < log_maximumCpus; k++) 
	   myRounds[k].role = DROPOUT;

       mySense[iYam]= 1;

       int k;
       for (k = 0; k < log_numCpus; k++) {
           int iYamMod = iYam % (1 << (k+1));
	   if (iYamMod == 0) {
	       opponent = iYam + (1 << k);
	       if (opponent < numCpus) 
		   myRounds[k].role = WINNER;
	       else 
		   myRounds[k].role = NOOP;
               myRounds[k].opponent = (int *) NULL;
           }
	   else if (iYamMod == (1 << k)) {
	        opponent = iYam - (1<<k);
	        myRounds[k].role = LOSER;
	        myRounds[k].opponent = (&rounds[opponent][k].flag);
	        break;
           }
	 
       }

       if (iYam == 0)
	   myRounds[k-1].role = CHAMPION;

       for (k=0; k < log_maximumCpus; k++)
	   myRounds[k].flag = 0;
	
    }
 
    lck.reserve();
    numThreads = numThreads + ThreadPiles[iYam].initCount;
    lck.release();
    while (numThreads != pHeight);

}

void TournamentBarrier::rendezvous()
{    

   register int iYam = CpuMux::cpuId();

   register PILE_REC* myPile = &ThreadPiles[iYam];

   if (++myPile->count == myPile->expected_count) {
       if (numCpus > 1) {
          register round_t *myRound = &rounds[iYam][0];
          register int *sense = &mySense[iYam];

          for (;;) {
	      if (myRound->role & LOSER) {
	          *(myRound->opponent) = *sense;
	          while (championSense != *sense);
	          break;
	      }
	      else if (myRound->role & WINNER)
		  while (myRound->flag != *sense);
	      else if (myRound->role & CHAMPION) {
		      while (myRound->flag != *sense);
		      // release
		      championSense = *sense;
#if defined(__ksr__)
		      pstsp((VolatileSpinLockType *)&championSense);
#endif
		      break;
	      }
	      myRound++;
	  }       
	  *sense = (int)(!(*sense));
       }

       if (myPile->count > 1){
	   myPile->count = 0;
           releaseAllOfMine(iYam);
       }
       else 
	   myPile->count = 0;

   }
   else {
       CpuMux::Cpu()->reserveByException(this);
   }

}

int
TournamentBarrier::reserveByException(Thread *byWho, ExceptionReserve&)
{
    ThreadPiles[CpuMux::cpuId()].pile -> add( byWho );
    return( 1 );
}
 
void
TournamentBarrier::releaseAllOfMine(int iYam)
{

    if ( ThreadPiles[iYam].pile != 0 ) {   
	while ( ! ThreadPiles[iYam].pile -> isEmpty() ) {
	    Thread *p = ThreadPiles[iYam].pile -> remove(); 
	    CpuMux::AddToCpu(iYam,p);
	}
    }
}










