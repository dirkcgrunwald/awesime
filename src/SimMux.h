// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//
#ifndef SimMux_h
#define SimMux_h

#include <CpuMux.h>
#include <HardwareContext.h>
#include "TimeUnit.h"

class OEv;


class SimMux : public CpuMux {
  
protected:
  static SimTimeUnit CurrentSimulatedTime;
  static const SimTimeUnit NullSimulatedTime;
  static SimMux *ThisSimMux;
  
protected:
  virtual void addAt_(Thread *, SimTimeUnit) =0;
  virtual void addWithDelay_(Thread *, SimTimeUnit);
  virtual void addAt_(OEv *, SimTimeUnit) =0;
  virtual void addWithDelay_(OEv *, SimTimeUnit);
  virtual void hold_( SimTimeUnit time );
  virtual void await_( SimTimeUnit time );
  
public:
  SimMux(int debug = 0);
  ~SimMux();
  
  virtual void fireItUp(int Cpus = 1, unsigned long shared = (4196 * 500)) = 0;
  
  static SimMux *Sim();
  static SimTimeUnit CurrentTime();
  static SimTimeUnit NullTime();
  
  static void addAt(Thread *, SimTimeUnit);
  static void addWithDelay(Thread *, SimTimeUnit);
  static void addAt(OEv *, SimTimeUnit);
  static void addWithDelay(OEv *, SimTimeUnit);
  static void hold( SimTimeUnit time );
  static void await( SimTimeUnit time );
  
};


inline SimMux*
SimMux::Sim()
{
    return( ThisSimMux );
}

inline SimTimeUnit 
SimMux::CurrentTime()
{
    return( CurrentSimulatedTime );
}

inline SimTimeUnit 
SimMux::NullTime()
{
    return( NullSimulatedTime );
}

inline void
SimMux::hold(SimTimeUnit holdFor)
{
    ThisSimMux -> await_( CurrentSimulatedTime + holdFor );
}

inline void 
SimMux::await(SimTimeUnit till)
{
    if ( till > CurrentSimulatedTime ) {
	ThisSimMux -> await_(till);
    }
}

inline void
SimMux::addAt(Thread *t, SimTimeUnit holdFor)
{
    ThisSimMux -> addAt_(t, holdFor);
}

inline void
SimMux::addWithDelay(Thread *t, SimTimeUnit holdFor)
{
    ThisSimMux -> addWithDelay_(t, holdFor);
}

inline void
SimMux::addAt(OEv *o, SimTimeUnit holdFor)
{
    ThisSimMux -> addAt_(o, holdFor);
}

inline void
SimMux::addWithDelay(OEv *o, SimTimeUnit holdFor)
{
    ThisSimMux -> addWithDelay_(o, holdFor);
}

#endif /* SimMux_h */
