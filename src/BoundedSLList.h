//-*-c++-*-
#ifndef _BoundedSLList_h_
#define _BoundedSLList_h_

#include "LowerSLList.h"

template <class T>
class BoundedSLList : public LowerSLList<T>
{
  protected:
    FifoScheduler upperBoundLockFifo;
    Semaphore upperBoundLock;

    void lock() {
	if ( ! iterating ) _lock.reserve();
    }

    void unlock() {
	if ( ! iterating ) _lock.release();
    }

  public:
    //
    // The constructors are unlocked because the user shouldn't
    // be able to call methods until after the constructor
    // has finished
    //
    BoundedSLList(int lth) : LowerSLList<T>(),
    upperBoundLock(lth, &upperBoundLockFifo),
    upperBoundLockFifo(2) {
	iterating = 0;
    }

    BoundedSLList(int lth, const SLList<T>& a) : LowerSLList<T>(),
    upperBoundLock(lth, &upperBoundLockFifo),
    upperBoundLockFifo(2) {
	    copy(a);
	    iterating = 0;
	}


    Pix prepend(T& item) {
	upperBoundLock.reserve();
	lock();
	Pix ret = BaseSLList::prepend(&item);
	unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix append(T& item) {
	upperBoundLock.reserve();
	lock();
	Pix ret = BaseSLList::append(&item);
	unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix prepend(SLNode<T>* node) {
	upperBoundLock.reserve();
	lock();
	Pix ret = BaseSLList::prepend(node);
	unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix append(SLNode<T>* node) {
	upperBoundLock.reserve();
	lock();
	Pix ret = BaseSLList::append(node);
	unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix ins_after(Pix p, T& item) {
	upperBoundLock.reserve();
	lock();
	Pix x = BaseSLList::ins_after(p, &item);
	unlock();
	lowerBoundLock.release();
	return x;
    }

    int remove_front(T& x) {
	lowerBoundLock.reserve();
	lock();
	int y = BaseSLList::remove_front(&x);
	unlock();
	upperBoundLock.release();
	return y;
    }

    T remove_front() {
	lowerBoundLock.reserve();
	lock();
	T dst; BaseSLList::remove_front(&dst, 1);
	unlock();
	upperBoundLock.release();
	return dst;
    }
};


#endif
