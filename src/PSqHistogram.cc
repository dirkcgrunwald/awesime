#include "AwesimeConfig.h"
#include <stdlib.h>
#include "PSqHistogram.h"
using namespace std;

//
// Quantile-Histogram Class
//

void PSqHistogram::reset(unsigned cellCount) {
    nc = cellCount;
    ns = 0;
    delete[] table;
    table = new Point[cellCount+1];
}

//
// Constructor
//
PSqHistogram::PSqHistogram(unsigned cellCount) {
    table = (Point *) 0;
    reset(cellCount);
}

PSqHistogram::PSqHistogram(unsigned _ns, unsigned _nc, Point *_table)
{
    nc = _nc;
    ns = _ns;
    table = new Point[nc+1];
    //
    // Although the user asks for NC buckets, we use NC+1 internally.
    //
    for (unsigned int i = 0; i <= nc; i++ ) {
	table[i] = _table[i];
    }
}

//
// We must have a copy constructor because of the pointer *table
//

PSqHistogram::PSqHistogram(PSqHistogram &src)
{
    register Point *dstp, *srcp = src.table; 
    register Point *endp = srcp + src.nc + 1;
    
    ns    = src.ns;
    nc    = src.nc;
    dstp  = new Point[nc+1];
    table = dstp;
    
    do {
	*dstp++ = *srcp++;
    } while (srcp != endp);
}

//
// Destructor
//
PSqHistogram::~PSqHistogram() {
    delete[] table;
}

//
// Reset the sample and clear the statistics
//
PSqHistogram &PSqHistogram::operator=(unsigned cellCount) {
    reset(cellCount);
    return *this;
}

void
PSqHistogram::invariant()
{
    int top = max();
    for ( int i = 0; i < top; i++ ) { 
	assert( table[i].n < table[i+1].n );
	assert( table[i].q <= table[i+1].q );
    }
}


//
// Add x to sample
//
PSqHistogram &PSqHistogram::operator+=(register double x)
{
    unsigned 	i;
    int 	d;
    double   	q;
    Point  *ip = table, *jp, *kp;
    
    if (ns <= nc) {
	//
	// Part A: insert x in increasing order
	//
	jp = ip + ns;
	jp->n = ns;
	--jp;
	if (ns != 0) {
	    do {
		double q = jp->q;
		if (x >= q) break;
		jp[1].q = q;
	    } while (--jp >= ip);
	}
	jp++;
	jp->q = x;
    } else {
	//
	// Part B: insert sample
	//
	jp = ip + nc;
	do {
	    if (x >= jp->q) break;
	    //
	    // increment marker positions k through b
	    //
	    jp->n++;
	} while (--jp >= ip);
	
	if (jp == ip - 1) { 
	    jp++;
	    //
	    // adjust miminum
	    //
	    jp->q = x;
	} else if (jp == ip + nc) {
	    jp->n++;
	    //
	    // adjust maximum
	    //
	    jp->q = x;
	}
	
	jp = ip+1;
	kp = jp+1;
	for (i = 1; i < nc; i++) {
	    //
	    // Adjust heights of markers if necessary
	    //
	    q = (double) i * (double) ns / (double) nc - (double) jp->n;
	    
	    if (q >= 1.0 && kp->n - jp->n > 1
		|| q <= -1.0 && ip->n - jp->n < -1) {
		if (q < 0) d = -1; else d = 1;
		q = jp->q + (double) d / (double) (kp->n - ip->n) 
		    * ( (double) (jp->n - ip->n + d) 
			* (kp->q - jp->q) / (double) (kp->n - jp->n)
			+ (double) (kp->n - jp->n - d) 
			* (jp->q - ip->q) / (double) (jp->n - ip->n)
		    );
		if (ip->q < q && q < kp->q) {
		    //
		    // use parabolic formula
		    //
		    jp->q = q;
		} else {
		    //
		    // use linear formula
		    //
		    if (d < 0) {
			double upper = (ip -> q - jp -> q);
			double lower = (ip->n - jp->n);
			jp->q -= (upper/lower);
		    } else {
			double upper = (kp -> q - jp -> q);
			double lower = (kp -> n - jp -> n);
			jp->q += (upper/lower);
		    }
		}
		jp->n += d;
	    }
	    ip++;
	    jp++;
	    kp++;
	}
    }
    ns++;

    return *this;
}

ostream &operator<<(ostream &s, PSqHistogram &q)
{
    
    s << "[" << q.buckets() << "](" << q.samples() << ") = [";
    
    int max = q.max();
    
    for (int i = 0; i <= max; i++) {
	const PSqHistogram::Point& p = q.point(i);
	s << " " << p.n << ":" << p.q;
    }
    s << "]";
    return s;
}
