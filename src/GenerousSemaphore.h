#ifndef _GenerousSemaphore_h_
#define _GenerousSemaphore_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include "Semaphore.h"

class GenerousSemaphore : public Semaphore {

public :
    GenerousSemaphore(int count = 1, ThreadContainer *scheduler = 0);
    virtual void release();
};

#endif
