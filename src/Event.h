/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef Event_h
#define Event_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Event.h
//	The barrier is initially closed.
// 

#include <ReserveByException.h>
#include <FifoScheduler.h>
#include <SpinLock.h>

class Event : public ReserveByException {
    SpinLock lock;
    int garcon;
    int generation;
    int pLoops;

    FifoScheduler pPileOfThreads;
    
    void releaseAll();
    
private:
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);
    
public:
    
    Event(int loops = 0);
    virtual ~Event();

    virtual int size();

    void waitFor();
    void trigger();
};

inline
Event::Event(int l)
{
    garcon = 0;
    generation = 0;
    pLoops = l;
}

#endif /* Event_h */
