//-*-c++-*-
// This may look like C code, but it is really -*- C++ -*-
//
//	Copyright (C) 1988,1989,1990,1991,1992
//	Dirk Grunwald (grunwald@cs.colorado.edu)
//	
//	Awesime is distributed in the hope that it will be useful, but
//	WITHOUT ANY WARRANTY.  No author or distributor accepts
//	responsibility to anyone for the consequences of using it or
//	for whether it serves any particular purpose or works at all,
//	unless he says so in writing.  
//	
//	Everyone is granted permission to copy, modify and
//	redistribute Awesime, but only under the conditions described
//	in the Awesime General Public License.  A copy of this license
//	is supposed to have been given to you along with Awesime so
//	you can know your rights and responsibilities.  It should be
//	in a file named COPYING.  Among other things, the copyright
//	notice and this notice must be preserved on all copies.
//	
#ifndef _Pareto_h
#define _Pareto_h 1

#ifdef __GNUG__
#  pragma interface
#endif

#include <Random.h>

//
//	The Pareto distribution. This distribution is used both to compute
//	the parameter 'a' and to draw samples from the distribution once
//	the parameter has been established.
// 

class ParetoParameter {
    double n;
    double sum_ln_xi;
public:
    ParetoParameter();
    void sample(double);
    void operator+=(double);	// same as sample
    double parameter();
};

class Pareto: public Random {
    double parameter_a;
    double parameter_k;
public:
  Pareto(double a, double k, RNG *gen) : Random(gen) {
    parameter_a = a;
    parameter_k = k;
  }

  virtual double operator()() {
    double  u = pGenerator -> asDouble(); 
    return( parameter_k * pow((1-u), -1.0 / parameter_a) );
  }

};
#endif
