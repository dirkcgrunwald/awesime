/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "ThreadContainer.h"
#include "assert.h"
#include "iolock.h"

const void * UNINITIALIZED = 0;

SpinFetchAndOp<int> Thread::ThreadsCreated(0);
SpinFetchAndOp<int> Thread::ThreadsDestroyed(0);

Thread::Thread(const char* name, unsigned stacksize,
	       int checkStack,
	       ThreadPriority priority, int xdebug)
    : Awesime(xdebug), pContext(checkStack, stacksize)
{
    threadName = name;
    threadPriority = priority;
    threadState = RUNNABLE;
    cpuAffinity = -1;

//    pContext.buildReturnFrame(this, voidFuncP(&Thread::startOff));

    pContext.buildReturnFrame(this, voidFuncP(&Thread::startOff));


    if (debugFlag) {
	cerr << "[create Thread " << name << " at " << this << "\n";
    }

    ThreadsCreated += 1;
}

Thread::~Thread()
{
    makeTerminated();
    pContext.reclaimStack();
    ThreadsDestroyed += 1;
}

void Thread::main()
{
  cerr << "[Thread] Subclass of thread forgot to specialize main()";
  abort();
}

void
Thread::startOff(Thread *p)
{
    CpuMux::handoff();
    p -> main();
    CpuMux::threadTerminateException( p );

    cerr << lock;
    cerr << "Thread::StartOff() - thread attempting to exit main\n";
    cerr << "Thread is " << p << " AKA " << p -> name() << "\n";
    int *foo = (int *) p;
    if ( *foo == 0 )  {
	cerr << "Hmm., *this is null too ...\n";
    }
    cerr << "Stack mallocAt = " << p -> pContext.stackMallocAt;
    cerr << "\n";
    cerr << unlock;

    cerr << "[Thread] Thread exits main";
    abort();
}

void Thread::makeTerminated()
{
    //
    //	this doesn't do anything yet. It's not clear it's needed either,
    //  since most termination-oriented things could happen in the
    //  destructor.
    //
}

void Thread::classPrintOn(ostream& strm)
{
    strm << "[Thread:"
	 << " " << threadName
         << "  pri: " << int(threadPriority)
         << "  state: ";
    switch (threadState) {
    case SUSPENDED:
	strm << "SUSPENDED";
	break;
    case RUNNING:
	strm << "RUNNING";
	break;
    case RUNNABLE:
	strm << "RUNNABLE";
	break;
    case TERMINATED:
	strm << "TERMINATED";
	break;

    case ZOMBIED:
    default: strm << "INVALID";
	break;
    }
    strm << "]\n";
    strm << pContext << "\n";;
}
