//-*-c++-*-
// This may look like C code, but it is really -*- C++ -*-
//
//	Copyright (C) 1988,1989,1990,1991,1992
//	Dirk Grunwald (grunwald@cs.colorado.edu)
//	
//	Awesime is distributed in the hope that it will be useful, but
//	WITHOUT ANY WARRANTY.  No author or distributor accepts
//	responsibility to anyone for the consequences of using it or
//	for whether it serves any particular purpose or works at all,
//	unless he says so in writing.  
//	
//	Everyone is granted permission to copy, modify and
//	redistribute Awesime, but only under the conditions described
//	in the Awesime General Public License.  A copy of this license
//	is supposed to have been given to you along with Awesime so
//	you can know your rights and responsibilities.  It should be
//	in a file named COPYING.  Among other things, the copyright
//	notice and this notice must be preserved on all copies.
//	
#ifndef _LowerBoundedFifo_h
#define _LowerBoundedFifo_h

#ifdef __GNUG__
#  pragma interface
#endif

#include <Semaphore.h>
#include <LockedFifo.h>
#include <FifoScheduler.h>

class LowerBoundedFifo : public LockedFifo {

protected:
    Semaphore lowerBoundLock;
    FifoScheduler lowerBoundLockFifo;
    
public:
    
    LowerBoundedFifo(int defaultLength = 0);
    
    virtual void add(AwesimeFifoItem *t);
    virtual int remove(AwesimeFifoItem *item);
    virtual int removeNoBlock(AwesimeFifoItem *item);

    virtual int removeIfFound(AwesimeFifoItem *item);
    
    virtual int doDelete(AwesimeFifoIndex& index);
    
    virtual unsigned size();
    
    virtual void classPrintOn(ostream& s);
};

#endif /* _LowerBoundedFifo_h */
