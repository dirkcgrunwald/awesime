#ifdef __GNUG__
#  pragma implementation
#endif

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "EventSequence.h"
#include "Thread.h"

EventSequence::EventSequence( int count_ ) :
  tickets(count_ - 1 )
{
    counter = count_;
    lock.release();
}

void
EventSequence::reset( int count_ ) 
{
    lock.reserve();
    counter = count_;
    tickets = count_ - 1;

    for (;; ) {
	if ( pile.empty() ) break;

	PileMap::iterator item = pile.begin();
	if ( (*item).first < counter ) {
	  Thread *thread = (*item).second;
	  CpuMux::add( thread);
	  pile.erase(item);
	} else {
	    break;
	}
    }

    lock.release();
}

EventSequence::~EventSequence()
{
    lock.reserve();
    pile.erase(pile.begin(), pile.end());
    lock.release();
}

void
EventSequence::advance(int x)
{
    lock.reserve();
    counter += x;
    for (;; ) {
	if ( pile.empty() ) break;

	PileMap::iterator item = pile.begin();
	if ( (*item).first < counter ) {
	  Thread *thread = (*item).second;
	  pile.erase(item);
	  CpuMux::add( thread );
	} else {
	  break;
	}
    }
    lock.release();
}

void
EventSequence::await(int x)
{
    lock.reserve();
    if ( x >= counter ) {
	lock.release();
	setContext(0, (void *) x);
	CpuMux::reserveByException( this );
    } else {
	lock.release();
    }
}

int
EventSequence::reserveByException(Thread *byWho, ExceptionReserve&)
{
    int blocked = 0;

    int x = (int) ((long) getContext(0));
    lock.reserve();
    if ( x >= counter ) {
      pile[x] = byWho;
      blocked = 1;
    }
    lock.release();
    return( blocked );
}
