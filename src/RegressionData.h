//-*-c++-*-
#ifndef _RegressionData_h_
#define _RegressionData_h_

#include "Statistic.h"

/**
 * A RegressionData collects data that is then
 * used to derive a RegressionModel. You can continue
 * to collect data once it's been turned into a RegressionModel
 * without influencing the model.
 *
 */

template <class T>
class RegressionData {

public:
  RegressionData() {
    reset();
  }

  /**
   * You can reset the Statistic, meaning that all previously
   * recorded data is discarded.
   */
  void reset() {
    x.reset();
    y.reset();
    sum_x_y = 0;
  }

  /**
   * Add a sample
   */
  void add(double xx, double yy) {
    x.add(xx);
    y.add(yy);
    sum_x_y += (xx * yy);
  }

  /**
   * Data needed by the Regression model. We use two
   * Statistics to record most of the data.
   */
  Statistic<T> x;
  Statistic<T> y;
  T sum_x_y;
};

#endif
