/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "LowerBoundedFifo.h"
#include "CpuMux.h"
#include "iostream"
#include "assert.h"
#include "iolock.h"

LowerBoundedFifo::LowerBoundedFifo(int defaultLength)
    : LockedFifo(defaultLength),
    lowerBoundLock(0, &lowerBoundLockFifo), lowerBoundLockFifo(2)
{
    //
    // do nothing
    //
}

void LowerBoundedFifo::add(AwesimeFifoItem *t) {
    LockedFifo::add(t);
    lowerBoundLock.release();
}

int
LowerBoundedFifo::remove(AwesimeFifoItem *item)
{
    (void) lowerBoundLock.reserve();
    int ok = LockedFifo::remove(item);
    //
    //	The remove had better have worked, or there's something screwy
    //  with the semaphores.
    //
    if ( ! ok ) {
      cerr << lock;
      cerr << "LowerBoundedFifo::remove ";
      cerr << "attempted to remove more items than we should be able to\n";
      cerr << unlock;
      abort();
    }
    return(ok);
}

int
LowerBoundedFifo::removeNoBlock(AwesimeFifoItem *item)
{
    int ok = lowerBoundLock.reserveNoBlock();
    if ( ok ) {
	ok = LockedFifo::remove(item);
    }
    return(ok);
}

int
LowerBoundedFifo::removeIfFound(AwesimeFifoItem *item) {
    int ok = LockedFifo::removeIfFound(item);
    if (ok) {
	(void) lowerBoundLock.reserve();
    }
    return(ok);
}

int
LowerBoundedFifo::doDelete(AwesimeFifoIndex& index) {
    //
    // This is problematic. Since this is a LockedFifo, this person
    // will be the only one with access to the list. If he blocks
    // at this point, he's dead-meat.
    //
    // So, is this an error? If so, should we protect him?
    //
    int whaHap = AwesimeFifo::doDelete(index);
    if (whaHap) {
	int noBlock = lowerBoundLock.reserveNoBlock();
	assert( noBlock );
    }
    return(whaHap);
}

unsigned int
LowerBoundedFifo::size()
{
    return(AwesimeFifo::size());
}

void LowerBoundedFifo::classPrintOn(ostream& s)
{
    s << "[LowerBoundedFifo " ;
    s << "\tSemaphore\n";
//    s << lowerBoundLock;
    s << "\n";
    s << "\tFifo";
    AwesimeFifo::classPrintOn(s);
    s << "\n]\n" ;
}
