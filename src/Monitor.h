#ifndef _Monitor_h_
#define _Monitor_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include "ReserveByException.h"
#include "SpinLock.h"

class Monitor : public ReserveByException {

protected:
    SpinLock lock;

public :

    virtual void reserve();
    virtual void release();
    virtual int reserveNoBlock();
};

#endif
