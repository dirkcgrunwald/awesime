// This may look like C code, but it is really -*- C++ -*-
/*
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef _LifoScheduler_h_
#define _LifoScheduler_h_

#include <ThreadContainer.h>
#include <SpinLock.h>

class Thread;

class LifoScheduler : public ThreadContainer {
public:
    class ll {
    public:
	Thread *thread;
	ll *next;
    };

private:
    ll *head;
    ll *last;
    int length;

    static SpinLock freelistLock;
    static ll *freelist;

    ll *alloc();
    void dispose(ll*);

public:

    LifoScheduler(int defaultLength = 10);

    virtual void add(Thread *t);
    virtual Thread* remove();

    virtual int isEmpty();
    virtual unsigned size();

    virtual Pix first();
    virtual void next(Pix&);
    virtual Thread*& operator()(Pix);

};
		     
#endif
