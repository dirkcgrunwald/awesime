//-*-c++-*-
#ifndef _iolock_h_
#define _iolock_h_


#include "AwesimeConfig.h"

#if GNU_C_BROKEN
//
// This should be sufficient, but Gnu C++ is broken
//
extern ios& lock(ios&);
extern ios& unlock(ios&);
#endif

extern ostream& lock(ostream&);
extern ostream& unlock(ostream&);

extern istream& lock(istream&);
extern istream& unlock(istream&);

#endif
