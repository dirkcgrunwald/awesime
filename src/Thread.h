/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
 
This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef	_Thread_h_
#define	_Thread_h_

#include <Awesime.h>
#include "AwesimeConfig.h"
#include <HardwareContext.h>
#include <ThreadContainer.h>
#include <SpinFetchAndOp.h>
#include <CpuMux.h>


enum ThreadState { SUSPENDED, RUNNING, RUNNABLE, ZOMBIED, TERMINATED };

#ifndef HARDWARE_CONTEXT_CHECK_NONE
#    define HARDWARE_CONTEXT_CHECK_NONE 0
#endif

#ifndef HARDWARE_CONTEXT_CHECK_SWITCH
#    define HARDWARE_CONTEXT_CHECK_SWITCH 1
#endif

#ifndef HARDWARE_CONTEXT_CHECK_MPROTECT
#    define HARDWARE_CONTEXT_CHECK_MPROTECT 2
#endif

typedef int ThreadPriority;

class Thread;
typedef Thread *ThreadPtr;

class SingleCpuMux;
class SingleSimMux;
class MultiCpuMux;
class MultiSimMux;
class SimMux;
class FifoScheduler;

class Thread : public Awesime {

    //
    // a sure sign of a mis-design..
    //
    friend class HardwareContext;
    friend class CpuMux;

    static SpinFetchAndOp<int> ThreadsCreated;
    static SpinFetchAndOp<int> ThreadsDestroyed;

    HardwareContext pContext;

    ThreadState threadState;
    ThreadPriority threadPriority;
    int cpuAffinity;

    ThreadContainer *suspendedOn;

    static void startOff(Thread *);
    void makeTerminated(); 
    const char* threadName;


protected:

    void name(const char *);
    void state(ThreadState s);

    void makeRunnable();

    virtual void main();

    Thread(const char* name ="", unsigned stacksize = 8000,
	   int checkStack = 1,
	   ThreadPriority priority = 0,
	   int debug = 0);
    

    //
    // access functions
    //
    
public:
    virtual ~Thread();
    const HardwareContext * context(); 
    const char* name();

    ThreadState state();

    long maxStackDepth();
    void checkStack();

    void priority(ThreadPriority newPriority);
    ThreadPriority priority();

    void affinity(int);
    int affinity();

    //
    // action functions
    //
    
    virtual void classPrintOn(ostream& strm);
};

inline const char*
Thread::name()
{
  if (threadName == NULL) {
    return "<unnamed>";
  } else {
    return threadName;
  }
}

inline void
Thread::name(const char *name)
{
    threadName = name;
}

inline void
Thread::state(ThreadState s)
{
    threadState = s;
}

inline ThreadState
Thread::state()
{
    return threadState;
}

inline long
Thread::maxStackDepth()
{
    return( pContext.maxStackDepth() );
}

inline void
Thread::checkStack()
{
    pContext.checkStack();
}

inline ThreadPriority
Thread::priority()
{
    return(threadPriority);
}

inline void
Thread::priority(ThreadPriority newPriority)
{
    threadPriority = newPriority;
}

inline int
Thread::affinity()
{
    return(cpuAffinity);
}

inline void
Thread::affinity(int new_)
{
    cpuAffinity = new_;
}

#endif
