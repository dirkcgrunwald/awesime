/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef SpinBarrier_h
#define SpinBarrier_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Implement a UNIX process barrier
//

#include <SpinLock.h>
#include <AwesimeConfig.h>
#include <assert.h>

class SpinBarrier : public SpinLock {
 protected:
    int pHeight;
    volatile int pCount;
    volatile int generation;
    int pLoops;
    int pMaxLoops;

public:
    SpinBarrier( int h, int loops = DefaultSpinBarrierLoops, int max = 0 );
    ~SpinBarrier();

    //
    // Normal entry. Everyone calls rendezvous()
    //
    int rendezvous();
    //
    // lowerBarrier forces all waiting jobs to continue
    //
    void lower();

    //
    // Changing the height of the barrier can cause people to continue
    // if it is lowered below the current pCount.
    //
    int height();
    void height(int h);

    //
    // loops() is the numbr of times a barrier spins before doing a getpid()
    // to release the UNIX cpu. maxLoopss() the maximum number of getpids()
    // to do before returning from the barrier (i.e. for barrier with timeout)
    //
    int loops();
    void loops(int h);
    int maxLoops();
    void maxLoops(int h);

    int count();
};

inline
SpinBarrier::SpinBarrier( int h, int l, int m )
{
    pHeight = h;
    pLoops = l;
    pMaxLoops = m;
    pCount = 0;
}

inline int
SpinBarrier::height()
{
    return(pHeight);
}

inline int
SpinBarrier::loops()
{
    return(pLoops);
}

inline void
SpinBarrier::loops(int h)
{
    pLoops = h;
}

inline int
SpinBarrier::maxLoops()
{
    return(pMaxLoops);
}

inline void
SpinBarrier::maxLoops(int h)
{
    pMaxLoops = h;
}

inline int
SpinBarrier::count()
{
    return(pCount);
}

inline
SpinBarrier::~SpinBarrier()
{
    assert( pCount == 0 );
}

#ifndef _USE_SPIN_LOCKS_
inline int
SpinBarrier::rendezvous()
{
    assert( (pCount+1) == pHeight );
    generation++;
    pCount = 0;
    return(1);
}

inline void
SpinBarrier::lower()
{
    generation++;
    pCount = 0;
}
#endif /* _USE_SPIN_LOCKS_ */

#endif /* SpinBarrier */
