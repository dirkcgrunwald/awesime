/* 
Copyright (C) 1990 by Dirk Grunwald 

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1991 University of Colorado, Boulder, Colorado
//
// written by David Wagner (wagner@cs.colorado.edu)
//
#ifndef _CCondition_h_
#define _CCondition_h_

#ifdef __GNUG__
#  pragma interface
#endif

//
//	CCondition.h
//
//	This implements condition variables used for the Cthreads
//	library, where the Monitor is based in via 'wait' rather
//	then at Condition creation.
//
//	Note that because all accesses to a CCondition are supposed to
//	be guarded by an external Mutex, there is no need for an internal
//	SpinLock.
//
//	When a thread blocks on
//	a CCondition, the associated mutex is released automatically.
// 

#include "Condition.h"
#include "FifoScheduler.h"

class CCondition : public Condition {
  FifoScheduler sched;
public:
    
    CCondition();
    virtual ~CCondition();

    // Assumption:  the caller is holding the associated mutex.
    virtual void wait();
    virtual void wait(Monitor&);
};

#endif /* CCondition_h */
