#include "SingleFifoMux.h"
#include "FifoScheduler.h"

SingleFifoMux::SingleFifoMux(int debug) : SingleCpuMux(debug)
{
    myCurrentEvents = allocateThreadContainer();
}

SingleFifoMux::~SingleFifoMux()
{
    // do nothing
}

ThreadContainer *
SingleFifoMux::allocateThreadContainer()
{
    return ( new FifoScheduler(512) );
}
