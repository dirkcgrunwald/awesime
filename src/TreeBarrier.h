/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef TreeBarrier_h
#define TreeBarrier_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	TreeBarrier.h
//	The barrier is initially closed.
// 
#include <CpuMuxP.h>
#include <ReserveByException.h>
#include <SpinLock.h>
#include <FifoScheduler.h>


#define FANIN 4

typedef struct CTREE {
    SpinLock lck;
    short count;
    short expected_count;
    int locksense;
    struct CTREE *parent;
} TREENODE;

typedef struct THREAD_PILE {
    FifoScheduler *pile;
    SpinLock      lck;
    int           expected_count;
    int           count;
    int           initCount;
} PILE_REC;

typedef TREENODE *TREENODEPTR;

class TreeBarrier : public ReserveByException {

    int pHeight;
    int numThreads;
    int initFlag;

 protected:
    TREENODEPTR BarrierTree[_MAXIMUM_CPU_MUXS_];
    PILE_REC    ThreadPiles[_MAXIMUM_CPU_MUXS_];
    TREENODEPTR myLeaf[_MAXIMUM_CPU_MUXS_];
    int        mySense[_MAXIMUM_CPU_MUXS_];
    
    int numCpus;
    int interior_nodes;
    SpinLock lck;
    virtual void rendezVousAux(TREENODEPTR, int *) = 0;
    void releaseAllOfMine(int iYam);
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);

  public: 
    virtual ~TreeBarrier();
    TreeBarrier(); 
    virtual void rendezvous() = 0;
    void initBarrier(int height, int cpus = CpuMux::Muxs);
    void checkIn();
    void checkOut();

};
#endif /* TreeBarrier_h */

















