//-*-c++-*-
// This may look like C code, but it is really -*- C++ -*-
//
//	Copyright (C) 1988,1989,1990,1991,1992
//	Dirk Grunwald (grunwald@cs.colorado.edu)
//	
//	Awesime is distributed in the hope that it will be useful, but
//	WITHOUT ANY WARRANTY.  No author or distributor accepts
//	responsibility to anyone for the consequences of using it or
//	for whether it serves any particular purpose or works at all,
//	unless he says so in writing.  
//	
//	Everyone is granted permission to copy, modify and
//	redistribute Awesime, but only under the conditions described
//	in the Awesime General Public License.  A copy of this license
//	is supposed to have been given to you along with Awesime so
//	you can know your rights and responsibilities.  It should be
//	in a file named COPYING.  Among other things, the copyright
//	notice and this notice must be preserved on all copies.
//	
#ifdef __GNUG__
#  pragma implementation
#endif

#include <assert.h>
#include "AwesimeConfig.h"

extern "C" {
    void abort();
    void exit(int);
}

const int SubClassResponsibility = 0;

void _assert(const char *reason,
	     const char *file, const int line)
{
    cerr << "Assertion " << reason << " failed: file " << file << " line " << line << "\n";
    abort();
    exit(1);
}

void _assert2(const char *reason,
	     const char *file, const int line,
	     const char *string)
{
    cerr << "Assertion " << reason << " failed: file " << file << " line " << line << "\n";
    if (string != 0) {
	cerr << "Comment: " << string << "\n";
    }
    cout.flush();
    cerr.flush();
    abort();
    exit(1);
}
