// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "SingleSimMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "OEv.h"
#include "ReserveByException.h"
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include "iolock.h"

SingleSimMux::SingleSimMux(int debug) : SimMux(debug)
{
  ThisSimMux = this;
  //    ThisCpu = this;  // this is done in CpuMux.h. took it out to 
  //   not have to create separate SingleSimMux.C for Posix version
    
  pNameTemplate = "SingleSimMux";
  iYam = 0;
  sprintf(nameSpace, "[%s-%d] ", pNameTemplate, iYam);
  pName = nameSpace;
  myEvents = new myHeapType();
}

SingleSimMux::~SingleSimMux()
{
  delete myEvents;
}

Thread *
SingleSimMux::remove()
{
  if ( myEvents -> empty() ) {
    return NULL;
  } else {
    SEv ev = myEvents -> top();
    myEvents -> deq();
    if ( ev.isOtherEvent() ) {
      myEvents -> insert(ev.time(), ev);
      return 0;
    } else {
      CurrentSimulatedTime = ev.time();
      return ev.thread();
    }
  }
}

void
SingleSimMux::fireItUp(int, unsigned long)
{
  pid = getpid();
  enabled = 1;
    
  raisedBy = 0;
  while( ! *terminated ) {
	
    if ( myEvents -> empty() ) {
      //
      // Exit scheduling loop
      //
      break;
    } else {
      SEv ev = myEvents -> top();
      myEvents -> deq();
      CurrentSimulatedTime = ev.time();
	    
      //
      // if it is not a thread event, execute the OE
      //
      if ( ev.isOtherEvent() ) {
	currentThread = 0;
	ev.oev()-> executeEvent();
      }
      else {
	currentThread = ev.thread();
		
	if ( CpuMux::Debug ) {
	  cerr << __FUNCTION__ << " [" << __FILE__ << ":" << __LINE__ << "] ";
	  cerr << name();
	  cerr << " switchTo to " << currentThread -> name();
	  cerr << " at " << CurrentSimulatedTime << "\n";
	}
		
	systemTransfer( currentThread );
	currentThread = 0;
	CpuMux::handoff();
      }
    }
  }
}

void
SingleSimMux::add_(Thread *who)
{
  addAt_(who, CurrentSimulatedTime);
}

void
SingleSimMux::addToCpu_(int, Thread *who)
{
  if ( CpuMux::Debug ) {
    cout << "AddToCpu " << who -> name() << " at ";
    cout << CurrentSimulatedTime <<"\n";
  }

  add_(who);
}

void
SingleSimMux::addAt_(Thread *who, SimTimeUnit when)
{
  if ( CpuMux::Debug ) {
    cerr << __FUNCTION__ << " [" << __FILE__ << ":" << __LINE__ << "] ";
    cout << "AddToCpu " << who -> name() << " at ";
    cout << when <<"\n";
  }

  if (when <= CurrentSimulatedTime) {
    when = CurrentSimulatedTime;
  }
    
  if ( CpuMux::Debug ) {
    cerr << lock;
    cerr << CpuMux::Cpu() -> name();
    cerr << " add " << who -> name() << " to pending\n";
    cerr << unlock;
  }
    
  //
  // Add them to pending events
  //
  SEv foo(who, when);
  myEvents -> insert( foo.time(), foo );
}

void
SingleSimMux::addAt_(OEv *who, SimTimeUnit when)
{
  if (when <= CurrentSimulatedTime) {
    when = CurrentSimulatedTime;
  }
    
  if ( CpuMux::Debug ) {
    cerr << lock;
    cerr << CpuMux::Cpu() -> name();
    cerr << " add OEv to pending\n";
    cerr << unlock;
  }
    
  //
  // Add them to pending events
  //
  SEv foo(who, when);
  myEvents -> insert( foo.time(), foo );
}


void
SingleSimMux::await_(SimTimeUnit when)
{
  if ( CpuMux::Debug ) {
    cerr << lock;
    cerr << __FUNCTION__ << " [" << __FILE__ << ":" << __LINE__ << "] ";
    cerr << name() << " " << currentThread -> name();
    cerr << " awaits " << when << " at " << CurrentSimulatedTime << "\n";
    cerr << unlock;
  }

  if ( *terminated ) {
    //
    // If we're terminating, raise a suspension. Actual
    // termination will occur in the main scheduler loop.
    //
    SEv foo(currentThread, when);
    myEvents -> insert( foo.time(), foo );
    CpuMux::Cpu() -> raise( &iveSuspendedException );
  }
    
  if (when > CurrentSimulatedTime) {
	
    if ( myEvents -> empty() ) {
      CurrentSimulatedTime = when;
    } else {
	    
      SEv& front = myEvents -> top();

      //
      // is the current one awaiting a time sooner than any other one?
      //
	    
      if ( when <= front.time() ) { 
	CurrentSimulatedTime = when;
		
	if ( CpuMux::Debug ) {
	  cerr << lock;
	  cerr << __FUNCTION__ << " [" << __FILE__ << ":" << __LINE__ << "] ";
	  cerr << CpuMux::Cpu() -> name();
	  cerr << " (await) bump time to " << when << "\n";
	  cerr << unlock;
	}
		
      } else {
		
	// if it is an event, not a thread, suspend and let
	// the scheduler do it in the UNIX process context.
	//
		
	if  ( front.isOtherEvent() ) {
		    
	  SEv foo(currentThread, when);
	  myEvents -> insert( foo.time(), foo );
	  CpuMux::Cpu() -> raise( &iveSuspendedException );
		    
	} else { 
	  Thread *wuz = currentThread;
	  CurrentSimulatedTime = front.time();
	  currentThread = front.thread();

	  myEvents -> deq();

	  SEv foo(wuz, when);
	  myEvents -> insert(foo.time(), foo);
		    
	  if ( CpuMux::Debug ) {
	    cerr << lock;
	    cerr << __FUNCTION__ << " [" << __FILE__ << ":" << __LINE__ << "] ";
	    cerr << CpuMux::Cpu() -> name();
	    cerr << "(await) transfer to " << currentThread -> name() << "\n";
	    cerr << unlock;
	  }
		    
	  threadTransfer(wuz, currentThread);
	}
      }
    }
  }
}

void
SingleSimMux::print_events(ostream& str)
{
  if (myEvents == NULL) {
    str << __FUNCTION__ << " ";
    str << "No events to print" << endl;
    return;
  }

  myHeapType &evts  = *myEvents;
  Pix pix = evts.first();
  while (pix != NULL) {
    SimTimeUnit tim = evts.key(pix);
    SEv& sev = evts.data(pix);
    str << "[Event @ " << tim << " is " << sev << "]" << endl;
    evts.next(pix);
  }
}
