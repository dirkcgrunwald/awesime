#include "SEv.h"
#include "Thread.h"

ostream&
operator<<(ostream& str, SEv& sev)
{
  if ( sev.isOtherEvent() ) {
    str << " other event";
  } else {
    Thread *t = sev.thread();
    str << " thread *" << t -> name() << "* for " << sev.time();
  }
  return str;
}

