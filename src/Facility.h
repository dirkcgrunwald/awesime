/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef _Facility_h_
#define _Facility_h_

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Facility.
//
//	This models a simulation facility. A facility is composed of multiple
//	servers. Servers are utilized for a period of time. The fraction of
//	use of all servers over the total time the facility has existed
//	(or since it was reset) determines the utilization of the facility.
//
//	A facility is not owned by specific threads. An OwnedFacility,
//	however, is owned by specific threads.
//

#include <Semaphore.h>
#include <TimeUnit.h>

class Thread;
class Facility : public Semaphore {

protected: 

    SpinLock dataLock;

    int servers;
    struct {
	double single;	// valid when servers == 1
	double *many;	// valid when servers > 1
    } whenServiced;

    double serviceStarted;
    double totalServiceTime;

    long pTotalReserves;
    long pTotalFailures;

    double pTotalDelay;

    double queueEntriesTimesTime;
    SimTimeUnit lastTimeQueueWasNoticed;

    virtual void reportErrorState(ostream&);
    void commonReserve(double);

    public :

    Facility(int servers = 1, ThreadContainer *scheduler =0);

    ~Facility();

    virtual void reserve();
    virtual void release();
    virtual int reserveNoBlock();

    virtual void use(double);

    virtual void reset();

    virtual double utilization();

    virtual long totalAttempts();
    virtual long totalReserves();
    virtual long totalFailures();

    //
    // Delay == time waiting in queue
    //
    virtual double totalDelay();
    virtual double meanDelay();

    virtual double perceivedUtilization();

    virtual int isEmpty();

    virtual unsigned queueLength();
    virtual double meanQueueLength();

    virtual unsigned activeServers();
    virtual unsigned size();

//    virtual void classPrintOn(ostream& s);
};

#endif
