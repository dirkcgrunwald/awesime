/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "SimMux.h"
#include "Facility.h"
#include "FifoScheduler.h"
#include "Thread.h"
#include "assert.h"
#include <math.h>

Facility::Facility(int xservers, ThreadContainer *xscheduler)
    : Semaphore(xservers,xscheduler)
{
    //
    //	Can't use reset because we need to set up service times with
    // SimMux::NullTime() value.
    //
    servers = xservers;
    totalServiceTime = 0.0;

    pTotalReserves = 0;
    pTotalFailures = 0;

    pTotalDelay = 0.0;
    serviceStarted = SimMux::CurrentTime();

    queueEntriesTimesTime = 0;
    lastTimeQueueWasNoticed = SimMux::CurrentTime();
    
    if (servers == 1) {
	whenServiced.single = SimMux::NullTime();
    } else {
	whenServiced.many = new double[servers];
	for (int i = 0; i < servers; i++) {
	    whenServiced.many[i] = SimMux::NullTime();
	}
    }
}

//
//	To reset a facility, we assume that there has been no service
//	so far (totalServiceTime = 0) and then set the beginning of
//	outstanding service intervals to be now.
//
//	Thus, right after being reset, a facility has a utilization of zero.
//
void
Facility::reset()
{
    dataLock.reserve();

    double now = SimMux::CurrentTime();
    totalServiceTime = 0.0;

    queueEntriesTimesTime = 0;
    lastTimeQueueWasNoticed = SimMux::CurrentTime();
    
    pTotalReserves = 0;
    pTotalFailures = 0;

    pTotalDelay = 0.0;
    serviceStarted = now;
    if (servers == 1) {
	if (whenServiced.single != SimMux::NullTime()) {
	    whenServiced.single = now;
	}
    } else {
	for (int i = 0; i < servers; i++) {
	    if (whenServiced.many[i] != SimMux::NullTime()) {
		whenServiced.many[i] = now;
	    }
	}
    }
    dataLock.release();
}

Facility::~Facility()
{
    dataLock.reserve();

    int error = 0;
    if (servers == 1) {
	error = (whenServiced.single != SimMux::NullTime());
    } else {
	for(int i = 0; i < servers; i++) {
	    error |= (whenServiced.many[i] != SimMux::NullTime());
	}
    }

    if (error) {
	cerr << "[Facility:~Facility]";
	cerr << "Attempted to de-allocate reserved facility\n";
//	cerr << *this;
	exit(1);
    }

    dataLock.release();
}

void Facility::commonReserve(double delayTime)
{

    dataLock.reserve();

    pTotalReserves++;
    pTotalDelay += delayTime;

    if (servers == 1) {
	if (whenServiced.single == SimMux::NullTime()) {
	    whenServiced.single = SimMux::CurrentTime();
	    dataLock.release();
	    return;
	}
    } else {
	for (int i = 0; i < servers; i++) {
	    if (whenServiced.many[i] == SimMux::NullTime()) {
		whenServiced.many[i] = SimMux::CurrentTime();
		dataLock.release();
		return;
	    }
	}
    }

    dataLock.release();	// need to release so reportErrorState works

    reportErrorState(cerr);
    cerr << "[Facility] state error with facility semaphore";
    abort();
}

void Facility::reserve()
{

    double startedReserve = SimMux::CurrentTime();
    Semaphore::reserve();
    commonReserve( SimMux::CurrentTime() - startedReserve );
}

void
Facility::use(double howLong)
{
    reserve();
    SimMux::hold(howLong);
    release();
}

int Facility::reserveNoBlock()
{
    if (Semaphore::reserveNoBlock()) {
	commonReserve(0.0);	// commonReserve will bumb pTotalReserves
	return(1);
    } else {
	dataLock.reserve();

	pTotalFailures++;

	dataLock.release();

	return(0);
    }
}

void Facility::release()
{
    dataLock.reserve();

    double now = SimMux::CurrentTime();
    int error = 0;

    double diff = now - lastTimeQueueWasNoticed;
    queueEntriesTimesTime += (diff * Semaphore::size());
    lastTimeQueueWasNoticed = now;

    SimTimeUnit null = SimMux::NullTime();
    
    if (servers == 1) {
	if (whenServiced.single != null ) {
	    totalServiceTime += (now - whenServiced.single);
	    whenServiced.single = null;
	} else {
	    error = 1;
	}
    } else {
	error = 1;
	for (int i = 0; i < servers; i++) {
	    if (whenServiced.many[i] != null ) {
		totalServiceTime += (now - whenServiced.many[i]);
		whenServiced.many[i] = null;
		error = 0;
		break;
	    }
	}
    }

    dataLock.release();
    
    if (error) {
	cerr << "[Facility::release] ";
	cerr << "Attempt to release un-reserved facility\n";
//	cerr << *this;
	exit(1);
    }
    Semaphore::release();
}

double Facility::utilization()
{
    //
    //	To compute utilization, we sum the outstanding request times,
    //	add in the current total service time, and divide this by
    //	the number of facilities & the number servers.
    // 

    dataLock.reserve();
    
    double totalTime = totalServiceTime;
    double now = SimMux::CurrentTime();
    
    if (servers == 1) {
	if (whenServiced.single != SimMux::NullTime()) {
	    totalTime += (now - whenServiced.single);
	}
    } else {
	for (int i = 0; i < servers; i++) {
	    if (whenServiced.many[i] != SimMux::NullTime()) {
		totalTime += (now - whenServiced.many[i]);
	    }
	}
    }
    
    if (now == serviceStarted) {
	totalTime = 0;
    } else {
	totalTime /= (servers * (now - serviceStarted));
    }
    dataLock.release();
    return(totalTime);
}

double
Facility::perceivedUtilization()
{
    dataLock.reserve();
    double util = 0;
    long pTotal = pTotalReserves + pTotalFailures;
    if ( pTotal >0 ) {
	util = pTotalFailures;
	util /= pTotal;
    }
    dataLock.release();
    return( util );
}

long
Facility::totalAttempts()
{
    dataLock.reserve();
    int res = pTotalReserves + pTotalFailures;
    dataLock.release();
    return(res);
}

long
Facility::totalReserves()
{
    dataLock.reserve();
    int res = pTotalReserves;
    dataLock.release();
    return(res);
}

long
Facility::totalFailures()
{
    dataLock.reserve();
    int res = pTotalFailures;
    dataLock.release();
    return(res);
}

double
Facility::totalDelay()
{
    return(pTotalDelay);
}

double
Facility::meanDelay()
{
    if (pTotalReserves > 0) {
	return( pTotalDelay / pTotalReserves);
    } else {
	return( 0.0 );
    }
}

unsigned Facility::queueLength()
{
    return(Semaphore::size());
}

double Facility::meanQueueLength()
{
    double delta = SimMux::CurrentTime() - serviceStarted;
    return( queueEntriesTimesTime / delta );
}

unsigned Facility::activeServers()
{
    dataLock.reserve();

    int outStanding = 0;
    if (servers == 1) {
	if (whenServiced.single != SimMux::NullTime()) {
	    outStanding++;
	}
    } else {
	for (int i = 0; i < servers; i++ ){
	    if (whenServiced.many[i] != SimMux::NullTime()) {
		outStanding++;
	    }
	}
    }
    dataLock.release();

    return(outStanding);
}

unsigned Facility::size()
{
    return(queueLength() + activeServers());
}

int Facility::isEmpty()
{
    return size() == 0;
}

#ifdef UNDEF
void Facility::classPrintOn(ostream &out)
{
    out << "Facility with " << activeServers() << " active servers and "
	<< queueLength() << " queued requests";
}
#endif

void Facility::reportErrorState(ostream &out)
{
//    out << *this << "\n";
    out << "Serviced started at " << serviceStarted
	<< " with a total service time of " << totalServiceTime << "\n";
    if (servers == 1) {
	out << "Server 1 ";
	if (whenServiced.single == SimMux::NullTime()) {
	    out << " is idle\n";
	} else {
	    out << " started serving at " << whenServiced.single << "\n";
	}
    } else {
	for (int i = 0; i < servers; i++) {
	    out << "Server " << i ;
	    if (whenServiced.many[i] == SimMux::NullTime()) {
		out << " is idle\n";
	    } else {
		out << " started serving at " << whenServiced.many[i] << "\n";
	    }
	}
    }
    out << "State of facility semaphore is:\n";
//    Semaphore::classPrintOn(out);
}
