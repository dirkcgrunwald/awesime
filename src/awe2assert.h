/* This may look like C code, but it is really -*- C++ -*- */
/* 
Copyright (C) 1988 Free Software Foundation
    written by Dirk Grunwald (grunwald@cs.uiuc.edu)

This file is part of GNU CC.

GNU CC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.  No author or distributor
accepts responsibility to anyone for the consequences of using it
or for whether it serves any particular purpose or works at all,
unless he says so in writing.  Refer to the GNU CC General Public
License for full details.

Everyone is granted permission to copy, modify and redistribute
GNU CC, but only under the conditions described in the
GNU CC General Public License.   A copy of this license is
supposed to have been given to you along with GNU CC so you
can know your rights and responsibilities.  It should be in a
file named COPYING.  Among other things, the copyright notice
and this notice must be preserved on all copies.  
*/

#ifdef __GNUG__
#  pragma interface
#endif

#ifndef NDEBUG

extern const int SubClassResponsibility;
extern void _awe2assert(const char *, const char *, const int);
extern void _awe2assert2(const char *, const char *, const int, const char *);

#if !defined( __cplusplus )
#  define awe2assert(ex) { if ((ex)) ; else _awe2assert( #ex, __FILE__,__LINE__); }
#  define awe2assert2(ex,s) { if ((ex)) ; else _awe2assert2( #ex, __FILE__,__LINE__, s); }
#else
#  include <stdlib.h>
#  define awe2assert(EX) (void)((EX) || ((cerr << "Awe2assertion failed: EX, file " << __FILE__ << ", line " << __LINE__ << endl), abort(), 0))
#  define awe2assert2(EX,s) (void)((EX) || ((cerr << "Awe2assertion failed: EX, file " << __FILE__ << ", line " << __LINE__ << endl << s << endl ), abort(), 0))
#endif

#else

#define awe2assert(ex) 
#define awe2assert2(ex,s)

#endif

using namespace std;
