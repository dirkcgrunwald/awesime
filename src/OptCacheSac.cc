#include "OptCacheSac.h"
#include <assert.h>
#include "AwesimeConfig.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

using namespace std;

#define ONE 1
#define TWO 2
#define B80000000 0x80000000
#define INVALID 0
#define MEM_AVAIL_HITARR 2097152	/* Memory available for hitarr */
#define MAXINT 2147483647   		/* 2^31-1 */
#define HASHNO 7211

static const int WORD_SIZE = 1;


/////////////////////////////////////////////////////////////////////////////
// the UNK data type
/////////////////////////////////////////////////////////////////////////////
class OTS_HT {
  public:
    OptCache::Addr addr;
    int grptime;
    int prty;
    OTS_HT *next;
    OTS_HT(OptCache::Addr addr_, int g_, int p_, OTS_HT *next_) {
	addr = addr_;
	grptime = g_;
	prty = p_;
	next = next_;
    }

    static OTS_HT *freelist;

    void *operator new(size_t size) {
	if ( freelist == 0 ) {
	    return( (void *) new char[ size ]);
	} else {
	    OTS_HT *foo = freelist;
	    freelist = freelist -> next;
	    return(foo);
	}
    }

    void operator delete(void *ptr) {
	OTS_HT *foo = (OTS_HT *) ptr;
	foo -> next = freelist;
	freelist = foo;
    }

    static void clean_freelist() {
	while (freelist != NULL) {
	    OTS_HT *q = freelist -> next;
	    char *p = (char *) freelist;
	    delete p;
	    freelist = q;
	}
    }
};

OTS_HT *OTS_HT::freelist = 0;


/**********************************************************************
Used to add unknowns to a hash table. The hash table is used by inf_handler
to get the dummy priority of the unknown.

Input: The address and the dummy prty of the address.
Output: None
Side effects: The address is added to the hash table
**************************************************************************/

void
OptCacheSac::unk_hash_add_sa (OptCache::Addr addr, int prty)
{
    ++unknowns;
    
    int loc = addr & ((ONE << B) - 1);
    OTS_HT *p = slot[loc];
    
    while ( p ) {
	if ( p -> addr != addr ) {
	    //
	    // Found it, error
	    //
	    cerr << "Error: found duplicate address in";
	    cerr << " OptCacheSac::unk_hash_add_sa\n";
	    abort();
	}
    }
    slot[loc] = new OTS_HT(addr, -1, prty, slot[loc]);
}


/*************************************************************************
Deletes unknowns from the hash table. Returns the dummy priority of
the address.

Input: Address
Outpur: The dummy priority
Side effects: The entry is deleted from the hash table
*************************************************************************/

int
OptCacheSac::unk_hash_del_sa (OptCache::Addr addr)
{

   int loc = addr & ((ONE << B) - 1);
   OTS_HT *p = slot[loc];
   OTS_HT *prev = NULL;

   while ( p != NULL ) {
       if ( p -> addr == addr ) {
	   if ( prev == NULL ) {
	       slot[loc] = p -> next;
	   } else {
	       prev -> next = p -> next;
	   }
	   int ret_prty = p -> prty;
	   delete p;
	   return ret_prty;
       }
       prev = p;
       p = p -> next;
   }

   return( 0 );
}

/********************************************************************
Given the address and max_prty, goes through the stack that adress maps
to and removes unknowns of priority greater than max_prty

Input: Address and maximum priority of unknown in stack
Output: None
Side effects: Removes inessential unknowns from the hash table
********************************************************************/

void
OptCacheSac::hash_clean_sa (OptCache::Addr addr, int max_prty)
{
    assert( max_prty < 0 );
    
    int loc = addr & ((ONE << B) - 1);
    OTS_HT *p = slot[loc];
    OTS_HT *prev = NULL;
    
    //
    // The original version of this code was fairly convoluted.
    // Lets see if this is the same thing. I belive the invarient
    // is:
    //   remove all entries in the hash table with a priority greater
    //   than max_prty (see block comment above)
    //
    while ( p != NULL ) {
	if ( p -> prty > max_prty ) {
	    --unknowns;
	    if ( p -> addr == addr ) {
		if ( prev == NULL ) {
		    slot[loc] = p -> next;
		} else {
		    prev -> next = p -> next;
		}

		OTS_HT *next = p -> next;
		delete p;
		p = next;
	    } else {
		//
		// Just move on to the next entry..
		//
		prev = p;
		p = p -> next;
	    }
	} else {
	    //
	    // Just move on to the next entry..
	    //
	    prev = p;
	    p = p -> next;
	}
    }
}

/////////////////////////////////////////////////////////////////////////////
// array allocation
/////////////////////////////////////////////////////////////////////////////
static unsigned **idim2(int row, int col)
{
   int i;
   register unsigned **prow, *pdata;

   pdata = (unsigned *) calloc (row*col, sizeof (int));
   if (pdata == (unsigned *) NULL) {
       cerr << "Problem alocating space\n";
       exit (1);
   }
   prow = (unsigned **) calloc (row, sizeof (int *));
   if (prow == (unsigned **) NULL) {
       cerr << "Problem alocating space\n";
       exit (1);
   }
   for (i=0;i<row;i++) {
       prow [i] = pdata;
       pdata += col;
   }

   return(prow);
}


/////////////////////////////////////////////////////////////////////////////
// The OptCacheSac stuff
/////////////////////////////////////////////////////////////////////////////

OptCacheSac::OptCacheSac(
			 int L_,
			 int N_,
			 int A_,
			 int B_
			 )
: OptCache(L_)
{
    N = N_;
    B = B_;
    A = A_;

    TWO_PWR_N = (ONE << N);

    MAX_DEPTH = B-A;

    TWO_POWER_MAX_DEPTH = (ONE << MAX_DEPTH);

    SET_MASK = ((ONE << A) - 1);

    DIFF_SET_MASK = ((ONE << MAX_DEPTH) - 1);

    SET_SIZE = WORD_SIZE * TWO_PWR_N;

    BASE_CACHE_SIZE = (ONE<<A)*SET_SIZE;
    
    slot = (OTS_HT **) calloc((ONE << B), sizeof(OTS_HT *));
    
    tag_arr = (OptCache::Addr *)
	calloc((TWO * (ONE << B) * TWO_PWR_N), sizeof(unsigned));

    hits = idim2(MAX_DEPTH + 2, TWO_PWR_N);

    hit0 = 0;
    
    all_hit_flag = (short *)
	calloc((ONE << A), sizeof(short));
    
    prty_arr = (int *)
	calloc((TWO * (ONE << B) * TWO_PWR_N), sizeof(unsigned));

    t_entries = 0;
    unknowns = 0;

    assert( tag_arr );
    assert( prty_arr );
    assert( all_hit_flag);
    
    for (int i=0; i < (TWO * (ONE << B) * TWO_PWR_N); i++) {
	//
	// 32 bit problem!
	//
	tag_arr[i] = 0xffffffff;
	prty_arr[i] = (5-MAXINT);
    }
    
    inf_count = 0;
}

OptCacheSac::~OptCacheSac()
{
    free( tag_arr );
    free( prty_arr );
    free( all_hit_flag );
    //
    // Watch how you do this - see idim2 for more info. hits[0]
    // should be the same as 'pdata' in idim2, and 'hits' should
    // be the same as 'prow'.
    //
    free( hits[0] );
    free( hits );
    free( slot );

    OTS_HT::clean_freelist();
}


/********************************************************************
Given the index of the address in the address array it determines the
priority of the address using the time_array.

Input: Index of current address in the addr_array.
Output: The priority of the current address
Side effects: None
********************************************************************/

int
OptCacheSac::Priority_sa(int i)
{
    if ( time_array [i] > 0) {
	//
	// inf_handler assumes this fn.
	// inf_handler has to be changed if this is changed 
	//
	return (MAXINT - time_array [i]); 
    }
    else if (time_array [i] == 0) {
	return (--inf_count);
    }
    else { 
	printf ("Error in Priority function\n");
	return(0);
    }
}


/********************************************************************
Routine that checks and updates the tag stores, and maintains the hit counts.

Input: Trace address and priority of address
Output: None
Side effects: Changes the tag stores, and updates the hit array
********************************************************************/

void
OptCacheSac::gfsoptls (OptCache::Addr addr, int priority)
{
    OptCache::Addr orig_tag = addr >> A;
    OptCache::Addr base_setno = addr & ((ONE << A) - 1);

    if (all_hit_flag[base_setno]) {

	if (tag_arr [base_setno << N ] == orig_tag) {
	    ++ hit0;
	    prty_arr [base_setno << N ] = priority;
	    return;
	}
	else {
	    int real_prty = prty_arr [base_setno << N ];

	    OptCache::Addr prev_addr
		= (tag_arr [base_setno << N] << A) | base_setno;

	    int cache_ptr = BASE_CACHE_SIZE;

	    for (int i=A+1; i<=B; i++) {
		OptCache::Addr setno = prev_addr & ((ONE << i) - 1);
		int set_ptr = cache_ptr + (setno << N);
		prty_arr[set_ptr] = real_prty;
		cache_ptr = TWO*cache_ptr + BASE_CACHE_SIZE;
	    }
	}
    }

    all_hit_flag [base_setno] = 1;
    int i = A;
    long cache_size = BASE_CACHE_SIZE;
    int cache_ptr = 0;
    int max_prty = (5 - MAXINT);
    int hit = 0;

    while (i <= B) {
	OptCache::Addr de_tag = orig_tag;
	int de_prty = priority;
	int setno = addr & ((ONE << i) - 1);
	int set_ptr = cache_ptr + (setno << N);

	hit = 0;

	int j;
	for (j=0; j < TWO_PWR_N; j++) {
	    if (tag_arr[set_ptr + j] == orig_tag) {
		hit = 1;
		++hits[i-A][j];
		tag_arr[set_ptr + j] = de_tag;
		prty_arr[set_ptr + j] = de_prty;
		break;
	    }
	    else {
		if (de_prty > prty_arr[set_ptr + j]) {

		    OptCache::Addr rem_tag = tag_arr[set_ptr + j];

		    tag_arr[set_ptr + j] = de_tag;
		    de_tag = rem_tag;

		    int rem_prty = prty_arr[set_ptr + j];
		    prty_arr[set_ptr + j] = de_prty;
		    de_prty = rem_prty;
		}

		if (i == B
		    && prty_arr[set_ptr + j] < 0
		    && max_prty < prty_arr[set_ptr + j])
		{
		    max_prty = prty_arr[set_ptr + j];
		}
	    }
	} /*for*/
	if (j > 0) {
	    all_hit_flag [base_setno] = 0;

	    if (j >= TWO_PWR_N
		&& (i == B)
		&& (de_prty < 0)
		&& (de_prty > (5-MAXINT))) {
		//
		// reforming address
		//
		ft_hash_del ((de_tag << A) | setno);
	    }
	}
	cache_ptr += cache_size;
	cache_size += cache_size;
	++i;
    } /*while*/

    if ((i > B) && (hit == 0)) {
	hash_clean_sa (addr, max_prty);
    }
}

/**********************************************************************
Main simulation routine. Processes the tag_array and time_array obtained
from the pre-processing routine.

Input: The range in the addr_array (time_array) to be processed
Output: -1 if limit on the addresses to be processed is reached
       0 otherwise
Side effects: The tag stores are updated by its child routine 'gfsoptls'
	      as the simulation proceeds.
**********************************************************************/

int
OptCacheSac::stack_proc(int start, // index of starting array location
			int end    // index of ending array location
			)
{
    int l;
    OptCache::Addr addr;
    int priority;
    
    for (l=start; l<end; l++) {
	++t_entries;
	
	addr = addr_array[l];
	
	priority = Priority_sa(l);
	if (priority < 0) {
	    unk_hash_add_sa (addr, priority);
	}
	
	gfsoptls (addr, priority);
	
	addr_array [l] = 0;
	time_array [l] = 0;
	if (t_entries > T) {
	    // Well, the comment say the error condition should be -1,
	    // but they're returning a 1. In any case,
	    // OptCache::reference was checking for a 1. We'll abort.
	    //
	    abort();
	}
    } /* for */
    return (0);
}

/****************************************************************
Output routine.

Input: None
Output: None
Side effects: None
****************************************************************/

void
OptCacheSac::output(ostream& out)
{
    int i;
    
    for (i=0; i <= (B-A); i++) {
	hits [i][0] += hit0;
    }
    hit0 = 0;
    
    out << "Addresses processed: " << dec << t_entries << "\n";
    out << "Line size: " << (ONE << L) << " bytes\n\n";
    out << "Miss Ratios\n";

    out << "\n";
    out << "Cache Size" << setw(20) << "Associativity\n";
    out << "  (bytes)";

    //
    // A total of 12 columns for each value. 4 columns seperate, followed
    // by the label in 4 columns, then another 4
    //

    for (i=0;i<TWO_PWR_N;i++) {
	out << setw(3) << " " << setw(7) << i+1 << setw(3) << " ";
    }
    out << "\n";

    for (i=0; i<=MAX_DEPTH; i++) {
	//
	// 7 columns
	//
	print_cachesize(out, (1 << (i+A)) * (1 << L)); 
	//
	// Add 4 more..
	//
	out << "    ";
	
	int sum = 0;
	for (int j=0; j<TWO_PWR_N; j++) {
	    sum += hits[i][j];
	    double miss_ratio = (1.0 - ((double)sum/(double)t_entries));

	    out << "   ";
	    out << setw(6) << setprecision(5);
	    out.setf(ios::fixed | ios::showpoint, ios::floatfield);
	    out << miss_ratio * 100.0 ;
	    out << "%  ";
	}
	fprintf(stdout, "\n");
    }
    fprintf (stdout, "\n\n\n");
} 


/*******************************************************************
Rearranges the positions of unknowns in the stack, when an unknown
becomes known.

Input: Address of unknown that becomes known, and the current time at
       pre-processor.
Output: None
Side effects: Rearranges the positions of unknowns in the stacks that addr
	      maps to
*******************************************************************/

void
OptCacheSac::inf_handler(OptCache::Addr addr, int cur_time)
{
    OptCache::Addr  setno, orig_tag, cache_size,
	de_tag, rem_tag, prev_addr,
	cache_ptr, set_ptr, base_setno;
    int de_prty, rem_prty,
        old_prty, priority,
	real_prty;
    int i, j;
    
    old_prty = unk_hash_del_sa (addr);
    if (old_prty == 0)
	return;
    
    --unknowns;
    
    priority = MAXINT - cur_time; /* Assumes a priority function */
    
    base_setno = addr & ((ONE << A) -1);
    if (all_hit_flag[base_setno]) {
	all_hit_flag[base_setno] = 0;
	real_prty = prty_arr [base_setno << N];
	prev_addr = (tag_arr [base_setno << N] << A) | base_setno;
	cache_ptr = BASE_CACHE_SIZE;
	for (i=A+1; i<=B; i++) {
	    setno = prev_addr & ((ONE << i) - 1);
	    set_ptr = cache_ptr + (setno << N);
	    prty_arr[set_ptr] = real_prty;
	    cache_ptr = TWO*cache_ptr + BASE_CACHE_SIZE;
	}
    }
    orig_tag = addr >> A;
    i = A;
    cache_size = BASE_CACHE_SIZE;
    cache_ptr = 0;
    while (i <= B) {
	de_tag = orig_tag;
	de_prty = priority;
	setno = addr & ((ONE << i) - 1);
	set_ptr = cache_ptr + (setno << N);
	for (j=0; j < TWO_PWR_N; j++) {
#ifdef PERF
	    ++compares;
#endif
	    if (tag_arr[set_ptr + j] == orig_tag)
		if (prty_arr[set_ptr + j] > 0)
		    printf ("Error: inconsistency in inf_handler\n");
		else {
		    tag_arr[set_ptr + j] = de_tag;
		    prty_arr[set_ptr + j] = de_prty;
		    break;
		}
	    else if (prty_arr[set_ptr + j] < 0) {
		if (old_prty < prty_arr[set_ptr + j]) {
		    if (de_prty > prty_arr[set_ptr + j]) {
			rem_tag = tag_arr[set_ptr + j];
			tag_arr[set_ptr + j] = de_tag;
			de_tag = rem_tag;
			rem_prty = prty_arr[set_ptr + j];
			prty_arr[set_ptr + j] = de_prty;
			de_prty = rem_prty;
		    }
		}
	    }
	} /*for*/
	cache_ptr += cache_size;
	cache_size += cache_size;
	++i;
    } /*while*/
}
