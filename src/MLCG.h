//-*-c++-*-
// This may look like C code, but it is really -*- C++ -*-
//
//	Copyright (C) 1988,1989,1990,1991,1992
//	Dirk Grunwald (grunwald@cs.colorado.edu)
//	
//	Awesime is distributed in the hope that it will be useful, but
//	WITHOUT ANY WARRANTY.  No author or distributor accepts
//	responsibility to anyone for the consequences of using it or
//	for whether it serves any particular purpose or works at all,
//	unless he says so in writing.  
//	
//	Everyone is granted permission to copy, modify and
//	redistribute Awesime, but only under the conditions described
//	in the Awesime General Public License.  A copy of this license
//	is supposed to have been given to you along with Awesime so
//	you can know your rights and responsibilities.  It should be
//	in a file named COPYING.  Among other things, the copyright
//	notice and this notice must be preserved on all copies.
//	
#ifndef _MLCG_h
#define _MLCG_h 1 

#ifdef __GNUG__
#  pragma interface
#endif

#include <RNG.h>
#include <math.h>

//
//	Multiplicative Linear Conguential Generator
//

class MLCG : public RNG {
    int initialSeedOne;
    int initialSeedTwo;
    int seedOne;
    int seedTwo;

protected:

public:
    MLCG(int seed1 = 0, int seed2 = 1);
    //
    // Return a int-words word of random bits
    //
    virtual unsigned int asInt();
    virtual void reset();
    int seed1();
    void seed1(int);
    int seed2();
    void seed2(int);
    void reseed(int, int);
};

inline int
MLCG::seed1()
{
    return(seedOne);
}

inline void
MLCG::seed1(int s)
{
    initialSeedOne = s;
    reset();
}

inline int
MLCG::seed2()
{
    return(seedTwo);
}

inline void
MLCG::seed2(int s)
{
    initialSeedTwo = s;
    reset();
}

inline void
MLCG::reseed(int s1, int s2)
{
    initialSeedOne = s1;
    initialSeedTwo = s2;
    reset();
}

#endif
