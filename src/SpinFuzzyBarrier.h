/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef SpinFuzzyBarrier_h
#define SpinFuzzyBarrier_h

//
//	Implement a UNIX process fuzzy barrier using spinning on shared memory.
//	A fuzzy barrier is akin to a barrier, but it allows us to distinguish
//	the entry to the barrier from the synchronization point.
//

#include <SpinBarrier.h>
#include <AwesimeConfig.h>
#include <assert.h>

class SpinFuzzyBarrier : public SpinBarrier {

public:
    SpinFuzzyBarrier(int h, int loops = 100, int max = 0 );
    ~SpinFuzzyBarrier();

    //
    // rendez() - join the barrier crowd. We return the generation count
    // at when we joined. We use this with vous() to determine if the
    // the barrier has been met. Thus, rendezvous() is equivilent to
    // vous(rendez()).
    //
    int rendez();
    int vous(int);
};

inline
SpinFuzzyBarrier::SpinFuzzyBarrier( int h, int l, int m )
     : SpinBarrier(h, l, m)
{
    // empty
}

#ifndef _USE_SPIN_LOCKS_
inline int
SpinFuzzyBarrier::rendez()
{
    assert( (pCount+1) == pHeight );
    int gen = generation;
    generation++;
    pCount = 0;
    return(gen);
}

inline int
SpinFuzzyBarrier::vous(int gen)
{
    assert( gen < generation );
    return(1);
}
#endif /* _USE_SPIN_LOCKS_ */

#endif /* SpinFuzzyBarrier */
