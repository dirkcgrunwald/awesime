// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "SimMux.h"
#include "CpuMuxP.h"
#include "HardwareContextP.h"
#include "CpuMuxExceptions.h"
#include "SpinLock.h"
#include "SpinBarrier.h"
#include "Thread.h"

#include "ReserveByException.h"
#include <math.h>

static const int NotReached = 0;

SimMux*  SimMux::ThisSimMux = 0;
SimTimeUnit SimMux::CurrentSimulatedTime = 0.0;
const SimTimeUnit SimMux::NullSimulatedTime = -1;

SimMux::SimMux(int debug)
{
    ThisSimMux = this;
    CpuMux::Debug = debug;
}

SimMux::~SimMux()
{
    // empty
}

void
SimMux::addWithDelay_(Thread *who, SimTimeUnit delay)
{
    delay += CurrentSimulatedTime;
    addAt_(who, delay);
}

void
SimMux::addWithDelay_(OEv *who, SimTimeUnit delay)
{
    delay += CurrentSimulatedTime;
    addAt_(who, delay);
}

void
SimMux::await_(SimTimeUnit when)
{
    assert( NotReached );
#if 0
    if (when > CurrentSimulatedTime) {

	addAt( CpuMux::Cpu() -> currentThread, when );

	Thread *next = CpuMux::Cpu() -> remove();

	if ( next == 0 ) {
	    CpuMux::Cpu() -> raise( &(CpuMux::Cpu() -> iveSuspendedException) );
	} else {
	    Thread *from = CpuMux::Cpu() -> currentThread;
	    CpuMux::Cpu() -> currentThread = next;

#ifndef NDEBUG
	    if (CpuMuxDebugFlag) {
		CpuMux::CerrLock.reserve();
		cerr << CpuMux::Cpu() -> name();
		cerr << " switch to " << CpuMux::Cpu() -> currentThread -> name() << "\n";
		CpuMux::CerrLock.release();
	    }
#endif	/* NDEBUG */
	    from -> pContext.switchContext ( &(CpuMux::Cpu() -> currentThread -> pContext) );
	}
    }
#endif

}

void
SimMux::hold_(SimTimeUnit holdFor)
{
    if (holdFor > 0) {
	await( CurrentSimulatedTime + holdFor );
    }
}
