#define _SPIN_NO_INLINE_

#include "SpinLock.h"

void SpinLock::reserve()
{
    SpinLockReserve(&state);
}

int SpinLock::reserveNoBlock()
{
    return( SpinLockReserveNoBlock(&state) );
}


