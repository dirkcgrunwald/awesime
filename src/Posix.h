#ifndef ThreadPackage_h
#define ThreadPackage_h

typedef void (*voidPtrFuncP)(void *);

typedef long aThread_t;
typedef long aProcessor_name_t;

typedef struct {
     void* originalToClone;
     long pid;
} ATHREAD_ARG;
#endif
