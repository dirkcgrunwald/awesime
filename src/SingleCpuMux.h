// Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
// 
// This file is part of Awesime.
// 
// Awesime is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY.  No author or distributor accepts responsibility to
// anyone for the consequences of using it or for whether it serves any
// particular purpose or works at all, unless he says so in writing.
// Refer to the GNU General Public License for full details.
// 
// Everyone is granted permission to copy, modify and redistribute
// Awesime, but only under the conditions described in the Gnu General
// Public License.  A copy of this license is supposed to have been given
// to you along with Awesime so you can know your rights and
// responsibilities.  It should be in a file named COPYING.  Among other
// things, the copyright notice and this notice must be preserved on all
// copies.
// 

// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef SingleCpuMux_h
#define SingleCpuMux_h

#include <CpuMux.h>
class ThreadContainer;

class SingleCpuMux : public CpuMux {

protected:
    ThreadContainer *myCurrentEvents;

    SingleCpuMux(int debug = 0);
    virtual ~SingleCpuMux();

    virtual ThreadContainer* allocateThreadContainer();

    virtual void add_(Thread *);
    virtual void addToCpu_(int, Thread*);

    //
    // Remove doesn't always return a thread in general,
    // but this remove will always return either NULL or a thread.
    //
    virtual Thread *remove();


public:
    virtual void fireItUp(int Cpus = 1, unsigned long shared = (4196 * 500));
};

#endif /* SingleCpuMux_h */
