// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "MonitorSimMux.h"
#include "CpuMuxP.h"
#include "SpinBarrier.h"
#include "Statistic.h"
#include "AwesimeConfig.h"
#include "OEv.h"
#include "Thread.h"
#include <stdio.h>
#include <unistd.h>
#include "iolock.h"

static Statistic<int> ThreadsPerTick;

extern int CpuMuxDebugFlag;

MonitorSimMux *ThisMonitorSimMux;

MonitorSimMux::MonitorSimMux(int debug) : SingleSimMux(debug)
{
    ThisMonitorSimMux = this;

    pNameTemplate = "MonitorSimMux";
    iYam = 0;
    sprintf(nameSpace, "[%s-%d] ", pNameTemplate, iYam);
    pName = nameSpace;
}

void
MonitorSimMux::fireItUp(int, unsigned long)
{
    if ( myEvents -> empty() ) {
	timeThisSet = myEvents -> top().time();
    } else {
	timeThisSet = SimMux::NullTime();
    }
    eventsThisSet = 0;

    pid = getpid();
    enabled = 1;
    
    while( ! *terminated ) {
	
	if ( myEvents -> empty() ) {
	    break;
	} else {
	    SEv ev = myEvents -> top();
	    myEvents -> deq();
	    CurrentSimulatedTime = ev.time();
	    
	    if ( CurrentSimulatedTime > timeThisSet ) {
		ThreadsPerTick += eventsThisSet;
		eventsThisSet = 1;
		timeThisSet = CurrentSimulatedTime;
	    } else {
		eventsThisSet++;
	    }
	    
	    //
	    // if it's not a thread event, execute the OE
	    //
	    if ( ev.isOtherEvent() ) {
		currentThread = 0;
		ev.oev() -> executeEvent();
	    }
	    else {
		currentThread = ev.thread();
		cerr << lock << CpuMux::Cpu() -> name();
		cerr << " switchTo to " << currentThread -> name();
		cerr << " at " << CurrentSimulatedTime << "\n" << unlock;
		
		systemTransfer( currentThread );

		raisedBy -> handleException();
		raisedBy = 0;
	    }
	}
    }
    
    cerr << lock;
    cerr << name() << "Statistics for threads per tick\n";
    cerr << name();
    ThreadsPerTick.out(cerr);
    cerr << "\n";
    cerr << name() << "Min = " << ThreadsPerTick.min() << "\n";
    cerr << name() << "Max = " << ThreadsPerTick.max() << "\n";
    cerr << unlock;
}

Statistic<int> *
MonitorSimMux::threadsPerTick()
{
    return (&ThreadsPerTick);
}

void
MonitorSimMux::await_(SimTimeUnit when)
{
    if (when > CurrentSimulatedTime) {
	
	if ( myEvents -> empty() ) {
	    CurrentSimulatedTime = when;
	} else {
	    
	    SEv& front = myEvents -> top();
	    
	    //
	    // is the current one awaiting a time sooner than any other one?
	    //
	    
	    if ( when <= front.time() ) { 
		CurrentSimulatedTime = when;

		cerr << lock << CpuMux::Cpu() -> name();
		cerr << " (await) bump time to " << when << "\n" << unlock;

	    } else {
		
		//
		// if it's an event, not a thread, suspend and let the
		// scheduler do it in the UNIX process context.
		//
		
		if  ( front.isOtherEvent() ) {
		    
		    SEv foo(currentThread, when);
		    myEvents -> insert( foo.time(), foo );
		    CpuMux::Cpu() -> raise( &iveSuspendedException );

		} else { 
		    Thread *wuz = currentThread;
		    CurrentSimulatedTime = front.time();
		    currentThread = front.thread();
		    myEvents -> deq();
		    
		    SEv foo(wuz, when);
		    myEvents -> insert( foo.time(), foo );

		    if ( CurrentSimulatedTime > timeThisSet ) {
			ThreadsPerTick += eventsThisSet;
			eventsThisSet = 1;
			timeThisSet = CurrentSimulatedTime;
		    } else {
			eventsThisSet++;
		    }
	    

		    cerr << lock << CpuMux::Cpu() -> name();
		    cerr << "(await) transfer to " << currentThread -> name();
		    cerr << "\n" << unlock;

		    threadTransfer(wuz, currentThread);

		}
	    }
	}
    }
}
