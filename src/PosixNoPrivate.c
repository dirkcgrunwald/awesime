#include <pthread.h>
#include <ThreadPackage.h>
#include <AwesimeConfig.h>
#include <stdio.h>

pthread_t iYams[_MAXIMUM_CPU_MUXS_];

int maxThreadId = 0;

void write_iYam(value)
   int value;
{
   
   if (value > maxThreadId)
     maxThreadId = value;

   iYams[value] = aSelf();
}

int read_iYam(void) {

   pthread_t id;
   int i;

   if ((id = aSelf()) == -1)
      return(-1);


   for (i=0; i <= maxThreadId; i++) {
       if (iYams[i] == id){
          return(i);
	}
     }
   printf("OOps: could not get iYam in read_IYam(), ThreadPackage.c\n");
   return(-1);
}


int aCreate(pidPtr,func,arg)
   pthread_t *pidPtr;
   voidPtrFuncP func;
   void* arg;
{

   if (pthread_create(pidPtr, pthread_attr_default, func, arg)  == -1)
       return(-1);

   if (aBind(*pidPtr,NULL))
       return(-1);
   return(0);
 }

long aGetproc(pid)
   pthread_t pid;
{
   processor_name_t proc_id;

   pthread_getproc(pid, &proc_id);
   return (proc_id);
} 

int aBind(pid,destproc)
    pthread_t pid;
    processor_name_t destproc;
{
    return(pthread_move(pid,destproc,TH_BOUND_PROCESSOR));
}
int aJoin(pid)
   pthread_t pid;
{
   return (pthread_join(pid, NULL));
}
long aSelf(void)
{
   return (pthread_self());
}



