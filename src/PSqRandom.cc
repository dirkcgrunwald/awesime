#ifdef __GNUG__
#  pragma implementation
#endif

#include "PSqRandom.h"
#include "PSqHistogram.h"
#include <stdlib.h>
#include <assert.h>

PSqRandom::PSqRandom(PSqHistogram *d, RNG *gen)
: Random(gen)
{
    distribution_ = d;
}

void
PSqRandom::distribution(PSqHistogram *d)
{
    distribution_ = d;
}

PSqHistogram *
PSqRandom::distribution()
{
    return distribution_;
}

double 
PSqRandom::operator()()
{

#if 0
    double d = pGenerator -> asDouble();
    int samples = distribution_ -> samples();
    //
    // Samples is in the range 0..(samples-1), so we don't need to round
    //
    int sample = int(samples * d);

    //
    // We need to iterate from 0..max(), not max() - 1
    //
    int min = 0;
    int max = distribution_ -> max();
    
    while ( (max-min) > 7) {
	int mid = (min + max) / 2;  
	PSqHistogram::Point& p = distribution_ -> point(mid);
	if ( sample <= p.n ) {
	    max = mid;
	} else {
	    min = mid;
	}
    }
    
    for (int i = min; i <= max; i++ ) {
	PSqHistogram::Point& p = distribution_ -> point(i);
	if ( sample <= p.n ) {
	    return( p.q );
	}
    }
    //
    // Hmmm...fell off the end.. This shouldn't be able to happen.
    //
    //    return distribution_ -> point(max).q;
    abort();
#else

    // 
    // The contents of the different cells are equiprobably distributed,
    // and thus, we can simply select a random cell in the range 1..cells
    //
    // Actually, this means we never pull in the minimum value, which is
    // in cell[0] (i.e. it has n == 0, which we *could* fetch
    // with the code above). However, this is an approximation to the
    // actual distribution, we it doesn't appear to cause too many errors.
    //

    double d = pGenerator -> asDouble();
    int cells = distribution_ -> buckets();
    int cell = int(d * cells) + 1;
    assert( cell > 0 && cell <= (cells) );
    return( distribution_ -> point(cell).q );

#endif
}

