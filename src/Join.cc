
#ifdef __GNUG__
#  pragma implementation
#endif

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Join.h"
#include "Thread.h"

Join::Join(Thread *child_)
{
    child = child_;
    childWaiting = 0;
    returnValue = 0;
    parent = 0;
}

Join::~Join()
{
    lock.reserve();

    // this is messy

    lock.release();
}

void *
Join::parentJoin()
{
    lock.reserve();
    int waiting = childWaiting;
    lock.release();

    if ( ! waiting ) {
	CpuMux::Cpu() -> reserveByException( this );
	//
	// execution resumes here when child is finally waiting
	assert( childWaiting );
    }

    lock.reserve();
    delete child;
    lock.release();

    return (returnValue);
}

void Join::childJoin(void *rv)
{
    returnValue = rv;
    CpuMux::Cpu() -> reserveByException( this );
    //
    // child should never returns
    //
    assert( 0 );
}

//
//	This is only executed in the context of a CpuMux, never
//	a thread.
//
int
Join::reserveByException(Thread *byWho, ExceptionReserve&)
{
    int blocked = 0;
    lock.reserve();
    if ( byWho == child ) {
	//
	// we're child, return 1 to indicate we're blocked
	//
	childWaiting = 1;
	blocked = 1;
	if ( parent != 0 )  {
	    CpuMux::Cpu() -> add ( parent );
	}
    } else {
	//
	// we're parent
	//
	if ( childWaiting ) {
	    blocked = 0;
	    //
	    // we will resume after the reserveByException in parentJoin
	    //
	} else {
	    //
	    // wait for the kid
	    //
	    parent = byWho;
	    blocked = 1;
	}
    }
    lock.release();
    return( blocked );
}
