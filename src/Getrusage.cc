#include "Getrusage.h"

Getrusage::Getrusage( int from_ )
{
    _from = from_;
}

void
Getrusage::start()
{
    getrusage(_from, &_start);
}

void
Getrusage::stop()
{
    getrusage(_from, &_stop);
}

static inline double
seconds(struct timeval& foo)
{
    return (foo.tv_sec + foo.tv_usec / 1000000.0);
}

double 
Getrusage::elapsed_cpu_seconds()
{
    double f = seconds(_start.ru_utime) + seconds(_start.ru_stime);
    double t = seconds(_stop.ru_utime) + seconds(_stop.ru_stime);
    return t - f;
}
