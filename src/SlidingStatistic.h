/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef SlidingStatistic_h
#define SlidingStatistic_h

#ifdef __GNUG__
#  pragma interface
#endif

#include <Statistic.h>

template <class T = double>
class SlidingStatistic : public Statistic<T> {
protected:
  T *sample;
  int maxSamples;
  int head;
  char cached;

  virtual void cacheValues();

public:
    
  SlidingStatistic( int samples ) {
    sample = new T [ s ]; 
    maxSamples = s;
    head = 0;
    cached = 0;
  }

  virtual ~SlidingStatistic() {
    if (maxSamples > 0) {
      delete sample;
    }
  }

  virtual void reset() {
    Statistic<T>::reset();
    cached = 0;
    head = 0;
  }

  virtual void operator+=(T value) {
    if ( head >= maxSamples ) {
      head = 0;
    }
    if ( n >= maxSamples ) {
      //
      //	Remove the oldest value
      //
      n--;
      x -= sample[head];
      x2 -= (sample[head] * sample[head]);
    }

    sample[ head ] = value;
    head++;

    n ++;
    x += value;
    x2 += (value * value);

    cached = 0;
  }

  void SlidingStatistic::cacheValues()
  {
    minValue = MAXDOUBLE;
    maxValue = -MAXDOUBLE;
    for ( int i = 0; i < n; i++ ) {
      minValue = (minValue < sample[i]) ? minValue : sample[i];
      maxValue = (maxValue < sample[i]) ? sample[i] : maxValue;
    }
    cached = 1;
  }

  virtual T min() {
    if (!cached) {
      cacheValues();
    }
    return( minValue );

  }

  virtual double max() {
    if (!cached) {
      cacheValues();
    }
    return( maxValue );
  }

};

#endif
