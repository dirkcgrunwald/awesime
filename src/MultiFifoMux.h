#ifndef _MultiFifoMux_h_
#define _MultiFifoMux_h_

#include "MultiCpuMux.h"

class MultiFifoMux : public MultiCpuMux {
    virtual ThreadContainer* allocateThreadContainer();
public:
    MultiCpuMux* clone();  // used by posix version
    MultiFifoMux(int debug = 0);
    virtual ~MultiFifoMux();
};

inline MultiCpuMux* MultiFifoMux::clone()
{
    return new MultiFifoMux(CpuMux::Debug);
}
#endif
