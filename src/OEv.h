#ifndef _OEv_h_
#define _OEv_h_

class OEv {
    //
    // class OEv contains no data, only a single public virtual function.
    // Class OEv itself should never be created, only subclasses of it.
    //
public:
    virtual void executeEvent();
};

#endif
