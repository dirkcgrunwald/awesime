/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef CpuMuxExceptions_h
#define CpuMuxExceptions_h

#include <ExceptionClass.h>

//
//	ExceptionByReserve -- Used to reserve a semaphore/barrier/etc
//
class ReserveByException;

class ExceptionReserve : public ExceptionClass {
    Thread *who;
    ReserveByException *toReserve;
public:
    ReserveByException *reserve();
    void reserve(Thread *, ReserveByException *);
    
    virtual void handleException();

    //
    // This is an interface to allow exceptions to pass additional
    // context information to the exception handler. See EventSequence
    // for an example.
    //
    void *context[10];
};

inline ReserveByException *
ExceptionReserve::reserve()
{
    return(toReserve);
}

inline void
ExceptionReserve::reserve(Thread *t, ReserveByException *r)
{
  who = t;
  toReserve = r;
}


//
//	ExceptionTerminate -- used to terminate a thread
//
class Thread;

class ExceptionTerminate : public ExceptionClass {
    Thread *deadThread; 
public:
    void terminate(Thread *);
    virtual void handleException();
};

inline void
ExceptionTerminate::terminate(Thread *r)
{
    deadThread = r;
}

//
//	ExceptionReschedule -- used to relenquish the CPU.
//

class ExceptionReschedule : public ExceptionClass {
private:
    Thread *oldThread;
    Thread *executeDirectly;

public:
    void switchTo(Thread* , Thread *);
    virtual void handleException();
};

inline void
ExceptionReschedule::switchTo(Thread *old,Thread *t)
{
    oldThread = old;
    executeDirectly = t;
}

//
//	ExceptionIveSuspended -- used when current thread has suspended
//	itself and another thread must be scheduled and run.
//

class ExceptionIveSuspended : public ExceptionClass {
public:
    virtual void handleException();
};

#endif /* CpuMuxExceptions_h */
