#ifndef _SingleRandomMux_h_
#define _SingleRandomMux_h_

#include "SingleCpuMux.h"

class SingleRandomMux : public SingleCpuMux {

protected:
    virtual ThreadContainer* allocateThreadContainer();

public:
    SingleRandomMux(int debug = 0);
    virtual ~SingleRandomMux();
};

#endif
