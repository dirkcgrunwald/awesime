#include <pthread.h>
#include <ThreadPackage.h>

__private int IYamWhatIYam;
__shared int currentId;

__shared processor_name_t *cells;

void write_iYam(value)
   int value;
{
   IYamWhatIYam = value;
}

int read_iYam(void) {
   return(IYamWhatIYam);
}

int aInit(cpus) 
int cpus;
{
   int numGiven;

   currentId = 1;

   cells = (processor_name_t *) malloc (sizeof(processor_name_t)*(cpus+1));
   
   numGiven = pthread_procsetinfo(cells,cpus);
   
   if (numGiven < cpus)
      printf("You asked for %d cpus. I can only allocate %d cpus at this time", cpus, numGiven);
   return(0);
}

int aCreate(pidPtr,func,arg)
   pthread_t *pidPtr;
   voidPtrFuncP func;
   void* arg;
{
   if (pthread_create(pidPtr, pthread_attr_default, func, arg)  == -1)
       return(-1);

   if (aBind(*pidPtr,currentId++))
       return(-1);
   return(0);
}

long aGetproc(pid)
   pthread_t pid;
{
   processor_name_t proc_id;

   pthread_getproc(pid, &proc_id);
   return (proc_id);
} 

int aBind(pid,destproc)
    pthread_t pid;
    int destproc;
{
    return(pthread_move(pid,cells[destproc],TH_BOUND_PROCESSOR));
}
int aJoin(pid)
   pthread_t pid;
{
   return (pthread_join(pid, NULL));
}
long aSelf(void)
{
   return (pthread_self());
}











