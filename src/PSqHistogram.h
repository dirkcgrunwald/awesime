// -*-C++-*-
#ifndef _PSqHistogram_h_
#define _PSqHistogram_h_

#include <assert.h>
#include "AwesimeConfig.h"


//
// To interpret the results from the [] operator, for the PSqHistogram, h, 
// imagine an array, A, in which all N=h.samples() values were stored in 
// sorted order, and there are B=h.size() buckets:
//
//     h[0].q = a[0],   h[0].n = 0     - the minimum
//     h[B].q = a[N-1], h[B].n = N-1,  - the maximum
//
// For a bucket i, 1 <= i < B:
//
//     h[i].n = the number of elements of A whose value is less than h[i].q.
//
// There are actually B+1 "elements" in h, numbered 0 to B inclusive.
// If too few samples have been accumulated, h.samples() <= h.size(), then
// the [] operator is undefined!
//
// Note that this algorithm is not exact.  It makes a good approximation by
// fitting a piecewise parabolic curve to the sample histogram.
//
// See
// "The P^2 Algorithm for Dynamic Calculation of Quantiles and Histograms
//  Without Storing Observations", by Raj Jain and Imrich Chlamtac, as
//  published in "Communications of the ACM", Vol. 28, No. 10, Oct-85, 
//  pp1076-1085.
//

class PSqHistogram {
public:
    
    struct Point {
	double   q;		// marker heights (x-axis)
	int      n;		// marker positions (y-axis)
    };
    
private:
    unsigned ns;			// number of samples        (n)
    unsigned nc;			// number of cells          (b)
    Point   *table;		// Statistics
    
    void reset(unsigned);	// reinitialize histogram & clear sample
    
public:
    PSqHistogram(unsigned cellCount);		// Initialize number of cells
    
    PSqHistogram(unsigned ns, unsigned nc,	// Initialize from pre-cooked
	       Point *table);
    
    ~PSqHistogram();				// Destructor
    unsigned  samples();			// Return number of samples
    unsigned  buckets();			// Return number of buckets
    int       max();				// Max index of operater[] 
    
    void check_index(int i);			// 0 <= i <= max()
    const Point &operator[](unsigned i);	// 0 <= i <= max()
    const Point &point(unsigned i);		// 0 <= i <= max()

    void invariant();
    
    PSqHistogram &operator  =(unsigned cellCount);	// Reinitialize 
    PSqHistogram &operator +=(double x);		// add x to sample
    PSqHistogram(PSqHistogram &);			// copy constructor
};

//
// Return the number of samples
//
inline unsigned PSqHistogram::samples() {
    return ns;
}

//
// Return the number buckets
//
inline unsigned PSqHistogram::buckets() {
    return nc;
}


//
// Return the maximum index for the histogram [] operator.  Use this to
// loop through the buckets.  The problem is that the results of the []
// operator are not fully defined until the number of samples is greater than
// the number of buckets.
// 
inline int PSqHistogram::max() {
    register unsigned top;
    
    if (ns < nc+1) top = ns-1; else top = nc;
    return top;
}

//
// Return the current value of the i'th bucket.
//
// See the comment over struct Point in the header file for what it means.
//
// This semantics is a bit strange.  If the number of samples collected so
// far is less than one plus the number of buckets, then the semantics are
// undefined.  This is a run-time error for now.  Use the max() member function
// to determine the current maximum defined substript.
//

inline void PSqHistogram::check_index(int index)
{
    assert( index <= max() );
}

inline const PSqHistogram::Point& 
PSqHistogram::operator[](unsigned index)
{
    check_index(index);
    return table[index];
}

//
// Like above, but unchecked. Primarily used by QHDist
//
inline const PSqHistogram::Point& 
PSqHistogram::point(unsigned index)
{
    return table[index];
}


#endif
