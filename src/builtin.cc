#include "builtin.h"
#include <stdlib.h>

_VOLATILE_VOID default_one_arg_error_handler(const char*)
{
    abort();
}

_VOLATILE_VOID default_two_arg_error_handler(const char*, const char*)
{
    abort();
}

two_arg_error_handler_t lib_error_handler;

two_arg_error_handler_t 
set_lib_error_handler(two_arg_error_handler_t f)
{
    abort();
#if defined(__ksr__)
    return(NULL);
#endif
}
