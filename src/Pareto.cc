#ifdef __GNUG__
#  pragma implementation
#endif

#include "Pareto.h"
#include <math.h>

ParetoParameter::ParetoParameter()
{
    n = 0;

    sum_ln_xi = 0;
}

void
ParetoParameter::sample(double xi)
{
    n++;
    sum_ln_xi += log(xi);
}

void
ParetoParameter::operator+=(double xi)
{
    sample(xi);
}

double ParetoParameter::parameter()
{
    double a = n / sum_ln_xi;
    return(a);
}



