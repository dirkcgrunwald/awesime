//-*-c++-*-
#ifndef _Getrusage_h_
#define _Getrusage_h_

extern "C" {
#  include <sys/time.h>
#  include <sys/resource.h>
}

class Getrusage {
  public:

    Getrusage( int from = RUSAGE_SELF );
    void start();
    void stop();

    double elapsed_cpu_seconds();
  private:
    int _from;
    struct rusage _start;
    struct rusage _stop;
};

#endif
