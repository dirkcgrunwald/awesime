/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "Thread.h"
#include "MLCG.h"
#include "RandomScheduler.h"
#include "assert.h"

RandomScheduler::ll *
RandomScheduler::freelist = 0;

SpinLock
RandomScheduler::freelistLock;

RandomScheduler::RandomScheduler(int defaultLength)
{
    head = NULL;
    last = NULL;
    length = 0;
    rGen = new MLCG(0,1);
}

inline RandomScheduler::ll*
RandomScheduler::alloc()
{
    freelistLock.reserve();
    ll *p;
    if ( freelist != NULL ) {
	p = freelist;
	freelist = freelist -> next;
	freelistLock.release();
    } else {
	freelistLock.release();
	p = new ll;
    }
    return p;
}

inline void
RandomScheduler::dispose(RandomScheduler::ll *p)
{
    freelistLock.reserve();
    p -> next = freelist;
    freelist = p;
    freelistLock.release();
}

void
RandomScheduler::add(Thread *t)
{
    ll *p = alloc();
    p -> thread = t;
    p -> next = NULL;
    length++;
    if ( head == NULL ) {
	head = last = p;
    } else {
	assert( last != NULL );
	last -> next = p;
	last = p;
    }
}

Thread *
RandomScheduler::remove()
{
  Thread *t;
  ll *temp;

    if ( head == NULL ) {
	return NULL;
    } else {
	// Figure out how far to randomly go down the list
	static long index_to_remove;
	
	index_to_remove = (long) ((rGen->asDouble()*length)+1);

	ll *p = head;

	if (length == 1) {
	  t = head -> thread;
	  dispose(head);
	  head = NULL;
	} 
	else if (length == 2) {
	  if (index_to_remove == 1) {
	    t = head -> thread;
	    temp = head;
	    head = head -> next;
	    dispose(temp);
	  }
	  else {
	    t = head -> next -> thread;
	    dispose(head -> next);
	    head -> next = NULL;
	    last = head;
	  }
	}
	else if (index_to_remove == 1) {
	  t = head -> thread;
	  temp = head;
	  head = head -> next;
	  dispose(temp);
	}
	else if (index_to_remove == 2) {
	  t = head -> next -> thread;
	  temp = head -> next;
	  head -> next = head -> next -> next;
	  dispose(temp);
	}
	else {
	
	  while (index_to_remove > 2) {
	    p = p -> next;
	    index_to_remove--;
	  }
	  
	  if (p -> next == last) {
	    t = last -> thread;
	    dispose(last);
	    last = p;
	    p -> next = NULL;
	  }
	  else {
	    t = p -> next -> thread;
	    temp = p -> next;
	    p -> next = p -> next -> next;
	    dispose(temp);
	  }
	}


	length--;

	//
	// Head may now be NULL, but last may be non-null. You can
	// only depend on value of last if you know head is non-null.
	//
	return t;
    }
}

int RandomScheduler::isEmpty()
{
    if ( length == 0 ) {
	return 1;
    } else {
	return 0;
    }
}

unsigned int RandomScheduler::size()
{
    return(length);
}

Pix RandomScheduler::first()
{
    return( Pix(head) );
}

void RandomScheduler::next(Pix& i)
{
    ll *l = (ll *) i;
    i = Pix(l -> next);
}

Thread*&
RandomScheduler::operator()(Pix i)
{
    ll *l = (ll *) i;
    return(l -> thread);
}
