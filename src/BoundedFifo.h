/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef BoundedFifo_h
#define BoundedFifo_h

#ifdef __GNUG__
#  pragma interface
#endif

#include <LowerBoundedFifo.h>

//
// A BoundedFifo is a LowerBoundedFifo that also has an upper bound.
// The members are implemented over the LowerBoundedFifo members to
// reduce the number of calls.
//
class BoundedFifo : public LowerBoundedFifo {

protected:
    int pMaxLength;
    FifoScheduler upperBoundLockFifo;
    Semaphore upperBoundLock;

public:

    BoundedFifo(int xmaxLength = 1, int defaultLength = 0);

    virtual void add(AwesimeFifoItem *t);
    virtual int remove(AwesimeFifoItem *item);
    virtual int removeNoBlock(AwesimeFifoItem *item);

    virtual int removeIfFound(AwesimeFifoItem *item);

    virtual int doDelete(AwesimeFifoIndex& index);

    virtual unsigned size();
};
		     
#endif
