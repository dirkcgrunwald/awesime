// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#include "SingleCpuMux.h"
#include "CpuMuxP.h"
#include "HardwareContextP.h"
#include "SpinLock.h"
#include "Thread.h"
#include "ThreadContainer.h"
#include "ReserveByException.h"
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "iolock.h"

//
//	This can not be private, or we wont see all the action
//
extern int CpuMuxDebugFlag;

SingleCpuMux::SingleCpuMux(int debug) : CpuMux(debug)
{
    CpuMuxDebugFlag = debug;
    pNameTemplate = "SingleCpuMux";
    iYam = 0;
    sprintf(nameSpace, "[%s-%d] ", pNameTemplate, iYam);
    pName = nameSpace;
}

SingleCpuMux::~SingleCpuMux()
{
    delete myCurrentEvents;
    myCurrentEvents = 0;
}
void
SingleCpuMux::add_(Thread *who)
{
    myCurrentEvents -> add( who );
}

void
SingleCpuMux::addToCpu_(int, Thread *x)
{
    add(x);
}

Thread *
SingleCpuMux::remove()
{
    return myCurrentEvents -> remove();
}

ThreadContainer*
SingleCpuMux::allocateThreadContainer()
{
    assert(0);
    abort();
    return((ThreadContainer*)NULL);     // some compilers require this
}

void
SingleCpuMux::fireItUp(int, unsigned long)
{
    pid = getpid();
    enabled = 1;
    
    currentThread = 0;
    
    raisedBy = 0;
    while( ! *terminated ) {
	currentThread = myCurrentEvents -> remove();
	if (currentThread == 0 ) {
	    return;
	}
	
	if ( CpuMux::Debug ) {
	    cerr << name() << " switch to ";
	    cerr << currentThread->name() << "\n";
	}
	
	systemTransfer( currentThread );
	currentThread = 0;
	CpuMux::handoff();
    }
}

