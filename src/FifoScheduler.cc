/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "Thread.h"
#include "FifoScheduler.h"
#include "assert.h"

FifoScheduler::ll *
FifoScheduler::freelist = 0;

SpinLock
FifoScheduler::freelistLock;

FifoScheduler::FifoScheduler(int defaultLength)
{
    head = NULL;
    last = NULL;
    length = 0;
}

inline FifoScheduler::ll*
FifoScheduler::alloc()
{
    freelistLock.reserve();
    ll *p;
    if ( freelist != NULL ) {
	p = freelist;
	freelist = freelist -> next;
	freelistLock.release();
    } else {
	freelistLock.release();
	p = new ll;
    }
    return p;
}

inline void
FifoScheduler::dispose(FifoScheduler::ll *p)
{
    freelistLock.reserve();
    p -> next = freelist;
    freelist = p;
    freelistLock.release();
}

void
FifoScheduler::add(Thread *t)
{
    ll *p = alloc();
    p -> thread = t;
    p -> next = NULL;
    length++;
    if ( head == NULL ) {
	head = last = p;
    } else {
	assert( last != NULL );
	last -> next = p;
	last = p;
    }
}

Thread *
FifoScheduler::remove()
{
    if ( head == NULL ) {
	return NULL;
    } else {
	ll *p = head;
	head = head -> next;
	length--;

	static Thread *t;
	t = p -> thread;
	dispose(p);

	//
	// Head may now be NULL, but last may be non-null. You can
	// only depend on value of last if you know head is non-null.
	//

return ( t );
//	return ( p -> thread );
    }
}

int FifoScheduler::isEmpty()
{
    if ( length == 0 ) {
	return 1;
    } else {
	return 0;
    }
}

unsigned int FifoScheduler::size()
{
    return(length);
}

Pix FifoScheduler::first()
{
    return( Pix(head) );
}

void FifoScheduler::next(Pix& i)
{
    ll *l = (ll *) i;
    i = Pix(l -> next);
}

Thread*&
FifoScheduler::operator()(Pix i)
{
    ll *l = (ll *) i;
    return(l -> thread);
}
