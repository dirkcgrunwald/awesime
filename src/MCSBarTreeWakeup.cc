/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "TreeBarTreeWakeup.h"

void 
TreeBarTreeWakeup::rendezVousAux(TREENODEPTR node, int *sense)
{

    node->lck.reserve();
    short oldvalue = node->count--;
    node->lck.release();
    if (oldvalue == 1) {
	if (node->parent) {
            rendezVousAux(node->parent,sense);
	}
	node->count = node->expected_count;

	node->locksense = (int)!node->locksense;

    }
    else {
        while (node->locksense != *sense);
    }
                              
}

void TreeBarTreeWakeup::rendezvous()
{


   int iYam = CpuMux::cpuId();

   if (++ThreadPiles[iYam].count == ThreadPiles[iYam].expected_count) {
      rendezVousAux(myLeaf[iYam], &mySense[iYam]);
      mySense[iYam] = (int)!(mySense[iYam]);
      ThreadPiles[iYam].count = 0;
      releaseAllOfMine(iYam);

   }
   else {
      CpuMux::Cpu()->reserveByException(this);
   }

}









