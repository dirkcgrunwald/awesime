#include "MultiFifoMux.h"
#include "FifoScheduler.h"

MultiFifoMux::MultiFifoMux(int debug) : MultiCpuMux(debug)
{
    //
    // Allocate event structures for first CPU
    //
    allocateLocalEventStructures();
}

MultiFifoMux::~MultiFifoMux()
{
    // do nothing
}

ThreadContainer *
MultiFifoMux::allocateThreadContainer()
{
    return ( new FifoScheduler(512) );
}
