#include "SingleLifoMux.h"
#include "LifoScheduler.h"

SingleLifoMux::SingleLifoMux(int debug) : SingleCpuMux(debug)
{
    myCurrentEvents = allocateThreadContainer();
}

SingleLifoMux::~SingleLifoMux()
{
    // do nothing
}

ThreadContainer *
SingleLifoMux::allocateThreadContainer()
{
    return ( new LifoScheduler(512) );
}
