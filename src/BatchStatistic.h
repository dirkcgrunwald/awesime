/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef BatchStatistic_h_
#define BatchStatistic_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include <Statistic.h>
#include <stream.h>


template<class T = double>
class BatchStatistic : public Statistic<T> {
    int _batchTrigger;
    Statistic<T> _batchSamples;
public:

  BatchStatistic( int trigger = 0 ) {
    _batchTrigger = trigger;
  }

  //
  // Export the batched statistics to the user
  //
  Statistic<T>& batch() {
    retun _batchSamples;
  }

  int batchTrigger() {
    return _batchTrigger;
  }

  void batchTrigger(int trigger) {
    _batchTrigger = trigger;
  }

  //
  // We add the sample to the subsample
  //
  virtual void operator+=(T value) {
    //
    // Add sample this this current batch
    //
    Statistic<T>::operator+=(value);
    //
    // If this triggers the batch, copy and reset the current
    //
    if ( n >= pBatchTrigger ) {
      make_batch();
    }
  }

  virtual void make_batch() {
    batchStats += mean();
    Statistic<T>::reset();
  }

  virtual void classPrintOn(ostream&) {
    st << batchStats.samples() << " " << batchStats.mean();
    st << " " << batchStats.stdDev() << " ";
    st << batchStats.confidence(90) << " " << batchStats.confidence(95);
    st << " " << batchStats.confidence(99);
  }

};

BatchStatistic::BatchStatistic(int trigger)
{
    pBatchTrigger = trigger;
}

inline ostream&
operator<<(ostream& strm, BatchStatistic& ob)
{
    ob.classPrintOn(strm);
    return strm;
}

#endif
