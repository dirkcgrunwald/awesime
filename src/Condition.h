/* 
Copyright (C) 1990 by David Wagner (wagner@bullwinkle.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1991 University of Colorado, Boulder, Colorado
//
// written by David Wagner (wagner@cs.colorado.edu)
//
#ifndef _Condition_h_
#define _Condition_h_

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Condition.h
//
//	This implements condition variables.
//
//	Note that because all accesses to a Condition are supposed to
//	be guarded by an external Mutex, there is no need for an internal
//	SpinLock.
//
//	When a thread blocks on
//	a Condition, the associated mutex is released automatically.
// 

#include "ReserveByException.h"
#include "ThreadContainer.h"
#include "Monitor.h"

class Condition : public ReserveByException {
protected:
    char* namePtr;
    Monitor* monitor;

    ThreadContainer *cScheduler;
    int iDidAlloc;
    
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);
    
public:

    Condition(Monitor*, ThreadContainer *scheduler);
    virtual ~Condition();

    virtual int size();

    // Assumption:  the caller is holding the associated mutex.
    virtual void wait();

    virtual void signal();
    virtual void broadcast();

    void set_name(char*);
    char* name();
	
};

inline void
Condition::set_name(char* namep)
{   namePtr = namep;
}

inline char*
Condition::name()
{   return namePtr;
}

#endif /* Condition_h */
