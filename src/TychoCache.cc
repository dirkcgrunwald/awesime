#include "TychoCache.h"
#include <memory.h>
#include <stdio.h>
#include "AwesimeConfig.h"
#include <assert.h>

int OutputDistance = 0;
int OutputVerbose = 0;

/////////////////////////////////////////////////////////////////////////////
// Global inline functions
/////////////////////////////////////////////////////////////////////////////
inline float
NONZERO(float i)
{
    if ( i == 0 ) {
	return 1.0;
    } else {
	return i;
    }
}

inline int
floor_lg(unsigned int num)
//
//  Computes the floor of the logarithm to base two of "num."
//
{
    int i;
    for (i = -1; num > 0; i++) num >>=1;
    return (i);
}

/////////////////////////////////////////////////////////////////////////////
// StackNode function
/////////////////////////////////////////////////////////////////////////////

class TychoStackNode {
public:
    TychoCache::IsValid valid;
    TychoTag tag;
    TychoStackNode *next;

    TychoStackNode() {
	tag = 0; next = NULL;
    }
    TychoStackNode(TychoCache::IsValid v, int t) {
	valid = v; tag = t; next = NULL;
    }
    
    static TychoStackNode *freelist;
    void *operator new(size_t size);
    void operator delete(void *);
};

TychoStackNode *
TychoStackNode::freelist = 0;

void *
TychoStackNode::operator new(size_t size)
{
    if ( freelist == 0 ) {
	return( (void *) new char[ size ]);
    } else {
	TychoStackNode *foo = freelist;
	freelist = freelist -> next;
	return(foo);
    }
}

void
TychoStackNode::operator delete(void *ptr)
{
    TychoStackNode *foo = (TychoStackNode *) ptr;
    foo -> next = freelist;
    freelist = foo;
}

/////////////////////////////////////////////////////////////////////////////
// TychoCache function
/////////////////////////////////////////////////////////////////////////////
inline TychoTag
TychoCache::movetotop(TychoStackNode *preptr, TychoStackNode *ptr, int stacknum)
{
  if (preptr!=NULL) {		// Not already TOS 
      preptr->next = ptr->next;	// Remove
      ptr->next = stack[stacknum].next; // Push 
      stack[stacknum].next = ptr;
  }
  return(stack[stacknum].tag);	// Return number in stack //
}

inline int
TychoCache::push(TychoStackNode *ptr, int stacknum)
{
  ptr->next = stack[stacknum].next;
  stack[stacknum].next = ptr;
  return(++(stack[stacknum].tag)); // Return number in stack //
}

inline TychoStackNode *
TychoCache::update_stack(int not_found, int stack_num,
			 TychoStackNode *ptr, TychoStackNode *preptr,
			 TychoCache::IsValid valid_bit,
			 TychoTag addr_tag)
{
    if ( not_found ) {
	ptr = new TychoStackNode(valid_bit, addr_tag);
	push(ptr, stack_num);
    } else {
	movetotop(preptr, ptr, stack_num);
    }
    return(ptr);
}


inline int
TychoCache::match(TychoTag tag1, TychoTag tag2, int diff_lg_sets)
{
  //
   //  Matches low-order bits.  For example, if diff_lg_sets==8:
   //
   //	tag1		tag2		match
   //	----		----		-----
   //	tag1		tag1		MAX_LGNUMSETS (33)
   //	0x0		0x1		0
   //	0xfff		0xffe		0
   //	0x0		0x2		1
   //	0xfff		0xffd		1
   //	0x0		0xfff0		4
   //	0x0		0xffc0		6
   //	0x0		0xff80		7
   //	0x0		0xff00		7 == diff_lg_sets - 1
   //	0x0		0xfe00		7 == diff_lg_sets - 1
   //

  TychoTag test =  tag1 ^ tag2;

  if (test & 1) return(0);	// A Perfect mismatch //
  if (test==0)  return(MAX_LGNUMSETS); // A Perfect match //

  int lg_set;

  for (lg_set = 1; lg_set < diff_lg_sets; lg_set++) {
    if (test & (1<<lg_set)) {
      return(lg_set);
    }
  }
 
  return(lg_set-1);
}	

void
TychoCache::update_distances(const int *above,
			     const int not_found,
			     const int stackdepth,
			     int refs_this_line)
{
    //  This function connects the two important arrays in this algorithm:
    //  metricp->distance[WORDSIZEINBITS][MAX_ASSOC+1] and above[MAX_ASSOC+1].
    //
    //  distance[lg_set,dist] holds the number of references to stack distance
    //  dist in a cache with 2^(lg_set+min_lgnumsets) sets.  Thus, the miss
    //  for a cache with 2^j sets and associativity k (set size in blocks) is
    //  1 - (sum i=1 to k of distance[j-min_lgnumsets,i])/num_references.
    //  Such a cache is of size (2^j)*k blocks.
    //
    //  above[lg_sets] is used the calculate stack distances. When a reference
    //  is found, its stack distance in a cache with 2^(lg_sets+min_lgnumsets)
    //  sets is (sum i=lg_sets to diff_lgnumsets of above[i])
    //
    
    //
    // If something  is not found, then we need to bump it once,
    if (not_found) {
	//
	// Redundant information:  distance[j,0] will hold the number
	//  of cold-start misses for each number of sets.
	//
	for (int lg_set = diff_lgnumsets - 1; lg_set >= 0; lg_set--) {

#if defined(DIRK_DEBUG)
	    fprintf(stderr,"n distance[%d][0] += %d\n", lg_set, refs_this_line);
#endif
	    distance[lg_set][0] += refs_this_line;
	}
#if defined(DIRK_DEBUG)
	fprintf(stderr,",\n");
#endif

    }
    else {
	//
	//  Found.
	//
	if (stackdepth > max_assoc_encountered) {
	    max_assoc_encountered = stackdepth;
	}
	
	int total_above = 0;
	for (int lg_set = diff_lgnumsets - 1; lg_set >= 0; lg_set--) {

	    total_above += above[lg_set];
	    
	    if (total_above>=max_assoc) {
		//
		//  Exceeded max_asscoc so pretend block
		//  was not found.
		//
#if defined(DIRK_DEBUG)
		fprintf(stderr,"FOO distance[%d][0] += %d\n",
			lg_set, refs_this_line);
#endif
		distance[lg_set][0] += refs_this_line;
	    }
	    else {

#if defined(DIRK_DEBUG)
		fprintf(stderr,"t distance[%d][%d] += %d\n",
			lg_set,
			total_above+1,
			refs_this_line);
#endif
		distance[lg_set][total_above+1] += refs_this_line;
	    }	
	}

#if defined(DIRK_DEBUG)
	fprintf(stderr,".\n");
#endif
    }
    
    num_references += refs_this_line;
    total_stack_depth += stackdepth;
}


void
TychoCache::for_each_reference(TychoAddr address, int refs_this_line)
{
    int above[MAX_LGNUMSETS];

    unsigned long blocknum = address >> blockshift;

    unsigned long last_blocknum
	= (address + (refs_this_line-1) * 4) >> blockshift;

    if ( blocknum != last_blocknum ) {
	fprintf(stderr,"something is seriously fucked\n");
    }


    TychoTag tag = blocknum >> min_lgnumsets;
    int set = blocknum & min_set_mask;
    
    while ( refs_this_line > 0 ) {
	//
	//	Initialize for this reference.
	//
	memset(above, 0, sizeof(int) * diff_lgnumsets);
    
	TychoStackNode *preptr = NULL;
	TychoStackNode *ptr = stack[set].next;
    
	int not_found = 1;
	int all_found = 1;
	int stackdepth;
	for (stackdepth = 0; ptr!=NULL && ptr->valid == Valid; stackdepth++) {
	
	    int num_lsb_matched = match(tag, ptr->tag, diff_lgnumsets);

	    if (num_lsb_matched < diff_lgnumsets) {
		//
		//  NOT a complete match
		//
		//  Increment "above" to record that another blocks has
		//  been found that is above the reference for caches
		//  with <= 2^(min_lgnumsets+numlsb_matched) sets.
		//
		above[num_lsb_matched]++;
		all_found = 0;
		preptr = ptr;
		ptr = ptr->next;
	    }
	    else {
		//
		//  A complete match -- the reference is found.
		//  Executed at most once per reference.
		//
		not_found = 0;
		break;
	    }
	}

	update_stack(not_found, set, ptr, preptr, Valid, tag);
	//
	// We we have to foll with the above array at all, process
	// the bliters one by one, else we can lump them into
	// one call...
	//
	if ( not_found | (!all_found) ) {
	    update_distances(above, not_found, stackdepth, 1);
	    refs_this_line--;
	} else {
	    update_distances(above, not_found, stackdepth, refs_this_line);
	    refs_this_line = 0;
	}
    }
}


// Remove node from a priority stack 
//
// affects: priority stacks
// returns: number left in stack "stacknum"
//
int
TychoCache::huh_remove(TychoStackNode *preptr, TychoStackNode *ptr, int stacknum)
{
    preptr->next = ptr->next;	// Remove //
    delete ptr;
    return(--(stack[stacknum].tag));
}


void
TychoCache::clean_up_stacks()
{
    // 	Remove from the stacks those nodes not in a cache with the
    //	maximum number of sets and the maximum associativity.
    //
    //  E.g.: 	max_lgnumsets	min_lgnumsets	num_sets_per	diff_set_mask
    //		-------------	-------------	------------	-------------
    //		9		3		64		0x1f
    // 		2		1		2		0x1
    // 		5		5		1		0x0
    //
    
    int num_sets_per_stack =  1 << (max_lgnumsets - min_lgnumsets);
    int diff_set_mask = num_sets_per_stack - 1;
    
    
    //
    //  For each stack (minimum number of sets)
    //
    for (unsigned int stacknum = 0; stacknum < num_stacks; stacknum++) {
	
	//
	// Init counters
	//
	int set_in_stack;
	
	for (set_in_stack=0;
	     set_in_stack < num_sets_per_stack;
	     set_in_stack++) {
	    
	    clean_up_counters[set_in_stack] = 0;
	}
	
	TychoStackNode *preptr = NULL;
	TychoStackNode *ptr = stack[stacknum].next;
	
	//
	// For each TychoStackNode
	//
	while (ptr!=NULL) {
	    
	    set_in_stack = ptr->tag & diff_set_mask;
	    
	    if (clean_up_counters[set_in_stack] <= max_assoc) {
		
		clean_up_counters[set_in_stack]++;
		
		// Go to next node
		preptr = ptr;
		ptr = ptr->next;
	    }
	    else {
		//
		// remove this node
		//
		huh_remove(preptr,ptr,stacknum);
		
		// Go to next node //
		ptr = preptr->next;
	    }
	}
    }
}

TychoCache::TychoCache( const int _blksize,
			const int _min_lns,
			const int _max_lns,
			const int _max_assoc
			)
{
    //
    //	Set num_stacks = 2^min_lgnumsets and min_set_mask to be 
    //	min_lgnumsets low-order ones.  E.g.:
    //
    //	min_lgnumsets	num_stacks	min_set_mask
    //	-------------	----------	------------
    //	0		1		0x0
    //	1		2		0x1
    //	2		4		0x3
    //	9		512		0x1ff
    //
    
    blocksize = _blksize;
    blockshift = floor_lg(blocksize);

    min_lgnumsets = _min_lns;
    max_lgnumsets = _max_lns;
    max_assoc = _max_assoc;
    
    //
    // Misc
    //
    num_stacks = 1 <<  min_lgnumsets;
    min_set_mask = num_stacks - 1;
    diff_lgnumsets = max_lgnumsets - min_lgnumsets + 1;
    max_lgassoc = floor_lg(max_assoc);

    assert( max_lgassoc >= 0 );
    assert( max_lgassoc < 32 );
    
    memset(distance, 0, sizeof(distance));
    memset(distance_double, 0, sizeof(distance_double));
    
    //
    // Init Metrics -- constructor initializes them to (0,NULL).
    //
    re_initmetric();
    max_assoc_encountered = 0;
    //
    // Init Stacks -- constructor initializes them to (0,NULL).
    //
    stack = new TychoStackNode[ num_stacks ];
}

void
TychoCache::re_initmetric()
{
    for (int lg_set = 0; lg_set < MAX_LGNUMSETS+1; lg_set++) {
	
	for (int assoc = 0; assoc < max_assoc+1; assoc++) {
	    distance [lg_set][assoc] = 0;
	    distance_double [lg_set][assoc] = 0.0;
	}
    }
    
    num_references = 0;
    num_references_double = 0.0;
    total_stack_depth = 0;
    total_stack_depth_double = 0.0;
}

void
TychoCache::checkintegrity(const int assoc_limit)
{
    int lg_set;
    int assoc;
    
    for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	
	unsigned int total = 0;
	for (assoc = 0; assoc <= assoc_limit; assoc++) {
	    total += distance[lg_set][assoc];
	}
 	
	if (total!=num_references) {
	    cerr << "\n---Error: ";
	    cerr << "Sum of the distance counts for 2^";
	    cerr << (min_lgnumsets+lg_set) << " sets is\n",
	    cerr << "          " << total << " != " << num_references;
	    cerr << ", the number of references.\n";
	}
    }
    
    if (diff_lgnumsets<MAX_LGNUMSETS) {
	lg_set = diff_lgnumsets;
	int total = 0;
	for (assoc = 0; assoc <= assoc_limit; assoc++) {
	    total += distance[lg_set][assoc];
	}
 	
	if (total!=0) {
	    cerr << "\n---Error: ";
	    cerr << "Out of bounds distance counts for 2^";
	    cerr << min_lgnumsets+lg_set << "sets are\n";
	    cerr << "not all zero, but rather sum to" << total << ".\n";
	}
    }
    
    if (assoc_limit < max_assoc) {
	assoc = assoc_limit + 1;
	
	int total = 0;
	for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	    total += distance[lg_set][assoc];
	}
 	
	if (total!=0) {
	    cerr << "\n---Error: ";
	    cerr << "Out of bounds distance counts for ";
	    cerr << "to much assocatiivity are";
	    cerr <<"not all zero, but rather sum to" << total << ".\n";

	}
    }
}

void
TychoCache::checkintegrity_double(const int assoc_limit)
{
    int lg_set;
    int assoc;
    double total;
    
    for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	
	total = 0.0;
	for (assoc = 0; assoc <= assoc_limit; assoc++) {
	    total += distance_double[lg_set][assoc];
	}
 	
	// if abs(difference) > 0.4 ..
	if ( ((total - num_references_double) > 0.4) |
	     ((total - num_references_double) < -0.4)) {
	    fprintf(stderr, "\n---Error: ");
	    fprintf(stderr, "Sum of the distance_double counts for 2^%d sets is\n",
		    (min_lgnumsets+lg_set));
	    fprintf(stderr, "          ");
	    fprintf(stderr, "%.3f != %.3f, the number of references.\n",
		    total,num_references_double);
	}
    }
    
    if (diff_lgnumsets<MAX_LGNUMSETS) {
	lg_set = diff_lgnumsets;
	total = 0.0;
	for (assoc = 0; assoc <= assoc_limit; assoc++) {
	    total += distance_double[lg_set][assoc];
	}
 	
	if (total > 0.4) {
	    fprintf(stderr, "\n---Error: ");
	    fprintf(stderr, "Out of bounds distance_double counts for 2^%d sets are\n",
		    (min_lgnumsets+lg_set));
	    fprintf(stderr, "          ");
	    fprintf(stderr, "not all zero, but rather sum to %.3f.\n", total);
	}
    }
    
    if (assoc_limit < max_assoc) {
	assoc = assoc_limit + 1;
	
	total = 0.0;
	for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	    total += distance_double[lg_set][assoc];
	}
 	
	if (total > 0.4) {
	    fprintf(stderr, "\n---Error: ");
	    fprintf(stderr, 
		    "Out of bounds distance_double counts for too much associativity are\n");
	    fprintf(stderr, "          ");
	    fprintf(stderr, "not all zero, but rather sum to %.3f.\n", total);
	}
    }
}


void
TychoCache::accumulate_metric()
{
    int lg_set; 
    int assoc;
    
    for (lg_set = 0; lg_set < MAX_LGNUMSETS+1; lg_set++) {
	for (assoc = 0; assoc < max_assoc+1; assoc++) {
	    distance_double [lg_set][assoc] += distance [lg_set][assoc];
	    distance [lg_set][assoc] = 0;
	}
    }
    
    num_references_double += num_references;
    num_references = 0;
    total_stack_depth_double += total_stack_depth;
    total_stack_depth = 0;
}


void
TychoCache::flushcache()		// Flush cache //
{
    unsigned int stacknum;
    
    for (stacknum=0; stacknum < num_stacks; stacknum++) {
	TychoStackNode *last = &(stack[stacknum]);
	
	if (last->next==NULL)		// Any to free? //
	    break;
	else {
	    TychoStackNode *first = last->next;
	    last->next = NULL;
	    last = first;
	    //
	    //	See how many nodes to free.
	    //
	    while (last -> next != NULL ) {
		TychoStackNode *next = last -> next;
		delete last;
		last = next;
	    }
	    break;
	}
    }
    
}

void
TychoCache::fini(ostream& out)
{
    flushcache();
    outputmetrics(out);
    outputmissratios(out);
}

void
TychoCache::outputmetrics(ostream& out)
{
    int lg_set;
    int assoc;
    int assoc_limit;
    int last_nonzero;
    
    //
    //  Put totals from ints to doubles
    //
    accumulate_metric();
    
    //
    //  assoc_limit is the minimum of the two.
    //
    if (max_assoc < max_assoc_encountered) {
	assoc_limit = max_assoc;
    }
    else {
	assoc_limit = max_assoc_encountered;
    }
    
    out << "\n";
    out << "(NumRefs " << setw(20) << num_references_double << "), ";
    out << "(MeanStack "
	<< (total_stack_depth_double/NONZERO(num_references_double)) << "), ";
    out << "(MaxStack " << max_assoc_encountered << ")\n";
    out << "(end parameters)\n\n";
    
    if ( OutputDistance ) {
	
	out << "Distance	Number of sets\n";
	
	out << "Counts	";
	for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	    out << "	2^" << min_lgnumsets+lg_set;
	}
	out << "\n";
	
	out << "------	";
	for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
	    out << "	----";
	}
	out << "\n";
	
	for (assoc = 0; assoc <= assoc_limit; assoc++) {
	    
	    last_nonzero = -1;
	    for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
		if (distance_double[lg_set][assoc] > 0.4) {
		    last_nonzero = lg_set;
		}
	    }
	    
	    if ( OutputVerbose ) {
		//
		//  Print all rows and columns
		//
		out << "(d\t" << assoc;
		for (lg_set = 0; lg_set < diff_lgnumsets; lg_set++) {
		    out << "\t";
		    out << setprecision(0);
		    out << distance_double[lg_set][assoc];
		}
		out << "\n";
	    }
	    else {
		if (last_nonzero > -1) {
		    //
		    //  Print rows with some non-zeros and print columns
		    //  until only zeros are left.
		    //
		    out << "(d\t" << assoc;
		    for (lg_set = 0; lg_set <=last_nonzero;	lg_set++) {
			out << "\t";
			out << setprecision(0);
			out << distance_double[lg_set][assoc];
		    }
		    out << "\n";
		}
	    }
	    
	    
	}
    } // end if (output>=WITHDIST) //
    
    out << "(end Kepler read)\n";
    
    checkintegrity(assoc_limit);
    checkintegrity_double(assoc_limit);
    out << "\n";
    
}


void
TychoCache::outputmissratios(ostream& out)
{
    int lg_set, lg_set_inner, lg_assoc, assoc;
    
    //
    //  Put totals from ints to doubles
    //
    accumulate_metric();
    
    //
    //  Compute Misses
    //
    
    double miss[MAX_LGNUMSETS+1][MAX_ASSOC+1];

    for (lg_set=0; lg_set<diff_lgnumsets; lg_set++) {
	for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	    
	    // assume all misses //
	    double misses = num_references_double;
	    
	    // subtract hits //
	    for (assoc=1;
		 (assoc<=max_assoc) & (assoc<=(1<<lg_assoc));
		 assoc++) {
		misses -= distance_double[lg_set][assoc];
	    }

	    miss[lg_set][lg_assoc] = misses;
	}
    }
    
    //
    //  Print Miss Ratios
    //
    
    out << "\n";
    out << "Miss Ratios:\n";
    
    out << "\n";
    out << "Cache Size" << setw(20) << "Associativity\n";
    
    out << "  (bytes)";
    //
    // A total of 12 columns for each value. 4 columns seperate, followed
    // by the label in 4 columns, then another 4
    //
    for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	out << setw(3) << " " << setw(6) << (1<<lg_assoc) << setw(3) << " ";
    }
    out << "\n";
    
    out << "----------";
    for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	out << "     ------  ";
    }
    out << "\n";
    
    
    for (lg_set=0; lg_set<diff_lgnumsets+max_lgassoc; lg_set++) {
	
	//
	// This should be 7 columns..
	//
	out << setw(6);
	print_cachesize(out,
			(1<<(lg_set+min_lgnumsets))*blocksize);
	//
	// Add 4 more..
	//
	out << "    ";
	
	lg_set_inner = lg_set;
	
	for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	    
	    if ((lg_set_inner>=0) & (lg_set_inner<diff_lgnumsets)) {
		
		double miss_ratio = miss[lg_set_inner][lg_assoc]
		    / NONZERO(num_references_double);
		out << "   ";
		out << setw(6) << setprecision(5);
		out.setf(ios::fixed | ios::showpoint, ios::floatfield);
		out << miss_ratio * 100.0 ;
		out << "%  ";
	    }
	    else {
		out << "   --------  ";
	    }
	    lg_set_inner--;
	}
	out << "\n";
    }
    out << "\n";
    
}

void
TychoCache::outputmissdata(ostream& out, char *title)
{
    int lg_set, lg_set_inner, lg_assoc, assoc;
    
    //
    //  Put totals from ints to doubles
    //
    accumulate_metric();
    
    out << title << "_NumRefs -> ";
    out << (unsigned long) num_references_double << "\n";

    //
    //  Compute Misses
    //
    
    double miss[MAX_LGNUMSETS+1][MAX_ASSOC+1];
    for (lg_set=0; lg_set<diff_lgnumsets; lg_set++) {
	for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	    
	    // assume all misses //
	    double misses = num_references_double;
	    
	    // subtract hits //
	    for (assoc=1;(assoc<=max_assoc) & (assoc<=(1<<lg_assoc));assoc++) {
		misses -= distance_double[lg_set][assoc];
	    }

	    miss[lg_set][lg_assoc] = misses;
	}
    }
    
    for (lg_set=0; lg_set<diff_lgnumsets+max_lgassoc; lg_set++) {
	lg_set_inner = lg_set;
	
	for (lg_assoc=0; lg_assoc<=max_lgassoc; lg_assoc++) {
	    if ((lg_set_inner>=0) & (lg_set_inner<diff_lgnumsets)) {
		
		double miss_ratio = miss[lg_set_inner][lg_assoc]
		    / NONZERO(num_references_double);

		out << title << "_MISSES_";
		print_cachesize(out, (1<<(lg_set+min_lgnumsets))*blocksize);
		out << "_";
		out << (1 << lg_assoc) << " -> ";
		out <<  (unsigned long) miss[lg_set_inner][lg_assoc];
		out << "\n";

		out << title << "_MISSRATE_";
		print_cachesize(out, (1<<(lg_set+min_lgnumsets))*blocksize);
		out << "_";
		out << (1 << lg_assoc) << " -> ";
		out << miss_ratio * 100.0 ;
		out << "\n";
	    }
	    lg_set_inner--;
	}
    }
}

int
TychoCache::print_cachesize(ostream& out, int cachesize)
//
//  Inverse of atoiKMG(), converts int to string using suffixes:
//
//      K    1024
//      M    1024//2 = 1048576
//      G    1024//3 = 1073741824
//
//  E.g., int 65536 is changed to string "64K" and printed.
//
{
    int prefix;
    
    if (cachesize >= 1073741824) {
	prefix = cachesize / 1073741824;
	out << prefix << "G";
	return(prefix);
    }
    
    if (cachesize >= 1048576) {
	prefix = cachesize / 1048576;
	out << prefix << "M";
	return(prefix);
    }
    
    if (cachesize >= 1024) {
	prefix = cachesize / 1024;
	out << prefix << "K";
	return(prefix);
    }
    
    prefix = cachesize;
    out << prefix << " ";  
    return(prefix);
}
