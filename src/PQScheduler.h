#ifndef _PQScheduler_h_
#define _PQScheduler_h_

#include <ThreadContainer.h>
#include <Thread.h>
#include <algorithm>
#include <functional>
#include <utility>
#include "CUT_FibHeap.h"

class PQScheduler : public ThreadContainer {

  typedef greater<ThreadPriority> gt;
  typedef CUT_FibHeap<ThreadPriority, Thread*, gt> myFibHeapType;
  myFibHeapType pq;


public:

    PQScheduler(int seed1=0, int seed2=2);
  virtual ~PQScheduler() {
    // Empty
  }

    virtual void add(Thread *t);
    virtual Thread* remove();

    virtual int isEmpty();
    virtual unsigned size();

    virtual Pix first();
    virtual void next(Pix&);
    virtual Thread*& operator()(Pix);

};
		     
#endif
