/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/

#ifdef __GNUG__
#  pragma implementation
#endif

#include "OwnedFacility.h"
#include "FifoScheduler.h"
#include "SimMux.h"
#include "Thread.h"
#include "assert.h"
#include <math.h>

OwnedFacility::OwnedFacility(int xservers, ThreadContainer *xscheduler)
    : Facility(xservers, xscheduler)
{
    if (servers == 1) {
	beingServiced.single = NULL;
    } else {
	beingServiced.many = new OwnedFacilityKey[servers];
	for (register int i = 0; i < servers; i++) {
	    beingServiced.many[i] = NULL;
	}
    }
}

//
//	Facility will check that OwnedFacility is de-allocated
//
OwnedFacility::~OwnedFacility()
{
}

void
OwnedFacility::commonReserve( double delayTime, OwnedFacilityKey xkey)
{
    dataLock.reserve();

    pTotalReserves++;
    pTotalDelay += delayTime;

    if (servers == 1) {
	if (whenServiced.single == SimMux::NullTime()) {
	    whenServiced.single = SimMux::CurrentTime();
	    beingServiced.single = xkey;
	    dataLock.release();
	    return;
	}
    } else {
	for (int i = 0; i < servers; i++) {
	    if (whenServiced.many[i] == SimMux::NullTime()) {
		beingServiced.many[i] = xkey;
		whenServiced.many[i] = SimMux::CurrentTime();
		dataLock.release();
		return;
	    }
	}
    }
    
    dataLock.release();	// need to release so reportErrorState works

    reportErrorState(cerr);
    cerr << "[OwnedFacility] state error with facility semaphore";
    abort();
}

void OwnedFacility::reserve()
{
    double startedReserve = SimMux::CurrentTime();
    Semaphore::reserve();
    commonReserve(SimMux::CurrentTime() - startedReserve,
		  CpuMux::CurrentThread() );
}

int OwnedFacility::reserveNoBlock()
{
    if (Semaphore::reserveNoBlock()) {
	commonReserve(0.0, CpuMux::CurrentThread());
	return(1);
    } else {
	return(0);
    }
}

void OwnedFacility::release()
{
    releaseKeyed(CpuMux::CurrentThread());
}

void OwnedFacility::reserveKeyed(OwnedFacilityKey xkey)
{
    double start = SimMux::CurrentTime();
    Semaphore::reserve();
    commonReserve( SimMux::CurrentTime() - start, xkey);
}
    
    
void OwnedFacility::releaseKeyed(OwnedFacilityKey xkey)
{
    dataLock.reserve();

    double now = SimMux::CurrentTime();
    int error;
    
    if (servers == 1) {
	if (whenServiced.single != SimMux::NullTime()) {
	    totalServiceTime += (now - whenServiced.single);
	    whenServiced.single = SimMux::NullTime();
	    error = beingServiced.single != xkey;
	} else {
	    error = 1;
	}
    } else {
      int i;
	for (i = 0; i < servers; i++) {
	    if (whenServiced.many[i] != SimMux::NullTime()
		&& beingServiced.many[i] == xkey) {
		totalServiceTime += (now - whenServiced.many[i]);
		whenServiced.many[i] = SimMux::NullTime();
		break;
	    }
	}
	error = (i == servers);
    }
    
    dataLock.release();

    if (error) {
	cerr << " Attempted to release an un-reserved facility with key ";
	cerr << xkey << "\n";
	reportErrorState(cerr);
	exit(1);
    }

    Semaphore::release();
}

#ifdef UNDEF
void OwnedFacility::classPrintOn(ostream &out)
{
    out << "OwnedFacility with " << activeServers() << " active servers and "
	<< queueLength() << " queued requests";
}
#endif
