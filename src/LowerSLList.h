//-*-c++-*-
#ifndef _LowerSLList_h_
#define _LowerSLList_h_

#include "LockedSLList.h"
#include "Semaphore.h"
#include <FifoScheduler.h>

template <class T>
class LowerSLList : public LockedSLList<T>
{
  protected:
    FifoScheduler lowerBoundLockFifo;
    Semaphore lowerBoundLock;

  public:
    //
    // The constructors are unlocked because the user shouldn't
    // be able to call methods until after the constructor
    // has finished
    //
    LowerSLList() : LockedSLList<T>(),
    lowerBoundLock(0, &lowerBoundLockFifo),
    lowerBoundLockFifo(2) {	 
     LockedSLList<T>::iterating = 0;
    }

    LowerSLList(const SLList<T>& a) : LockedSLList<T>(),
    lowerBoundLock(0, &lowerBoundLockFifo),
    lowerBoundLockFifo(2) {
	    copy(a);
	    LockedSLList<T>::iterating = 0;
	}


    Pix prepend(T& item) {
	LockedSLList<T>::lock();
	Pix ret = BaseSLList::prepend(&item);
	LockedSLList<T>::unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix append(T& item) {
	LockedSLList<T>::lock();
	Pix ret = BaseSLList::append(&item);
	LockedSLList<T>::unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix prepend(SLNode<T>* node) {
	LockedSLList<T>::lock();
	Pix ret = BaseSLList::prepend(node);
	LockedSLList<T>::unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix append(SLNode<T>* node) {
	LockedSLList<T>::lock();
	Pix ret = BaseSLList::append(node);
	LockedSLList<T>::unlock();
	lowerBoundLock.release();
	return ret;
    }

    Pix ins_after(Pix p, T& item) {
	LockedSLList<T>::lock();
	Pix x = BaseSLList::ins_after(p, &item);
	LockedSLList<T>::unlock();
	lowerBoundLock.release();
	return x;
    }
    
    int remove_front(T& x) {
	lowerBoundLock.reserve();
	LockedSLList<T>::lock();
	int y = BaseSLList::remove_front(&x);
	LockedSLList<T>::unlock();
	return y;
    }

    T remove_front() {
	lowerBoundLock.reserve();
	LockedSLList<T>::lock();
	T dst; BaseSLList::remove_front(&dst, 1);
	LockedSLList<T>::unlock();
	return dst;
    }
};


#endif
