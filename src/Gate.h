/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef Gate_h
#define Gate_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Gate.h
//	The gate is initially closed. Gates can be open or closed,
//	everyone waits when it's closed, everyone passes when it's
//	open. Unlike events, Gates don't fiddle with the state --
//	it's controlled by external tasks.
// 

#include <CpuMux.h>
#include <ReserveByException.h>
#include <FifoScheduler.h>
#include <SpinLock.h>

typedef enum {GateLocked, GateUnlocked} GateState;

class Gate : public ReserveByException {
    SpinLock lock;
    GateState pState;

    FifoScheduler pPileOfThreads;
    
    void hiddenWait();
    
private:
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);
    
public:
    
    Gate(GateState startState = GateLocked);
    virtual ~Gate();
    
    //
    //	Number of taskings waiting
    //
    int size();
    //
    //	GateLocked or GateUnlocked
    //
    GateState state();

    void wait();
    void open();
    void close();
};

inline
Gate::Gate(GateState startState)
{
    pState = startState;
}


inline GateState
Gate::state()
{
    lock.reserve();
    GateState s = pState;
    lock.release();
    return( s );
}


inline int
Gate::size()
{
    lock.reserve();
    int s = pPileOfThreads.size();
    lock.release();
    return(s);
}

inline void
Gate::wait()
{
    lock.reserve();
    GateState s = pState;
    lock.release();
    if ( s == GateLocked ) {
	hiddenWait();
    }
}

inline void
Gate::close()
{
    lock.reserve();
    pState = GateLocked;
    lock.release();
}

#endif /* Gate_h */
