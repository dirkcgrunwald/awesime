
void* realloc(void* mem, unsigned int bytes)
{
    if ( mem == 0 ) {
	return ( malloc(bytes) );
    } else {
	void *newmem = malloc(bytes);
	bcopy(mem, newmem, bytes);
	free(mem);
	return( newmem );
    }
}


void* calloc(unsigned int n, unsigned int elem_size)
{
  unsigned int sz = n * elem_size;
  void* p = malloc(sz);
  bzero(p, sz);
  return p;
}

void cfree(void * ptr)
{
    HardwareMemoryAllocator.free( (void *) ptr );
}


void* valloc(unsigned int bytes)
{
    //
    // if you ever free this memory, you're in deep shit.
    //

    static unsigned int malloc_pagesize = 0;

    if (malloc_pagesize == 0) malloc_pagesize = getpagesize();

    bytes = bytes + malloc_pagesize;

    char *m = malloc(bytes);
    //
    // this assumes malloc_pagesize is a p^2
    //
    unsigned long  f = long(m) + malloc_pagesize;
    unsigned long b = malloc_pagesize -1 ;
    m = (char *) (f & b);
    return( m ) ;
}


