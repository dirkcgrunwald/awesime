// -*-C++-*-
#ifndef _PSqRandom_h_
#define _PSqRandom_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include "Random.h"
#include "Histogram.h"

class PSqHistogram;

class PSqRandom : public Random {
  PSqHistogram *distribution_;
 public:

  PSqRandom(PSqHistogram *distribution, RNG *);
  void distribution(PSqHistogram *);
  PSqHistogram *distribution();
  virtual double operator()();
};

#endif
