// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
//
// Define the following things:
//	FIFO_NAME	-- the name of the fifo type
//	FIFO_INDEX	-- the index type; defaults to unsigned short
//	FIFO_ITEM	-- the type name for an item; should be typedefed
//	FIFO_INDEX_NULL	-- the null value for the heap index; defaults to 65535
//	FIFI_BASE_CLASS -- the base class for this derived FIFO
//

#include <Awesime.h>
#include <Generic.h>

#ifndef FIFO_INDEX
#define FIFO_INDEX unsigned int
#endif

#ifndef FIFO_INDEX_NULL
#define FIFO_INDEX_NULL 0xffff
#endif

#ifndef FIFO_BASE_CLASS
#define FIFO_BASE_CLASS public Awesime
#endif

typedef FIFO_ITEM GENERIC2(FIFO_NAME,Item);
typedef FIFO_INDEX GENERIC2(FIFO_NAME,Index);

class FIFO_NAME : FIFO_BASE_CLASS {
    
    GENERIC2(FIFO_NAME,Item) *list;
    GENERIC2(FIFO_NAME,Index) allocatedSize;
    GENERIC2(FIFO_NAME,Index) listHead;
    GENERIC2(FIFO_NAME,Index) listTail;

    char *pValid;
    unsigned int listElements;
    
    GENERIC2(FIFO_NAME,Index) advance(GENERIC2(FIFO_NAME,Index) i);

public:
    
    FIFO_NAME(int defaultLength = 0, int xdebug = 0);
    virtual ~FIFO_NAME();
    
    void reSize(unsigned int howMany);
    
    //
    // Operations on the list
    //
    virtual void add(GENERIC2(FIFO_NAME,Item) *t);
    virtual int remove(GENERIC2(FIFO_NAME,Item) *item);
    virtual int removeIfFound(GENERIC2(FIFO_NAME,Item)* item);
    
    //
    //	The next four members allow you to search a FIFO_NAME and mark
    //	items as invalid. 
    //
    //	doStart initilizes the index and returns the first item in the list.
    //	doNext moves to the next item and returns that item.
    //	Both return a int indicating whether the value in item has any
    //	meaning. When int=0, you have searched everything.
    //  Call doDone at the end to insure compatibility with subclasses.
    //

    virtual int doStart( GENERIC2(FIFO_NAME,Index) &index,
			 GENERIC2(FIFO_NAME,Item)* item);
    virtual int doDelete(GENERIC2(FIFO_NAME,Index) &index);
    virtual int doNext( GENERIC2(FIFO_NAME,Index) &index,
			GENERIC2(FIFO_NAME,Item)* item);
    virtual void doDone();
    

    int valid(GENERIC2(FIFO_NAME,Index) index);
    GENERIC2(FIFO_NAME,Item) & item(GENERIC2(FIFO_NAME,Index) i);
    GENERIC2(FIFO_NAME,Index) maxIndex();

    virtual int isEmpty();
    virtual unsigned int size();
    
    virtual void classPrintOn(ostream& s);
};

inline GENERIC2(FIFO_NAME,Index) FIFO_NAME::advance(GENERIC2(FIFO_NAME,Index) i)
{
    return( ((i+1) >= allocatedSize) ? (0) : (i+1) );
}

inline int FIFO_NAME::valid(GENERIC2(FIFO_NAME,Index) index)
{
    return ( (int) (pValid[index] != 0) );
}

inline GENERIC2(FIFO_NAME,Item) & FIFO_NAME::item(GENERIC2(FIFO_NAME,Index) i)
{
    return( list[i] );
}

inline GENERIC2(FIFO_NAME,Index) FIFO_NAME::maxIndex()
{
    return(allocatedSize);
}


#undef FIFO_NAME
#undef FIFO_ITEM
#undef FIFO_KEY
#undef FIFO_INDEX
#undef FIFO_BASE_CLASS
