//
// This hasn't been re-integrated yet
//
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "MultiSimMux.h"
#include "CpuMuxP.h"
#include "HardwareContextP.h"
#include "SpinLock.h"
#include "SpinBarrier.h"
#include "SpinFetchAndOp.h"
#include "Thread.h"
#include "ThreadContainer.h"
#include "ReserveByException.h"
#include "RSTree.h"
#include "OEv.h"
#include <math.h>
#include <signal.h>
#include <stdlib.h>

extern "C" {
#if 0 // RTM July 10 1992
    extern void mkdir(char*, int);
#endif
    extern int wait(void*);
    extern moncontrol(int);
};

// Each CPU has its own event list, though idle CPUs can
// poach from busy ones.
struct CPUEvents{
  SEvPQ *events;
  SpinLock lock;
  int dummy[6]; // use up a whole cache block.
};
static CPUEvents events[_MAXIMUM_CPU_MUXS_];

// Are there thought to be threads for the current time in
// the Events list? This errs on the side of optimism; if
// it is zero, there surely are not events waiting. If it
// is one, there might be.
static volatile int CurrentEventsExist;

// Count of the number of CPUs running current events; the
// rest are waiting for either more events to appear for
// this time, or for time to advance. Time should only
// advance if nobody is running events for the current time.
static volatile int ActiveCPUs;

// ActiveCPUs, and the advancement of the current time,
// are protected by TimeLock.
static SpinLock TimeLock;

#define LE(cpu) { events[cpu].lock.reserve(); }
#define UE(cpu) { events[cpu].lock.release(); }

static SpinBarrier CpuBarrier(1);

MultiSimMux::MultiSimMux(bool debug) : SimMux(debug)
{
    pNameTemplate = "MultiSimMux";
    iYam = 0;
    CpuMux::Debug = debug;
    allocateLocalEventStructures(0, 1);
}

MultiSimMux::~MultiSimMux()
{
}

void
MultiSimMux::allocateLocalEventStructures(int newIYam, int outOf)
{
    iYam = newIYam;
    sprintf(nameSpace, "[%s-%d] ", pNameTemplate, iYam);
    pName = nameSpace;
    
    CERR_PRE;
    cerr << name() << "Allocate CpuMux structures for new CPU (";
    cerr << newIYam << ")\n";
    CERR_POST;

    CERR_PRE;
    cerr << name() << "set CpuMux::Muxs to " << outOf << "\n";
    CERR_POST;

    events[iYam].events = new SEvSplayPQ();

    TimeLock.reserve();
    ActiveCPUs += 1;
    TimeLock.release();
    
    CpuMux::MuxsLock.reserve();
    CpuMux::Muxs = outOf;
    CpuMux::MuxsLock.release();
}

//
// Assumes iYam != 0
//
void
MultiSimMux::deallocateEventStructures()
{
    assert( iYam != 0 );

    CERR_PRE;
    cerr << name() << "Deallocate CpuMux structures for " << iYam << "\n";
    CERR_POST;

    delete events[iYam].events;
    events[iYam].events = 0;

    TimeLock.reserve();
    ActiveCPUs -= 1;
    TimeLock.release();
    
    CpuMux::MuxsLock.reserve();
    CpuMux::Muxs--;
    CpuMux::MuxsLock.release();
    
    CERR_PRE;
    cerr << name() << "set CpuMux::Muxs to " << CpuMux::Muxs;
    cerr << "\n";
    CERR_POST;
}

static void YouKillMe(int)
{
    cerr << "Change in child state - has child has died?\n";
}

void
MultiSimMux::fireItUp(int cpus, unsigned shared)
{
    assert(cpus > 0);
    
    if ( cpus > _MAXIMUM_CPU_MUXS_ ) {
	cpus = _MAXIMUM_CPU_MUXS_;
    }
    
    CERR_PRE;
    cerr << name() << "Allocate " << shared << " bytes of shared memory\n";
    CERR_POST;
    
    if ( cpus > 1 ) {
	extern void SharedMemoryInit( unsigned );
	SharedMemoryInit( shared * sizeof(HardwareContextQuad) );
    }

    //
    // Set the barrier height so everyone can rendezvous..
    //
    CpuBarrier.height(cpus);
    
    if ( iYam == 0 ) {
	signal(SIGCHLD, YouKillMe);
    }

    warmThePot(cpus);
    
    CERR_PRE;
    cerr << name() << " returns from warmThePot, join barrier\n";
    CERR_POST;
    
    CpuBarrier.rendezvous();
    
    stirItAround();

    if ( iYam == 0 ) {
	signal(SIGCHLD, SignalDefault);
    }

    coolItDown();
}

void
MultiSimMux::warmThePot(int cpus)
{
    assert(cpus > 0);
    CpuMux::Muxs = cpus;
    enabled = 1;
    
    //
    //	Spawn the children, giving each a unique number from 0..(cpus-1).
    //  The first child gets id (cpus-1), and the original process gets 0.
    //
    
    iYam = 0;

    CERR_PRE;
    cerr << name() << "Allocate " << CpuMux::Muxs << " cpus\n";
    CERR_POST;
    
    for (int whoAmI = 1; whoAmI < CpuMux::Muxs; whoAmI++) {
	if (iYam == 0) {
	    int childPid = fork();
	    
	    if (childPid == 0) {	// child 
		
		CERR_PRE;
		cerr << getpid() << " is born,";
		CERR_POST;

		moncontrol(0);
		allocateLocalEventStructures(whoAmI, CpuMux::Muxs);
		break;
	    } else {
		
		CERR_PRE;
		cerr << name() << "Parent spawns child "<< childPid << "\n";
		CERR_POST;
		
		if ( pid == -1 ) {
		    cerr << "Error in spawn:\n";
		    perror("fork");
		    exit(99);
		}
	    }
	}
    }
    pid = getpid();
    
    CERR_PRE;
    cerr << name() << "I am now id " << iYam << " and pid " << pid <<" \n";

    //
    // give each child a distinct temporary directory for core dumps
    // when debugging.
    //

    char tempName[L_tmpnam];
    tmpnam(tempName);
    mkdir(tempName,0777);
    int xx = chdir(tempName);

    cerr << name() << "change dir to " << tempName;
    if ( xx >= 0 ) {
	cerr << " worked\n";
    } else {
	cerr << " did not work\n";
    }

    CERR_POST;

}

void
MultiSimMux::coolItDown()
{
    if (iYam > 0) {
	
	CERR_PRE;
	cerr << name() << "exit\n";
	CERR_POST;
	
	deallocateEventStructures();
	_exit(0);
    }
    else {
	//
	//	reap the dead children. This way we know they are all dead.
	//	The caller can then safely exit.
	//
	while (CpuMux::Muxs > 1) {
	    int pid = wait(0);
	    if (pid == -1) {
		perror("wait");
		break;
	    }
	}
	//
	//  In case of break in above loop
	//
	CpuMux::Muxs = 1;
    }
}

void
MultiSimMux::add_(Thread *who)
{
    addAt_(who, CurrentSimulatedTime);
}

void
MultiSimMux::addAt_(OEv *who, SimTimeUnit when)
{
  assert(0);
  abort();
}

void
MultiSimMux::addAt_(Thread *who, SimTimeUnit when)
{
  if(when <= CurrentSimulatedTime)
    when = CurrentSimulatedTime;

  LE(iYam);

  events[iYam].events->enq(SEv(who, when));

  if(when == CurrentSimulatedTime)
    CurrentEventsExist = 1;

  UE(iYam);
}

// Try to return a suitable thread to run.
// If there is an event waiting for the current time, return it.
// If there is an event waiting for a future time, and there
// are no CPUs running events at the current time, advance the time,
// and notify other CPUs that there might be threads to run.
// If doWait is not set, returns 0 if it cannot immediately find
// a thread. If doWait is set, returns 0 only if the CPU
// is terminated.
//
// If enqThis is not null, put it on this CPU's event list.
Thread *
MultiSimMux::remove(int doWait, Thread *enqThis, SimTimeUnit enqWhen)
{
  Thread *t = 0;
  SEv ev;
  int cpu, firstCpu;
  SimTimeUnit firstTime;
  
  while(*terminated == 0){
    // Check for a current event in each CPU's queue, starting
    // with ourselves.
    cpu = iYam;
    do{
      LE(cpu);
      if(cpu == iYam && enqThis){
	events[cpu].events->enq(SEv(enqThis, enqWhen));
	enqThis = 0;
      }
      if(events[cpu].events->length() > 0){
	ev = events[cpu].events->front();
	if(ev.time() == CurrentSimulatedTime){
	  events[cpu].events->del_front();
	  t = ev.thread();
	  assert(t);
	}
      }
      UE(cpu);
      
      cpu++;
      if(cpu >= CpuMux::Muxs)
	cpu = 0;
    } while(t == 0 && cpu != iYam);
    
    if(t || doWait == 0)
      return(t);
    
    // There don't seem to be any threads for the current time,
    // although one might have slipped in. So advance the time
    // if we're the only active CPU. Otherwise wait for somebody
    // else to do so.

    TimeLock.reserve();

    if(ActiveCPUs == 1){
      firstTime = 0;
      firstCpu = -1;
      for(cpu = 0; cpu < CpuMux::Muxs; cpu++){
	if(events[cpu].events->length() > 0){
	  ev = events[cpu].events->front();
	  if(firstCpu == -1 || ev.time() < firstTime){
	    firstCpu = cpu;
	    firstTime = ev.time();
	  }
	}
      }
      
      assert(firstCpu != -1);
      CurrentSimulatedTime = firstTime;

      TimeLock.release();

      CurrentEventsExist = 1;
    } else {
      CurrentEventsExist = 0;
      ActiveCPUs -= 1;
      assert(ActiveCPUs > 0);

      TimeLock.release();

      while(CurrentEventsExist == 0 && *terminated == 0)
	;

      TimeLock.reserve();
      ActiveCPUs++;
      TimeLock.release();
    }
  }

  return(0);
}

//
// This is the job dispatcher.
//

void
MultiSimMux::stirItAround()
{
  currentThread = 0;
  
  if (!enabled) {
    cerr << "Need to initialize CpuMux before using it\n";
  }
  
  while(*terminated == 0){
    if(currentThread == 0)
      currentThread = remove(1, 0, 0);
    if(currentThread == 0)
      break;
    
    systemTransfer( currentThread );
    
    assert(raisedBy != 0);
    
    raisedBy -> handleException();
    raisedBy = 0;
  }
}

void
MultiSimMux::await_(SimTimeUnit when)
{
  Thread *t = 0;

  if(when <= CurrentSimulatedTime)
    return;

  // The 0 means do not wait; it is bad to allow time to pass
  // while this thread is on the queue and we are in its
  // context, since it might be run on a different cpu.
  // remove() will first put the current thread onto the event list.
  t = remove(0, currentThread, when);

  if(t){
    if(t != currentThread){
      Thread *old = currentThread;
      currentThread = t;
      threadTransfer(old, currentThread);
    }
  } else {
    CpuMux::Cpu()->raise(&iveSuspendedException);
  }
}
