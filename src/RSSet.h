// -*-c++-*-
//
// Adapted from the LEDA-3.0 class 'rs_tree.h',
// but only vaguely similar now.
// adapted by Dirk Grunwald (grunwald@cs.colorado.edu)
//
// (Generalized) Randomized Search Trees
// -------------------------------------
// by C.Aragon and R.Seidel
//

#ifndef RSSet_H
#define RSSet_H

#include <math.h>
#include <stdlib.h>
//
// Ansi equiv of values
//
#include <limits.h>
#include <Pix.h>
#include <RNG.h>
#include <MLCG.h>

//
// Note - this is shared between all threads -- you should
// really specify your only RNG for the RSSet classes.
//
class MLCG;
extern MLCG RSTreeDefaultRng;

template <class MapKey>
class RSSet
{
    class RstNode{
  public:
    MapKey key ;
    int prio ;    // priority between 0 and INT_MAX-1
    RstNode *parent;
    RstNode *l_child;
    RstNode *r_child;
    RstNode *p_item;
    RstNode *s_item;

    RstNode(MapKey& key_, int prio_) {
	key = key_;
	prio = prio_;
	parent = l_child = r_child = p_item = s_item = 0;
    }

    RstNode() {}
};

  enum rst_dir { rst_left=1, rst_right=2 } ;
  
  typedef RstNode* RstItem ;
  
  RstItem root ;			// pointer to root AND leaf
  int count ;				// number of entries
  RNG *rng;
  
  inline RstItem& child( RstItem p, rst_dir d ) const  {
      if ( d == rst_left ) {
	  return p ->l_child;
      } else if ( d == rst_right ) {
	  return p -> r_child;
      } else {
	  abort();
      }
  }
  
  RstItem search( MapKey& searchkey, RstItem& parent ) const  {
      MapKey key = searchkey ;
      root->key = key ;				// ensure stop at root
      
      RstItem pp = root;                       // parent of p 
      RstItem p = pp -> r_child ;		// actual node
      
      while( key != p->key  ) {
	  pp = p ;
	  if( key < p -> key ) 
	      p = p -> l_child ; 
	  else 
	      p = p -> r_child ;
      }
      
      parent = pp ;
      return (p==root) ? 0 : p ;
  }
  
  RstItem insert_at_item( RstItem pn, MapKey& searchkey) {
      root->prio = INT_MAX ; 			// ensure stop at root

      int prio = rng -> asInt();
      if (prio < 0) prio = -prio;

      MapKey key = searchkey ;
      RstItem p;
      RstItem pp = pn ;

      count++ ;					// create new node

      p = new RstNode(key,prio);

      RstItem pl, pr ;

      if( key < pp->key ) { 			// update pred-succ-ptr
	  pl = pp -> p_item ;
	  pr = pp ;
      }
      else {
	  pr = pp -> s_item ;
	  pl = pp ;
      }

      pl -> s_item = p ;
      p -> p_item = pl ;
      p -> s_item = pr ;
      pr -> p_item = p ;
      
      pl = pr = root ;				// children of p

      while( prio > pp->prio ) {			// rotate p up

	  if( key < pp->key) {
	      pp -> l_child = pr ;  pr -> parent = pp ;
	      pr = pp ;  pp = pp -> parent ;
	  }
	  else {
	      pp -> r_child = pl ;  pl -> parent = pp ;
	      pl = pp ;  pp = pp -> parent ;
	  }
      } ;

      if( prio==pp->prio && key < pp->key ) {	// one more rot 
	  pp -> l_child = pr ;  pr -> parent = pp ;	// not needed for corr.
	  pr = pp ;  pp = pp -> parent ;
      }

      p -> l_child = pl ;  pl -> parent = p ;		// insert rs node
      p -> r_child = pr ;  pr -> parent = p ;

      if( key < pp->key )
	  pp -> l_child = p ;
      else
	  pp -> r_child = p ;
      p -> parent = pp ;

      return p ;
  }

  void del_item( RstItem p ) {
      root->prio = -1 ;			// ensure stop at root
      
      RstItem 
	  pp = p -> parent,
	  pl = p -> p_item,
	  pr = p -> s_item ;  
      pl -> s_item = pr;
      pr -> p_item = pl;			// update pred-succ-ptr
      
      rst_dir dir = ( p==pp -> r_child ? rst_right : rst_left ) ;
      
      pl = p -> l_child ;
      pr = p -> r_child ;

      delete p ;
      count--;
      
      //
      // Rebalance tree
      //
      while( pl != pr ) {
	  if( pr->prio > pl->prio ) {
	      child(pp,dir) = pr ;  pr -> parent = pp ;
	      pp = pr ;  dir = rst_left ;  pr = child(pp,dir) ;  
	  }
	  else {
	      child(pp,dir) = pl ;  pl -> parent = pp ;
	      pp = pl ;  dir = rst_right ;  pl = child(pp,dir) ;  
	  }
      } ;
      child(pp,dir) = root ;
  }

public:
  
  
  Pix pred( Pix p_ ) const {
      RstItem p = RstItem(p_);
      RstItem q = p->p_item ;
      return Pix((q==root) ? 0 : q) ;
  }
  
  Pix succ( Pix p_ ) const {
      RstItem p = RstItem(p_);
      RstItem q = p->s_item ;
      return Pix((q==root) ? 0 : q) ;
  }

  Pix first() const  {
      return(succ(Pix(root)));
  };

  void next( Pix& p_ ) const  {
      p_ = succ(p_);
  }
  
  Pix min() const {return succ(Pix(root)) ; }
  Pix max() const { return pred(Pix(root)) ; }
  int size() const { return count;}
  int empty() const {return (count==0);};
  
  MapKey& key( Pix p_ ) const {
      RstItem p = RstItem(p_);
      return p -> key;
  }

  MapKey operator()(Pix p_) const {
      RstItem p = RstItem(p_);
      return p->key ;
  };
  
  //
  // Constructor
  //
  RSSet(RNG& rng_ = RSTreeDefaultRng) {
      rng = &rng_;
      root = new RstNode;
      root -> r_child = root -> p_item = root -> s_item = root ;
      root -> l_child = root -> parent = 0 ;
      //
      // value of root -> l_child and root -> parent are always undefined
      //
      count = 0 ;  
  }
  
  RSSet( const RSSet<MapKey>& rst, RNG& rng_ = RSTreeDefaultRng) {
      rng = &rng_;
      root = new RstNode;	// create empty tree
      root -> r_child = root -> p_item = root -> s_item = root ;
      root -> l_child = root -> parent = 0 ;
      count = 0 ;
      
      Pix p = rst.first() ;
      
      while( p ) {					// insert all elements
	  operator+=(rst.key(p));
	  rst.next(p) ;
      }
      
  }
  
  ~RSSet()  {
      clear() ;
      delete root ;
  }
  
  //
  // Lookup the key and return a pointer to the RstItem
  //
  Pix lookup( MapKey key ) const {
      RstItem pp ;
      RstItem p = search( key, pp ); 
      return Pix(p); 
  }

    RSSet<MapKey>& operator+=( MapKey key ) {
	RstItem pp,
	  p = search( key, pp ) ;
      if( ! p ) {
	  Pix(insert_at_item( pp, key)) ;
      }
	return *this;
    }

    RSSet<MapKey>& operator=( const RSSet<MapKey>& rst ) {
	clear() ;  
	for (Pix p = rst.first(); p; rst.next(p) ) {
	    MapKey key = rst.key(p);
	    operator+=(key);
	}
	return *this ;
    }

    RSSet<MapKey>& operator+=( RSSet<MapKey>& rst ) {
	for (Pix pix = rst.first(); pix ; rst.next(pix)) {
	    MapKey& key(rst.key(pix));
	    RstItem pp, p = search( key, pp ) ;
	    if( ! p ) {
		Pix(insert_at_item( pp, key)) ;
	    }
	}
	return *this;
    }

  RSSet<MapKey>& operator-=( MapKey key ) {
      RstItem pp;
      RstItem p = search( key, pp ) ;
      if( p ) {
	  del_item( p ) ;
      }
      return *this;
  }

  int operator==( const RSSet<MapKey>& rst ) {
      if (count != rst.count) return 0;
      Pix p = first();
      Pix q = rst.first();
      while ( p && q ) {
	  if ( key(p) != rst.key(q) ) return 0;
	  next(p);
	  rst.next(q);
      }
      if ( p != 0 & q == 0 ) return 0;
      if ( p == 0 & q != 0) return 0;
      return 1;
  }

  void clear()  {
      Pix p = first() ;
      while( p ) {
	  Pix q = p;
	  next(p) ;
	  delete q ;
      }
      root -> r_child = root -> p_item = root -> s_item = root ;
      root -> l_child = root -> parent = 0 ;
      count = 0 ;
  }
};

#endif
