#ifndef _SingleFifoMux_h_
#define _SingleFifoMux_h_

#include "SingleCpuMux.h"

class SingleFifoMux : public SingleCpuMux {

protected:
    virtual ThreadContainer* allocateThreadContainer();

public:
    SingleFifoMux(int debug = 0);
    virtual ~SingleFifoMux();
};

#endif
