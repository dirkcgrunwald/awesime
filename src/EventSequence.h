#ifndef _EventSequence_h_
#define _EventSequence_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include "ReserveByException.h"
#include "SpinFetchAndOp.h"
#include <map>

class EventSequence : public ReserveByException {

protected:
  SpinLock lock;
  typedef map<int, Thread*> PileMap;
  PileMap pile;;
  int counter;
  SpinFetchAndOp<int> tickets;

private:
  virtual int reserveByException( Thread *byWho, ExceptionReserve& );
    
public :

  EventSequence( int count = 1 );
  virtual ~EventSequence();

  //
  // Advance the counter by X, freeing everyone who was waiting
  // for something before the new value of X.
  //
  virtual void advance(int X = 1);

  //
  // Get a ticket that you can wait for
  //
  int ticket() {
    return( tickets += 1 );
  }

  //
  // Await a specific event value
  //
  virtual void await(int);
  int count() {
    lock.reserve();
    int i = counter;
    lock.release();
    return( i );
  }

  //
  // Reset the EventSequence to some value. This will release
  //
  virtual void reset(int = 1);
};

#endif
