#include "AwesimeConfig.h"
#include <stdlib.h>
#include "Quantile.h"

void Quantile::reset(double quantile) {
   if (quantile < 0.0 || quantile > 1.0) {
      cerr << __FILE__ << ":" << __LINE__ << "--argument(" 
	   << quantile << ") not in range 0.0 to 1.0\n";
      abort();
   }
   pq = quantile;
   ns = 0;
   di[0] = 0.0;
   di[1] = pq + pq;
   di[2] = 4.0 * pq;
   di[3] = 2.0 + pq + pq;
   di[4] = 4.0;
}

//
// Constructor
//
Quantile::Quantile(double quantile) {
   reset(quantile);
}

//
// Reset the sample quantile and clear the statistics
// The rationall behind using these assignment semantics are analgous to 
// specifing an argument to the "constructor" for an int as it's initial value.
//
Quantile &Quantile::operator=(double quantile) {
   reset(quantile);
   return *this;
}

//
// Return the number of samples
//
unsigned long Quantile::samples() {
   return ns;
}

//
// Return the current value of the pq-quantile
//
double Quantile::operator()(){
   if (ns <= 5) {
      return qi[(int) (pq * (double) ns)];
   } else {
      return qi[2];
   }
}

ostream &operator<<(ostream &s, Quantile &q) {
   register unsigned i;
   s << q.ns << "(" << q.pq << "): [";
   for (i = 0; i < 5; i++) {
     s << q.ni[i] << " ";
   }
   s << "] [";
   for (i = 0; i < 5; i++) {
      s << q.di[i] << " ";
   }
   s << "] [";
   for (i = 0; i < 5; i++) {
      s << q.qi[i] << " ";
   }
   s << "]";
   return s;
}

//
// Add x to sample
//
// Computation of the p-quantile when the sample size is < 5 has not been
// rigoursly evaluated.  For example, the median of 4 values is defined to
// be the average of the middle two samples, but this program will return
// the value of the the higher of the two.  The question is what to return
// when computing something other than the median?
//
// For the answer to this and other questions, look at the paper by
// Jain.
//
Quantile &Quantile::operator+=(register double x)
{
    int     *np;
    double  *qp;
    int      d;
    unsigned i;
    double   q;
    
    //
    // Part A: insert x in increasing order
    //
    if (ns < 5) {
	qp = qi + ns - 1;
	if (ns != 0) do {
	    q = *qp;
	    if (x >= q) break;
	    qp[1] = q;
	} while (--qp >= qi);
	qp[1]  = x;
	ni[ns] = ns;
    } else {
	//
	// Part B: insert sample
	//
	// update desired positions of all markers
	//
	di[1] += pq * 0.5;		
	di[2] += pq;
	di[3] += (pq + 1.0) * 0.5;
	di[4] += 1.0;
	
	qp = qi + 4;
	np = ni + 5;
	do {
	    if (x >= *qp) break;
	    //
	    // increment marker positions k through 4
	    //
	    (*--np)++;
	} while (--qp >= qi);
	
	if (qp == qi - 1) { 
	    //
	    // adjust miminum
	    //
	    qp[1] = x;
	} else if (qp == qi + 4) {
	    //
	    // adjust maximum
	    //
	    *qp = x;
	    (*--np)++;
	} 
	
	//
	// Adjust heights of markers 1-3 if necessary
	//
	for (i = 1; i < 4; i++) {
	    q = di[i] - (double) ni[i];
	    if (q >=  1.0 && ni[i+1] - ni[i] > 1
		||  q <= -1.0 && ni[i-1] - ni[i] < -1) 
	    {
		if (q < 0) d = -1; else d = 1;
		q = qi[i] + (double) d / (double) (ni[i+1] - ni[i-1]) 
		    * ( (double) (ni[i] - ni[i-1] + d) 
		       * (qi[i+1] - qi[i]) / (double) (ni[i+1] - ni[i])
		       + (double) (ni[i+1] - ni[i] - d) 
		       * (qi[i] - qi[i-1]) / (double) (ni[i] - ni[i-1])
		       );
		//
		// use parabolic formula
		//
		if (qi[i-1] < q && q < qi[i+1]) {
		    qi[i] = q;
		} else {
		    //
		    // use linear formula
		    //
		    qi[i] += (double) d * (qi[i+d] - qi[i]) 
			/ (double) (ni[i+d] - ni[i]);
		}
		ni[i] += d;
	    }
	}
    }
    ns++;
    return *this;
}
