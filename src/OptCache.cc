#include "OptCache.h"
#include "AwesimeConfig.h"
#include <sys/types.h>
#include <stdlib.h>
#include <iomanip>

inline int
floor_lg(unsigned int num)
/*
**  Computes the floor of the logarithm to base two of "num."
*/
{
    int i;
    for (i = -1; num > 0; i++) num >>=1;
    return (i);
}

/////////////////////////////////////////////////////////////////////////////
// OptCache::toggle
/////////////////////////////////////////////////////////////////////////////
static inline void toggle(int& i)
{
    if ( i ) { i = 0; } else { i = 1; }
}



// OptCache hash table type - should we continue to use this or use a
// LEDA implementation?
//
class OT_HT {
  public:
    OptCache::Addr addr;
    int pt;
    OT_HT *next;

    OT_HT(OptCache::Addr addr_, int pt_, OT_HT *next_) {
	addr = addr_;
	pt = pt_;
	next = next_;
    }

    static OT_HT *freelist;
    void *operator new(size_t size) {
	if ( freelist == 0 ) {
	    return( (void *) new char[ size ]);
	} else {
	    OT_HT *foo = freelist;
	    freelist = freelist -> next;
	    return(foo);
	}
    }

    void operator delete(void *ptr) {
	OT_HT *foo = (OT_HT *) ptr;
	foo -> next = freelist;
	freelist = foo;
    }
};

OT_HT *OT_HT::freelist = 0;

/***************************************************************
Hashing routine. Adds addr to the hash table

Input: address
Output: -1 if addr is not found
        previous time of arrival if it is
Side effects: An entry for address is added to the hash table if address
	      is not found.
***************************************************************/

int
OptCache::ft_hash(OptCache::Addr addr) /* Address to be looked up */
{
    int loc = addr % HASHNO;
    
    OT_HT *p = ft_slot[loc];
    
    while ( p ) {
	if ( p -> addr == addr ) {
	    //
	    // Found it, update
	    //
	    int prev_time = p->pt;
	    p->pt = p_inum;
	    return(prev_time);
	}
    }
    //
    // put it in the front..
    //
    ft_slot[loc] = new OT_HT(addr, p_inum, ft_slot[loc]);
    return -1;
}

/************************************************************************
Deletes addresses from the hash table. It is used when entries are deleted
from the stack due to stack overflow.

Input: Address
Output: None
Side effects: The entry for the address is deleted from the hash table
	      (The address need not necessarily be found, (e.g.) when
	       an address is referenced multiple times in the same window
	       and both references overflow).
*************************************************************************/

void
OptCache::ft_hash_del(OptCache::Addr addr)
{
    int loc = addr % HASHNO;
    OT_HT *p = ft_slot[loc];
    OT_HT *prev = NULL;

    while ( p != NULL ) {
	if ( p -> addr == addr ) {
	    if ( prev == NULL ) {
		ft_slot[loc] = p -> next;
	    } else {
		prev -> next = p -> next;
	    }
	    delete p;
	    return;
	}

	prev = p;
	p = p -> next;
    }
}

/////////////////////////////////////////////////////////////////////////////
// OptCache routines..
/////////////////////////////////////////////////////////////////////////////
OptCache::OptCache(int L_)
{
    L = floor_lg(L_);

    for (int i = 0; i < HASHNO; i++ ) {
	ft_slot[i] = NULL;
    }

    la_limit = 2*LA_DIST;
    base = 0;
    // p_inum wasn't explicitly initialized in old cheetah, we assume
    // this is correct value.
    p_inum = 0;
}

/*********************************************************************
Pre-processor for optimal simulation. Reads in trace addresses and goes back
to their previous point of reference and stores current time. The stack
processing routine can use this information to determine the priority of
addresses. There is a lookahead of at least LA_DIST always.
Calls 'inf_handler' to correct the stack when an address
whose priority was unknown is encountered.

Input: the line size
Output: None
Side effects: Does pre-processing on the trace and calls stack_proc till
	      the trace is processed or the limit on the address count is
	      reached (checked inside stack_proc).
*********************************************************************/

void
OptCache::for_each_reference(OptCache::Addr addr)
{
    if (p_inum >= la_limit) {
	int start = (la_limit % (2*LA_DIST));
	int end = start + LA_DIST;

	int sf = stack_proc(start, end);
	assert( sf != 1 );
	la_limit += LA_DIST;
	toggle (oe_flag);		
	if (oe_flag) {
	    base += 2*LA_DIST;
	}
    }

    //
    // I bet this is amazingly slow if LA_DIST isn't a Po2
    //
    addr >>= L;
    addr_array [(p_inum - base) % (2*LA_DIST)] = addr;

    int prev_time = ft_hash(addr);
    if (prev_time >= 0) {

	if ((la_limit - prev_time) <= 2*LA_DIST) {
	    time_array [(prev_time - base) % (2*LA_DIST)] = p_inum;
	}
	else {
	    inf_handler(addr, p_inum);
	}
	
    }
    else {
	inf_handler(addr, p_inum);
    }
    ++p_inum;
}


void
OptCache::residual()
{
/* Residual processing */
    
    int start = la_limit % (2*LA_DIST);
    int end = start + LA_DIST;

    if (p_inum < LA_DIST) end = p_inum; /* For small traces */

    stack_proc(start, end);

    if (p_inum >= LA_DIST) {
	toggle (oe_flag);
	if (oe_flag) {
	    base += 2*LA_DIST;
	}
	la_limit += LA_DIST;
	start = la_limit % (2*LA_DIST);

	unsigned long t;
	if ( p_inum % LA_DIST == 0) {
	    t = LA_DIST;
	} else {
	    t = (p_inum % LA_DIST);
	}
	end = start + t;
	stack_proc(start, end);
    }
}

void
OptCache::fini(ostream& out)
{
    residual();
    output(out);
}

int
OptCache::print_cachesize(ostream& out, int cachesize)
/*
**  Inverse of atoiKMG(), converts int to string using suffixes:
**
**      K    1024
**      M    1024**2 = 1048576
**      G    1024**3 = 1073741824
**
**  E.g., int 65536 is changed to string "64K" and printed.
*/
{
    int prefix;
    
    if (cachesize >= 1073741824) {
	prefix = cachesize / 1073741824;
	out << setw(6) << prefix << "G";
	return(prefix);
    }
    
    if (cachesize >= 1048576) {
	prefix = cachesize / 1048576;
	out << setw(6) << prefix << "M";
	return(prefix);
    }
    
    if (cachesize >= 1024) {
	prefix = cachesize / 1024;
	out << setw(6) << prefix << "K";
	return(prefix);
    }
    
    prefix = cachesize;
    out << setw(7) << prefix << " ";  
    return(prefix);
}
