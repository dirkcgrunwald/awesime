#include "SingleRandomMux.h"
#include "MLCG.h"
#include "RandomScheduler.h"

SingleRandomMux::SingleRandomMux(int debug) : SingleCpuMux(debug)
{
    myCurrentEvents = allocateThreadContainer();
}

SingleRandomMux::~SingleRandomMux()
{
    // do nothing
}

ThreadContainer *
SingleRandomMux::allocateThreadContainer()
{
    return ( new RandomScheduler(512) );
}
