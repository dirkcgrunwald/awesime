/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Semaphore.h"
#include "FifoScheduler.h"
#include "Thread.h"

Semaphore::Semaphore(int count, ThreadContainer *scheduler)
{
    if (scheduler == 0) {
	pScheduler = new FifoScheduler;
	iDidAlloc = 1;
    } else {
	pScheduler = scheduler;
	iDidAlloc = 0;
    }
    pCount = count;
}

Semaphore::~Semaphore()
{
    lock.reserve();

    if (pScheduler != 0) {
	if (! pScheduler  -> isEmpty() ) {
	    cerr << "[Semaphore] Attempted to delete non-empty semaphore\n";
	    exit(1);
	}
	if (iDidAlloc) delete pScheduler;
	pScheduler = 0;
	iDidAlloc = 0;
    }

    lock.release();
}

void Semaphore::reserve()
{
    lock.reserve();
    if (pCount < 1) {
	lock.release();
	CpuMux::reserveByException( this );
    }
    else {
	pCount--;
	lock.release();
    }
}

//
//	This is only executed in the context of a CpuMux, never
//	a thread.
//
int
Semaphore::reserveByException(Thread *byWho, ExceptionReserve&)
{
    int blocked = 0;
    lock.reserve();
    pCount--;
    if (pCount < 0) {
	pScheduler -> add( byWho );
	blocked = 1;
    };
    lock.release();
    return( blocked );
}

int Semaphore::reserveNoBlock()
{
    int gotIt = 1;

    lock.reserve();
    if (pCount < 1) {
	gotIt = 0;
    }
    else {
	pCount--;
    }
    lock.release();
    return( gotIt );
}
    
void Semaphore::release()
{
    Thread *p = 0;
    lock.reserve();
    if (pCount < 0) {
	assert( !pScheduler -> isEmpty() );
	p = pScheduler -> remove();
    }
    pCount++;
    lock.release();

    if (p != 0) {
	CpuMux::add(p);
    }
}

//
//	Need to lock these
//

unsigned Semaphore::size()
{
    return(pScheduler -> size());
}

int Semaphore::count(int xcount)
{
    int done = 0;
    lock.reserve();
    if ( size() == 0 ) {
	pCount = xcount;
	done = 1;
    }
    lock.release();
    return(done);
}

int Semaphore::count()
{
    lock.reserve();
    int tmp = pCount;
    lock.release();
    return(tmp);
}

void Semaphore::incrCount(int increment)
{
    lock.reserve();
    pCount += increment;
    lock.release();
}

#ifdef UNDEF
void Semaphore::classPrintOn(ostream& out)
{
    int size = pScheduler -> size();

    out << " Semaphore with pCount =" << pCount << " and "
	<< size << " enqueued tasks\n";
    if (size > 0) {
	out << "Tasks are:\n" << *pScheduler << "\n";
    }
}
#endif
