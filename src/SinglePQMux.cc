#include "SinglePQMux.h"
#include "PQScheduler.h"

SinglePQMux::SinglePQMux(int debug) : SingleCpuMux(debug)
{
    myCurrentEvents = allocateThreadContainer();
}

SinglePQMux::~SinglePQMux()
{
    // do nothing
}

ThreadContainer *
SinglePQMux::allocateThreadContainer()
{
    return ( new PQScheduler );
}
