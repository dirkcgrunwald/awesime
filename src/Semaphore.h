#ifndef _Semaphore_h_
#define _Semaphore_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include "Monitor.h"
#include "ThreadContainer.h"

class Semaphore : public Monitor {

protected:
    ThreadContainer *pScheduler;
    int pCount;
    char iDidAlloc;

private:
    virtual int reserveByException( Thread *byWho, ExceptionReserve&);
    
public :

    Semaphore(int count = 1, ThreadContainer *scheduler = 0);
    virtual ~Semaphore();

    virtual void reserve();
    virtual void release();
    virtual int reserveNoBlock();

    virtual unsigned size();

    //
    //	You should not change the count when threads are blocked
    //
    virtual int count();
    virtual int count(int count);
    virtual void incrCount(int increment);

    int isEmpty();
};

inline int
Semaphore::isEmpty()
{
    return( int(size() == 0) );
}

#endif
