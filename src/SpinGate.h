/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef SpinGate_h
#define SpinGate_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Implement a UNIX event
//

#include <SpinLock.h>

const int EventMaxTimesAround = 1000;

class SpinGate : private SpinLock {
    int generation;
    int pLoops;
    int state;
    int garcon;	// waiters
public:
    SpinGate( int pLoops = EventMaxTimesAround );
    ~SpinGate();
    void rendezvous();
    void wait();
    void release();
    void waiting();
    int size();
};

inline
SpinGate::SpinGate(int l)
{
    pLoops = l;
    state = 1;
    garcon = 0;
}

inline
SpinGate::~SpinGate()
{
}

void
SpinGate::release()
{
    SpinLock::reserve();
    garcon = 0;
    generation++;
    SpinLock::release();
}

int
SpinGate::size()
{
    SpinLock::reserve();
    int was = garcon;
    SpinLock::release();
    return(was);
}

#endif
