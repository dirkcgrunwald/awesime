//-*-c++-*-
#ifndef _Histogram_h_
#define _Histogram_h_

#include "Statistic.h"
#include "AwesimeConfig.h"
#include <assert.h>


template <class T = double>
class Histogram : public Statistic<T> {
  typedef map<T, long> HistMap;
  typedef typename HistMap::iterator HistMapIt;
  HistMap histmap;

  long num_samples;
  int num_quantiles;

public:
  Histogram() {
    num_samples = 0;
    num_quantiles = 10;
  }

  Histogram(int quantiles) {
    num_samples = 0;
    num_quantiles = quantiles;
  }

  double median() {
    return quantile(0.50);
  }

  T quantile(double cut) {
    long sum = 0;
    double Quantile50 = (double) num_samples * cut;

    HistMapIt item;
    for (item = histmap.begin(); item != histmap.end(); item++) {
      double value = (*item).first;
      double times = (*item).second;
      sum += times;

      if (Quantile50 <= sum) {
	return value;
      }
    }

    return (-1);
  }

  virtual void reset() {
    Statistic<T>::reset();
    histmap.erase(histmap.begin(), histmap.end());
    num_samples = 0;
  }

  virtual void add(T value, long samples = 1) {
    Statistic<T>::add( value, samples );

    num_samples += samples;
    int key = int(value);

    HistMapIt d = histmap.find( key );
    if ( d == histmap.end() ) {
      histmap[key] = samples;
    } else {
      long& s = (*d).second;
      s += samples;
    }
  }

  virtual void out(ostream& out) {
    double quantiles[10];
    double value = 0;

    if (num_quantiles > 10) {
      num_quantiles = 10;
    }

    double interval_inc = 1.0 / num_quantiles;

    int i;
    for (i=0; i < num_quantiles; i++) {
      quantiles[i] = ((double) num_samples * (i+1.0) * interval_inc);
    }
    double median = (num_samples / 2);

    Statistic<T>::out(out);
    out << "samples -> " << num_samples << "\n";
    int looking_for_median = 1;
    i = 0;

    double sum = 0.0;
    HistMapIt item;
    for (item = histmap.begin(); item != histmap.end(); item++)
      {
	double value = (*item).first;
	double times = (*item).second;
	sum += times;
	if ((i < num_quantiles) && (quantiles[i] <= sum)) {
	  i++;
	  out << "quantile-" << (((double) i) * interval_inc);
	  out << " -> " << setw(4) << value << "\n";
	}

	if (looking_for_median && (median <= sum)) {
	  looking_for_median = 0;
	  median = value;
	}
      }
    out << "median -> " << median << "\n";
    double cum = 0.0;
    for (item = histmap.begin(); item != histmap.end(); item++) {
      double value = (*item).first;
      double times = (*item).second;

      double percent = times;
      percent /= num_samples;
      percent *= 100;
      cum += percent;

      out << setw(8) << value << " -> " << setw(8) << times;
      out << " (" << setw(4) << percent << "%, " << setw(4) << cum << "%) ";
      out<< "\n";
    }
  }

  virtual void digestable_report(ostream& out, char* prefix) {
    double quantiles[10];

    if (num_quantiles > 10) {
      num_quantiles = 10;
    }

    double interval_inc = 1.0 / num_quantiles;

    int i;
    for (i=0; i < num_quantiles; i++) {
      quantiles[i] = ((double) num_samples * (i+1.0) * interval_inc);
    }

    Statistic<T>::digestable_report(out, prefix);
    out << prefix << "_quantile_samples -> " << num_samples << "\n";

    double sum = 0;

    HistMapIt item;
    for (item = histmap.begin(); item != histmap.end(); item++)
      {
	double value = (*item).first;
	double times = (*item).second;
	sum += times;
	if ((i < num_quantiles) && (quantiles[i] <= sum)) {
	  i++;
	  out << prefix << "_quantile_" << (((double) i) * interval_inc);
	  out << " -> " << value << "\n";
	}
      }

    double cum = 0.0;
    for (item = histmap.begin(); item != histmap.end(); item++) {

      double value = (*item).first;
      double times = (*item).second;

      double percent = times;
      percent /= num_samples;
      percent *= 100;
      cum += percent;

      out << prefix << "_Samples_" << value << " -> " << times << "\n";
      out << prefix << "_Percent_" << value << " -> " << percent << "\n";
      out << prefix << "_CumPercent_" << value << " -> " << cum << "\n";
    }

  }
};

template <class T>
ostream&
operator<<(ostream& o, Histogram<T> s)
{
  s.out(o);
  return o;
}


#endif


