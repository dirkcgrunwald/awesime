//-*-c++-*-
#ifndef _LockedSLList_h_
#define _LockedSLList_h_

#include "SLList.h"
#include "SpinLock.h"


template <class T>
class LockedSLList : public SLList<T>
{
  protected:
    SpinLock _lock;
    int iterating;

    void lock() {
	if ( ! iterating ) _lock.reserve();
    }

    void unlock() {
	if ( ! iterating ) _lock.release();
    }

  public:
    //
    // The constructors are unlocked because the user shouldn't
    // be able to call methods until after the constructor
    // has finished
    //
    LockedSLList() : SLList<T>() {
	iterating = 0;
    }

    LockedSLList(const SLList<T>& a) : SLList<T>() {
	copy(a);
	iterating = 0;
    }


    LockedSLList<T>& operator = (const SLList<T>& a) {
	lock();
	BaseSLList::operator=((const BaseSLList&) a);
	unlock();
	return *this;
    }
    
    //
    // Destructor and remaining operations should be locked..
    //
    virtual ~LockedSLList() {
	lock();		
	SLList<T>::clear();
	unlock();
    }

    int empty() {
	lock();
	int foo = (SLList<T>::last == 0);
	unlock();
	return foo;
    }

    Pix prepend(T& item) {
	lock();
	Pix ret = BaseSLList::prepend(&item);
	unlock();
	return ret;
    }

    Pix append(T& item) {
	lock();
	Pix ret = BaseSLList::append(&item);
	unlock();
	return ret;
    }

    Pix prepend(SLNode<T>* node) {
	lock();
	Pix ret = BaseSLList::prepend(node);
	unlock();
	return ret;
    }

    Pix append(SLNode<T>* node) {
	lock();
	Pix ret = BaseSLList::append(node);
	unlock();
	return ret;
    }

    T& operator () (Pix p) {
	lock();
	if (p == 0) SLList<T>::error("null Pix");
	T& foo = ((SLNode<T>*)(p))->hd;
	unlock();
	return foo;
    }

    //
    // The iterators pose a slight problem. We're not really
    // locking/unlocking on each reference, because the list
    // can't be mutated during the traversal. Thus, we extend
    // the protocol when the entire structure must be locked,
    // requiring the user to do a begin_iteration ... end_iteration.
    //

    void  begin_iteration() {
	lock();
	iterating = 1;
    }

    void  end_iteration() {
	iterating = 0;
	unlock();
    }

    inline Pix first() {
	lock();
	Pix ret = (SLList<T>::last == 0)? 0 : Pix(SLList<T>::last->tl);
	return ret;
	unlock();
    }

    void next(Pix& p) {
	lock();
	p = (p == 0 || p == SLList<T>::last)? 0 : Pix(((SLNode<T>*)(p))->tl);
	unlock();
    }

    Pix ins_after(Pix p, T& item) {
	lock();
	Pix x = BaseSLList::ins_after(p, &item);
	unlock();
	return x;
    }

    void join(SLList<T>& a) {
	lock();
	BaseSLList::join(a);
	unlock();
    }
    
    T& front() {
	lock();
	if (SLList<T>::last == 0) SLList<T>::error("front: empty list");
	T& foo = ((SLNode<T>*)SLList<T>::last->tl)->hd;
	unlock();
	return foo;
    }

    T& rear() {
	lock();
	if (SLList<T>::last == 0) SLList<T>::error("rear: empty list");
	T& foo = ((SLNode<T>*)SLList<T>::last)->hd;
	unlock();
	return foo;
    }

    int remove_front(T& x) {
	lock();
	int y = BaseSLList::remove_front(&x);
	unlock();
	return y;
    }

    T remove_front() {
	lock();
	T dst; BaseSLList::remove_front(&dst, 1);
	unlock();
	return dst;
    }
};


#endif
