#ifndef _SinglePQMux_h_
#define _SinglePQMux_h_

#include "SingleCpuMux.h"

class SinglePQMux : public SingleCpuMux {

protected:
    virtual ThreadContainer* allocateThreadContainer();

public:
    SinglePQMux(int debug = 0);
    virtual ~SinglePQMux();
};

#endif
