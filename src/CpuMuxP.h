/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef CpuMuxP_h
#define CpuMuxP_h

#include <CpuMux.h>
#include <HardwareContext.h>
#include <HardwareContextP.h>
#include <SpinLock.h>
#include <Thread.h>

//
//	Expansions for exception handling
//
inline void
CpuMux::systemTransfer(Thread *thread)
{
    systemContext.switchContext(&(thread -> pContext));
}

inline void
CpuMux::threadTransfer(Thread *from, Thread *to)
{
    from -> pContext.switchContext(&(to -> pContext));
}

inline void
CpuMux::raise(ExceptionClass *by)
{
    CpuMux* ThisCpu = Cpu();

    assert( by != 0 );
    ThisCpu -> raisedBy = by;
    ThisCpu -> currentThread -> pContext.switchContext( &(ThisCpu -> systemContext) );
}

inline void
CpuMux::reserveByException( ReserveByException *sem )
{
    CpuMux* ThisCpu = Cpu();

    ThisCpu -> reserveException.reserve( ThisCpu -> currentThread, sem);
    //
    //  Attempt handoff
    //
    Thread *thread = ThisCpu -> remove();
    //
    // Found a valid thread?
    //
    if ( thread == NULL ) {
	//
	// Let system context deal
	//
	ThisCpu -> raise( &(ThisCpu -> reserveException) );
    } else {
	Thread *old = ThisCpu -> currentThread;
	ThisCpu -> currentThread = thread;
	ThisCpu -> raisedBy = &(ThisCpu -> reserveException);
	CpuMux::threadTransfer(old, thread);
    }
}

inline void
CpuMux::threadTerminateException( Thread *killMe )
{
    CpuMux * ThisCpu = Cpu();

    ThisCpu -> terminateException.terminate( killMe );
    //
    //  Attempt handoff
    //
    Thread *thread = ThisCpu -> remove();
    //
    // Found a valid thread?
    //
    if ( thread == NULL ) {
	//
	// Let system context deal
	//
	ThisCpu -> raise( &(ThisCpu -> terminateException) );
    } else {
	Thread *old = ThisCpu -> currentThread;
	ThisCpu -> currentThread = thread;
	ThisCpu -> raisedBy = &(ThisCpu -> terminateException);
	CpuMux::threadTransfer(old, thread);
    }

}

#endif /* CpuMuxP_h */
