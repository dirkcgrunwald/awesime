// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//
#ifndef SingleSimMux_h
#define SingleSimMux_h
#ifdef __GNUG__
#  pragma interface
#endif

#include <SimMux.h>
#include <SingleCpuMux.h>
#include <HardwareContext.h>
#include <SEv.h>

#include "CUT_BinHeap.h"

class SingleSimMux : public SimMux {
protected:

  typedef less<SimTimeUnit> lt;
  typedef CUT_BinHeap<SimTimeUnit, SEv, lt> myHeapType;
  myHeapType *myEvents;
  virtual void add_(Thread *);
  virtual void addToCpu_(int Cpu, Thread* t);

  virtual void addAt_(Thread *, SimTimeUnit);
  virtual void addAt_(OEv *, SimTimeUnit);
  virtual void await_(SimTimeUnit);
  
  //
  // Remove doesn't always return a thread in general,
  // but this remove will always return either NULL or a thread.
  //
  virtual Thread *remove();
  
public:
  SingleSimMux( int debug = 0 );
  virtual ~SingleSimMux();
  
  virtual void fireItUp(int Cpus, unsigned long);

  virtual void print_events(ostream& str);
};
#endif /* SingleSimMux_h */
