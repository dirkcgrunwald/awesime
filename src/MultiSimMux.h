// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//
#ifndef MultiSimMux_h
#define MultiSimMux_h
#ifdef __GNUG__
#  pragma interface
#endif

#include <SimMux.h>

//
//	Filled in by the single original UNIX process. This value may
//	change during execution
//

class ThreadContainer;

class MultiSimMux : public SimMux {
protected:
    void allocateLocalEventStructures(int,int);
    void deallocateEventStructures();

    Thread *remove(int doWait, Thread *add, SimTimeUnit when);

    virtual void warmThePot(int);
    virtual void stirItAround();
    virtual void coolItDown();

    virtual void add_(Thread *);
    virtual void addAt_(Thread *, SimTimeUnit);
    virtual void addAt_(OEv *, SimTimeUnit);
    virtual void await_(SimTimeUnit);

public:

    MultiSimMux(bool debug = false);
    virtual ~MultiSimMux();

    virtual void fireItUp(int Cpus = 1, unsigned shared = (4196 * 500));

    static MultiSimMux *Cpu();
};

inline MultiSimMux *MultiSimMux::Cpu()
{
    return( (MultiSimMux *) ThisCpu ) ;
}

#endif /* MultiSimMux_h */
