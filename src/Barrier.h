/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef Barrier_h
#define Barrier_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Barrier.h
//	The barrier is initially closed.
// 

#include <ReserveByException.h>
#include <SpinLock.h>
#include "AwesimeConfig.h"

class FifoScheduler;

class Barrier : public ReserveByException {
    SpinLock lock;

    int pHeight;
    VolatileInt pCount;

    FifoScheduler *pPileOfThreads;
    
    void releaseAll();
    
private:
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);
    
public:
    
    Barrier(int height=0);
    virtual ~Barrier();

    void rendezvous();

    //
    // lowerBarrier forces all waiting jobs to continue
    //
    void lower();

    //
    // Changing the height of the barrier can cause people to continue
    // if it is lowered below the current pCount.
    //
    int height();
    void height(int h);

    //
    // how many tasks are waiting?
    //
    int count();
};

inline int
Barrier::height()
{
    return(pHeight);
}

inline int
Barrier::count()
{
    return( pCount );
}

#endif /* Barrier_h */
