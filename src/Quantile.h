// -*-C++-*-
#ifndef _Quantile_h
#define _Quantile_h

//
// Statistic Class to estimate the p-quantile of a set of values using
// the P^2 algorithm:
//
// "The P^2 Algorithm for Dynamic Calculation of Quantiles and Histograms
//  Without Storing Observations", by Raj Jain and Imrich Chlamtac, as
//  published in "Communications of the ACM", Vol. 28, No. 10, Oct-85, 
//  pp1076-1085.
//
// Also published in this paper is a way to compute a b-cell histograms using 
// equiprobable cells (see QHistogram.h)
//
// Written by David A. Barrett (barrett@asgard.cs.Colorado.EDU) 27-Mar-92
//

#include "AwesimeConfig.h"

class Quantile {
private:
   unsigned long ns;		// number of samples        (n)
   int      ni[5];		// marker positions         (n[i])
   double   qi[5];		// marker heights           (q[i])
   double   di[5];		// desired marker positions (n'[i])
   double   pq;			// p-quantile               (p)

   void reset(double p);	// reinitialize quantile & clear sample

public:
   Quantile(double p);			// Initialize quantile of interest
   unsigned  long samples();			// Return number of samples
   double    operator ()();		// return p-quantile of sample
   Quantile &operator  =(double p);	// Reinitialize quantile & clear sample
   Quantile &operator +=(double x);	// add x to sample, return new quantile
   friend ostream &operator <<(ostream &, Quantile &);
};
#endif
