// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1991 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald
//
// Copyright (C) 1990 by Dirk grunwald 
// 
// This file is part of Awesime.
// 
// Awesime is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY.  No author or distributor accepts responsibility to
// anyone for the consequences of using it or for whether it serves any
// particular purpose or works at all, unless he says so in writing.
// Refer to the GNU General Public License for full details.
// 
// Everyone is granted permission to copy, modify and redistribute
// Awesime, but only under the conditions described in the Gnu General
// Public License.  A copy of this license is supposed to have been given
// to you along with Awesime so you can know your rights and
// responsibilities.  It should be in a file named COPYING.  Among other
// things, the copyright notice and this notice must be preserved on all
// copies.
// 
// 
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "CCondition.h"
#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "FifoScheduler.h"

CCondition::CCondition() : Condition(0,&sched)
{
    monitor = 0;
    cScheduler = &sched;
    iDidAlloc = 0;
}

CCondition::~CCondition()
{
    monitor = 0;
}

void
CCondition::wait()
{
    assert( monitor != 0 );
    Condition::wait();
}

void
CCondition::wait(Monitor& m)
{
    if ( monitor == 0 ) {
	monitor = &m;
    } else {
	assert( monitor == &m );
    }
    // Assumption:  the caller is holding the associated mutex.
    // The semantics of a CCondition are that the thread is put to
    // sleep and the associated Monitor is released as one atomic action.
    // Thus, the exception handler will release the mutex.
    CpuMux::reserveByException( this );
    // Re-obtain the mutex before proceeding
    monitor->reserve();
}
