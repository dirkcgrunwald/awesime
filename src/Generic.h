/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef _Generic_h
#define _Generic_h

#ifdef __GNUG__
#  pragma interface
#endif

#if defined(__GNU__) || defined(__STDC__) || defined(__GNUG__) || defined(__DECCXX)
/*
 *	See the CPP manual, argument prescan section for explination
 *	of this crap.
 */
#define GENERIC2(a,b) gEnErIc2(a,b)
#define gEnErIc2(a,b) a ## b

#define GENERIC3(a,b,c) gEnErIc3(a,b,c)
#define gEnErIc3(a,b,c) a ## b ## c

#define GENERIC4(a,b,c,d) gEnErIc4(a,b,c,d)
#define gEnErIc4(a,b,c,d) a ## b ## c ## d

#define GENERIC_STRING(a) gEnErIcStRiNg(a)
#define gEnErIcStRiNg(a) #a

#else
#if defined(sun)

#define GENERIC2(a,b)	a/**/b
#define GENERIC3(a,b,c)	a/**/b/**/c
#define GENERIC4(a,b,c,d)	a/**/b/**/c/**/d
#define GENERIC_STRING(a) "\
a \
"
#else
#if defined(BSD)
#define GENERIC2(a,b) a\
b
#define GENERIC3(a,b,c) a\
b\
c
#define GENERIC4(a,b,c,d) a\
b\
c\
d
#define GENERIC_STRING(a) "\
a \
"
#else
	I do not know how to make GENERICs for your machine
#endif /* BSD */
#endif /* SUN */
#endif /* GNU */

#endif /* Generic_h */
