/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef LOCKEDFIFOH
#define LOCKEDFIFOH

#ifdef __GNUG__
#  pragma interface
#endif

#include <Semaphore.h>
#include <AwesimeFifo.h>

class LockedFifo : public AwesimeFifo {

protected:
    Semaphore fifoLock;
public:

    LockedFifo(int defaultLength = 0);

    //
    // Operations on the list -- these simply lock the list and
    // then use the AwesimeFifo routines to perform the action.
    //
    virtual void add(AwesimeFifoItem *t);
    virtual int remove(AwesimeFifoItem *item);
    virtual int removeIfFound(AwesimeFifoItem* item);

    //
    // Over-ride the doStart and doDone; these use the semaphore
    // to lock out other threads.
    //

    virtual int doStart( AwesimeFifoIndex& index, AwesimeFifoItem* item);
    virtual void doDone();

    //
    // The size and printer also locks out other threads.
    //
    virtual unsigned int size();
    virtual void classPrintOn(ostream& s);
};


#endif

