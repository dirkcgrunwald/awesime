#ifndef _ThreadPtrdefs_h_
#define _ThreadPtrdefs_h_ 1

#include "Thread.h"

// equality operator
#ifndef ThreadPtrEQ
#define ThreadPtrEQ(a, b)  ((a)->priority() == (b)->priority())
#endif

// less-than-or-equal
#ifndef ThreadPtrLE
#define ThreadPtrLE(a, b)  ((a)->priority() <= (b)->priority())
#endif

// comparison : less-than -> < 0; equal -> 0; greater-than -> > 0
#ifndef ThreadPtrCMP
#define ThreadPtrCMP(a, b) (((a)->priority()) - ((b)->priority()))
#endif

// hash function
#ifndef ThreadPtrHASH
extern unsigned int hash(ThreadPtr&);
#define ThreadPtrHASH(x)  hash(x)
#endif

// initial capacity for structures requiring one

#ifndef DEFAULT_INITIAL_CAPACITY
#define DEFAULT_INITIAL_CAPACITY 100
#endif

#ifndef DEFAULT_INITIAL_CAPACITY
#define DEFAULT_INITIAL_CAPACITY 100
#endif

#endif
