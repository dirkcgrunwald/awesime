// This may look like C code, but it is really -*- C++ -*-
#ifndef Statistic_h
#define Statistic_h

#include "AwesimeConfig.h"
#include <math.h>
#include <values.h>
#include "StudentTDistribution.h"

using namespace std;

template<class T = double>
class Statistic {

protected:
    long n;
    T x;
    T x2;
    T minValue, maxValue;

public:

  Statistic() {
    reset();
  }

  virtual void reset() {
    n = 0; x = x2 = (T) 0.0;
    maxValue = (T) -MAXDOUBLE;
    minValue = (T) MAXDOUBLE;
  }

  virtual void add(T value, long samples = 1) {
    n += samples;
    x += (value * samples);
    x2 += ((value * value) * samples);
    if ( minValue > value) minValue = value;
    if ( maxValue < value) maxValue = value;
  }

  void operator+=(T value) {
    add(value);
  }

  long samples() { return n; }

  double mean() {
    if ( n > 0) {
	return (x / n);
    }
    else {
	return ( 0.0 );
    }
  }

  double stdDev() {
    double v = var();
    if ( n <= 0 || v <= 0) {
	return(0);
    } else {
	return( sqrt( double(v) ) );
    }
  }

  double var() {
    if ( n > 1) {
	return(( (double) x2 - ((double) (x * x) /  (double) n)) / ( n - 1));
    }
    else {
	return ( 0.0 );
    }
  }

  T min() {
    return minValue;
  }

  T max() {
    return maxValue;
  }

  T sum() {
    return x;
  }

  T sum_squared() {
    return x2;
  }

  double confidence(double p_value) {
    double samp = samples() - 1;
    if (samp <= 0) return ( 0.0 );

    double t = StudentTDistribution::tval((1.0 + p_value) * 0.5, samp);
    if (t == HUGE) {
	return t;
    }
    else {
	return (t * stdDev()) / sqrt(double( samp ));
    }
  }

  virtual void out(ostream& st) {
    st << samples() << " " << mean() << " " << stdDev() << " "
       << confidence(90) << " " << confidence(95) << " " << confidence(99);
  }

  void digestable_report(ostream& out, char *prefix = "") {
    out << prefix << "_samples -> " << samples() << "\n";
    out << prefix << "_mean -> " << mean() << "\n";
    out << prefix << "_stddev -> " << stdDev() << "\n";
    out << prefix << "_conf90 -> " << confidence(0.90) << "\n";
    out << prefix << "_conf95 -> " << confidence(0.95) << "\n";
    out << prefix << "_conf99 -> " << confidence(0.99) << "\n";
  }
};

template <class T>
ostream&
operator<<(ostream& o, Statistic<T> s)
{
  s.out(o);
  return o;
}

#endif
