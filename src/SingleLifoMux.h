#ifndef _SingleLifoMux_h_
#define _SingleLifoMux_h_

#include "SingleCpuMux.h"

class SingleLifoMux : public SingleCpuMux {

protected:
    virtual ThreadContainer* allocateThreadContainer();

public:
    SingleLifoMux(int debug = 0);
    virtual ~SingleLifoMux();
};

#endif
