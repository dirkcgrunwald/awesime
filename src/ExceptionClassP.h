#include "CpuMux.h"

#ifdef __GNUG__
#  pragma interface
#endif

inline void
ExceptionClass::setCurrentThread(Thread *t)
{
    CpuMux::Cpu() -> currentThread = t;
}

