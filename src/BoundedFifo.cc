/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "BoundedFifo.h"
#include "iostream"
#include "assert.h"

BoundedFifo::BoundedFifo(int xpMaxLength, int defaultLength)
    : LowerBoundedFifo(defaultLength),
      upperBoundLockFifo(),
      upperBoundLock(xpMaxLength, &upperBoundLockFifo)
{
    pMaxLength = xpMaxLength;
}

void
BoundedFifo::add(AwesimeFifoItem* t) {
    upperBoundLock.reserve();
    LockedFifo::add(t);
    lowerBoundLock.release();
}

int
BoundedFifo::remove(AwesimeFifoItem *item)
{
    (void) lowerBoundLock.reserve();
    int ok = LockedFifo::remove(item);
    assert(ok);
    upperBoundLock.release();
    return(ok);
}

int
BoundedFifo::removeNoBlock(AwesimeFifoItem *item)
{
    int ok = removeNoBlock( item );
    if (ok) {
	upperBoundLock.release();
    }
    return(ok);
}

int BoundedFifo::removeIfFound(AwesimeFifoItem *item) {
    (void) lowerBoundLock.reserve();
    int ok = LockedFifo::removeIfFound(item);
    if (ok) {
	upperBoundLock.release();
    } else {
	lowerBoundLock.release();
    }
    return(ok);
}

int
BoundedFifo::doDelete(AwesimeFifoIndex& index) {
    //
    // This is problematic. Since this is a LockedFifo, this person
    // will be the only one with access to the list. If he blocks
    // at this point, he's dead-meat.
    //
    // So, is this an error? If so, should we protect him?
    //
    int whaHap = AwesimeFifo::doDelete(index);
    if (whaHap) {
	int noBlock = lowerBoundLock.reserveNoBlock();
	assert( noBlock );
	lowerBoundLock.reserve();
	upperBoundLock.release();
    }
    return(whaHap);
}

unsigned int BoundedFifo::size()
{
    return(AwesimeFifo::size());
}
