//-*-c++-*-
#ifndef _ThreadPipe_h_
#define _ThreadPipe_h_

#include <list>
#include <vector>
#include <Semaphore.h>
#include <SpinLock.h>

template<class Contained, class Container, class SemType>
class ThreadPipe
{
  Container container_;
  //
  // Mutex used for atomicity
  //
  SpinLock mutex_;
  //
  // Semaphore used for get access
  //
  SemType get_access_;

public:
  ThreadPipe() : get_access_(0)
  {
    //
    // empty
    //
  }

  void put(const Contained& item)
  {
    mutex_.reserve();
    container_.push_back(item);
    mutex_.release();
    get_access_.release();
  }

  void get(Contained& item)
  {
    get_access_.reserve();
    mutex_.reserve();
    item = container_.front();
    container_.erase(container_.begin());
    mutex_.release();
  }

  bool tryGet(Contained& item)
  {
    if ( get_access_.reserveNoBlock() ) {
      mutex_.reserve();
      item = container_.front();
      container_.erase(container_.begin());
      mutex_.release();
      return true;
    } else {
      return false;
    }
  }

  void unget(Contained& item) {
  	mutex_.reserve();
	container_.push_front(item);
	mutex_.release();
	get_access_.release();
  }

  long length()
  {
    long retval;
    mutex_.reserve();
    retval = container_.size();
    mutex_.release();
    return retval;
  }

  //
  // It's not very advisable to use these methods to modify the
  // contents of a container, or certainly not to add or remove items
  // from the container, since the mutex will not remain
  // synchronized. These routines may be useful if you're putting a
  // lot of items into the container, or if you need to access the
  // container to print items.
  //
  void reserve() {
    mutex_.reserve();
  }

  void release() {
    mutex_.release();
  }

  const Container& container() {
    return container_;
  }

};

template<class Contained, class Container, class SemType>
class BoundedThreadPipe : public ThreadPipe<Contained,Container,SemType>
{
  //
  // Semaphore used for get access
  //
  SemType put_access_;
  long max_size_;

public:
  BoundedThreadPipe(long max_size) : put_access_(max_size)
  {
    max_size_ = max_size;
  }

  void put(const Contained& item)
  {
    put_access_.reserve();
    ThreadPipe<Contained,Container,SemType>::put(item);
  }

  bool tryPut(const Contained& item)
  {
    if ( put_access_.reserveNoBlock() ) {
      ThreadPipe<Contained,Container,SemType>::put(item);
      return true;
    } else {
      return false;
    }
  }

  void get(Contained& item)
  {
    ThreadPipe<Contained,Container,SemType>::get(item);
    put_access_.release();
  }

  void unget(Contained& item)
  {
    ThreadPipe<Contained,Container,SemType>::unget(item);
    put_access_.release();
  }

  bool tryGet(Contained& item)
  {
    if ( ThreadPipe<Contained,Container,SemType>::tryGet(item) ) {
      put_access_.release();
      return true;
    } else {
      return false;
    }
  }
};

//
// Short cuts
//
template <class Contained>
class PipeList : public ThreadPipe< Contained, list<Contained>, Semaphore>
{
  //
  // Empty
  //
};

template <class Contained>
class BoundedPipeList
  : public BoundedThreadPipe< Contained, list<Contained>, Semaphore>
{

public:
  BoundedPipeList(long max_size)
    : BoundedThreadPipe< Contained, list<Contained>, Semaphore>(max_size)
  {
    //
    // Empty
    //
  }
};

#endif
