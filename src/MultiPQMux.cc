#include "MultiPQMux.h"
#include "PQScheduler.h"

MultiPQMux::MultiPQMux(int debug) : MultiCpuMux(debug)
{
    allocateLocalEventStructures();
}

MultiPQMux::~MultiPQMux()
{
    // do nothing
}

ThreadContainer *
MultiPQMux::allocateThreadContainer()
{
    return ( new PQScheduler );
}
