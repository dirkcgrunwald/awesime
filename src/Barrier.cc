/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "Barrier.h"
#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "FifoScheduler.h"

Barrier::Barrier( int h )
{
    pHeight = h;
    pCount = 0;
    pPileOfThreads = new FifoScheduler;
}


Barrier::~Barrier()
{
    if ( pPileOfThreads != 0 ) {
	delete pPileOfThreads;
    }
}

void
Barrier::releaseAll()
{
    if ( pPileOfThreads != 0 ) {
	//
	// Note: this completely locks up the current CPU
	//
	CpuMux::Cpu() -> addReserve();
	while ( ! pPileOfThreads -> isEmpty() ) {
	    Thread *p = pPileOfThreads -> remove();
	    CpuMux::Cpu() -> addUnlocked(p);
	}
	CpuMux::Cpu() -> addRelease();
    }
}

//
//	Make everyone exit
//
void
Barrier::lower()
{
    lock.reserve();
    pCount = 0;
    releaseAll();
    lock.release();
}

void
Barrier::rendezvous()
{
    lock.reserve();
    pCount++;
    if ( pCount == pHeight) {
	pCount = 0;
	releaseAll();
	lock.release();
    } else {
	CpuMux::Cpu() -> reserveByException( this );
    }
}

int
Barrier::reserveByException(Thread *byWho, ExceptionReserve&)
{

    pPileOfThreads -> add( byWho );
    lock.release();
    return( 1 );
}

void
Barrier::height(int newHeight)
{
    lock.reserve();
    if (pCount >= newHeight) {
	pCount = 0;
	releaseAll();
    }
    else {
	pHeight = newHeight;
    }
    lock.release();
}

