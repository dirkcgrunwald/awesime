/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef MCSBarrier_h
#define MCSBarrier_h

#ifdef __GNUG__
#  pragma interface
#endif

//
//	MCSBarrier.h
//	The barrier is initially closed.
// 
#include <CpuMuxP.h>
#include <ReserveByException.h>
#include <SpinLock.h>
#include <FifoScheduler.h>

typedef union {
        int whole;
        int parts[4];
} whole_and_parts;

typedef struct {
        int *parentflag;
        whole_and_parts havechild;
        whole_and_parts childnotready;
        int dummy;
        int pad[4];
} TREENODE;

typedef struct THREAD_PILE {
    FifoScheduler *pile;
    SpinLock      lck;
    int           expected_count;
    int           count;
    int           initCount;
} PILE_REC;

typedef TREENODE *TREENODEPTR;

class MCSBarrier : public ReserveByException {

    int pHeight;
    int numThreads;
    int initFlag;
    SpinLock lck;

 protected:
    TREENODEPTR BarrierTree[_MAXIMUM_CPU_MUXS_];
    PILE_REC    ThreadPiles[_MAXIMUM_CPU_MUXS_];
    TREENODEPTR myLeaf[_MAXIMUM_CPU_MUXS_];
    int     mySense[_MAXIMUM_CPU_MUXS_];
    
    int numCpus;
    void releaseAllOfMine(int iYam);
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);
    VolatileSpinLockType championSense;

  public: 
    virtual ~MCSBarrier();
    MCSBarrier(); 
    virtual void rendezvous() = 0;
    void initBarrier(int height, int cpus = CpuMux::Muxs);
    void checkIn();
    void checkOut();

};
#endif /* MCSBarrier_h */

















