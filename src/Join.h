#ifndef JOINH
#define JOINH

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Join.h


#include <ReserveByException.h>
#include <SpinLock.h>

class Join : public ReserveByException {

protected:
    SpinLock lock;
    Thread *parent;
    Thread *child;
    volatile int childWaiting;
    void *returnValue;

private:
    virtual int reserveByException( Thread *byWho, ExceptionReserve& );
    
public :

    Join(Thread *child);
    virtual ~Join();

    //
    // parentJoin returns join value of child
    //
    void * parentJoin();

    //
    // child returns value and dies
    //
    void childJoin(void *returnValue);
};

#endif
