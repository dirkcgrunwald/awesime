/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif


#include "MCSBarrier.h"
#include "Thread.h"
#include "FifoScheduler.h"
#include "AwesimeConfig.h"
#include <math.h>
#include <iolock.h>
#include <assert.h>

MCSBarrier::MCSBarrier()
{
    pHeight = 0;
    initFlag = 0;
    championSense = 0;
}

MCSBarrier::~MCSBarrier()
{
  for (int i=0; i < _MAXIMUM_CPU_MUXS_; i++) {
       delete BarrierTree[i];
       delete ThreadPiles[i].pile;
  }
}

void MCSBarrier::initBarrier(int h,int cpus ) {    
    int index,i;

    lck.reserve();
    if (cpus > h)
	cpus = h;
    numCpus = cpus;
    pHeight = h;

    for (index = 0; index < numCpus; index++) {
       BarrierTree[index] = new TREENODE;
       ThreadPiles[index].pile = new FifoScheduler;
       ThreadPiles[index].expected_count = 0;
       ThreadPiles[index].count = 0;
       ThreadPiles[index].initCount = 0;
     }
     numThreads = 0;
     for (i=0; i < pHeight; i++) 
        ThreadPiles[i%numCpus].expected_count++;
    initFlag = 1;
    lck.release();
}

void MCSBarrier::checkOut() {
    Thread *thread = CpuMux::CurrentThread();
    thread->affinity(-1);
}

void MCSBarrier::checkIn() {

    int myHeightReached = 0;

    while (initFlag == 0);

    int iYam = CpuMux::cpuId();
    Thread *thread = CpuMux::CurrentThread();


    int oldiYam = iYam;
    int found = 0;

    while (!found) {
        if (ThreadPiles[iYam].initCount  < ThreadPiles[iYam].expected_count ) {
	    ThreadPiles[iYam].lck.reserve();
	    if (ThreadPiles[iYam].initCount < ThreadPiles[iYam].expected_count) 
	       if (++ThreadPiles[iYam].initCount == ThreadPiles[iYam].expected_count)
  	           myHeightReached = 1;
	    ThreadPiles[iYam].lck.release();
	    found = 1;
	}
	else 
  	   iYam = ++iYam%numCpus;
    }

    thread->affinity(iYam);
    if (!myHeightReached) {
	CpuMux::reschedule();
	return;
    }

// PER PROCESS WORK

    if (oldiYam !=iYam) 
	CpuMux::reschedule();

    mySense[iYam] = 1;

    int i,child_id;
    TREENODEPTR mynode = BarrierTree[iYam];
    if (iYam == 0) mynode->parentflag = &mynode->dummy;
    else {
         int parentid = (iYam -1)/4;
         int my_index = iYam - (parentid * 4) - 1;
         mynode->parentflag =
                        &BarrierTree[parentid]->childnotready.parts[my_index];
    }
    for(i=0,child_id = 4 * iYam + 1;i<4;i++,child_id++) {
         /* have child i in arrival tree */
         mynode->havechild.parts[i] = (numCpus > child_id);
    }
    mynode->childnotready.whole = mynode->havechild.whole;
    
    // must wait for all processors to complete because the counts in the
    // tree have to be initialized

    lck.reserve();
    numThreads = numThreads + ThreadPiles[iYam].initCount;
    lck.release();
    while (numThreads != pHeight);


}

int
MCSBarrier::reserveByException(Thread *byWho, ExceptionReserve&)
{
    ThreadPiles[CpuMux::cpuId()].pile -> add( byWho );
    return( 1 );
}
 
void
MCSBarrier::releaseAllOfMine(int iYam)
{
   // int count = 0;

    if ( ThreadPiles[iYam].pile != 0 ) {   
	while ( ! ThreadPiles[iYam].pile -> isEmpty() ) {
	    Thread *p = ThreadPiles[iYam].pile -> remove(); 
	    CpuMux::AddToCpu(iYam,p);
	    // cout << lock << iYam << " releasing " << p->name() << "\n";
	    // cout << unlock;
	    // count++;
	}
    }
}









