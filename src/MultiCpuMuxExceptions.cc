// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
//
// written by Dirk Grunwald (grunwald@cs.uiuc.edu)
//

#include "MultiCpuMuxExceptions.h"
#include "ReserveByException.h"
#include "ExceptionClassP.h"
#include "CpuMux.h"
#include "CpuMuxP.h"
#include "HardwareContextP.h"
#include "MultiCpuMux.h"
#include "SpinBarrier.h"
#include "iostream"
#include "Thread.h"
#include <unistd.h>
#include "iolock.h"

void
ExceptionRelocate::handleException()
{
    //
    // Switch to another thread when we havenot gotten
    // rid of the current one.
    //
#ifndef NDEBUG
    if (CpuMux::debug()) {
	cerr << lock;
	cerr << CpuMux::name() << " reschedule exception to ";
	cerr << newCpu << "\n";
	cerr << unlock;
    }
#endif /* NDEBUG */

    Thread *t = CpuMux::CurrentThread();
    assert(t != 0 );
    MultiCpuMux::AddToAnother(newCpu, t);
    setCurrentThread( 0 );
}

//
//	Used to add or remove CPUs from the current pool of CPUs
//


void
ExceptionEnrollDismissCpu::handleException()
{
    MultiCpuMux *cpu = (MultiCpuMux *) CpuMux::Cpu();
    cpu->EnrollDismissCpu(enrollOrDismiss);
}
 
