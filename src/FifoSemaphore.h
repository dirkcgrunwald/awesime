/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
#ifndef FifoSemaphore_h_
#define FifoSemaphore_h_

#ifdef __GNUG__
#  pragma interface
#endif

#include <Semaphore.h>
#include <FifoScheduler.h>

class FifoSemaphore : public Semaphore
{
    FifoScheduler fifo;
public :
    FifoSemaphore(int count = 1, int def = 10);
    virtual ~FifoSemaphore();
};

inline
FifoSemaphore::FifoSemaphore(int count, int def)
    : Semaphore(count, &fifo), fifo(def)
{
    // do nothing
}

#endif
