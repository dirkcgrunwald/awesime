// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "AwesimeConfig.h"
#include "Generic.h"
#include <assert.h>
#include <memory.h>
//
//	GenericFifo: A FIFO scheduler which uses a (fast) circular list
//	to record threads.
//
//	voids are kept in the list. listHead is the index of the
//	next thread to be removed. listTail is the index of the next
//	*free slot*. 
//
//	To search the list, you start at listHead and move towards listTail,
//	skipping over invalidated items.
//
//	when listElements == 0, the queue is *empty*, but listElements
//	can be 0 and we can still have items in the list, since we
//	removed mid-list items by zeroing them.
//
//	Thus, listHead == listTail implies listElements == 0, but
//	it's not an iff.
//
//	When the queue becomes too big for the current list, it is
//	expanded & the existing entries are copied to the new queue.
//
//	If the queue becomes empty & is below some threashold, it
//	is removed.
// 

static const char *GENERIC2(FIFO_NAME,Name) = GENERIC_STRING(FIFO_NAME) ;

FIFO_NAME::FIFO_NAME(int defaultLength, int xdebug) : Awesime(xdebug)
{
    listHead = listTail = 0;
    listElements = 0;
    allocatedSize = 0;
    list = 0;
    pValid = 0;
    if (defaultLength > 0) {
	reSize(defaultLength);
    }
}

FIFO_NAME::~FIFO_NAME()
{
    if (allocatedSize > 0) {
	delete list;
	delete pValid;
    }
}

//
//	makeRoom assumes that listElements, listHead & listTail have been
//	set
// 
void
FIFO_NAME::reSize(unsigned int howMuch)
{
    
    //
    //	We can't remove any items, so we must insure we'll have enough
    //	for the existing elements.
    // 
    
    if (howMuch > listElements) {
	
	GENERIC2(FIFO_NAME,Item) *p = new GENERIC2(FIFO_NAME,Item)[howMuch];
	char *validP = new char[howMuch];

	//	assert2( p != 0 && validP != 0, "Unable to lengthen FIFO");
	
	memset(p, 0, sizeof(GENERIC2(FIFO_NAME,Item)) * howMuch);
	memset(validP, 0, sizeof(char) * howMuch);
	
	if (listElements > 0) {
	    
	    //
	    //	Move over old list contents, taking care to compress the null
	    //	entries.
	    // 
	    
	    if (list == 0) {
		cerr << "[" << GENERIC2(FIFO_NAME,Name) << "::makeRoom] null list found?\n";
		exit(1);
	    }
	    
	    register GENERIC2(FIFO_NAME,Index) i;
	    register unsigned int moved;
	    for (i = listHead, moved = 0; moved < listElements;
		 i = advance(i)) {
		     if (pValid[i] != 0) {
			 p[moved] = list[i];
			 validP[moved] = 1;
			 moved++;
		     }
		 }
	    delete list;
	    delete pValid;
	}
	
	list = p;
	pValid = validP;
	listTail = listElements;
	listHead = 0;
	allocatedSize = howMuch;
    }
}

void 
FIFO_NAME::add(GENERIC2(FIFO_NAME,Item) *t) {
    if (advance(listTail) == listHead 
	|| listElements >= allocatedSize)  {
	    reSize((allocatedSize <= 0)
		   ? 2
		   : ((allocatedSize * 3) / 2) );
	}
    list[listTail] = *t;
    pValid[listTail] = 1;
    listTail = advance(listTail);
    listElements++;
}

int FIFO_NAME::remove(GENERIC2(FIFO_NAME,Item) *item) {
    int ok = 1;
    if ((listElements > 0) && pValid[listHead]) {
	*item = list[listHead];
	pValid[listHead] = 0;
	listHead = advance(listHead);
	listElements--;
    } else {
	char valid = 0;
	ok = 1;
	do {
	    if (listHead == listTail || list == 0) {
		ok = 0;
		break;
	    }
	    *item = list[listHead];
	    valid = pValid[listHead];
	    pValid[listHead] = 0; // either 0 now, or make 0
	    listHead = advance(listHead);
	} while(valid == 0);
	if (ok) {
	    listElements--;
	}
    }
    return(ok);
}

int FIFO_NAME::removeIfFound(GENERIC2(FIFO_NAME,Item)* toRemove) {
    int ok = 1;
    if ((listElements > 0)
	&& pValid[listHead]
	&& list[listHead] == *toRemove) {
	    pValid[listHead]=0;
	    listHead = advance(listHead);
	    listElements--;
	} else {
	    if (listElements == 0 || list == 0) {
		ok = 0;
	    } else {
		//
		// check for common case -- head of list
		//
		int valid = 0;
		//
		// make sure the validity of the item in the list
		//
		if (pValid[listHead] && list[listHead] == *toRemove) {
		    valid = 1;
		    pValid[listHead] = 0;
		    listHead = advance(listHead);
		    listElements--;
		} else {
		    // 
		    //	Mid-list removal -- just nullify the pointer
		    //
		    register GENERIC2(FIFO_NAME,Index) i = listHead;
		    register unsigned int examined = 0;
		    //
		    // Set ok to 0. If we find the itme in the
		    // the list, we set ok to 1, indicating
		    // that it was deleted.
		    //
		    ok = 0;
		    while(examined < listElements) {
			//
			// If this is a valid item to consider...
			//
			if (pValid[i]) {
			    examined++;
			    if (list[i] == *toRemove) {
				ok = 1;
				pValid[i] = 0;
				listElements--;
				break;
			    }
			}
			i = advance(i);
		    }
		}
	    }
	}
    return(ok);
}

int
FIFO_NAME::doStart(GENERIC2(FIFO_NAME,Index)& index,
		  GENERIC2(FIFO_NAME,Item)* item)
{
    index = listHead;
    while (index != listTail) {
	if (pValid[index]) {
	    *item = list[index];
	    return(1);
	}
	index++;
    }
    return(0);
}

int
FIFO_NAME::doDelete(GENERIC2(FIFO_NAME,Index)& index)
{
    int didDelete = 0;
    if (pValid[index]) {
	pValid[index] = 0;
	listElements--;
	didDelete = 1;
    }
    return(didDelete);
}

int
FIFO_NAME::doNext(GENERIC2(FIFO_NAME,Index)& index,
		 GENERIC2(FIFO_NAME,Item)* item)
{
    if (index == listTail) return(0);
    
    index = advance(index);
    
    while (index != listTail) {
	if (pValid[index]) {
	    *item = list[index];
	    return(1);
	}
    }
    return(0);
}

void
FIFO_NAME::doDone()
{
    //
    // Do nothing.
    //
}

int
FIFO_NAME::isEmpty()
{
    return size() == 0;
}

unsigned int
FIFO_NAME::size()
{
    return(listElements);
}
