#include "AwesimeConfig.h"
#include "TimeClock.h"
#include "assert.h"
#include "SimMux.h"

TimeClock::TimeClock(int xservers, SimTimeUnit startTime)
{
    //
    //	Can't use reset because we need to set up service times with
    // NullTime value.
    //
    servers = xservers;
    reset( startTime );
}

TimeClock::~TimeClock()
{
    if ( servers > 1 ) {
	delete isBusy.many;
	delete whenServiced.many;
    }
}


void
TimeClock::reset(SimTimeUnit startTime)
{
    totalServiceTime = 0;
    pTotalReserves = 0;
    pTotalFailures = 0;

    serviceStarted = startTime;
    
    if (servers == 1) {
	whenServiced.single = SimMux::NullTime();
	isBusy.single = 0;
    } else {
	whenServiced.many = new SimTimeUnit[servers];
	isBusy.many = new int[servers];
	for (int i = 0; i < servers; i++) {
	    whenServiced.many[i] = SimMux::NullTime();
	    isBusy.many[i] = 0;
	}
    }
}

inline SimTimeUnit&
TimeClock::serviced(int idx)
{
    assert( idx < servers && idx >= 0 );
    if ( servers == 1 ) {
	return( whenServiced.single );
    } else {
	return( whenServiced.many[idx] );
    }
}

inline int&
TimeClock::busy(int idx)
{
    assert( idx < servers && idx >= 0 );
    if ( servers == 1 ) {
	return( isBusy.single );
    } else {
	return( isBusy.many[idx] );
    }
}

int
TimeClock::punchIn(SimTimeUnit when)
{

    //
    // Time is 'when'. Find inactive server & reserve. If no inactive
    // servers, return 0.
    //

    int foundOne = 0;
    for ( int i = 0; i < servers; i++ ) {
	if ( busy(i) ) {
	    //
	    // update service stats
	    //
	    totalServiceTime += (when - serviced(i));
	    serviced(i) = when;
	} else {
	    if( !foundOne ) {
		busy(i) = 1;
		serviced(i) = when;
		foundOne = 1;
	    }
	}
    }

    if ( foundOne ) {
	pTotalReserves += 1;
    } else  {
	pTotalFailures += 1;
    }

    return ( foundOne );
}

void
TimeClock::punchOut(SimTimeUnit when)
{

    //
    // Time is 'when'. Find inactive server & reserve. If no inactive
    // servers, return 0.
    //

    int foundOne = 0;
    for ( int i = 0; i < servers; i++ ) {
	if ( busy(i) ) {
	    //
	    // update service stats
	    //
	    totalServiceTime += (when - serviced(i));
	    serviced(i) = when;

	    //
	    // idle this busy server
	    //
	    if ( !foundOne ) {
		foundOne = 1;
		busy(i) = 0;
		return;
	    }
	}
    }
    assert( foundOne == 1 );
}

