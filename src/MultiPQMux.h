#ifndef _MultiPQMux_h_
#define _MultiPQMux_h_

#include "MultiCpuMux.h"

class MultiPQMux : public MultiCpuMux {

    virtual ThreadContainer* allocateThreadContainer();
public:
    MultiCpuMux* clone();   //used only in posix versions
    MultiPQMux(int debug = 0);
    virtual ~MultiPQMux();
};

inline MultiCpuMux* MultiPQMux::clone()
{ 
  return new MultiPQMux(CpuMux::Debug);
}
#endif
