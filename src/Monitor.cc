#ifdef __GNUG__
#  pragma implementation
#endif

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Monitor.h"
#include "FifoScheduler.h"
#include "Thread.h"

void Monitor::reserve()
{
    lock.reserve();
}

int Monitor::reserveNoBlock()
{
    if ( lock.reserveNoBlock() ) {
	return 1;
    } else {
	return 0;
    }
}
    
void Monitor::release()
{
    lock.release();
}
