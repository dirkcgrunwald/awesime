//-*-c++-*-
#ifndef _FibHeap_h_
#define _FibHeap_h_

//
// The Fibheap class is the base class for FibHeap and MaxFibHeap.
//

#include <Pix.h>
#include <assert.h>

//----------------------------------------------------------------------------
// Fibonacci Heap - modelled after LEDA version.
//----------------------------------------------------------------------------

template <class Key, class Data>
class FibHeap
{ 
 protected:
  ///////////////////////////////////////////////////////////////////////////
  // Beginning of Node class
  ///////////////////////////////////////////////////////////////////////////

    class node {
  public:
      node* next;          // used to link all used items 
      node* prev;          // used to link all used items 

      node* left;          // left and right siblings (circular List)
      node* right;   
      node* parent;        // parent node
      node* children;      // a child
      short mark;                   // ( 1=true, 0=false )
      short rank;                   // number of children
      Key key;
      Data data;
    
      node(Key k, Data d, node* n, node *p) {
	  key = k; data = d; next = n; prev = p;
	  rank = 0; mark = 0; parent = 0; children = 0;
      }

    //
    // Link a child to the current node.
    //
    void link(node* child) {
	child->left->right = child->right;
	child->right->left = child->left;
	if ( children == 0 ) {
	    children = child;
	    child->left = child;
	    child->right = child;
	}
	else {
	    child->left = children;
	    child->right = children->right;
	    children->right->left = child;
	    children->right = child;
	}

	child->parent = this;
	rank++;
	child->mark = 0;
    }

    void cut(node* m_ptr) { 
	if ( parent->rank == 1 ) {
	    parent->children = 0;
	}
	else  {
	    if ( parent->children == this ) {
		parent->children = right;
	    }
	    left->right = right;
	    right->left = left;
	} 
	parent->rank--;
	parent=0;

	left = m_ptr;
	right = m_ptr->right;
	m_ptr->right->left = this;
	m_ptr->right = this;
    }
};
  
  ///////////////////////////////////////////////////////////////////////////
  // End of Node class
  ///////////////////////////////////////////////////////////////////////////

  int node_count;  // number of nodes

  node* node_list; // List of all nodes
  node** rank_field;
  
  int max_rank() const {
      // max_rank <= 1.4404 * log_2(node_count)
      int x = 0; int n = node_count;
      while (n) { x++; n >>= 1; }
      x = x * 3;
      x = x / 2;
      return x;
  }
  
  node* new_node(Key k, Data d) {
      //
      // Create a new node and splice it into the doubly linked list.
      //
      node* x = new node(k,d,node_list, NULL);
      if ( node_list ) {
	  node_list -> prev = x;
      }
      node_list = x;
      return node_list;
  }

  void unlink_node(node *n) {
      node *prev = n -> prev;
      node *next = n -> next;
      if ( prev ) {
	  prev -> next = next;
      } else {
	  //
	  // If no predecessors, this must be the first node in node_list..
	  //
	  assert( n == node_list );
	  node_list = n -> next;
      }
      if ( next ) { next -> prev = prev; }
      delete n;
  }

  node* item(Pix p) const { return (node*)p; }

public:

  //
  // Iterator functions, used in other functions. We can't let the
  // user change the key, but we can let them change the data.
  //

  Key  key(Pix xx) const { node* x = (node *) xx; return x->key; }
  Data&  data(Pix xx) const { node* x = (node *) xx; return x->data;}
  Data&  operator()(Pix xx) const { node* x = (node *) xx; return x->data;}

  Pix first() const { return (Pix) node_list; }

  void next(Pix& pp) const {
      node* p = (node *) pp; assert(p); pp = (Pix) p -> next;
  }

  void clear() {
      int i = node_count;
    for (node *FF = node_list; FF != 0; ) {
	node *NN = FF -> next;
	delete FF;
	i--;
	FF = NN;
    }
    node_count = 0;
    node_list  = 0;
  }

  int  size()  const { return node_count; }

 /////////////////////////////////////////////////////////////////////////////
 // Constructors/Destructors
 /////////////////////////////////////////////////////////////////////////////

  FibHeap() {
      node_count = 0; node_list = 0;
      rank_field = new node*[64];
  }

  FibHeap(const FibHeap<Key,Data>& old) {
      node_count = 0; node_list = 0;
      rank_field = new node*[64];
  }

 ~FibHeap()  {
     clear();
     delete rank_field;
 }

};

#endif
