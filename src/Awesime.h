// -*-c++-*- 
// Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
// 
// This file is part of Awesime.
// 
// Awesime is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY.  No author or distributor accepts responsibility to
// anyone for the consequences of using it or for whether it serves any
// particular purpose or works at all, unless he says so in writing.
// Refer to the GNU General Public License for full details.
// 
// Everyone is granted permission to copy, modify and redistribute
// Awesime, but only under the conditions described in the Gnu General
// Public License.  A copy of this license is supposed to have been given
// to you along with Awesime so you can know your rights and
// responsibilities.  It should be in a file named COPYING.  Among other
// things, the copyright notice and this notice must be preserved on all
// copies.
// 
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef AWESIMEH
#define AWESIMEH

#ifdef __GNUG__
#  pragma interface
#endif

//
//	Awesime.h
//
//	Awesime is a component of the Discret Event Simulation Environment.
//	The major purpose of Awesime is to provide a virtual function
//	resetInternalState which is used to reset a simulation.
//	In addition, each Awesime can provide a function 'reportState' which
//	lists the current state of that simulation component in a standard
//	format.
//
//	These virtuals are used to implement Resetables and Reportables,
//	and the Awesime serves to define a simulation environment.
// 

#include <stdlib.h>
#include "AwesimeConfig.h"

using namespace std;

class Awesime;
typedef Awesime *AwesimePtr;
typedef void *VoidPtr;

class Awesime {
protected:
    int debugFlag;

public :

    Awesime(int xdebug = 0);

    virtual int debug();
    virtual int debug(int x);

    virtual void classPrintOn(ostream& s);
    virtual void subClassResponsibility(char *name = "");
};

inline
Awesime::Awesime(int xdebug)
{
    debugFlag = xdebug;
}

inline ostream&
operator<<(ostream& strm, Awesime& ob)
{
    ob.classPrintOn(strm);
    return strm;
}

#endif
