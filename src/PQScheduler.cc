#include "Thread.h"
#include "PQScheduler.h"
#include "iostream"
#include "assert.h"

PQScheduler::PQScheduler(int seed1,int seed2)
{
    // empty
}

void
PQScheduler::add(Thread *t)
{
    pq.insert(t -> priority(), t);
}

Thread *
PQScheduler::remove()
{
    if ( pq.empty() ) {
	return(0);
    } else {
	Thread *t = pq.front();
	pq.deq();
	return( t );
    }
}

int PQScheduler::isEmpty()
{
    return (pq.empty());
}

unsigned int PQScheduler::size()
{
    return(pq.size());
}

Pix PQScheduler::first()
{
    return( pq.first() );
}

void PQScheduler::next(Pix& i)
{
    pq.next(i);
}

Thread*&
PQScheduler::operator()(Pix i)
{
    return(pq(i));
}
