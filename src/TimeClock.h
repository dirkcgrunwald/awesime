#ifndef _TimeClock_h_
#define _TimeClock_h_

#include "TimeUnit.h"

//
// A time clock does not manage queuing, like a facility; it simply
// records the utilizaton of a resource set
//

class TimeClock {

    int servers;

    struct {
	SimTimeUnit single;	// valid when servers == 1
	SimTimeUnit *many;	// valid when servers > 1
    } whenServiced;

    struct {
	int single;
	int *many;	// valid when servers > 1
    } isBusy;

    SimTimeUnit serviceStarted;
    SimTimeUnit totalServiceTime;

    long pTotalReserves;
    long pTotalFailures;

    //
    // access functions for the variable-sized data above
    //
    SimTimeUnit& serviced(int);
    int& busy(int);

public:
    TimeClock(int servers, SimTimeUnit start);
    virtual ~TimeClock();

    virtual int punchIn(SimTimeUnit when);
    virtual void punchOut(SimTimeUnit when);
    virtual void reset(SimTimeUnit when);
    virtual double utilization(SimTimeUnit when);

    virtual long totalAttempts();
    virtual long totalReserves();
    virtual long totalFailures();

    virtual unsigned activeServers();
    virtual unsigned size();
};

#endif
