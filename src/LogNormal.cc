/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/

#ifdef __GNUG__
#  pragma implementation
#endif

#include "LogNormal.h"
#include <math.h>

//
//	See Simulation, Modelling & Analysis by Law & Kelton, pp260
//
// 

void LogNormal::setState()
{
    double m2 = logMean * logMean;
    pMean = log(m2 / sqrt(logVariance + m2) );
    pVariance = log((sqrt(logVariance + m2)/m2 ));
}

LogNormal::LogNormal(double mean, double variance, RNG *gen)
    : Normal(mean, variance, gen)
{
    logMean = mean;
    logVariance = variance;
    setState();
}

double LogNormal::mean() {
    return logMean;
}

double LogNormal::mean(double x)
{
    logMean = x; setState();
    return logMean;
}

double LogNormal::variance() {
    return logVariance;
}

double LogNormal::variance(double x)
{
    logVariance = x; setState();
    return logVariance;
}

double LogNormal::operator()()
{
    return( pow(M_E, Normal::operator()() ) );
}
