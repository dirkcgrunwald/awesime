/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "AwesimeConfig.h"
#include "TreeBarrier.h"
#include "Thread.h"
#include "FifoScheduler.h"
#include "AwesimeConfig.h"
#include <math.h>
#include <iolock.h>
#include <assert.h>

TreeBarrier::TreeBarrier()
{
    pHeight = 0;
    initFlag = 0;
    for (int i=0; i < _MAXIMUM_CPU_MUXS_; i++) {
	BarrierTree[i] = NULL;
	ThreadPiles[i].pile = NULL;
    }
}

TreeBarrier::~TreeBarrier()
{
  for (int i=0; i < _MAXIMUM_CPU_MUXS_; i++) {
       delete BarrierTree[i];
       delete ThreadPiles[i].pile;
  }
}

void TreeBarrier::initBarrier(int h , int cpus) {    
    int index,i;

    lck.reserve();
    if (cpus > h )
	cpus = h;
    numCpus = cpus;
    pHeight = h;

    for (index = 0; index < numCpus; index++) {
       if (BarrierTree[index] == NULL)
          BarrierTree[index] = new TREENODE;
       BarrierTree[index]->count = 0;
       BarrierTree[index]->expected_count = 0;
       BarrierTree[index]->locksense = 0;
       BarrierTree[index]->parent = NULL;
       if (ThreadPiles[index].pile == NULL)
	   ThreadPiles[index].pile = new FifoScheduler;
       ThreadPiles[index].expected_count = 0;
       ThreadPiles[index].count = 0;
       ThreadPiles[index].initCount = 0;
     }
     interior_nodes =0;
     for (i=1; (i*FANIN) <= numCpus; i*=FANIN)
	interior_nodes +=i;
     numThreads = 0;
     for (i=0; i < pHeight; i++) 
        ThreadPiles[i%numCpus].expected_count++;
    initFlag = 1;
    lck.release();
}

void TreeBarrier::checkOut() {

    Thread *p = CpuMux::CurrentThread();
    
    p->affinity(-1);
}

void TreeBarrier::checkIn() {
    int index;
    TREENODEPTR node;
    int myHeightReached = 0;

    while (initFlag == 0);

    int iYam = CpuMux::cpuId();
    Thread *thread = CpuMux::CurrentThread();


    int oldiYam = iYam;
    int found = 0;

    while (!found) {
        if (ThreadPiles[iYam].initCount  < ThreadPiles[iYam].expected_count ) {
	    ThreadPiles[iYam].lck.reserve();
	    if (ThreadPiles[iYam].initCount < ThreadPiles[iYam].expected_count) 
	       if (++ThreadPiles[iYam].initCount == ThreadPiles[iYam].expected_count)
  	           myHeightReached = 1;
	    ThreadPiles[iYam].lck.release();
	    found = 1;
	}
	else 
  	   iYam = ++iYam%numCpus;
    }

    thread->affinity(iYam);
    if (!myHeightReached) {
	CpuMux::reschedule();
	return;
    }

// PER PROCESS WORK

    if (oldiYam !=iYam) 
	CpuMux::reschedule();

    index = interior_nodes + iYam/FANIN;

    myLeaf[iYam] = BarrierTree[index];
    mySense[iYam] = 1;

    node = BarrierTree[index];	   
    while (node) {  /* initialize combining tree from the leafs up */
        node->lck.reserve();
        node->expected_count++;
        short oldvalue = node->count++;
        node->lck.release();
        if (oldvalue == 0) {
            if (index == 0)
		node->parent = (TREENODE *) NULL;
            else {
		index = (index - 1)/FANIN;
		node->parent = BarrierTree[index];
	    }
	}
        else 
	    break;
	node = node->parent;
    }

    mySense[iYam] = 1;
    BarrierTree[iYam]->locksense = 0;

    // must wait for all processors to complete because the counts in the
    // tree have to be initialized

    lck.reserve();
    numThreads = numThreads + ThreadPiles[iYam].initCount;
    lck.release();
    while (numThreads != pHeight);


}

int
TreeBarrier::reserveByException(Thread *byWho, ExceptionReserve&)
{
    ThreadPiles[CpuMux::cpuId()].pile -> add( byWho );
    return( 1 );
}
 
void
TreeBarrier::releaseAllOfMine(int iYam)
{
   // int count = 0;

    if ( ThreadPiles[iYam].pile != 0 ) {   
	while ( ! ThreadPiles[iYam].pile -> isEmpty() ) {
	    Thread *p = ThreadPiles[iYam].pile -> remove(); 
            CpuMux::AddToCpu(iYam,p);
	    // cout << lock << iYam << " releasing " << p->name() << "\n";
	    // cout << unlock;
	    // count++;
	}
    }
}









