/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/

#ifdef __GNUG__
#  pragma implementation
#endif

#include <Gate.h>
#include "CpuMuxP.h"

Gate::~Gate()
{
    lock.reserve();
    assert( pPileOfThreads.isEmpty() );
    lock.release();
}

//
//	This is only executed in the context of a CpuMux, never
//	a thread.
//

void
Gate::hiddenWait()
{
    CpuMux::Cpu() -> reserveByException( this );
}

int
Gate::reserveByException(Thread *byWho, ExceptionReserve&)
{
    int blocked = 0;

    lock.reserve();
    if ( pState == GateLocked ) {
	pPileOfThreads.add( byWho );
	blocked = 1;
    }
    lock.release();

    return( blocked );
}

void
Gate::open()
{
    lock.reserve();
    pState = GateUnlocked;

    while ( ! pPileOfThreads.isEmpty() ) {
	Thread *p = (Thread *) (pPileOfThreads.remove());
	CpuMux::Cpu() -> add(p);
    }
    
    lock.release();
}
