/* This may look like C code, but it is really -*- C -*- */
/*	(well, it's a mix of C and C++) */

/* 
 *  Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)
 *  
 *  This file is part of Awesime.
 *  
 *  Awesime is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY.  No author or distributor accepts responsibility to
 *  anyone for the consequences of using it or for whether it serves any
 *  particular purpose or works at all, unless he says so in writing.
 *  Refer to the GNU General Public License for full details.
 *  
 *  Everyone is granted permission to copy, modify and redistribute
 *  Awesime, but only under the conditions described in the Gnu General
 *  Public License.  A copy of this license is supposed to have been given
 *  to you along with Awesime so you can know your rights and
 *  responsibilities.  It should be in a file named COPYING.  Among other
 *  things, the copyright notice and this notice must be preserved on all
 *  copies.
 *  
 */

#ifndef SpinLock_h
#define SpinLock_h

#ifdef __GNUG__
#  pragma interface
#endif

/*
 *
 *	Implement a UNIX process spin-lock. Suitable for Threads,
 *	(if you know blocking thread isn't switched) and for Unix
 *	processes.
 */

#include <assert.h>
#include <AwesimeConfig.h>

#ifdef _SPIN_NO_INLINE_
#  define _MALLOC_INLINE_ 
#else
#  define _MALLOC_INLINE_ inline
#endif

#include <SpinLock-rep.h>

class SpinLock {
  VolatileSpinLockType state;
 public:
  SpinLock();
  void reserve();
  int reserveNoBlock();
  void release();
};

inline SpinLock::SpinLock()
{
  state = SpinLockFree;
}

#ifndef _SPIN_NO_INLINE_
inline void SpinLock::reserve()
{
  SpinLockReserve(&state);
}

inline int SpinLock::reserveNoBlock()
{
  return( SpinLockReserveNoBlock(&state) );
}
#endif

inline void SpinLock::release()
{
#ifdef __GNUG__
    // This is done to avoid code motion in G++. Is there any other
    // way of doing this?
    //
  asm ("");
#endif
  SpinLockRelease(&state);
}

#endif
