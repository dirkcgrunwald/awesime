//-*-c++-*-
#ifndef _CUT_FibHeap_h_
#define _CUT_FibHeap_h_

//
// The Fibheap class is the base class for FibHeap and MaxFibHeap.
//

#include <assert.h>
#include "AwesimeConfig.h"

//----------------------------------------------------------------------------
// Fibonacci Heap - modelled after LEDA version.
//----------------------------------------------------------------------------

//
// Turn on this assertion if you run into memory corruption problems.
// It checks various invariants to make certain your heap is still valid.
//

// #define CHECK_FIBHEAP_INVARIANTS

template <typename Key, typename Data, typename Compare>
class CUT_FibHeap
{ 
public:
  typedef void *Pix;
 protected:
  ///////////////////////////////////////////////////////////////////////////
  // Beginning of Node class
  ///////////////////////////////////////////////////////////////////////////

    class node {
  public:
      node* next;          // used to link all used items 
      node* prev;          // used to link all used items 

      node* left;          // left and right siblings (circular List)
      node* right;   
      node* parent;        // parent node
      node* children;      // a child
      short mark;                   // ( 1=true, 0=false )
      short rank;                   // number of children
      Key key;
      Data data;
    
      node(Key k, Data d, node* n, node *p) {
	init(k,d,n,p);
      }

      void init(Key k, Data d, node* n, node *p) {
	  key = k; data = d; next = n; prev = p;
	  rank = 0; mark = 0; parent = 0; children = 0;
	  right = 0;
	  left = 0;
      }

      void print(ostream& str) {
	str << "[ ptr = " << this << ", key = " << key << ", data = " << data;
	str << ",n = " << next << ", p = " << prev << "]\n";
      }

    //
    // Link a child to the current node.
    //
    void link(node* child) {
	child->left->right = child->right;
	child->right->left = child->left;
	if ( children == 0 ) {
	    children = child;
	    child->left = child;
	    child->right = child;
	}
	else {
	    child->left = children;
	    child->right = children->right;
	    children->right->left = child;
	    children->right = child;
	}

	child->parent = this;
	rank++;
	child->mark = 0;
    }

    void cut(node* m_ptr) { 
	if ( parent->rank == 1 ) {
	    parent->children = 0;
	}
	else  {
	    if ( parent->children == this ) {
		parent->children = right;
	    }
	    left->right = right;
	    right->left = left;
	} 
	parent->rank--;
	parent=0;

	left = m_ptr;
	right = m_ptr->right;
	m_ptr->right->left = this;
	m_ptr->right = this;
    }
    };
  
  //
  // Allocate a node using the linked list..
  //
  node* linked_list;

  node *node_get(Key k, Data d, node* n, node *p)
  {
    if ( linked_list == NULL ) {
      return new node(k,d,n,p);
    } else {
      node* foo = linked_list;
      linked_list = linked_list -> next;
      foo -> init(k,d,n,p);
      return foo;
    }
  }

  void node_put(node *x) {
    x -> next = linked_list;
    linked_list = x;
  }


  ///////////////////////////////////////////////////////////////////////////
  // End of Node class
  ///////////////////////////////////////////////////////////////////////////

  int node_count;  // number of nodes

  node* node_list; // List of all nodes
  node** rank_field;
  
  int max_rank() const {
      // max_rank <= 1.4404 * log_2(node_count)
      int x = 0; int n = node_count;
      while (n) { x++; n >>= 1; }
      x = x * 3;
      x = x / 2;
      return x;
  }
  
  node* new_node(Key k, Data d) {
      //
      // Create a new node and splice it into the doubly linked list.
      //
      node* x = node_get(k,d, node_list, NULL);

      if ( node_list ) {
#ifdef CHECK_FIBHEAP_INVARIANTS
	assert(node_list -> prev == NULL);
#endif
	node_list -> prev = x;
      }

      node_list = x;
      return x;
  }

  void unlink_node(node *n, int line) {
      node *prev = n -> prev;
      node *next = n -> next;

      if ( node_list == n ) {
	node_list = node_list -> next;
	if ( node_list) {
	  node_list -> prev = NULL;
	}
      } else {
	if ( prev != NULL ) {
	  prev -> next = next;
	} else {
	  //
	  // If no predecessors, this must be the first node in node_list..
	  //
#ifdef CHECK_FIBHEAP_INVARIANTS
	  if (node_list != n) {
	    cerr << "called from line " << line << endl;
	    cerr << hex;
	    cerr << "node_list = "; node_list -> print(cerr);
	    cerr << "maxptr = "; maxptr -> print(cerr);
	    cerr << "n = "; n -> print(cerr);
	  }

	  assert( n == node_list );
#endif
	  node_list = n -> next;
	}

	if ( next ) {
	  next -> prev = prev;
	}
      }

      node_put(n);
  }

public:

  //
  // Iterator functions, used in other functions. We can't let the
  // user change the key, but we can let them change the data.
  //

  const Key&  key(Pix xx) const {
    node* x = (node *) xx;
    return x->key;
  }

  Data&  data(Pix xx) const {
    node* x = (node *) xx;
    return x->data;
  }

  Data&  operator()(Pix xx) const {
    node* x = (node *) xx;
    return x->data;
  }

  Pix first() const {
#ifdef CHECK_FIBHEAP_INVARIANTS
    if ( node_list ) {
      assert(node_list -> prev == NULL);
    }
#endif
    return (Pix) node_list;
  }

  Data& front() const {
#ifdef CHECK_FIBHEAP_INVARIANTS
    if ( node_list ) {
      assert(node_list -> prev == NULL);
    }
#endif
    return node_list -> data;
  }

  void invariant() const {
#ifdef CHECK_FIBHEAP_INVARIANTS
    assert(node_count >= 0);

    if ( node_count == 0 ) {
      assert(maxptr == NULL);
      assert(node_list == NULL);
    }

    if ( node_count == 1 ) {
      assert (node_list == maxptr);
      assert (node_list != NULL);
      assert(maxptr -> prev == NULL);
      assert(maxptr -> next == NULL);
    }

    if ( maxptr && maxptr == node_list) {
      assert(maxptr -> prev == NULL);
    }

    if ( node_list ) {
      assert(node_list -> prev == NULL);

#if 0
      int nodes_i_counted = 0;
      node *last = node_list;

      while (last) {
	node *n = last -> next;
	nodes_i_counted++;

	if ( nodes_i_counted > node_count ) {
	  cerr << "nodes_i_counted = " << nodes_i_counted << endl;
	  cerr << "node_count = " << node_count << endl;
	  break;
	}

	if ( n == NULL ) {
	  break;
	}
	assert(n -> prev == last);
	last = n;
      }
      if ( nodes_i_counted != node_count ) {
	abort();
      }
#endif

    }
#endif
  }

  void next(Pix& pp) const {
      node* p = (node *) pp;
      assert(p != NULL);
      invariant();
      pp = (Pix) p -> next;
  }

  void clear() {
    int i = node_count;
    for (node *FF = node_list; FF != 0; ) {
      node *NN = FF -> next;
      node_put(FF);
      i--;
      FF = NN;
    }
    node_count = 0;
    node_list  = 0;
    maxptr = 0;
  }

  int  size()  const {
    return node_count;
  }

 /////////////////////////////////////////////////////////////////////////////
 // Constructors/Destructors
 /////////////////////////////////////////////////////////////////////////////

  CUT_FibHeap() {
      node_count = 0; node_list = 0;
      rank_field = new node*[64];
      maxptr = 0;

      linked_list = NULL;
  }

 ~CUT_FibHeap()  {
     clear();
     while (linked_list) {
       node *e = linked_list -> next;
       delete linked_list;
       linked_list = e;
     }
     delete rank_field;
 }
public:

  /////////////////////////////////////////////////////////////////////////////
  // Insertion/deletion
  /////////////////////////////////////////////////////////////////////////////
  Pix insert(Key k, Data d) {
      invariant();

      node *gnu = new_node(k,d);
      node_count++;

      if ( maxptr == 0 ) {
	  maxptr = gnu;

#ifdef CHECK_FIBHEAP_INVARIANTS
	  if ( maxptr -> prev == NULL ) {
	    assert(maxptr == node_list);
	  }
#endif

	  gnu->right = gnu;
	  gnu->left = gnu;
      }
      else {
	  gnu->left = maxptr;
	  gnu->right = maxptr->right;
	  maxptr->right->left = gnu;
	  maxptr->right = gnu;
	  if (compare(k, maxptr -> key) ) {
	    maxptr = gnu;
	  }
      }

      invariant();
      return ( (Pix) gnu );
  }


  void deq() {

      invariant();

#ifdef CHECK_FIBHEAP_INVARIANTS

      if ( maxptr == NULL ) {
	assert (node_count == 0);
      }

      if ( node_count == 0 ) {
	assert(maxptr == NULL);
	assert(node_list == NULL);
      }

      if ( maxptr -> prev == NULL ) {
	assert(maxptr == node_list);
      }
#endif

      if (maxptr == NULL ) {
	return;
      }

      if ( maxptr -> children ) {
	  node *r2 = maxptr->children;
	  node *r1 = maxptr->right;
	  while ( r2->parent ) {
	    r2->parent = 0; r2 = r2->right;
	  }    
	  r2->left->right = r1;
	  r1->left->right = r2;

	  node *help = r1->left;
	  r1->left = r2->left;
	  r2->left = help;
      }

      int max = max_rank();

      for ( int i = 0 ; i <= max ; i++ ) {
	rank_field[i] = 0;
      }

#ifdef CHECK_FIBHEAP_INVARIANTS
      if (maxptr == node_list && node_count == 1) {
	assert(maxptr -> right == maxptr);
	assert(maxptr -> left == maxptr);
	assert(maxptr -> children == 0);
      }
#endif

      node *lauf  = maxptr->right;
      node *help  = lauf;

      while (lauf != maxptr)  {
	  node* r1 = lauf;
	  int rank = r1->rank;
	  lauf = lauf->right;
	  while ( rank_field[rank] ) {
	      node *r2 = rank_field[rank];
	      rank_field[rank] = 0;
	      if (compare(r1->key, r2->key)) { 
		  r1->link(r2);
	      } else {
		  r2->link(r1);
		  r1 = r2; 
	      }
	      rank++;
	  }
	  rank_field[rank] = r1;
	  if (compare(r1->key, help->key)) {
	    help = r1;
	  }
      }
      maxptr->left->right = maxptr->right;
      maxptr->right->left = maxptr->left;

#ifdef CHECK_FIBHEAP_INVARIANTS
      if ( node_count < 1 ) {
	assert(maxptr == node_list);
	assert(maxptr -> prev == NULL);
	assert(maxptr -> next == NULL);
      }

      if ( maxptr && maxptr == node_list) {
	  assert(maxptr -> prev == NULL);
      }

      assert(node_list -> prev == NULL);

      if ( maxptr && maxptr -> prev == NULL ) {
	assert(maxptr == node_list);
      }
#endif

      unlink_node(maxptr,__LINE__);
      node_count--;
      if (maxptr == help ) {
	maxptr = NULL;
      } else {
	maxptr = help;
      }

      invariant();
  }

  Pix find_top() const {
    return((Pix) maxptr);
  }

  Data& top() const {
#ifdef CHECK_FIBHEAP_INVARIANTS
      assert( maxptr != NULL );
#endif
      return maxptr -> data;
  }

  int  empty() const {
    return (find_top()==0) ? 1 : 0;
  }

 /////////////////////////////////////////////////////////////////////////////
 // Constructors/Destructors
 /////////////////////////////////////////////////////////////////////////////


private:
  node *maxptr;
  Compare compare;
};

#endif
