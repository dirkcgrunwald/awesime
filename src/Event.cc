/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "Event.h"
#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"

Event::~Event()
{
    lock.reserve();
    assert( pPileOfThreads.isEmpty() );
    lock.release();
}

void
Event::releaseAll()
{
    while ( ! pPileOfThreads.isEmpty() ) {
	Thread *p = (Thread *) (pPileOfThreads.remove());
	CpuMux::Cpu() -> add(p);
    }
}

void
Event::trigger()
{
    lock.reserve();
    generation++;
    releaseAll();
    lock.release();
}

void
Event::waitFor()
{
    lock.reserve();
    int gen = generation;
    lock.release();

    for (int i = 0; i < pLoops; i++) {
	if (gen != generation) break;
    }

    if (gen == generation) {
	CpuMux::Cpu() -> reserveByException( this );
    }
}

int
Event::reserveByException(Thread *byWho, ExceptionReserve&)
{
    lock.reserve();
    pPileOfThreads.add( byWho );
    lock.release();
    return(1);
}

int
Event::size()
{
    lock.reserve();
    int s = pPileOfThreads.size();
    lock.release();
    return(s);
}

