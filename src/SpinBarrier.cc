/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "AwesimeConfig.h"
#include "SpinBarrier.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

extern "C" {
  pid_t getpid(void);
}

//
//	Make everyone exit
//
#ifdef _USE_SPIN_LOCKS_

void
SpinBarrier::lower()
{
    reserve();
    generation++;
    pCount = 0;
    release();
}

int
SpinBarrier::rendezvous()
{
    reserve();
    pCount++;

    //
    // Waiting people just read generation, and the final process
    // just writes it, so we dont really even need to use the locks here.
    //
    if ( pCount == pHeight ) {
	pCount = 0;
	generation++;
	release();
    } else {

	int gen = generation;
	VolatileInt *genp = &generation;	// bug in 1.35

	release();
	int timesAround = 0;
	int totalGetPids = 0;
	while ( gen == *genp ) {
	    if ( timesAround > pLoops ) {
		(void) getpid();
		timesAround = 0;
		totalGetPids++;
		if ( pMaxLoops > 0 && totalGetPids > pMaxLoops ) {
		    return(0);
		}
	    }
	    timesAround++;

	}
    }
    return(1);
}
#endif /* _USE_SPIN_LOCKS_ */

void
SpinBarrier::height(int newHeight)
{
    reserve();
    pCount = 0;
    generation++;
    pHeight = newHeight;
    release();
}
