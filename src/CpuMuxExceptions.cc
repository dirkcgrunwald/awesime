/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//

#include "CpuMuxExceptions.h"
#include "ReserveByException.h"
#include "ExceptionClassP.h"
#include "CpuMux.h"
#include "SpinBarrier.h"
#include "iostream"
#include "Thread.h"
#include "iolock.h"

void
ExceptionReserve::handleException()
{
    if ( ! toReserve -> reserveByException(who, *this)) {
	//
	// Never modified the current thread pointer, we might
	// be doing this as another thread...
	//
	// This means the thread attempting to block did not
	// in fact block - this is a rare event (right?)
	//
	CpuMux::add( who );
    }
}

void
ExceptionTerminate::handleException()
{
    // do before delete so thread frees stack
#if 0
    cerr << lock;
    cerr << CpuMux::name() << "@" << hex(long(CpuMux::Cpu())) << " -> ";
    cerr << hex(long(CpuMux::Cpu() -> currentThread)) << "\n";
    cerr << unlock;
#endif

    delete deadThread;
}

void
ExceptionReschedule::handleException()
{
    //
    // Switch to another thread when we havenot gotten
    // rid of the current one.
    //
//    Thread *t = CpuMux::CurrentThread();
//    assert( t != 0 );

    CpuMux::add( oldThread);
    setCurrentThread( executeDirectly );
}

void
ExceptionIveSuspended::handleException()
{
    //
    // Switch to another thread when we havenot gotten
    // rid of the current one.
    //
    setCurrentThread( 0 );
}
