/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1988 University of Illinois, Urbana, Illinois
// Copyright (C) 1989 University of Colorado, Boulder, Colorado
// Copyright (C) 1990 University of Colorado, Boulder, Colorado
//
// written by Dirk Grunwald (grunwald@foobar.colorado.edu)
//
#ifndef ReserveByException_h
#define ReserveByException_h

#ifdef __GNUG__
#  pragma interface
#endif

class ExceptionReserve;
class Thread;

#include "AwesimeConfig.h"
#include <CpuMux.h>
#include "CpuMuxP.h"

class ReserveByException {
    friend class ExceptionReserve;

private:
    virtual int reserveByException(Thread *byWho, ExceptionReserve&);

    //
    // These allows subclasses to pass args to themselves.
    //
protected:
    void setContext(int i_, void *);
    void *getContext(int i_);

public:
    ReserveByException();
//    virtual void classPrintOn(ostream&);
};

inline
ReserveByException::ReserveByException()
{
    // empty
}

inline void
ReserveByException::setContext(int i, void *value)
{
    CpuMux::Cpu() -> reserveException.context[i] = value;
}

inline void *
ReserveByException::getContext(int i)
{
    return( CpuMux::Cpu() -> reserveException.context[i] );
}

#ifdef UNDEF
inline ostream&
operator<<(ostream& strm, const ReserveByException& ob)
{
    ob.classPrintOn(strm);
    return strm;
}
#endif

#endif /* ReserveByException_h */
