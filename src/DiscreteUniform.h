// This may look like C code, but it is really -*- C++ -*-
/* 
Copyright (C) 1988 Free Software Foundation
    written by Dirk Grunwald (grunwald@cs.uiuc.edu)

This file is part of GNU CC.

GNU CC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.  No author or distributor
accepts responsibility to anyone for the consequences of using it
or for whether it serves any particular purpose or works at all,
unless he says so in writing.  Refer to the GNU CC General Public
License for full details.

Everyone is granted permission to copy, modify and redistribute
GNU CC, but only under the conditions described in the
GNU CC General Public License.   A copy of this license is
supposed to have been given to you along with GNU CC so you
can know your rights and responsibilities.  It should be in a
file named COPYING.  Among other things, the copyright notice
and this notice must be preserved on all copies.  
*/
#ifndef _DiscreteUniform_h
#define _DiscreteUniform_h 1

#include <AwesimeConfig.h>
#include <Random.h>

//
//	The interval [lo..hi)
// 

class DiscreteUniform: public Random {
    u32bit pLow;
    u32bit pHigh;
    double delta;
public:
    DiscreteUniform(u32bit low, u32bit high, RNG *gen);

    u32bit low();
    u32bit low(int x);
    u32bit high();
    u32bit high(int x);

    virtual double operator()();
};

//#ifdef __OPTIMIZE__


inline DiscreteUniform::DiscreteUniform(u32bit low, u32bit high, RNG *gen)
: Random(gen)
{
    pLow = (low < high) ? low : high;
    pHigh = (low < high) ? high : low;
    delta = (pHigh - pLow) + 1;
}

inline u32bit DiscreteUniform::low() { return pLow; }

inline u32bit DiscreteUniform::low(int x) {
  u32bit tmp = pLow;
  pLow = x;
  delta = (pHigh - pLow) + 1;
  return tmp;
}

inline u32bit DiscreteUniform::high() { return pHigh; }

inline u32bit DiscreteUniform::high(int x) {
  u32bit tmp = pHigh;
  pHigh = x;
  delta = (pHigh - pLow) + 1;
  return tmp;
}

//#endif
#endif
