/* 
Copyright (C) 1990 by Dirk Grunwald (grunwald@foobar.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/

#ifdef __GNUG__
#  pragma implementation
#endif
#include "RNG.h"
#include "AwesimeConfig.h"
#include "assert.h"
#include "AwesimeConfig.h"
#include <stdlib.h>

int RNG::initialized = 0;
PrivateRNGSingleType RNG::singleMantissa;
PrivateRNGDoubleType RNG::doubleMantissa;

RNG::RNG()
{
    if (!initialized) {
	if ( sizeof(double) != 2 * sizeof(u32bit) ) {
	    cerr << "ERROR in configuration:\n";
	    cerr << "sizeof(double)   = " << sizeof(double) << "\n";
	    cerr << "2*sizeof(u33bit) = " << 2 * sizeof(u32bit) << "\n";
	    cerr << "These two sizes should be the same.\n";
	    cerr << "Change the AwesimeConfig files if they'r enot\n";
	    assert(sizeof(double) == 2 * sizeof(u32bit));
	}
	//
	//	The following is a hack that I attribute to
	//	Andres Nowatzyk at CMU. The intent of the loop
	//	is to form the smallest number 0 <= x < 1.0,
	//	which is then used as a mask for two longwords.
	//	this gives us a fast way way to produce double
	//	precision numbers from longwords.
	//
	//	I know that this works for IEEE and VAX floating
	//	point representations.
	//
	//	A further complication is that gnu C will blow
	//	the following loop, unless compiled with -ffloat-store,
	//	because it uses extended representations for some of
	//	of the comparisons. Thus, we have the following hack.
	//	If we could specify #pragma optimize, we wouldn't need this.
	//

	PrivateRNGDoubleType t;
	PrivateRNGSingleType s;

#ifdef _IEEE
	
	t.d = 1.5;
	if ( t.u[1] == 0 ) {		// sun word order?
#ifdef _LITTLE_ENDIAN_
	    cout << "Update Config.h with your machine endianess";
	    exit(0);
#endif
	    t.u[0] = 0x3fffffff;
	    t.u[1] = 0xffffffff;
	}
	else {
#ifdef _BIG_ENDIAN_
	    cout << "Update Config.h with your machine endianess";
	    exit(0);
#endif
	    t.u[0] = 0xffffffff;	// encore word order?
	    t.u[1] = 0x3fffffff;
	}

	s.u = 0x3fffffff;
#else
	volatile double x = 1.0;
	volatile double y = 0.5;
	do {			    // find largest fp-number < 2.0
	    t.d = x;
	    x += y;
	    y *= 0.5;
	} while (x != t.d && x < 2.0);

	volatile float xx = 1.0;
	volatile float yy = 0.5;
	do {			    // find largest fp-number < 2.0
	    s.s = xx;
	    xx += yy;
	    yy *= 0.5;
	} while (xx != s.s && xx < 2.0);
#endif
	// set doubleMantissa to 1 for each doubleMantissa bit
	doubleMantissa.d = 1.0;
	doubleMantissa.u[0] ^= t.u[0];
	doubleMantissa.u[1] ^= t.u[1];

	// set singleMantissa to 1 for each singleMantissa bit
	singleMantissa.s = 1.0;
	singleMantissa.u ^= s.u;

	initialized = 1;
    }
}
