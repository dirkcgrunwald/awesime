/* 
Copyright (C) 1990 by David Wagner (wagner@bullwinkle.colorado.edu)

This file is part of Awesime.

Awesime is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY.  No author or distributor accepts responsibility to
anyone for the consequences of using it or for whether it serves any
particular purpose or works at all, unless he says so in writing.
Refer to the GNU General Public License for full details.

Everyone is granted permission to copy, modify and redistribute
Awesime, but only under the conditions described in the Gnu General
Public License.  A copy of this license is supposed to have been given
to you along with Awesime so you can know your rights and
responsibilities.  It should be in a file named COPYING.  Among other
things, the copyright notice and this notice must be preserved on all
copies.

*/
// This may look like C code, but it is really -*- C++ -*-
// 
// Copyright (C) 1991 University of Colorado, Boulder, Colorado
//
// written by David Wagner (wagner@bullwinkle.colorado.edu)
//

#ifdef __GNUG__
#  pragma implementation
#endif

#include "Condition.h"
#include "CpuMux.h"
#include "CpuMuxP.h"
#include "Thread.h"
#include "FifoScheduler.h"

Condition::Condition(Monitor *m, ThreadContainer *scheduler=0)
{
    monitor = m;

    if (scheduler == 0) {
        cScheduler = new FifoScheduler;
        iDidAlloc = 1;
    } else {
        cScheduler = scheduler;
        iDidAlloc = 0;
    }
}

Condition::~Condition()
{
    monitor = 0;

    if (cScheduler != 0) {
	if (!cScheduler->isEmpty()) {
	    cerr << "[Condition] Attempted to delete non-empty condition\n";
	    exit(1);
	}
	if (iDidAlloc) delete cScheduler;
	cScheduler = 0;
	iDidAlloc = 0;
    }
}

void
Condition::wait()
{
    // Assumption:  the caller is holding the associated mutex.
    // The semantics of a Condition are that the thread is put to
    // sleep and the associated Monitor is released as one atomic action.
    // Thus, the exception handler will release the mutex.
    CpuMux::reserveByException( this );
    // Re-obtain the mutex before proceeding
    monitor->reserve();
}

int
Condition::reserveByException(Thread *byWho, ExceptionReserve&)
{
    // Suspend this thread and THEN release the mutex
    cScheduler->add( byWho );
    monitor->release();
    return(1);	// blocked
}

void
Condition::signal()
{
    // Assumption:  the caller is holding the associated mutex.
    if ( !cScheduler->isEmpty() ) {
	Thread *p = cScheduler->remove();
	CpuMux::add(p);
    }
}

void
Condition::broadcast()
{
    // Assumption:  the caller is holding the associated mutex.
    CpuMux::addReserve();
    while ( ! cScheduler->isEmpty() ) {
	Thread *p = (Thread *) (cScheduler->remove());
	CpuMux::addUnlocked(p);
    }
    CpuMux::addRelease();
}

int
Condition::size()
{
    int s = cScheduler->size();
    return(s);
}
