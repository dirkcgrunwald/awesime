#ifndef _SEv_h
#define _SEv_h 1

#ifdef __GNUG__
#endif

#include "TimeUnit.h"
#include "AwesimeConfig.h"

using namespace std;

class Thread;
class OEv;

class SEv {
protected:
    Thread *pThread;
    OEv *pOtherEvent;
    int pPriority;   // priority of events occuring at the same time
    SimTimeUnit pTime;
public:
    SEv(int prio = 0);
    SEv(Thread *p, int prio = 0);
    SEv(OEv *oev, int prio = 0);

    SEv(Thread *p, SimTimeUnit when, int prio = 0);
    SEv(OEv *oev, SimTimeUnit when, int prio = 0);

    Thread *thread();
    OEv *oev();
    int isThread();
    int isOtherEvent();
    SimTimeUnit time();

    int compare(SEv &p);
    int operator==(SEv &p);
    int operator<(SEv &p);
    int operator<=(SEv &p);
    int operator>(SEv &p);
    int operator>=(SEv &p);
};


inline
SEv::SEv(int prio)
{
    pThread = 0;
    pPriority = prio;
    pTime = 0;
}

inline
SEv::SEv(Thread *p, int prio)
{
    pThread = p;
    pPriority = prio;
    pTime = 0;
}

inline
SEv::SEv(OEv *oev, int prio)
{
    pThread = 0;
    pOtherEvent = oev;
    pPriority = prio;
    pTime = 0;
}

inline
SEv::SEv(Thread *p, SimTimeUnit w, int prio)
{
    pThread = p;
    pPriority = prio;
    pTime = w;
}

inline
SEv::SEv(OEv *oev, SimTimeUnit w, int prio)
{
    pThread = 0;
    pOtherEvent = oev;
    pPriority = prio;
    pTime = w;
}

inline SimTimeUnit
SEv::time()
{
    return(pTime);
}

inline int
SEv::isThread()
{
    return(pThread != 0);
}

inline int
SEv::isOtherEvent()
{
    return(pThread == 0);
}

inline Thread *
SEv::thread()
{
    return(pThread);
}

inline OEv *
SEv::oev()
{
    return(pOtherEvent);
}

inline int
SEv::compare(SEv &p)
{
    if ( pTime < p.pTime ) {
	return( -1 );
    } else if (pTime == p.pTime ) {
	return (pPriority - p.pPriority);
    } else return ( 1 );
}

inline int
SEv::operator==(SEv &p)
{
    return( pPriority == p.pPriority && pTime == p.pTime );
}

inline int
SEv::operator<(SEv &p)
{
    return( pTime < p.pTime 
	   || ( pTime == p.pTime && pPriority < p.pPriority ) );
}

inline int
SEv::operator<=(SEv &p)
{
    return( pTime < p.pTime 
	   || ( pTime == p.pTime && pPriority <= p.pPriority ) );
}

inline int
SEv::operator>(SEv &p)
{
    return( pTime > p.pTime 
	   || ( pTime == p.pTime && pPriority > p.pPriority ) );
}

inline int
SEv::operator>=(SEv &p)
{
    return( pTime > p.pTime 
	   || ( pTime == p.pTime && pPriority >= p.pPriority ) );
}

extern ostream& operator<<(ostream& str, SEv& sev);

#endif
