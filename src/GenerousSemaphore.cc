#ifdef __GNUG__
#  pragma implementation
#endif

#include "CpuMux.h"
#include "CpuMuxP.h"
#include "GenerousSemaphore.h"
#include "Thread.h"

GenerousSemaphore::GenerousSemaphore(int count, ThreadContainer *scheduler)
: Semaphore(count, scheduler)
{
    // nada
}


//
// A generous semaphore allows the released task to execute directly
// on the current CPU. It does this using the reschedule method.
//
void GenerousSemaphore::release()
{
    Thread *p = 0;
    lock.reserve();
    if (pCount < 0) {
	assert( !pScheduler -> isEmpty() );
	p = pScheduler -> remove();
    }
    pCount++;
    lock.release();

    if (p != 0) {
	CpuMux::reschedule(p);
    }
}
