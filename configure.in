dnl Process this file with autoconf to produce a configure script.

AC_PREREQ(2.50)
AC_INIT([awesime], [3.0], [grunwald@cs.colorado.edu])
AC_CONFIG_SRCDIR([src/Facility.cc])
AM_CONFIG_HEADER(src/aweconfig.h)

#PACKAGE=awesime
#VERSION=3.0

AM_INIT_AUTOMAKE($PACKAGE_NAME, $PACKAGE_VERSION)

dnl Checks for programs.
AC_PROG_CC
AC_PROG_CXX
AC_LANG(C++)
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_RANLIB
AC_PREFIX_DEFAULT([/usr/local])

AC_CANONICAL_BUILD
AC_CANONICAL_HOST
AC_MSG_CHECKING([Checking to see if host $host and compiler $CXX are supported])
case $host in
i486-*-linux-* | i586-*-linux-* | i686-*-linux-* )
  case $CXX in
	g++ | c++)
	  HARDWARE_CONTEXT=HardwareContext-i386.cc
	  HARDWARE_CONTEXT_H=HardwareContext-i386.h
	  HARDWARE_CONTEXTP_H=HardwareContextP-i386.h
          CPU_MUX_H=CpuMux-fork.h
          CPU_MUX_CC=CpuMux-fork.cc
          MULTICPU_MUX_H=MultiCpuMux-fork.h
          MULTICPU_MUX_CC=MultiCpuMux-fork.cc
          RSTREE_H=RSTree-NT.h
          SHAREDMEMORY_CC=SharedMemory-fake.cc
          SHAREDMALLOC_CC=SharedMalloc-fake.cc
	;;
	*)
   	AC_MSG_ERROR([Unsupported compiler/configuration for x86])
	;;
  esac
;;
alphaev67-*-linux | alpha*-linux-*)
  case $CXX in
	g++ | c++)
	  HARDWARE_CONTEXT=HardwareContext-alpha-g++.cc
	  HARDWARE_CONTEXT_H=HardwareContext-alpha-g++.h
	  HARDWARE_CONTEXTP_H=HardwareContextP-alpha-g++.h
          CPU_MUX_H=CpuMux-fork.h
          CPU_MUX_CC=CpuMux-fork.cc
          MULTICPU_MUX_H=MultiCpuMux-fork.h
          MULTICPU_MUX_CC=MultiCpuMux-fork.cc
          RSTREE_H=RSTree-NT.h
          SHAREDMEMORY_CC=SharedMemory-fake.cc
          SHAREDMALLOC_CC=SharedMalloc-fake.cc
	;;
	*)
   	AC_MSG_ERROR([Unsupported compiler/configuration for x86])
	;;
  esac
;;
powerpc-apple-darwin*)
  case $CXX in
	g++ | c++)
	  HARDWARE_CONTEXT=HardwareContext-ppc-g++.cc
	  HARDWARE_CONTEXT_H=HardwareContext-ppc-g++.h
	  HARDWARE_CONTEXTP_H=HardwareContextP-ppc-g++.h
          CPU_MUX_H=CpuMux-fork.h
          CPU_MUX_CC=CpuMux-fork.cc
          MULTICPU_MUX_H=MultiCpuMux-fork.h
          MULTICPU_MUX_CC=MultiCpuMux-fork.cc
          RSTREE_H=RSTree-NT.h
          SHAREDMEMORY_CC=SharedMemory-fake.cc
          SHAREDMALLOC_CC=SharedMalloc-fake.cc
	;;
	*)
   	AC_MSG_ERROR([Unsupported compiler/configuration for x86])
	;;
  esac
;;
*)
   	AC_MSG_ERROR([Unsupported system and compiler configuration])
;;
esac
AC_CONFIG_LINKS(src/HardwareContext.cc:config/$HARDWARE_CONTEXT)
AC_CONFIG_LINKS(src/HardwareContext.h:config/$HARDWARE_CONTEXT_H)
AC_CONFIG_LINKS(src/HardwareContextP.h:config/$HARDWARE_CONTEXTP_H)
AC_CONFIG_LINKS(src/CpuMux.h:config/$CPU_MUX_H)
AC_CONFIG_LINKS(src/CpuMux.cc:config/$CPU_MUX_CC)
AC_CONFIG_LINKS(src/MultiCpuMux.h:config/$MULTICPU_MUX_H)
AC_CONFIG_LINKS(src/MultiCpuMux.cc:config/$MULTICPU_MUX_CC)
AC_CONFIG_LINKS(src/RSTree.h:config/$RSTREE_H)
AC_CONFIG_LINKS(src/SharedMemory.cc:config/$SHAREDMEMORY_CC)
AC_CONFIG_LINKS(src/SharedMalloc.cc:config/$SHAREDMALLOC_CC)
AC_MSG_RESULT($HARDWARE_CONTEXT)

##
## Determine if they need a serial or paralle configuration
AC_ARG_ENABLE(parallel,
     [  --enable-parallel    Use this option to specify parallel vs. serial config],
     [parallel=true],
     [parallel=false])

AM_CONDITIONAL(AWE_PARALLEL_BUILD, test x$parallel = xtrue)   

case $AWE_PARALLEL_BUILD in
 true)
  AWE_CONFIG=Config-parallel.h
  AC_MSG_ERROR([FIXME - need to select appropriate spinlock type])
;;
  *)
  AWE_CONFIG=Config-serial.h
  SPINLOCK_REP_H=SpinLock-assert.h
;;
esac
AC_CONFIG_LINKS(src/AwesimeConfig.h:config/$AWE_CONFIG)
AC_CONFIG_LINKS(src/SpinLock-rep.h:config/$SPINLOCK_REP_H)

dnl Checks for libraries.

#awelib version
AWELIB_VERSION=3
AWELIB_SUBVERSION=0
AWELIB_MINORVERSION=0
AC_SUBST(LIBPROC_VERSION)
AC_SUBST(LIBPROC_SUBVERSION)
AC_SUBST(LIBPROC_MINORVERSION)
AWELIB_LIBVERSION=`expr $AWELIB_VERSION + $AWELIB_SUBVERSION`:$AWELIB_MINORVERSION:$AWELIB_SUBVERSION
AC_SUBST(AWELIB_LIBVERSION)

dnl Checks for header files.
AC_HEADER_STDC

AC_CHECK_HEADER(ostream, AC_DEFINE(USE_STD_CXX_HEADERS,1,[check for new style C++ headers]))

dnl Checks for typedefs, structures, and compiler characteristics.

CFLAGS="$CFLAGS"
CXXFLAGS="$CFLAGS"
AC_SUBST(CFLAGS)
AC_SUBST(CXXFLAGS)

# Checks for library functions.

AC_FUNC_MMAP

AC_DEFUN(AC_FUNC_MKSTEMP,
[AC_CACHE_CHECK(whether mkstemp exists,
[AC_TRY_RUN([#include <stdlib.h> 
main() { mkstemp("/tmp/abcXXXXXX"); }],
  ac_cv_func_mkstemp=no, ac_cv_func_mkstemp=yes, ac_cv_func_mkstemp=yes)])
if test $ac_cv_func_mkstemp = yes; then
  AC_DEFINE(MKSTEMP)
fi
])

dnl AC_FUNC_MKSTEMP

AC_CONFIG_FILES([./Makefile
	./src/Makefile
	./examples/Makefile
	./examples/Barrier-cpu/Makefile
	./examples/Barrier-sim/Makefile
	./examples/Eigenvalue/Makefile
	./examples/EventSequence-cpu/Makefile
	./examples/Gauss/Makefile
	./examples/MM1-bogus/Makefile
	./examples/MM1/Makefile
	./examples/MM2/Makefile
	./examples/ParallelMalloc/Makefile
	./examples/Phil-1/Makefile
	./examples/Phil-2/Makefile
	./examples/Phil-3/Makefile
	./examples/Phil-4/Makefile
	./examples/Quantiles/Makefile
	./examples/Random/Makefile
	./examples/Sem-cpu/Makefile
	./examples/SharedMemory/Makefile
	./examples/Simulation/Makefile
	./examples/SR/Makefile
	./examples/TestRandom/Makefile
	./examples/TestRSTree/Makefile
	./examples/TestSim/Makefile
	./examples/WebSim/Makefile])
AC_OUTPUT
