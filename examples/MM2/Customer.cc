#include "SimMux.h"
#include "Customer.h"
#include "Facility.h"

Customer::Customer( double serviceTime_, Foo *q_ )
    : Thread("Customer")
{
    q = q_;
    serviceTime = serviceTime_;
}

void
Customer::main() 
{
    //
    // Wait for facility...
    //
    q -> reserve();
    //
    // Set starting time...
    //
    q -> start = SimMux::CurrentTime();
    //
    // Use the facility for our duration...
    //
    SimMux::hold( serviceTime );
    //
    // Release it..
    //
    q -> release();
}
