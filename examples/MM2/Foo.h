//-*-c++-*-
#ifndef _Foo_h_
#define _Foo_h_

#include <Facility.h>
#include <TimeClock.h>
#include <SimMux.h>

class Foo : public Facility {
public:
    SimTimeUnit start;

    Foo () : Facility(2)
    {
	start = 0;
    }

    SimTimeUnit residual() {
	return SimMux::CurrentTime() - start;
    }
};

#endif
