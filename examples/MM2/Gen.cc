#include <sys/time.h>
#include "SimMux.h"
#include "Customer.h"
#include "Gen.h"
#include "Foo.h"
#include "NegativeExpntl.h"

extern Foo* Queues[2];

Gen::Gen(int customers_, double meanService, double meanArrival)
	: Thread( "Gen", 10000),
	  rng(time(0), time(0)),
	  serviceTime(meanService, &rng),
	  arrivalTime(meanArrival, &rng),
	  which_queue(0,1,&rng)
{
    customers = customers_;
}

void
Gen::main()
{
    //
    // Add all the customers
    //

    for (int i = 0; i < customers; i++ ) {
	Foo *q1 = Queues[0];
	Foo *q2 = Queues[1];
	Foo* q;

	if ( q1 -> isEmpty() ) {
	    q = q1;
	} else if (q2 -> isEmpty() ) {
	    q = q2;
	} else {
	    //
	    // Both  buys
	    if ( q1 -> residual() < q2 -> residual() ) {
		q = q1;
	    } else {
		q = q2;
	    }
	}

	SimMux::add( new Customer( serviceTime(), q));
	SimMux::hold( arrivalTime() );
    }
}
