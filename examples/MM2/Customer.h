#ifndef Customer_h
#define Customer_h

#include "Thread.h"
#include "Foo.h"

class Customer : public Thread {
    Foo *q;
    double serviceTime;
public:
    Customer( double serviceTime, Foo* q);
    virtual void main();
};

#endif
