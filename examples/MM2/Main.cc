#include "SingleSimMux.h"
#include "MonitorSimMux.h"

#include "Main.h"
#include "Customer.h"
#include "Gen.h"
#include "Foo.h"

Foo *Queues[2];

main(int argc, char **argv)
{
    
    int maxCustomers = (argc > 1) ? atoi(argv[1]) : MAX_CUSTOMERS;
    double meanArrival = (argc > 2) ? atof(argv[2]) : T_ARRIVAL;
    double meanService = (argc > 3) ? atof(argv[3]) : T_SERVICE;
    int debug = (argc > 4) ? 1 : 0;
    
//    MonitorSimMux Simulator( debug );
    SingleSimMux Simulator( debug );
    
    Simulator.add(new Gen(maxCustomers, meanService, meanArrival));

    cout << "\nSINGLE SERVER Queueing System:\n";
    cout << "-----------------------------\n";
    cout << "\tNumber of Customers   \t" << maxCustomers << "\n";
    cout << "\tMean InterArrival Time\t" << meanArrival << "\n";
    cout << "\tMean Service Time     \t" << meanService << "\n";
    
    Queues[0] = new Foo;
    Queues[1] = new Foo;

    Simulator.fireItUp(1,100 * 4196);
    
    for (int q = 0; q < 2; q++) {
	cout << "Server utilization was : ";
	cout << Queues[q] -> utilization() << "(should be ~0.5)\n";
	cout << "Server mean delay was  : ";
	cout << Queues[q] -> meanDelay() << " (should be ~0.5)\n";
	cout << "Server mean queue was  : ";
	cout << Queues[q] -> meanQueueLength() << "(should be ~1)\n";
    }


    double mu = 1.0 / meanService;
    double lambda = 1.0 / meanArrival;
    double rho = lambda/mu;

    cout << "Traffic intensity = " << rho << "\n";
    cout << "Expected number of customers in system = ";
    cout << rho / ( 1 - rho ) << "\n";
    cout << "Variance of customers = ";
    cout << (rho / ( (1-rho) * (1-rho) ) ) << "\n";
    cout << "expected response time is " << (1.0 / mu) / ( 1 - rho ) << "\n";

    cout << "\nDONE\n";
    return 0;
}
