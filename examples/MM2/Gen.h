#ifndef Gen_h
#define Gen_h

#include "Thread.h"
#include "MLCG.h"
#include "NegativeExpntl.h"
#include "DiscreteUniform.h"

class Gen : public Thread {
    int customers;
    MLCG rng;
    NegativeExpntl serviceTime;
    NegativeExpntl arrivalTime;
    DiscreteUniform which_queue;
public:
    Gen(int cust, double svc, double arr);
    virtual void main();
};

#endif
