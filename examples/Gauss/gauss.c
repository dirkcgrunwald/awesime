#include <stdio.h>
#include <task.h>
#include <ulocks.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/param.h>
#include "externs.h"

volatile float **array;
extern int matrix_x,matrix_y;
extern int num_workers;
extern int verbose;
extern volatile int running;

void init_array()
{
  int i,j;
  
  array = (float **)malloc((matrix_x)*sizeof(float *));
  if (array == (float **)-1) 
    printf("OOPSS\n");
  for(i=0;i<matrix_x;i++)
    {
      array[i] = (float *)malloc((matrix_y)*sizeof(float));
      if (array[i] == (float *)-1)
        printf("OOPS 1 %d\n",i);
    }
  for(i=0;i<matrix_x;i++)
    for(j=0;j<matrix_y;j++)
      array[i][j] = (float)((int)random()%10000);
}

void print_matrix()
{
  int i,j;

  for(i=0;i<matrix_x;i++)
    {
      for(j=0;j<matrix_y;j++)
        printf("%6.2f ",array[i][j]);
      printf("\n");
    }
}

do_work(int id)
{
  float *index,*index1,*index2;
  float mult;
  int lsense;
  int n,i,j,k,l,next;

  while (start_signal == 0);
  setblockproccnt(proc_id[id],0);
  lsense = 0;
  n = 0;
  for (k=1;k<matrix_x;k++) {
    i = k;
    l = k-1;
    while ((i%num_workers) != id) i++;
    next = i;
    for(i=next;i<matrix_x;i+=num_workers) {
      mult = array[i][l]/array[l][l];
      for(j=l;j<matrix_x;j++) {
	array[i][j] = array[i][j] - array[l][j] * mult;
      }
    }
    bar((n%2),id,&lsense,n);
    n++;
  }
  running = 0;
  exit(0);
}

