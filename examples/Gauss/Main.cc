#include "MultiFifoMux.h"
//#include "SingleFifoMux.h"
#include "ControlTask.h"
#include "Barrier.h"

main(int argc, char** argv)
{

    int cpus = (argc > 1) ? atoi(argv[1]) : 1;
    int debug = (argc > 2) ? atoi(argv[2]) : 0;
    int X = (argc > 3) ? atoi(argv[3]) : 4;
    int Y = (argc > 4) ? atoi(argv[4]) : 4;
    int noThreads = (argc > 5) ? atoi(argv[5]) : 4;

    MultiFifoMux Cpu(debug);
    cout << "X dim: " << X << "\n";
    cout << "Y dim: " << Y << "\n";
    cout << "Using " << noThreads << " threads\n";

    CpuMux::add( new ControlTask(X,Y,noThreads) );

    Cpu.fireItUp( cpus,0);

    cout << "Done!\n";

    return (0);
}

