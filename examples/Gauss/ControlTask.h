#include "Thread.h"
#include "Statistic.h"

class ControlTask : public Thread {
    int from;
    int to;
    int by;
    double elapsed;
    int noThreads;
    Statistic<double> stats;
    int X;
    int Y;

public:
    static float **array;
    void PrintMatrix();
    ControlTask(int,int,int);
    virtual void main();
};

