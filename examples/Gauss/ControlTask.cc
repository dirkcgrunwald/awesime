#include <stdio.h>
#include "CpuMux.h"
#include "Barrier.h"
#include "ControlTask.h"
#include "Gauss.h"
#include "MLCG.h"
#include <sys/time.h>
#include <sys/times.h>
#include <unistd.h>
#include "iolock.h"


extern Barrier TheBarrier;
extern Barrier GaussBarrier;

float **ControlTask::array;

ControlTask::ControlTask(int matrix_x, int matrix_y, int numThreads)
: Thread("Control Task",
   10000,
   HARDWARE_CONTEXT_CHECK_MPROTECT
   )
{
  int i,j;
  long seed1 = time(0);
  long seed2 = time(0);


  MLCG *rGen = new MLCG(seed1,seed2);
  

  X = matrix_x;
  Y = matrix_y;

  noThreads = numThreads;

  array = (float **)malloc((matrix_x)*sizeof(float *));

  if (array == (float **)-1)
    cerr << "OOPSS\n";

  for(i=0;i<matrix_x;i++)
  {
      array[i] = (float *)malloc((matrix_y)*sizeof(float));
      if (array[i] == (float *)-1)
        cerr << "OOPS 1 " << i << " \n";
  }

  for(i=0;i<matrix_x;i++)
    for(j=0;j<matrix_y;j++)
      array[i][j] = rGen->asFloat();

    
}

void ControlTask::PrintMatrix()
{

    if (X > 30) return;
    cout << "Content of Matrix: \n\n";
    cout.precision(2);
    for(int i=0; i<X;  i++) {
      for(int j=0; j<Y; j++) {
        printf("%7.4f ",array[i][j]);
      }
      cerr << "\n";
    }

}
void
ControlTask::main()
{
    struct tms tim;

    cerr << lock << "Initial " << unlock;
    PrintMatrix();
    
    TheBarrier.height(noThreads+1);
    GaussBarrier.height(noThreads);

    double s_time = times(&tim);

    for (int i=0; i < noThreads; i++) {
      CpuMux::add(new Gauss(i,X,Y,noThreads));
    }

    TheBarrier.rendezvous();

    double eltime = times(&tim) - s_time;
    cerr << lock << "Total time elapsed in secs: " << (eltime/sysconf(_SC_CLK_TCK)) << "\n"<< unlock; 
    cerr << lock << "Final " << unlock;
    PrintMatrix();
}



