//
//	A timing test for barrier. Everything is in the one file.
//


#include "Gauss.h"
#include "CpuMux.h"
#include "Barrier.h"
#include "ControlTask.h"
#include <stdio.h>

Barrier TheBarrier(0);
Barrier GaussBarrier(0);



Gauss::Gauss(int p, int matrix_x, int matrix_y, int numThreads)
: Thread("BarrierTest", 15000, HARDWARE_CONTEXT_CHECK_MPROTECT)
{
    sprintf(namestr,"Gauss-%d", p);
    name(namestr);
    pid = p;
    X = matrix_x;
    Y = matrix_y;
    noThreads = numThreads;
}

void
Gauss::main()
{
    //
    //    cerr << "Gauss " << pid << " starts\n";
    //
    
    int i,j,k,l,next;
    float mult;

    GaussBarrier.rendezvous();
    
    for (k=1; k < X; k++) {
	i = k;
        l = k-1;
        while ((i%noThreads) != pid) i++;
        next = i;
        for (i=next; i<X; i+=noThreads) {
            mult = ControlTask::array[i][l]/ControlTask::array[l][l];
	    for (j=l; j<X; j++) {
    	       ControlTask::array[i][j] -=  ControlTask::array[l][j] *mult;
	    }
        }
        GaussBarrier.rendezvous();
    }
    TheBarrier.rendezvous();
    

}
