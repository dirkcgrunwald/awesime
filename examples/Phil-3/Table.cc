#include "Table.h"
#include "CpuMux.h"

Table TheTable;

Table::Table() : table( PHILOSOPHERS - 1 )
{
}

void
Table::getForks(int left, int right)
{
    table.reserve();
    fork[left].reserve();
    fork[right].reserve();
}

void
Table::returnForks(int left, int right)
{
    fork[right].release();
    fork[left].release();
    table.release();
}
