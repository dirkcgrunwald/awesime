#ifndef _Table_h_
#define _Table_h_

#include "GenerousSemaphore.h"

#define PHILOSOPHERS 5

class Table {
    GenerousSemaphore table;
    Semaphore fork[ PHILOSOPHERS ];
public:
    Table();
    virtual void getForks(int left, int right);
    virtual void returnForks(int left, int right);
};


#endif
