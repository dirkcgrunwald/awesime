
#include "MultiFifoMux.h"
#include "Table.h"
#include "Phil.h"

extern Table TheTable;

main(int argc)
{
    MultiFifoMux Cpu;

    for (int i = 0; i < PHILOSOPHERS; i++) {
	CpuMux::add( new Phil(i) );
    }
    Cpu.fireItUp(3, 20000);
    cout << "All done!\n";

    return 0;
}
