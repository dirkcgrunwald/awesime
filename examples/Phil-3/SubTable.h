#ifndef _SubTable_h_
#define _SubTable_h_

#include "Table.h"

class SubTable : public Table {
    Semaphore table;
    Semaphore fork[ PHILOSOPHERS ];
public:
    SubTable();
    void getForks(int left, int right);
    void returnForks(int left, int right);
};

extern SubTable TheSubTable;

#endif
