#include "SubTable.h"
#include "CpuMux.h"

SubTable TheSubTable;

SubTable::SubTable() : table(PHILOSOPHERS-1) 
{
    //
}

void
SubTable::getForks(int left, int right)
{
    table.reserve();
    fork[left].reserve();
    fork[right].reserve();
}

void
SubTable::returnForks(int left, int right)
{
    fork[right].release();
    fork[left].release();
    table.release();
}
