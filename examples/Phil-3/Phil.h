#ifndef _Recv_h_
#define _Recv_h_

#include "Thread.h"

class Phil : public Thread {
    int pid;
    char nameBuffer[128];
    static SpinFetchAndOp<int> mouthFulls;
public:
    Phil(int pid);
    virtual void main();
};

#endif
