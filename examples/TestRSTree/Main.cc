#include "RSTree.h"
#include "CUT_MaxFibHeap.h"
#include "MinFibHeap.h"
#include "DiscreteHistogram.h"
#include "MLCG.h"
#include "Normal.h"
#include "Pix.h"

#define ITEMS 10000


int sort_int_max_to_min (const void *a, const void *b)
{
    int *aa = (int *) a; int *bb = (int *) b;
    return (*bb - *aa);
}

int sort_int_min_to_max (const void *a, const void *b)
{
    int *aa = (int *) a; int *bb = (int *) b;
    return (*aa - *bb);
}

void
TestRand(Random& rand)
{
    DiscreteHistogram histo;
    for (int i = 0; i < ITEMS;i++) {
	histo += rand(); 
    }
    cout << histo << "\n";
}

void
FlexRSTree(RSTree<int,int>& tree)
{
    //
    // clear it..
    //
    tree.clear();
    //
    // Put stuff in it..
    //
    for (int i = 0; i < ITEMS; i++ ) {
	tree.insert(i,i);
    }
    //
    // Now, modify the entries..
    //
    for (Pix p = tree.first(); p; tree.next(p)) {
	int key = tree.key(p);
	int& data = tree.data(p);
	data += 1;
    }
    //
    // Now, check the entries..
    //
    for (p = tree.first(); p; tree.next(p)) {
	if ( tree.data(p) != (tree.key(p) + 1) ) {
	    cerr << "Check1: i = " << i << ", data = " << tree.data(p) <<"\n";
	}
	Pix q = tree.lookup(tree.key(p));
	if ( q != p ) {
	    cerr << "Check1: lookup returns different Pix--";
	    cerr << "lookup -> " <<  q << " key -> " << p  << "\n";
	}
    }
    //
    // Now, deleete some of the entries...
    //
    for (i = 0; i < ITEMS; i += 2) {
	tree.del(i);
    }
    if ( tree.size() != 5000 ) {
	cerr << "ERROR: Size = " << tree.size() << "\n";
    }
    //
    // Now, check the entries..
    //
    for (p = tree.first(); p; tree.next(p)) {
	if ( tree.data(p) != (tree.key(p) + 1) ) {
	    cerr << "Check2: i = " << i << ", data = " << tree.data(p) <<"\n";
	}
    }
    //
    // clear it.. again
    //
    tree.clear();
    if ( tree.size() != 0 ) {
	cout << "ERROR: Size = " << tree.size() << "\n";
    }
    if ( ! tree.empty() ) {
	cout << "ERROR: !empty()\n";
    }
}

void
FlexMaxFibHeap(MaxFibHeap<int,int>& heap)
{
    int *keys = new int[ITEMS];
    int k = 0;

    MLCG rng(1,10);
    Normal norm(10000,2000,&rng);

    heap.clear();
    for (int i = 0; i < ITEMS; i++ ) {
	int key = norm();
	keys[k++] = key;
	heap.insert(key,i);
    }
    for (Pix p = heap.first(); p; heap.next(p)) {
	int key = heap.key(p);
	int& data = heap.data(p);
	data += 1;
    }
    //
    // Sort the keys, make certain that is the order we
    // extract them in..
    //
    qsort((void *) keys, ITEMS, sizeof(int), sort_int_max_to_min);
    k = 0;

    for (i = 0; i < ITEMS; i += 2) {
	int m = heap.key(heap.find_max());
	if (m != keys[k] ) {
	    cout << "ERROR: m = " << m << ", key = " << keys[k] << "\n";
	}
	k++;
	heap.deq();
    }
    if ( heap.size() != 5000 ) {
	cerr << "ERROR: Size = " << heap.size() << "\n";
    }

    while (! heap.empty() ) {
	int m = heap.key(heap.find_max());
	assert( k < ITEMS );
	if (m != keys[k] ) {
	    cout << "ERROR: m = " << m << ", key = " << keys[k] << "\n";
	}
	k++;
	heap.deq();
    }

    heap.clear();
    if ( heap.size() != 0 ) {
	cout << "ERROR: Size = " << heap.size() << "\n";
    }
    if ( ! heap.empty() ) {
	cout << "ERROR: !empty()\n";
    }


    heap.clear();
    for (i = 0; i < 10; i++ ) {
	heap.insert(1,i);
    }
    for (i = 0; i < 10; i++ ) {
	heap.insert(2,i);
    }
}

void
FlexMinFibHeap(MinFibHeap<int,int>& heap)
{
    int *keys = new int[ITEMS];
    int k = 0;

    MLCG rng(1,10);
    Normal norm(10000,2000,&rng);

    heap.clear();
    for (int i = 0; i < ITEMS; i++ ) {
	int key = norm();
	keys[k++] = key;
	heap.insert(key,i);
    }
    for (Pix p = heap.first(); p; heap.next(p)) {
	int key = heap.key(p);
	int& data = heap.data(p);
	data += 1;
    }
    //
    // Sort the keys, make certain that is the order we
    // extract them in..
    //
    qsort((void *) keys, ITEMS, sizeof(int), sort_int_min_to_max);
    k = 0;

    for (i = 0; i < ITEMS; i += 2) {
	int m = heap.key(heap.find_min());
	if (m != keys[k] ) {
	    cout << "ERROR: m = " << m << ", key = " << keys[k] << "\n";
	}
	k++;
	heap.deq();
    }
    if ( heap.size() != 5000 ) {
	cerr << "ERROR: Size = " << heap.size() << "\n";
    }

    while (! heap.empty() ) {
	int m = heap.key(heap.find_min());
	assert( k < ITEMS );
	if (m != keys[k] ) {
	    cout << "ERROR: m = " << m << ", key = " << keys[k] << "\n";
	}
	k++;
	heap.deq();
    }

    heap.clear();
    if ( heap.size() != 0 ) {
	cout << "ERROR: Size = " << heap.size() << "\n";
    }
    if ( ! heap.empty() ) {
	cout << "ERROR: !empty()\n";
    }
}

int
main(int argc, char **argv)
{
    int trials = 1;
    if ( argc > 1 ) {
	trials = atoi(argv[1]);
    }
    
    MLCG rng;
    Normal norm(100,10,&rng);
//    TestRand(norm);

    cout << "Check RSTree\n";
    RSTree<int,int> footree(rng);
    RSTree<int,int> bartree(rng);
    for (int i = 0; i < trials; i++) {
	FlexRSTree(footree);
	FlexRSTree(bartree);
    }

    cout << "Check MaxFibHeap\n";
    MaxFibHeap<int,int> foomaxheap;
    MaxFibHeap<int,int> barmaxheap;
    for (i = 0; i < trials; i++) {
	FlexMaxFibHeap(foomaxheap);
	FlexMaxFibHeap(barmaxheap);
    }

    cout << "Check MinFibHeap\n";
    MinFibHeap<int,int> foominheap;
    MinFibHeap<int,int> barminheap;
    for (i = 0; i < trials; i++) {
	FlexMinFibHeap(foominheap);
	FlexMinFibHeap(barminheap);
    }

    return(0);
}
