#include "Table.h"
#include "CpuMux.h"

Table TheTable;

Table::Table()
{
    for (int i = 0; i < PHILOSOPHERS; i++ ) {
	available[i] = 1;
    }
}

void
Table::getForks(int left, int right)
{
    int ok = 0;
    while ( ok == 0 ) {
	lock.reserve();
	if ( available[left] && available[right] ) {
	    ok = 1;
	    available[left] = 0;
	    available[right] = 0;
	}
	lock.release();
	if ( ok == 0 ) CpuMux::reschedule();
    }
}

void
Table::returnForks(int left, int right)
{
    lock.reserve();
    available[left] = 1;
    available[right] = 1;
    lock.release();
}
