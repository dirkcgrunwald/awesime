#ifndef _Table_h_
#define _Table_h_

#include "Semaphore.h"

#define PHILOSOPHERS 5

class Table {
    SpinLock lock;
    int available[ PHILOSOPHERS ];
public:
    Table();
    virtual void getForks(int left, int right);
    virtual void returnForks(int left, int right);
};


#endif
