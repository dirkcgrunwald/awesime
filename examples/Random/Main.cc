#include <stdlib.h>

#include "MLCG.h"

main(int argc, char **argv)
{
    long seed1 = (argc > 1) ? atol(argv[1]) : 0;	//time(0);
    long seed2 = (argc > 2) ? atol(argv[2]) : 1;	//time(0);

    MLCG *rGen = new MLCG(seed1, seed2);

    cout << "asFloat:\n";
    for (int i = 0; i < 10; i++)
	cout << rGen->asFloat() << "\n";
    cout << "\n";

    cout << "asDouble:\n";
    for (int i = 0; i < 10; i++)
	cout << rGen->asDouble() << "\n";

    return 0;
}
