#include "MultiFifoMux.h"
#include "SingleFifoMux.h"
#include "Table.h"
#include "Phil.h"

extern Table TheTable;

main(int argc)
{
    SingleFifoMux Cpu(1);

    for (int i = 0; i < PHILOSOPHERS; i++) {
	CpuMux::add( new Phil(i) );
    }
    Cpu.fireItUp(3, 20000);
    cout << "All done!\n";
    return 0;
}



