#ifndef _Table_h_
#define _Table_h_

#include "EventSequence.h"

#define PHILOSOPHERS 5

class Table {
    EventSequence table;
    EventSequence fork[ PHILOSOPHERS ];
public:
    Table();
    virtual void getForks(int left, int right);
    virtual void returnForks(int left, int right);
};


#endif
