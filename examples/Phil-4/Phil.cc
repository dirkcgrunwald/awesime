#include <stdio.h>
#include "Main.h"
#include "Table.h"
#include "Phil.h"
#include "CpuMux.h"
#include "Semaphore.h"
#include "GenerousSemaphore.h"
#include "Barrier.h"
#include "iolock.h"

SpinFetchAndOp<int> Phil::mouthFulls(0);

extern Table TheTable;

Phil::Phil(int xpid) : Thread( "Philosopher", 5000, HARDWARE_CONTEXT_CHECK_MPROTECT) 
{
    pid = xpid;
    sprintf(nameBuffer,"Phil-%d", pid);
    name(nameBuffer);
}

void
Phil::main()
{
    int toTheRight = (pid + 1) % PHILOSOPHERS;

    cerr << lock << name() << " using forks " << pid << " and " << toTheRight << "\n" << unlock;

    static Barrier sayGrace( PHILOSOPHERS );
    sayGrace.rendezvous();

    while( mouthFulls.value() < 10 ) {
	TheTable.getForks(pid, toTheRight);
	cerr << lock << name() << " gets to eat\n" << unlock;
	TheTable.returnForks(pid, toTheRight);
	mouthFulls += 1;
    }
    cerr << lock << name() << " wanders off to burb awhile\n" << unlock;
}
