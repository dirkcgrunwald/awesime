#include "Table.h"
#include "CpuMux.h"

Table TheTable;

Table::Table() : table( PHILOSOPHERS - 1 )
{
}

void
Table::getForks(int left, int right)
{
    table.await( table.ticket() );
    fork[left].await( fork[left].ticket() );
    fork[right].await( fork[right].ticket() );
}

void
Table::returnForks(int left, int right)
{
    fork[right].advance();
    fork[left].advance();
    table.advance();
}
