#include <sys/time.h>
#include <time.h>
#include <Main.h>
#include "SimMux.h"
#include "Customer.h"
#include "Gen.h"
#include "Facility.h"
#include "NegativeExpntl.h"
#include <fstream>

Gen::Gen(int customers_, double meanService, double meanArrival)
	: Thread( "Gen", 10000),
	  rng(time(0), time(0)),
	  serviceTime(meanService, &rng),
	  arrivalTime(meanArrival, &rng)

{
    customers = customers_;
}

void
Gen::main()
{
    //
    // Add all the customers
    //

  ofstream graph("utilization-graph");

    for (int i = 0; i < customers; i++ ) {
      //      graph << SimMux::CurrentTime() << " " << counter.utilization() << endl;

      graph << counter.utilization() << endl;

	SimMux::add( new Customer( serviceTime() ));
	SimMux::hold( arrivalTime() );
    }
}
