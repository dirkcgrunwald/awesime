#include "SimMux.h"
#include "Main.h"
#include "Customer.h"
#include "Facility.h"

Customer::Customer( double serviceTime_ ) : Thread("Customer", 10000)
{
    serviceTime = serviceTime_;
}

void
Customer::main() 
{
    counter.use( serviceTime );
}
