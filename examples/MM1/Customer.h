#ifndef Customer_h
#define Customer_h

#include "Thread.h"

class Customer : public Thread {
    double serviceTime;
public:
    Customer( double serviceTime );
    virtual void main();
};

#endif
