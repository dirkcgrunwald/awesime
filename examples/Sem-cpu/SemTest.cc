//
//	A timing test for barrier. Everything is in the one file.
//

#include <stdio.h>
#include "Semaphore.h"
#include "Barrier.h"
#include "SemTest.h"
#include "ControlTask.h"
#include "CpuMux.h"
#include "iolock.h"

Semaphore TheSemaphore;
Barrier TheBarrier(0);
int TheDataValue = 0;

SemTest::SemTest(int p, int times_) : Thread("SemTest", 10000)
{
    pid = p;
    times = times_;
    sprintf(namestr,"Semtest-%d", pid);
    name(namestr);
}

int globalthing[200];
int globalthing2[200];

void
SemTest::main()
{
    
    for ( int i = 0; i < times; i++ ) {
	
	globalthing[pid] = i;
	globalthing2[pid] = times;
	
	TheSemaphore.reserve();
	TheDataValue++;
	CpuMux::reschedule();
	TheSemaphore.release();
	
	assert(globalthing[pid] == i);
	assert(globalthing2[pid] == times);
	
    }

    TheBarrier.rendezvous();
    static volatile int printed = 0;

    if ( printed == 0 ) {
	if ( printed == 0 ) { 
	    printed = 1;
	    cerr <<lock << name() << " 1st semtest after rendezvous\n"<<unlock;
	}
    }
}
