#ifndef Customer_h
#define Customer_h

#include "Thread.h"
#include "NegativeExpntl.h"

class RNG;
class Server;

class Customer : public Thread {
    int maxCustomers;
    NegativeExpntl arrivalTime;
    Server *theServer;
public:
    Customer(int nMax, double mean, Server *server, RNG *rng);
    virtual void main();
};

#endif
