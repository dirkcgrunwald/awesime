#include "SimMux.h"

#include "Server.h"

Server::Server(int nMax, double mean, RNG *rng)
      : Thread( "SERVER", 10000),
	serviceTime(mean, rng)
{
    maxCustomers = nMax;
    delayedCustomers = 0;
    totalDelay = 0.0;
    timeByDelayedCustomer = 0.0;
    lastEvent = 0.0;
}

void
Server::main()
{
    for (int customerNo = 0; customerNo < maxCustomers; customerNo++) {
	timeByDelayedCustomer +=
	    (theQueue.size()) ?
		(theQueue.size() * (SimMux::CurrentTime() - lastEvent))
		    : 0.0;

	double *tmpVal;
	theQueue.remove((AwesimeFifoItem *)&tmpVal);
	double timeArrival = *tmpVal;
	delete tmpVal;

	delayedCustomers++;
	double delay = (SimMux::CurrentTime() - timeArrival);
	totalDelay += delay;
	lastEvent = SimMux::CurrentTime();
#if 0
	cout << "Server GOT " << timeArrival
	     << " at " << SimMux::CurrentTime() << "\n"
	     << "\tdelay = " << delay << "\n"
	     << "\tQue Size = " << theQueue.size() << "\n"
	     << "\tdelayedCustomers = " << delayedCustomers << "\n"
	     << "\ttotalDelay = " << totalDelay << "\n"
	     << "\ttimeByDelayedCustomer = " << timeByDelayedCustomer << "\n";
#endif
	SimMux::hold(serviceTime());
	//cout << "Server DONE at " << SimMux::CurrentTime() << "\n";
    }

    double totalTime = SimMux::CurrentTime();

    cout << "\n"
	 << "\tQueue Delay           \t" <<
	    (totalDelay / delayedCustomers) << "\n"
	 << "\tQueue Length          \t" <<
	    (timeByDelayedCustomer / totalTime) << "\n\n";
}

void
Server::add(double timeArrival)
{
    timeByDelayedCustomer += (theQueue.size()) ?
	(theQueue.size() * (SimMux::CurrentTime() - lastEvent))
	    : 0.0;
    double *tmpVal = new double;
    *tmpVal = timeArrival;

    theQueue.add((AwesimeFifoItem *)&tmpVal);
    lastEvent = SimMux::CurrentTime();
}
