#include "SingleSimMux.h"
#include "MLCG.h"

#include "Main.h"
#include "Customer.h"
#include "Server.h"

main(int argc, char **argv)
{

    int maxCustomers = (argc > 1) ? atoi(argv[1]) : MAX_CUSTOMERS;
    double meanArrival = (argc > 2) ? atof(argv[2]) : T_ARRIVAL;
    double meanService = (argc > 3) ? atof(argv[3]) : T_SERVICE;
    bool debug = (argc > 3) ? true : false;

    SingleSimMux Simulator( debug );

    long seed1 = (argc > 4) ? atol(argv[4]) : 0;	//time(0);
    long seed2 = (argc > 5) ? atol(argv[5]) : 1;	//time(0);

    MLCG *rGen = new MLCG(seed1, seed2);

    Server *theServer = new Server(maxCustomers, meanService, rGen);
    Simulator.add(theServer);

    Customer *theArrival =
		new Customer(maxCustomers, meanArrival, theServer, rGen);
    Simulator.add(theArrival);

    cout << "\nSINGLE SERVER Queueing System:\n"
	 <<   "-----------------------------\n"
	 << "\tNumber of Customers   \t" << maxCustomers << "\n"
	 << "\tMean InterArrival Time\t" << meanArrival << "\n"
	 << "\tMean Service Time     \t" << meanService << "\n";

    Simulator.fireItUp(1,100 * 4196);

    cout << "\nDONE\n";
    exit(0);
}
