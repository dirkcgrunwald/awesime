#ifndef Server_h
#define Server_h

#include "Thread.h"
#include "LowerBoundedFifo.h"
#include "NegativeExpntl.h"

class RNG;

class Server : public Thread {
    int maxCustomers;
    LowerBoundedFifo theQueue;
    NegativeExpntl serviceTime;
    int delayedCustomers;
    double lastEvent, totalDelay, timeByDelayedCustomer;
public:
    Server(int nMax, double mean, RNG *rng);
    virtual void main();
    void add(double timeArrival);
};

#endif
