#include "SimMux.h"

#include "Customer.h"
#include "Server.h"

Customer::Customer(int nMax, double mean, Server *server, RNG *rng)
	: Thread( "CUSTMR", 10000),
	  arrivalTime(mean, rng)
{
    maxCustomers = nMax;
    theServer = server;
}

void
Customer::main()
{
    for (int i = 0; i < maxCustomers; i++) {
	SimMux::hold( arrivalTime() );
	//cout << "Customer ADD at " << SimMux::CurrentTime() << "\n";
	double arrTime = SimMux::CurrentTime();
	theServer->add(arrTime);
    }
}
