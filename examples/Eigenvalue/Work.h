//-*-c++-*-
#ifndef _Work_h_
#define _Work_h_
#include "Thread.h"

class Work : public Thread {
   int pid;
   int matrixSize;
   double interval_low;
   double interval_high;
  public:
    Work(int _pid, int N, double low, double high);
    EigenResult myResult;
    virtual void main();
};

#endif
