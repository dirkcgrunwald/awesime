//-*-c++-*-
#ifndef _Main_h_
#define _Main_h_
#include "MCSBarCentralWakeup.h"

class EigenSearch {
  public:
     double a;
     double b;
     int sa;
     int sb;

     EigenSearch(double a_, double b_, int sa_, int sb_) {
	 a = a_; b = b_; sa = sa_; sb = sb_;
     }
};


class EigenResult {
  public:
    int n_eval;
    double *eval;
    int *mult;
};

extern double *diag;
extern double *e;
extern double *e2;

#include "LockedSLList.h"
extern LockedSLList<EigenSearch*> WorkQueue;

extern int Cpus;
extern MCSBarCentralWakeup *CpuBarrier;
extern double PIV;
extern int sturm_sequence(double x, int n);

#define MIN(z,y) ((z) > (y) ? (y) : (z))
#define MAX(w,t) ((w) > (t) ? (w) : (t))
#define FAB(x)   ((x) >= 0  ? (x) : -(x))
#define macheps 1.e-16
#define MAXT 30
#define safe_min 1.e-307 


inline int sturm_sequence(double x, int n, double PIV)
{
   register int s, i;
   static double *q;

   if (q == 0)
       q = (double *) malloc(n*sizeof(double));

   s = 0;
   q[0] = diag[0]-x;

   if (FAB(q[0]) <= PIV) q[0] = -PIV;

   if(q[0] < 0) s++;

   for (i=1; i < n; i++) {
       q[i] = diag[i]-x-e2[i]/q[i-1];
       if (FAB(q[i]) <= PIV) q[i] = -PIV;
       if (q[i] < 0) s++;
   }

   return(s);
}

#endif

