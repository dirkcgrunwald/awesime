#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "MLCG.h"
#include "Main.h"
#include "Work.h"
#include "MultiFifoMux.h"
#include "SpinLock.h"


double *diag;
double *e;
double *e2;

enum MatrixKind {Matrix0, Matrix1, MatrixRand};


LockedSLList<EigenSearch*> WorkQueue;

int Cpus = 0;
double PIV;
MCSBarCentralWakeup *CpuBarrier;
bool conferenceTime=0;
bool requestConference = 0;
SpinLock conference;

/////////////////////////////////////////////////////////////////////////////
// Generate a matrix
/////////////////////////////////////////////////////////////////////////////

void
generate_m(int par, int n)
{
    int i, j, m;
    int seed = 1;
    double Max;
    int CONS = 1 << 15;
    MLCG rand(seed,seed);

    diag = (double *)malloc(n*sizeof(double));
    e = (double *)malloc(n*sizeof(double));
    e2 = (double *)malloc(n*sizeof(double));
    
    switch(par) {
    case Matrix0:
	for (i=0; i < n; i++) {
	    diag[i] = (double)2.e0;
	    e[i] = (double)1.e0;
	    e2[i] = (double)1.e0;
	}
	PIV = 1*safe_min;
	break;

    case Matrix1:
	//
	// 
	m = MIN(21,n);
	for (i=0; i<m; i++)
	    diag[i] = FAB((double)(10-i));
	
	e[0] = 1.e-14;
	
	for (i=1; i<m; i++)
	    e[i] = 1;
	
	for (i=21; i<n; i++) {
	    j = i%21;
	    diag[i] = diag[j];
	    e[i] = e[j];
	}
	
	e[0] = 0;
	
	for (i=1; i<n; i++)
	    e2[i] = e[i]*e[i];
	
	PIV = 1*safe_min;
	break;
	
    case MatrixRand:
	//
	// A Random matrix
	//

	for (i=0; i<n; i++)
	    diag[i] =  2.0 * (rand.asDouble()/CONS) - 1.0;
	for (i=1; i<n; i++)
	    e[i] = 2.0 * (rand.asDouble()/CONS) - 1.0;

	for (i=1; i<n; i++)
	    e2[i] = e[i]*e[i];
	Max = e[1];
	for (i=2; i<n; i++)
	    if( e[i] > Max ) Max = e[i];
	PIV = Max * safe_min;
	break;
	
/* if another matrix is added, it should be computed also
   PIV = max{e_j} * safe_min
   */
	
    default:
	cout << "wrong matrix type\n";
	break;
	
    }
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

main(int argc, char** argv)
{
    double interval_low = 0.0;
    double interval_high = 1.0;
    MatrixKind matrix_kind = MatrixRand;
    int matrix_size = 50;
    int debug = 0;;

    Cpus = 1;
    for (int arg_num = 1; arg_num < argc; arg_num++ ) {
	char *arg = argv[arg_num]; //String not supported by all C++ compilers
	if (!strcmp(arg ,"-low" )) {
	    arg_num++;
	    interval_low = atof(argv[arg_num]);
	} else if ( !strcmp(arg,"-high" )) {
	    arg_num++;
	    interval_high = atof(argv[arg_num]);
	} else if ( !strcmp(arg,"-cpus" )) {
	    arg_num++;
	    Cpus = atoi(argv[arg_num]);
	} else if ( !strcmp(arg,"-0" )) {
	    matrix_kind = Matrix0;
	} else if ( !strcmp(arg, "-1")) {
	    matrix_kind = Matrix1;
	} else if ( !strcmp(arg, "-rand") ) {
	    matrix_kind = MatrixRand;
	} else if ( !strcmp (arg, "-size" ) ) {
	    arg_num++;
	    matrix_size = atoi(argv[arg_num]);
	} else if ( !strcmp(arg, "-debug")) {
	    arg_num++;
	    debug = atoi(argv[arg_num]);
        }
    }

    MultiFifoMux Cpu((bool)debug);

    generate_m(matrix_kind, matrix_size);

    CpuBarrier = new MCSBarCentralWakeup;

    // first param to initBarrier is height, second is the number of cpus
    // to use. Here they are the same.
    CpuBarrier->initBarrier(Cpus,Cpus);  
    for (int cpu = 0; cpu < Cpus; cpu++ ) {
	CpuMux::add( new Work(cpu, matrix_size,interval_low,interval_high));
    }
    printf(" THREAD    EIGENVALUE       MULTIPLICITIES  \n");
    Cpu.fireItUp(Cpus);
    
}



