#include "Main.h"
#include "Work.h"
#include "stdio.h"
#include "SpinLock.h"
#include "iolock.h"

extern bool conferenceTime;
extern bool requestConference;
extern SpinLock conference;

Work::Work(int _pid,int N, double low, double high) 
: Thread("Work", 5000, HARDWARE_CONTEXT_CHECK_MPROTECT)
{
    pid = _pid;
    matrixSize = N;
    interval_low = low;
    interval_high = high;
    myResult.n_eval = 0;
    myResult.eval = (double *)malloc((N+1)*(sizeof(double)));
    myResult.mult = (int *) malloc((N+1) * (sizeof(int)));
}

void Work::main()
{
    register double m,h,delta;
    register double my_a, my_b;
    register int sm,my_sa,my_sb;
    register EigenSearch *Work;
 
    // hack to increase parallelism and avoid contention in WorkQueue first time.
    delta = FAB(interval_high-interval_low)/Cpus;

    if (pid < Cpus-1) {
        my_a = interval_low + pid*delta;
        my_b = my_a + delta;
    }
    else {
        my_a = interval_low + pid*delta;
        my_b = interval_high;
    }        
    my_sb = sturm_sequence(my_b,matrixSize,PIV);
    my_sa = sturm_sequence(my_a,matrixSize,PIV);
    CpuBarrier->checkIn();
    goto  START;       // ..so you want to make a federal case out of it.

    for(;;) {
	    if ( WorkQueue.remove_front(Work) ) {
		my_a = Work -> a;
		my_b = Work -> b;
		my_sa = Work -> sa;
		my_sb = Work -> sb;
START:		h = FAB(my_a-my_b)/2.0;
		while(h > 2.0 * macheps * MIN( FAB(my_a), FAB(my_b) )) {
		    m = (my_a+my_b)/2.0;
		    sm = sturm_sequence(m,matrixSize,PIV);
		    if (my_sa < sm ) {
			if (sm < my_sb) {
			    // if sm in between my_sa and my_sb, give right 
			    // half to another process, and ...
			    Work = new EigenSearch(m,my_b,sm,my_sb);
			    if (requestConference) {
			      conferenceTime = 1;
#if defined(__ksr__)
                                pstsp((VolatileSpinLockType *)&conferenceTime);
#endif
				CpuBarrier->rendezvous();
			        WorkQueue.append(Work);
				CpuBarrier->rendezvous();
				conferenceTime = 0;
                                CpuBarrier->rendezvous();
		            }
			    else {
				WorkQueue.append(Work);
			    }
			}
			// ... process the left half myself
			my_b = m;
			my_sb = sm;
		    }
		    else if (my_sa == sm && sm < my_sb) {
			my_a = m;
			my_sa = sm;
		    }
		    else    {
			// not within the interval; go look for more work.
			break;
		    }
		    h = FAB(my_a-my_b)/2.0;
		}
		    
		if (conferenceTime) {
		    CpuBarrier->rendezvous();
		    CpuBarrier->rendezvous();
		    CpuBarrier->rendezvous();
		}
		// Aa Haaa! Found an eigenvalue
		myResult.eval[myResult.n_eval] = (my_a + my_b)/2.0;
		myResult.mult[myResult.n_eval] = my_sb - my_sa;
		myResult.n_eval++;
	   } 
           else {
	     requestConference = 1;
#if defined(__ksr__)
               pstsp((VolatileSpinLockType *)&requestConference);
#endif

	       CpuBarrier -> rendezvous();
	       CpuBarrier -> rendezvous();
               requestConference = 0;
	       int was_empty = WorkQueue.empty();
	       CpuBarrier -> rendezvous();
	       if (was_empty)
	           break;
           }
		
    }
    // print my results 
//    cout << lock;
    for (int i=0; i < myResult.n_eval; i++)
	printf("   %2d     %10.9f          %2d\n",pid, myResult.eval[i], myResult.mult[i]);
//    cout << unlock;
    CpuBarrier -> checkOut();
    
}








