#include "SimMux.h"
#include "Barrier.h"
#include "ControlTask.h"
#include "BarrierTest.h"

#define TRIALS 100
#define SAMPLES 10

extern Barrier TheBarrier;

ControlTask::ControlTask(int f, int t, int b)
{
    from = f;
    to = t;
    by = b;
    testers = 0;
}

void
ControlTask::main()
{
    for (int i = from; i <= to; i += by ) {
	TheBarrier.height(i+1);
	cout << "Build testers..\n";
	while( testers < i) {
	    CpuMux::add(new BarrierTest);
	    testers++;
	}

	cout << "Let everyone rendezvous\n";

	TheBarrier.rendezvous();
	stats.reset();
	//
	// start collecting sampls
	//
	for (int sample = 0; sample < SAMPLES; sample++ ) {
	    cout << ".." << sample << ".."; cout.flush();
	    for (int trial = 0; trial < TRIALS; trial++) {
		TheBarrier.rendezvous();
	    }
	}
    }
}
