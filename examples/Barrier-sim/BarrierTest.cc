//
//	A timing test for barrier. Everything is in the one file.
//

#include "Barrier.h"
#include "BarrierTest.h"

Barrier TheBarrier(0);

void
BarrierTest::main()
{
    for ( ;; ) {
	TheBarrier.rendezvous();
    }
}
