#include "SingleSimMux.h"
#include "ControlTask.h"

main(int argc, char** argv)
{
    SingleSimMux Cpu;

    int from = (argc > 1) ? atoi(argv[1]) : 50;
    int to = (argc > 2) ? atoi(argv[2]) : (from + 100);
    int by = (argc > 3) ? atoi(argv[3]) : 50;
    int cpus = (argc > 4) ? atoi(argv[4]) : 1;

    cout << "From = " << from << " and to = " << to << "\n";
    cout << "with " << cpus << " cpus\n";

    Cpu.add( new ControlTask(from,to,by) );
    Cpu.fireItUp( cpus, to * 1024 + 4000000);

    cout << "Done!\n";
    return 0;
}
