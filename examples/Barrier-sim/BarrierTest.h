#include "Thread.h"

class BarrierTest : public Thread {
    int howMany;
public:
    BarrierTest();
    virtual void main();
};

inline
BarrierTest::BarrierTest()
     : Thread("BarrierTest", 15000, HARDWARE_CONTEXT_CHECK_MPROTECT)
{
    // nothing
}
