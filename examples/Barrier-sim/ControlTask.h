#include "Thread.h"
#include "Statistic.h"

class ControlTask : public Thread {
    int from;
    int to;
    int by;
    double start;
    double elapsed;
    int testers;
    Statistic<double> stats;

public:
    ControlTask(int,int,int);
    virtual void main();
};
