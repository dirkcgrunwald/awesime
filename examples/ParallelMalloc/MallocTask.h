#include "Thread.h"

class MallocTask : public Thread {
    int size;
    int chunks;
    int delay;

public:
    MallocTask(int size, int chunks, int delay);
    virtual void main();
};

