#include "SingleFifoMux.h"
#include "MultiFifoMux.h"
#include "MallocTask.h"


main(int argc, char** argv)
{

    int from = (argc > 1) ? atoi(argv[1]) : 50;
    int to = (argc > 2) ? atoi(argv[2]) : (from * 10);
    int by = (argc > 3) ? atoi(argv[3]) : (from * 2);
    int cpus = (argc > 4) ? atoi(argv[4]) : 2;
    int debug = (argc > 5) ? atoi(argv[4]) : 0;

    SingleFifoMux Cpu(debug);

    cout << "From = " << from << " and to = " << to << "\n";
    cout << "with " << cpus << " cpus\n";

    unsigned long mem = 0;

    int flicks = 1;
    for ( int i = from; i < to; i += by ) {
	cout << "Allocate task to malloc " << i << " sized chunks\n";
	Cpu.add( new MallocTask(i, 1500, flicks++) );
	mem += i * 1500;
    }

    Cpu.fireItUp( cpus, to * 1024 + mem + 4000000);

    cout << "Done!\n";
    return 0;
}

void
externalRoutine()
{
    CpuMux::reschedule();
}
