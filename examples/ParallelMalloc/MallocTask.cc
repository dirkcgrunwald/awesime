#include "SimMux.h"
#include "MallocTask.h"
#include <stdlib.h>

MallocTask::MallocTask(int size_, int chunks_, int delay_)
: Thread("Malloc Task")
{
    size = size_;
    chunks = chunks_;
    delay = delay_;
}

void
MallocTask::main()
{
    for (int i = 0; i < chunks; i++ ) {
	int j;
	extern void externalRoutine();

	CpuMux::reschedule();

	char *x = (char *) malloc(size);

	CpuMux::reschedule();

	free(x);

    }
} 
