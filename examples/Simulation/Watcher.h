#ifndef Watcher_h
#define Watcher_h

#include "Thread.h"

class Watcher : public Thread {
  int _interval;
public:
  Watcher(int interval);
  virtual void main();
};

inline
Watcher::Watcher( int interval )
{
  _interval = interval;
}

#endif
