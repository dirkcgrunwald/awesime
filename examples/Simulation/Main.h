#ifndef Main_h
#define Main_h

#define	MAX_REQUESTS	100000

#include <Facility.h>
#include "SpinFetchAndOp.h"

#define MAX_DISKS 2
extern Facility Disks[MAX_DISKS];
extern SpinFetchAndOp<int> ActiveThreads;

#endif /* Main_h */
