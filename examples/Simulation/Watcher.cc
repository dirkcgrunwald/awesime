#include "SimMux.h"
#include "Watcher.h"
#include <Facility.h>
#include <MLCG.h>
#include <NegativeExpntl.h>
#include "Main.h"
#include <fstream>

void
Watcher::main()
{
  ofstream disk1graph("disk1-graph");
  ofstream disk2graph("disk2-graph");
  
  while (ActiveThreads.value() > 0) {
    SimMux::hold(_interval);

    disk1graph << SimMux::CurrentTime() << " " <<
      Disks[0].utilization() << endl;

    disk2graph << SimMux::CurrentTime() << " " <<
      Disks[1].utilization() << endl;
  }
}
