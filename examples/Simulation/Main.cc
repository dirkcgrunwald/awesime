#include "SingleSimMux.h"
#include "MonitorSimMux.h"

#include "Main.h"
#include "Cpu.h"
#include "Watcher.h"
#include "Facility.h"
#include "SpinFetchAndOp.h"

Facility Disks[MAX_DISKS];

SpinFetchAndOp<int> ActiveThreads(0);

main(int argc, char **argv)
{
    
    SingleSimMux Simulator;

    cerr << "Add 1st CPU, mean request rate 2\n";
    Simulator.add(new Cpu(1, 3, 2));
    ActiveThreads += 1;
    
    cerr << "Add 2nd CPU, mean request rate 1\n";
    Simulator.add(new Cpu(2, 1, 6));
    ActiveThreads += 1;
    
    cerr << "Add Monitoring\n";
    Simulator.add(new Watcher(2));
    
    Simulator.fireItUp(1,100 * 4196);
    
    cout << "Disk Utilizations:\n";
    for ( int i = 0; i < MAX_DISKS; i++ ) {
	cerr << "Disk #" << i << "\n";
	Facility& fac = Disks[i];

	cout << "Disk utilization was : ";
	cout << fac.utilization() << "\n";
	cout << "Disk mean delay was  : ";
	cout << fac.meanDelay() << "\n";
	cout << "Disk mean queue was  : ";
	cout << fac.meanQueueLength() << "\n";
    }
    return 0;
}
