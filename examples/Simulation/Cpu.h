#ifndef Customer_h
#define Customer_h

#include "Thread.h"

class Cpu : public Thread {
  int pid;
  double requestInterval;
  double requestDuration;
public:
  Cpu(int iYam,  double interval, double duration);
  virtual void main();
};

inline
Cpu::Cpu( int iYam, double interval, double duration )
{
  pid = iYam;
  requestInterval = interval;
  requestDuration = duration;
}

#endif
