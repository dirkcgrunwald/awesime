#include "SimMux.h"
#include "Cpu.h"
#include <Facility.h>
#include <MLCG.h>
#include <NegativeExpntl.h>
#include "Main.h"

void
Cpu::main()
{
    MLCG rng(requestInterval, requestDuration);
    NegativeExpntl interval(requestInterval, &rng);
    NegativeExpntl duration(requestDuration, &rng);

    double total_delay = 0;
    for (int i = 0; i < MAX_REQUESTS; i++ ) {
	int disk = i % MAX_DISKS;
	SimMux::hold ( interval() );
	Disks[disk].reserve();

	SimTimeUnit now = SimMux::CurrentTime();
	double wait = duration();
	SimMux::hold ( wait );
	//
	// Record total time I spend waiting
	//
	double delay = (now - SimMux::CurrentTime());
	double really = (delay - wait);
	total_delay -= really;

	Disks[disk].release();
    }
    cerr << "Cpu " << pid << " spent " << total_delay << " waiting\n";
    ActiveThreads -= 1;
}
