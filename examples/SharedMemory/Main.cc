#include <stdio.h>
#include <SpinLock.h>
#include <assert.h>

SpinLock parentLock;
SpinLock childLock;
SpinLock stderrLock;

int BufferOfStuff[100];

extern "C" {
    int getpid();
    int fork();
};

void SharedMemoryInit( unsigned );

main(int argc, char **argv)
{
    
    int i;

    SharedMemoryInit( 1000 * 1024 );

    /* set things up so child will execute first */
    
    childLock.release();
    parentLock.reserve();
    
    int parent = getpid();
    
    /* fill buffer with parent data */
    
    for (int j = 0; j < 100; j++ ) {
	BufferOfStuff[j] = parent;
    }
    
    int kid = fork();
    
    int pid = getpid();
    int iteration = 0;
    
    while(iteration < 10) {
	
	iteration++;
	int j;
	
	if (kid == 0) {
	    /* child */
	    childLock.reserve();

	    stderrLock.reserve();
	    fprintf(stderr,"I am child process, I am %d\n", pid);
	    
	    int mismatch = 0;
	    for (j = 0; j < 100; j++ ) {
		if ( BufferOfStuff[j] != parent ) 
		    mismatch++;
	    }

	    fprintf(stderr,"Child found %d mismatches\n", mismatch);
	    
	    for (j = 0; j < 100; j++ ) {
		BufferOfStuff[j] = pid;
	    }
	    
	    fprintf(stderr,"Process %d will now wait for parent (%d)\n",
		    pid, parent);
	    
	    fflush(stderr);
	    stderrLock.release();
	    parentLock.release();
	}
	else {
	    parentLock.reserve();
	    /* parent */

	    stderrLock.reserve();

	    fprintf(stderr,"I am parent process, I am %d\n", pid);
	    fprintf(stderr,"This is iteration %d\n", iteration);
	    
	    int mismatch = 0;
	    for (j = 0; j < 100; j++ ) {
		if ( BufferOfStuff[j] != kid ) 
		    mismatch++;
	    }

	    fprintf(stderr,"Parent found %d mismatches\n", mismatch);
	    
	    for (j = 0; j < 100; j++ ) {
		BufferOfStuff[j] = pid;
	    }
	    
	    fprintf(stderr,"Process %d will now wait for child (%d)\n",
		    pid,kid);
	    
	    fflush(stderr);
	    stderrLock.release();
	    childLock.release();
	}
    }
}
