#ifndef Customer_h
#define Customer_h

#include "Thread.h"
#include "MLCG.h"
#include "DiscreteUniform.h"
#include <math.h>
#include <assert.h>

class Client : public Thread {
  int pid;

  int _requests;

  double requestInterval;
  double requestDuration;

  MLCG rng;

public:
  Client( int iYam, int requests )
    : rng(iYam, -iYam)
  {
    pid = iYam;
    _requests = requests;
  }

  
  void use(Facility& f, double time, char *name = "") {
    if ( Verbose ) {
      cout << "Use facility " << name << " for " << time << endl;
    }
    f.use(time);
  }


  void process_single_component(int netnum, int hostnum, int disknum, int requestSize)
  {
    //
    // Process a single request.
    //
    int frames =  (requestSize + Config_frame_size - 1 ) / Config_frame_size ;

    assert( netnum >= 0 && netnum < Config_networks );
    Network& network = Networks[netnum];

    assert(hostnum >= 0 && hostnum < Config_servers_per_network);
    Server& server = network.servers[hostnum];

    assert(disknum >= 0 && disknum < Config_disks_per_server);
    Disk& disk = server.disks[disknum];

    double xmit_delay = (double) Config_frame_size / (double) Config_bandwidth;

    if ( Verbose ) {
      cout << "Process request of size " << requestSize;
      cout << " from host (" << netnum << ", " << hostnum << "), disk " << disknum << endl;
      cout << "Delay for network is " << xmit_delay << endl;
    }

    //
    // Transfer request to server (assumes single block at a time)
    //
    for (int i = 0; i < frames; i++) {

      SimMux::hold( xmit_delay);

      //
      // Use the network switch..
      //
      use(network, (*DistNetFrameTime)(), "network");

      //
      // Use the host..
      //
      use(server, (*DistServerFrameTime)(), "server");
    }
    //
    // Process the file request. Bogus assumption is that each frame
    // is a disk transaction.
    //
    for (int i = 0; i < frames; i++) {
      use(server, (*DistServerFrameTime)(), "server");
      use(disk, (*DistDiskFrameTime)(), "disk");
    }
    //
    // Now, send it back to the client..
    //
    for (int i = 0; i < frames; i++) {
      //
      // Use the host..
      //
      use(server, (*DistServerFrameTime)(), "server");
      //
      // Use the network switch..
      //
      use(network, (*DistNetFrameTime)(), "network");

      SimMux::hold( xmit_delay);
    }
    //
    // Done with request..
    //
  }

  virtual void main()
  {
    
    //
    // Distributions we use to select a specific host/disk to use
    //
    DiscreteUniform fromNetwork(0, Config_networks-1, &rng);
    DiscreteUniform fromHost(0, Config_servers_per_network-1, &rng);
    DiscreteUniform fromDisk(0, Config_disks_per_server-1, &rng);

    for (int i = 0; i < _requests; i++ ) {

      //
      // Record current time..
      //
      double start = SimMux::CurrentTime();


      //
      // Each request is broken into a set of embedded documents.
      // We round up.
      //

      int embedded = int( DistEmbedded() + 0.50 );
      if (Verbose) {
	cout << "Request with " << embedded << " embedded documents" << endl;
      }
      for (int j = 0; j < embedded; j++) {
	//
	// Pick a request size, network, server and disk
	//
	int requestSize = int(DistRequestSize());
	int network = int(fromNetwork());
	int host = int(fromHost());
	int disk = int(fromDisk());

	//
	// Process this component of the request
	//

	process_single_component(network, host, disk, requestSize);
	//
	// sleep for the activeoff time..
	//
	double ht = DistActiveOffSize();
	if ( Verbose ) {
	  cout << "Active off delay is " << ht << endl;
	}
	SimMux::hold(ht );
      }
      //
      // Done with this request. Update statistics.
      //
      RequestResponseTime  += (SimMux::CurrentTime() - start);
      MeasuredResponseTime += (SimMux::CurrentTime() - start);
      //
      // Now for active off..
      //
      double ht = DistInactiveOffSize();
      if ( Verbose ) {
	cout << "Inactive off delay is " << ht << endl;
      }
      SimMux::hold( ht );
    }
    ActiveThreads -= 1;
  }
};


#endif
