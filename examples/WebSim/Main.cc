#include <unistd.h>
#define _GNU_SOURCE
#include <getopt.h>

#include "SingleSimMux.h"
#include "MonitorSimMux.h"

#include "Main.h"
#include "Client.h"
#include "Watcher.h"
#include "Facility.h"
#include "Statistic.h"

#include "SpinFetchAndOp.h"

#include "Network.h"
#include "Server.h"
#include "Disk.h"
#include "MLCG.h"
#include "NegativeExpntl.h"

/////////////////////////////////////////////////////////////////////////////
// Sample distributions
//
// We use reference parameters to simplify the source in Client.h and
// still allow us to change the distributions late.
/////////////////////////////////////////////////////////////////////////////

MLCG RandomSource(0,1);
Random& DistRequestSize = *(new Pareto(1.0, 1000, &RandomSource));
Random& DistActiveOffSize = *(new Weibull(1.46, 0.382, &RandomSource));
Random& DistInactiveOffSize = *(new Pareto(1.5, 1,&RandomSource));
Random& DistEmbedded = *(new Pareto(2.43, 1, &RandomSource));

Random *DistNetFrameTime = NULL;
Random *DistServerFrameTime = NULL;
Random *DistDiskFrameTime = NULL;

/////////////////////////////////////////////////////////////////////////////
// Configuration information
/////////////////////////////////////////////////////////////////////////////

int Config_clients = 1;
int Config_requests_per_client = 1000;
int Config_disks_per_server = 2;;
int Config_servers_per_network = 5;
int Config_networks = 2;

//
// 2Mb/s connection
//
int Config_bandwidth = 20000000;

//
// We're going to assume 1500 byte frames
//
int Config_frame_size = 1500;

//
// Recalculated below...
//
double Config_network_time_per_frame;
//
// We're going to assume 10ms to initiate a transaction and 10ms to
// send it back. 
//
double Config_server_time_per_frame = 0.0010;
//
// We're going to assume a 9.5ms average disk access time
//
double Config_disk_time_per_frame = 0.0095;



int UseWatcher = 0;

int Verbose = 0;

Network *Networks = NULL;

//
// Reset every time it's read (by Watcher)
//
Statistic<double> RequestResponseTime;

//
// Same as above, but it's never reset..
//
Statistic<double> MeasuredResponseTime;

/////////////////////////////////////////////////////////////////////////////
// Count of active threads, used to tell the Watcher to die..
/////////////////////////////////////////////////////////////////////////////

SpinFetchAndOp<int> ActiveThreads(0);

int
main(int argc, char **argv)
{
    
  //
  // Parse options
  //
  int digit_optind = 0;

  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;

    char c = getopt(argc, argv, "r:c:d:s:n:v");
    if ( c == -1 ) break; // while

    switch (c ) {

    case 'c':
      Config_clients = atoi(optarg);
      break;

    case 'r':
      Config_requests_per_client = atoi(optarg);
      break;

    case 'd':
      Config_disks_per_server = atoi(optarg);
      break;

    case 's':
      Config_servers_per_network = atoi(optarg);
      break;

    case 'n':
      Config_networks = atoi(optarg);
      break;

    case 'b':
      Config_bandwidth = atoi(optarg);
      break;

    case 'v':
      Verbose = ! Verbose;
      break;

    case 'w':
      UseWatcher = ! UseWatcher;
      break;

    default:
      cerr << "Unknown option *" << c << "*" << endl;
    }
  }

  cerr << "Running with " << Config_clients;
  cerr << " clients, " << Config_networks;
  cerr << " networks with " << Config_servers_per_network;
  cerr << " servers per network and " << Config_disks_per_server;
  cerr << " disks per server" << endl;
  cerr << "Network is assumed to have " << Config_bandwidth << " b/s bandwidth" << endl;

  //
  // Initialize Frame distributions based on what was specified..
  //

  Config_network_time_per_frame = (double) (Config_frame_size * 8) / (double) Config_bandwidth;

  NegativeExpntl NetFrameTime( Config_network_time_per_frame, &RandomSource);
  DistNetFrameTime = & NetFrameTime;

  NegativeExpntl ServerFrameTime( Config_server_time_per_frame, &RandomSource);
  DistServerFrameTime = &ServerFrameTime;

  NegativeExpntl DiskFrameTime( Config_disk_time_per_frame, &RandomSource);
  DistDiskFrameTime = &DiskFrameTime;

  if ( Verbose ) {
    cout << "Mean net frame time = " << NetFrameTime.mean() << endl;
    cout << "Mean server frame time = " << ServerFrameTime.mean() << endl;
    cout << "Mean disk frame time = " << DiskFrameTime.mean() << endl;
  }

  SingleSimMux Simulator;

  if ( UseWatcher ) {
    cerr << "Add Monitoring\n";
    Simulator.add(new Watcher(10));
  }

  cerr << "Creating Servers" << endl;
  //
  // Create the actual networks. For each network,
  // we also create the disks (see Network.h)
  //
  Networks = new Network[Config_networks];
  
  //
  // Go through, configure the system and
  // also build a simple array to access
  // all of the servers and/or disks.
  //


  for (int i = 0; i < Config_networks; i++) {
    Networks[i].configure(Config_servers_per_network);

    for (int j = 0; j < Config_servers_per_network; j++) {
      Networks[i].servers[j].configure(Config_disks_per_server);
    }
  }
    
  cerr << "Adding clients" << endl;
  for (int i = 0; i < Config_clients; i++) {
    ActiveThreads += 1;
    Simulator.add(new Client(i, Config_requests_per_client));
  }

  //
  // Start simulator...
  //
  Simulator.fireItUp(1,100 * 4196);

  cout << "Done with simulation" << endl;
  cout << "Measured response time is " << MeasuredResponseTime << endl;
    
  return 0;
}
