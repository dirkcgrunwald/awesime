#ifndef Watcher_h
#define Watcher_h

#include <iostream>
#include <fstream>

#include "SimMux.h"
#include "Main.h"
#include "Thread.h"

class Watcher : public Thread {
  int _interval;
public:
  Watcher(int interval) {
    _interval = interval;
  }

  virtual void main() {

    cerr << "here is watcher..\n";

    ofstream netgraph("network-utilization");
    ofstream servergraph("server-utilization");
    ofstream diskgraph("disk-utilization");
    ofstream responsegraph("response-utilization");
  
    while (ActiveThreads.value() > 0) {
      //
      // Delay for the sampling interval...
      //
      SimMux::hold(_interval);
      double now = SimMux::CurrentTime();

      //
      // Report values..
      //
      netgraph  << now << " ";
      servergraph  << now << " ";
      diskgraph  << now << " ";

      for (int n = 0; n < Config_networks; n++) {
	Network& network = Networks[n];

	netgraph << network.utilization() << " ";

	for (int s = 0; s < Config_servers_per_network; s++) {
	  Server& server = network.servers[s];

	  servergraph << server.utilization() << " ";

	  for (int d = 0; d < Config_disks_per_server; d++) {
	    Disk& disk = server.disks[d];

	    diskgraph << disk.utilization() << " ";
	  }
	}
      }

      //
      // End lines..
      //
      netgraph << endl;
      servergraph << endl;
      diskgraph << endl;

      responsegraph << now << " " << RequestResponseTime.mean() << endl;
      RequestResponseTime.reset();
    }
  }
};

#endif
