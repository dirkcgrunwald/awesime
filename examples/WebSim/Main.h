#ifndef Main_h
#define Main_h

#define	MAX_REQUESTS	100000

#include <Facility.h>
#include "SpinFetchAndOp.h"
#include "Network.h"
#include "Server.h"
#include "Disk.h"

#include "Pareto.h"
#include "Weibull.h"

extern Random& DistRequestSize;
extern Random& DistActiveOffSize;
extern Random& DistInactiveOffSize;
extern Random& DistEmbedded;

//
// Debugging info?
//
extern int Verbose;

extern int Config_clients;
extern int Config_requests_per_client;
extern int Config_disks_per_server;
extern int Config_servers_per_network;
extern int Config_networks;
extern int Config_bandwidth;

extern int Config_frame_size;
extern double Config_network_time_per_frame;
extern double Config_server_time_per_frame;
extern double Config_disk_time_per_frame;

//
// Distributions from which to draw samples about the disk, server and
// network times
//

extern Random* DistNetFrameTime;
extern Random* DistServerFrameTime;
extern Random* DistDiskFrameTime;

extern SpinFetchAndOp<int> ActiveThreads;

//
// Pointer to the networks array..
//
extern Network *Networks;

//
// The statistic for response times..
//
extern Statistic<double> RequestResponseTime;
extern Statistic<double> MeasuredResponseTime;



#endif /* Main_h */
