//-*-c++-*-
#ifndef _Server_h_
#define _Server_h_

#include "Disk.h"

class Server : public Facility {

public:
  //
  // Poor programming style, but this provides access to all the
  // clients..
  //
  int _num_disks;
  Disk *disks;

  //
  // Default constructor
  //
  Server() {
    _num_disks = 0;
  }

  void configure(int num_disks) {
    _num_disks = num_disks;
    disks = new Disk[num_disks];
  }
  
};

#endif
