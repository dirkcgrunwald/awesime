//-*-c++-*-
#ifndef _Network_h_
#define _Network_h_

#include "Server.h"

class Network : public Facility {

public:
  //
  // Poor programming style, but this provides access to all the
  // clients..
  //
  int _num_servers;
  Server *servers;

  //
  // Default constructor
  //
  Network() {
    _num_servers = 0;
  }

  void configure(int num_servers) {
    _num_servers = num_servers;
    servers = new Server[num_servers];
  }
  
};

#endif
