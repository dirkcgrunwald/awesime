#include "ACG.h"
#include "LogNormal.h"
#include "Histogram.h"

#include <math.h>

main(void)
{

    double alpha = 0.0;
    double beta = 1.0;
    double beta2 = beta * beta;

    double mean = pow(M_E, alpha + beta2 / 2);
    double variance = pow(M_E, 2 * alpha + beta2 / 2)
	+( pow(M_E, beta2) - 1);


    mean = log(10.0);
    variance = log(5.0);

    ACG rng(1);

    LogNormal foo(mean, variance, &rng);

    Histogram<double> samples;

   for (int i = 0; i < 1000000; i++ ) {
	double sample = foo();
	samples += sample;

    }
    cout << "Histogram:\n";
    cout << "mean = " << mean << "\n";
    cout << "variance = " << variance << "\n";
    cout << "Histo mean = " << samples.mean() << "\n";
    cout << "Histo var = " << samples.var();
    cout << samples;

    return 0;
}





