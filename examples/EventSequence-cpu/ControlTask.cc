#include "SimMux.h"
#include "EventSequence.h"
#include "Barrier.h"
#include "ControlTask.h"
#include "EventTest.h"
#include "iolock.h"

extern EventSequence TheEventSequence;
extern Barrier TheBarrier;
extern int TheDataValue;
extern int TotalShouldBe;

ControlTask::ControlTask(int f, int t, int b)
: Thread("Control Task")
{
    from = f;
    to = t;
    by = b;
    testers = 0;
}

void
ControlTask::main()
{
    int blib = (to - from)/by; 
    TotalShouldBe = 0;

    TheBarrier.height(to+1);

    cout << "Build testers..will do " << blib << " reserves\n";
    for (int i = 0; i < to; i++ ) {
	CpuMux::add(new EventTest(i, blib));
	TotalShouldBe += blib;
    }

    TheBarrier.rendezvous();

    cerr << lock << "TheDataValue == " << TheDataValue << "\n";
    cerr << "TotalShouldBe == " << TotalShouldBe << "\n" << unlock;
} 
