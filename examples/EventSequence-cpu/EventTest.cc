//
//	A timing test for barrier. Everything is in the one file.
//

#include <stdio.h>
#include "EventSequence.h"
#include "Barrier.h"
#include "EventTest.h"
#include "ControlTask.h"
#include "CpuMux.h"
#include "iolock.h"

EventSequence TheEventSequence;
Barrier TheBarrier(0);
int TheDataValue = 0;

EventTest::EventTest(int p, int times_)
: Thread("EventTest")
{
    pid = p;
    times = times_;
    sprintf(namestr,"EventTest-%d", pid);
    name(namestr);
}

void
EventTest::main()
{
    
    for ( int i = 0; i < times; i++ ) {
	
	int ticket = TheEventSequence.ticket();
	TheEventSequence.await( ticket );

	TheDataValue++;
	
	TheEventSequence.advance();
    }

    TheBarrier.rendezvous();
    static volatile int printed = 0;

    if ( printed == 0 ) {
	if ( printed == 0 ) { 
	    printed = 1;
	    cerr  << lock << name() << " 1st semtest after rendezvous\n" << unlock;
	}
    }
}
