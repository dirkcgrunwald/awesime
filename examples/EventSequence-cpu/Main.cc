//#include "SingleFifoMux.h"
#include "MultiFifoMux.h"
#include "ControlTask.h"
#include "Barrier.h"

int TotalShouldBe = 0;
extern int TheDataValue;

main(int argc, char** argv)
{
    int cpus = (argc > 1) ? atoi(argv[1]) : 1;
    int debug = (argc > 2) ? atoi(argv[2]) : 0;
    int from = (argc > 3) ? atoi(argv[3]) : 50;
    int to = (argc > 4) ? atoi(argv[4]) : (from + 100);
    int by = (argc > 5) ? atoi(argv[5]) : 50;

    MultiFifoMux Cpu(debug);

    cout << "From = " << from << " and to = " << to << "\n";
    cout << "with " << cpus << " cpus\n";

    Cpu.add( new ControlTask(from,to,by) );

    Cpu.fireItUp( cpus, to * 5000 + 1000000);

    cout << "Done!\n";
    cout << "TotalShouldBe = " << TotalShouldBe << "\n";
    cout << "TheDataValue  = " << TheDataValue << "\n";

    return 0;
}
