#include <math.h>
#include <string.h>

//
//	Things for the simulation environment
// 

#include "MultiFifoMux.h"
#include "SingleFifoMux.h"
#include "Statistic.h"

#include "Recv.h"
#include "Send.h"

main(int argc)
{
    int i;

    MultiFifoMux Cpu;

    for (i = 0; i < MAX_RECVS; i++) {
	cout << "Create recv(" << i << ")\n";
	theRecvs[i] = new Recv(i);
	cout << "add recv(" << i << ") to cpu\n";
	Cpu.add(theRecvs[i]);
    }

    cout << "create and add send\n";
    Cpu.add( new Send(1,MAX_RECVS, 50));

    cout << "call fireitup \n";
    Cpu.fireItUp(2,100 * 4196);

    cout << "All done!\n";

    return 0;
}
