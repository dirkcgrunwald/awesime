#include "CpuMux.h"
#include "Main.h"
#include "Send.h"
#include "Recv.h"
#include "assert.h"
#include <iolock.h>

Send::Send(int xpid, int recvs, int xhowMany)
    : Thread( "SEND", 20000, HARDWARE_CONTEXT_CHECK_MPROTECT,
	      (ThreadPriority)-xpid ),
      rnd(0, recvs-1, &gen), gen(-xpid,xpid)
{
    pid = xpid;
    howMany = xhowMany;
}


void
Send::main()
{
    int i;
    for (i = 0; i < howMany; i++) {
	int to = int (rnd());
	cout << lock << "Send " << i << " to " << to << "\n" << unlock;
	theRecvs[ to ] -> add( i );
	CpuMux::reschedule();
    }
}

Send::~Send()
{
    cout << lock << "Sender bids good bye\n" << unlock;
}
