#include "Main.h"
#include "Recv.h"
#include "assert.h"
#include <stdio.h>
#include <iolock.h>

Recv *theRecvs[MAX_RECVS];

Recv::Recv(int xpid)
    : Thread( "RECV", 20000, HARDWARE_CONTEXT_CHECK_MPROTECT,
	      (ThreadPriority)-xpid ) 
{
    pid = xpid;
    sprintf(nameBuffer,"Recv-%d", pid);
    name(nameBuffer);
}

void
Recv::main()
{
    cout << lock;
    cout << "Hi mom, I'm #" << pid << "\n";
    cout << unlock;

    while(1) {
	int msg = mailBox.remove_front();
	cout << lock << "#" << pid << " got message " << msg << "\n" << unlock;
    }
}

void
Recv::add(int message)
{
    mailBox.append( message );
}

	
