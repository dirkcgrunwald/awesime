#ifndef SENDH
#define SENDH

#include "Thread.h"
#include "DiscreteUniform.h"
#include "ACG.h"
#include "MLCG.h"

class Send : public Thread {

    int pid;
    int howMany;
//    ACG gen;
    MLCG gen;
    DiscreteUniform rnd;

public:
    Send(int xpid, int recvs, int xhowMany);
    virtual void main();
    virtual ~Send();
};

#endif
