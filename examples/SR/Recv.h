#ifndef Recv_h
#define Recv_h

#include "Thread.h"
#include "LowerSLList.h"

class Recv : public Thread {

    int pid;
    LowerSLList<int> mailBox;
    char nameBuffer[128];

public:

    Recv(int xpid);

    virtual void main();

    void add(int message);
};

#define MAX_RECVS 5
extern Recv *theRecvs[MAX_RECVS];

#endif
