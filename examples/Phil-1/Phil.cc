#include <iostream>
#include <stdio.h>
#include "Main.h"
#include "Table.h"
#include "Phil.h"
#include "CpuMux.h"
#include "SpinFetchAndOp.h"
#include "Semaphore.h"
#include "GenerousSemaphore.h"
#include "Barrier.h"
#include "iolock.h"

GenerousSemaphore foo(0);
SpinFetchAndOp<int> Phil::mouthFulls(0);

extern Table TheTable;

Phil::Phil(int xpid)
: Thread( "Philosopher",
	  100000,
	  HARDWARE_CONTEXT_CHECK_MPROTECT,
	  -xpid,
	  0) 
{
    pid = xpid;
    sprintf(nameBuffer,"Phil-%d", pid);
    name(nameBuffer);
}

void
Phil::main()
{
    int toTheRight = (pid + 1) % PHILOSOPHERS;

    cout << lock;
    cout << name() << " using forks " << pid << " and " << toTheRight << "\n";
    cout << unlock;

    while( mouthFulls.value() < 10 ) {

	TheTable.getForks(pid, toTheRight);

	cout << lock;
	cout << name() << " gets to eat\n";
	cout << unlock;

	TheTable.returnForks(pid, toTheRight);

	mouthFulls += 1;
    }
    cout << lock;
    cout << name() << " wanders off to burb awhile\n";
    cout << unlock;
}
