#ifndef _Phil_h_
#define _Phil_h_

#include "Thread.h"
#include "SpinFetchAndOp.h"

class Phil : public Thread {
    int pid;
    char nameBuffer[128];
    static SpinFetchAndOp<int> mouthFulls;
public:
    Phil(int pid);
    virtual void main();
};

#endif
