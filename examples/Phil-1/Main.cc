#include <iostream>

#include "MultiPQMux.h"
//#include "MultiFifoMux.h"
#include "Table.h"
#include "Phil.h"

extern Table TheTable;

main(int argc, char** argv)
{
   int cpus = (argc > 1) ? atoi(argv[1]) : 2;
   int debug = (argc > 2) ? atoi(argv[2]) : 0;

   MultiPQMux Cpu(debug);
   
   for (int i = 0; i < PHILOSOPHERS; i++) {
     CpuMux::add( new Phil(i) );
   }
   Cpu.fireItUp(cpus, 100000);
   cout << "All done!\n";
   return 0;
}



