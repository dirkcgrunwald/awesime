#ifndef Main_h
#define Main_h

#define	MAX_TIME 1000000

#include "aweconfig.h"

inline void report_memory()
{
  char *x = (char *)sbrk(0);
  long xx = (long) x;
  cout << "sbrk = " << hex << xx << "\n";
}

#endif /* Main_h */
