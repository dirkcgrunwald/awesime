#include "SimMux.h"
#include <MLCG.h>
#include <NegativeExpntl.h>
#include "Main.h"
#include "Worker.h"

void
Worker::main()
{
    MLCG rng(pid, pid+1);
    NegativeExpntl interval(10, &rng);
    double report = 10000;

    int iteration = 0;
    while( SimMux::CurrentTime() < MAX_TIME ) {
      iteration++;
      if ( iteration % 100 == 0) {
	cout << "For " << name() << " time is " << SimMux::CurrentTime() << endl;
      }
	SimMux::hold ( interval() );
	if (report <= SimMux::CurrentTime() ) {
	    cout << "At time " << SimMux::CurrentTime() << " ";
	    report_memory();
	    report = SimMux::CurrentTime() + 1000000;
	}
    }
    cout << "Exit\n";
    report_memory();
	     
}
