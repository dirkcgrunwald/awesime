#include "SingleSimMux.h"
#include "MonitorSimMux.h"

#include "Main.h"
#include "Worker.h"

main(int argc, char **argv)
{
    
    SingleSimMux Simulator(0);

    Simulator.add( new Worker(1) );
    Simulator.add( new Worker(2) );
    Simulator.add( new Worker(3) );
    Simulator.add( new Worker(4) );

    Simulator.fireItUp(1,100 * 4196);
    return(0);
}
