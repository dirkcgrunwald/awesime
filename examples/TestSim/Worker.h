#ifndef Customer_h
#define Customer_h

#include <stdio.h>
#include <string.h>
#include "Thread.h"

class Worker : public Thread {
  int pid;
public:
  Worker(int iYam);
  virtual void main();
};

inline
Worker::Worker( int iYam )
{
  char buffer[1000];
  sprintf(buffer,"Worker-%d", iYam);
  char *foo = strdup(buffer);
  name(foo);
  pid = iYam;
}

#endif
