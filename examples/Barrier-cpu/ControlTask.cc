#include "CpuMux.h"
#include "Barrier.h"
#include "ControlTask.h"
#include "BarrierTest.h"
#include "iolock.h"

#define TRIALS 100
#define SAMPLES 10

extern Barrier TheBarrier;

ControlTask::ControlTask(int f, int t, int b) : Thread("Control Task")
{
    from = f;
    to = t;
    by = b;
    testers = 0;
}

void
ControlTask::main()
{
    for (int i = from; i <= to; i += by ) {

        cout << "i = " << i << "\n";
	TheBarrier.height(i+1);

	cout << "Build testers..i = " << i << "from,to = " << from << "," << to <<"\n";

	while( testers < i) {
	    CpuMux::add(new BarrierTest(testers));
	    testers++;
	}

	cout << "Let everyone rendezvous\n";

	TheBarrier.rendezvous();

	cout << "Now, begin the tests..\n";

	stats.reset();
	//
	// start collecting sampls
	//
	for (int sample = 0; sample < SAMPLES; sample++ ) {
	    cout <<lock << ".." << sample << ".."<< unlock; cout.flush();
	    for (int trial = 0; trial < TRIALS; trial++) {
		TheBarrier.rendezvous();
	    }
	}
    }
    cout << "Exiting..\n";
    BarrierTest::done = 1;
    TheBarrier.rendezvous();
}
