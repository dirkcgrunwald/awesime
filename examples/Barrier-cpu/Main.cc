#include "MultiFifoMux.h"
//#include "SingleFifoMux.h"
#include "ControlTask.h"
#include "Barrier.h"

extern "C" {
    void pmon_dump();
}

main(int argc, char** argv)
{
    int cpus = (argc > 1) ? atoi(argv[1]) : 1;
    int debug = (argc > 2) ? atoi(argv[2]) : 0;
    int from = (argc > 3) ? atoi(argv[3]) : 50;
    int to = (argc > 4) ? atoi(argv[4]) : (from + 100);
    int by = (argc > 5) ? atoi(argv[5]) : 50;

    MultiFifoMux Cpu(debug);
//    SingleFifoMux Cpu((bool)debug);
    cout << "From = " << from << " and to = " << to << "\n";
    cout << "with " << cpus << " cpus\n";

    CpuMux::add( new ControlTask(from,to,by) );


    int space = to * 5000 + 2000 ;
    Cpu.fireItUp( cpus, space );

    cout << "Done!\n";
    extern Barrier TheBarrier;
    cout << "There are " << TheBarrier.count() << " tasks at the barrier \n";

    return (0);
}

