//
//	A timing test for barrier. Everything is in the one file.
//

#include <stdio.h>
#include "CpuMux.h"
#include "Barrier.h"
#include "BarrierTest.h"
#include "ControlTask.h"

Barrier TheBarrier(0);

int BarrierTest::done = 0;

BarrierTest::BarrierTest(int p)
: Thread("BarrierTest", 15000, HARDWARE_CONTEXT_CHECK_MPROTECT)
{
    sprintf(namestr,"BarrierTest-%d", p);
    name(namestr);
    pid = p;
}

void
BarrierTest::main()
{
    //
    //    cerr << "BarrierTask " << pid << " starts\n";
    //
    for ( ;; ) {
	TheBarrier.rendezvous();
	if ( done ) break;
    }
#if 0
    CERR_ALWAYS_PRE;
    cerr << "Thread " << name() << " exiting..\n";
    CERR_POST;
#endif
}
