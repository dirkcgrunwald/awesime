#include "Quantile.h"
#include "PSqHistogram.h"
#include "MLCG.h"
#include "Normal.h"
#include "PSqRandom.h"
#include "Statistic.h"
#include "Data.h"

//
// Test the Quantile data structures
//

double samples[] = {
    0.02, 0.15, 0.74, 3.39, 0.83,
    22.37, 10.15, 15.43, 38.62, 15.92,
    34.60, 10.28, 1.47, 0.40, 0.05,
    11.39, 0.27, 0.42, 0.09, 11.37 };


void
TestQuantile()
{
    Quantile q(0.5);
    double     y;
    unsigned count = (sizeof samples) / (sizeof samples[0]);
    unsigned i;
    
    cout << "Add samples to the Quantile(0.5)\n";
    cout << "Initial value of 'q' is:" ;
    cout << q << "\n";
    cout << "Add           , with 0.5 quantile being           \n";
    for (i = 0; i < count; i++) {
	q += samples[i];
	y = q();
	cout << "    " << setw(10) << samples[i] << setw(10) << " ";
	cout << setw(10) << y << "\n";
    }
}

void
TestHistogram()
{
    const unsigned buckets = 4;
    unsigned count = (sizeof samples) / (sizeof samples[0]);
    PSqHistogram h(buckets);
    
    cout << "\n\n";
    cout << "Add samples to the PSqHistogram...\n";
    
    int i;
    for (i = 0; i < count; i++) {
	h += samples[i];
    }
    cout << "Histogram looks like...\n";
    for (i = 0; i <= buckets; i++) {
	cout << h[i].n << ", "<<  h[i].q << "\n";
    }
}

void
TestHistogramNormal(double mean, double variance, int show_histo = 0)
{
    MLCG rng(0,1);
    Normal normal(mean, variance,&rng);
    int buckets = 100;
    int samples = 10000;
    
    Statistic<double> normalStats;
    Statistic<double> qhistoStats;
    
    PSqHistogram h(buckets);
    
    cout << "\n\n";
    cout << "Normal histogram\n";
    cout << "Add samples to the PSqHistogram...\n";
    int i;
    
    for (i = 0; i < samples; i++) {
	double d = normal();
	h += d;
	normalStats += d;
    }
    
    if ( show_histo ) {
	cout << "Histogram looks like...\n";
	for (i = 0; i <= buckets; i++) {
//	    PSqHistogram::Point& x = h[i];
//	    cout << x.n << "		= " << x.q << "\n";
	    cout << h[i].n << "		= " << h[i].q << "\n";
	}
    }
    
    PSqRandom qhisto(&h, &rng);
    
    for (i = 0; i < samples; i++) {
	double d = qhisto();
	qhistoStats += d;
    }
    
    cout << "Original mean was " << mean << " with variance ";
    cout << variance << "\n";
    cout << "Normal stats = " << normalStats << "\n";
    cout << "QHisto stats = " << qhistoStats << "\n";
}


void
AnotherTest(char *name, int samples, int buckets,
	    PSqHistogram::Point *points)
{
    MLCG rng(0,1);
    PSqHistogram foo(samples, buckets, points);
    PSqRandom bar(&foo, &rng);
    Statistic<double> baz;

    for (int i = 0; i < foo.samples(); i++ ) {
	baz += bar();
    }
    cout << "Mean of " << name << " is " << baz << "\n";
}

int
main()
{
    TestQuantile();
    TestHistogram();
    TestHistogramNormal(0,1,1);
    TestHistogramNormal(4333,17,0);
    AnotherTest("Size", QH_Size_samples, QH_Size_buckets, QH_Size_points );
    AnotherTest("HT", QH_HT_samples, QH_HT_buckets, QH_HT_points );
    AnotherTest("IAT", QH_IAT_samples, QH_IAT_buckets, QH_IAT_points );
    return 0;
}
