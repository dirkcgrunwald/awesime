#ifndef _Data_h_
#define _Data_h_

#include <PSqHistogram.h>

extern int QH_IAT_samples;
extern int QH_IAT_buckets;
extern PSqHistogram::Point *QH_IAT_points;

extern int QH_HT_samples;
extern int QH_HT_buckets;
extern PSqHistogram::Point *QH_HT_points;

extern int QH_Size_samples;
extern int QH_Size_buckets;
extern PSqHistogram::Point *QH_Size_points;

#endif
